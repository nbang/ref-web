<%-- 
    Document   : edit-all
    Created on : Jan 25, 2014, 5:38:46 AM
    Author     : piece3
--%>
<%@ include file="taglibs.jsp"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html lang="en">
	<c:set var="contextPath" value="<%=request.getContextPath()%>"></c:set>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all" />
<link href="${contextPath}/resources/css/bootstrap-progressbar.min.css" rel="stylesheet" type="text/css" >
<link href="//ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/css/jquery.dataTables.css" rel="stylesheet" type="text/css" media="all" />
<%-- <link href="${contextPath}/resources/css/chosen/chosen.min.css" rel="stylesheet" type="text/css" media="all" /> --%>
<link href="${contextPath}/resources/css/item_admin.css" rel="stylesheet" type="text/css" media="all" />
<title>Admin Edit All</title>
<style type="text/css">
	.green {
		color: #009900;
	}
	.red {
		color: red;
	}
	#kidzsaigonTable tbody tr.highlighted { background-color: rgb(236, 255, 216);}
	#kidzsaigonTable tbody tr:HOVER { cursor: pointer;}
	p {
		margin: 0 0 10px 0;
	}
	#back-top {
		bottom: 30px;
		opacity: 1;
		position: fixed;
		right: 20px;
		visibility: visible;
	}
	#back-top span {
		background: #43997e url("${contextPath}/resources/images/back-top.png") no-repeat;
		display: block;
		height: 50px;
		width: 50px;
		cursor: pointer;
		/* rounded corners */
		-webkit-border-radius: 100px;
		-moz-border-radius: 100px;
		border-radius: 100px;
		/* transition */
		-webkit-transition: 1s;
		-moz-transition: 1s;
		transition: 1s;
	}
</style>
</head>
<body>
	<div>
		Welcome,
		<sec:authentication property="principal.username" />
	</div>
	<a href="${contextPath}/j_spring_security_logout">Logout</a>
	<div class="container">
	<h3><a href="${contextPath}/item"><spring:message code="label.menu.admin.website"/></a>
	 | <a href="${contextPath}/admin/item"><spring:message code="label.menu.admin.new"/></a>
         | <!--<a href="${contextPath}/admin/edit">--><spring:message code="label.menu.admin.edit"/><!--</a>-->
	 | <!--<a href="${contextPath}/admin/banner/list">--><spring:message code="label.menu.admin.banners"/><!--</a>-->
<!--	 | <a href="${contextPath}/admin/banner">Add banner</a>-->
	 | <a href="${contextPath}/admin/list/update"><spring:message code="label.menu.admin.edit_items"/></a>
	 | <a href="${contextPath}/admin/company/edit"><spring:message code="label.menu.admin.edit_companies"/></a>
	</h3>
        <div class="row"><c:forEach var="message" items="${messages}">
        <div id="display-${message.key}" class="display-${message.key}"><spring:message code="${message.value}" /></div>
      </c:forEach>
      <div id="com-display-error" class="display-error"></div>
      <div id="com-display-success" class="display-success"></div>
      <form:form id="itemForm" role="form" method="POST" commandName="itemModelView" action="${contextPath}/admin/item/update?src=edit-all" enctype="multipart/form-data">
				<h3 class="modal-header">
					<spring:message code="label.title.item.edit"/>
				</h3>
				<form:hidden path="itemId" />
				<c:set var="itemId" value="${itemModelView.itemId}"></c:set>
				<div class="form-group">
					<div class="row">
						<div class="col-lg-12"><form:errors path="locale1" cssStyle="color: red;"></form:errors></div>
					</div>
					<div class="row">
						<div class="col-lg-6">
							<c:set value="${itemModelView.locale1}" var="tmpLocale1"></c:set>
              <form:select cssClass="form-control" path="locale1">
                <c:forEach items="${locales}" var="locale">
                  <c:choose>
                    <c:when test="${locale.code eq tmpLocale1}">
                      <form:option value="${locale.code}" label="${locale.local_name}"></form:option>
                    </c:when>
                    <c:otherwise>
                      <form:option value="${locale.code}" label="${locale.local_name}" disabled="true"></form:option>
                    </c:otherwise>
                  </c:choose>
                </c:forEach>
              </form:select>
						</div>
						<div class="col-lg-6">
							<%-- <form:select cssClass="form-control" path="locale2" id="locale2" items="${locales}" itemValue="code" itemLabel="local_name" onchange="this.selectedIndex=2;" readonly="true"></form:select> --%>
              <c:set value="${itemModelView.locale2}" var="tmpLocale2"></c:set>
              <form:select cssClass="form-control" path="locale2">
                <c:forEach items="${locales}" var="locale">
                  <c:choose>
                    <c:when test="${locale.code eq tmpLocale2}">
                      <form:option value="${locale.code}" label="${locale.local_name}"></form:option>
                    </c:when>
                    <c:otherwise>
                      <form:option value="${locale.code}" label="${locale.local_name}" disabled="true"></form:option>
                    </c:otherwise>
                  </c:choose>
                </c:forEach>
              </form:select>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-lg-12">
							<table class="display table-hover" id="kidzsaigonTable"></table>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-lg-6">
							<label for="company1"><spring:message code="label.company" /></label>&nbsp;<form:errors path="company1" cssStyle="color: red;"></form:errors>
							<form:select cssClass="form-control" path="company1" id="company1" disabled="false">
								<c:if test="${!empty companies1}">
								<c:forEach items="${companies1}" var="comp">
									<form:option value="${comp.title}" label="${comp.title}" disabled="true"></form:option>
								</c:forEach>
								</c:if>
							</form:select>
						</div>
						<div class="col-lg-6">
							<label for="company2">&nbsp;</label>&nbsp;<form:errors path="company2" cssStyle="color: red;"></form:errors>
							<form:select cssClass="form-control" path="company2" id="company2">
								<c:if test="${!empty companies2}">
								<c:forEach items="${companies2}" var="comp">
									<form:option value="${comp.title}" label="${comp.title}" disabled="true"></form:option>
								</c:forEach>
								</c:if>
							</form:select>
						</div>
					</div>
				</div>
<!-- 				<div class="form-group"> -->
<%-- 					<a class="btn btn-info" data-toggle="modal" href="${contextPath}/admin/company" data-target="#company-info"> --%>
<%-- 						<spring:message code="label.company.add" /> --%>
<!-- 					</a> -->
<!-- 				</div> -->
				<div class="form-group">
					<div class="row">
						<div class="col-lg-4">
							<label for="category"><spring:message code="label.category" /></label>&nbsp;<form:errors path="category" cssStyle="color: red;"></form:errors>
							<form:select cssClass="form-control category" id="category" path="category" multiple="true" size="10">
								<c:if test="${!empty categories}">
								<c:forEach items="${categories}" var="cat">
									<form:option value="${cat.title}" label="${cat.title}" title="${cat.catdesc}"></form:option>
								</c:forEach>
								</c:if>
							</form:select>
						</div>
						<div class="col-lg-4">
							<label for="subcategory1"><spring:message code="label.subcategory" /></label>
              <a class="btn btn-info btn-xs" id="btn-add-subcat" data-toggle="modal" data-target="#add-subcat">
                 <span class="glyphicon glyphicon-plus-sign" title="Add new subcategory"></span>
              </a>
							<form:select cssClass="form-control" id="subcategory1" path="subCategory1" multiple="true" size="10">
								<c:if test="${!empty subCategories1}">
								<c:forEach items="${subCategories1}" var="subcat">
									<form:option value="${subcat.subname}" label="${subcat.subname}"></form:option>
								</c:forEach>
								</c:if>
							</form:select>
						</div>
						<div class="col-lg-4">
							<label for="subcategory2">&nbsp;</label>
							<form:select cssClass="form-control" id="subcategory2" path="subCategory2" multiple="true" size="10">
								<c:if test="${!empty subCategories2}">
								<c:forEach items="${subCategories2}" var="subcat">
									<form:option value="${subcat.subname}" label="${subcat.subname}"></form:option>
								</c:forEach>
								</c:if>
							</form:select>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-lg-2">
							<label for="cost"><spring:message code="label.cost" /></label>&nbsp;
							<form:select cssClass="form-control" path="cost" id="cost">
								<c:forEach items="${costList}" var="cost">
									<form:option value="${cost.key}" label="${cost.value}"></form:option>
								</c:forEach>
							</form:select>
						</div>
						<div class="col-lg-2">
							<label for="weight"><spring:message code="label.weight.eateries" /></label>&nbsp;<form:errors path="weight" cssStyle="color: red;"></form:errors>
							<form:input cssClass="form-control weight" path="weight" id="weight-eateries" placeholder="Weight" disabled="true"></form:input>
						</div>
            <div class="col-lg-2">
              <label for="weight"><spring:message code="label.weight.shopping" /></label>&nbsp;<form:errors path="weight" cssStyle="color: red;"></form:errors>
              <form:input cssClass="form-control weight" path="weight" id="weight-shopping" placeholder="Weight" disabled="true"></form:input>
            </div>
            <div class="col-lg-2">
              <label for="weight"><spring:message code="label.weight.activities" /></label>&nbsp;<form:errors path="weight" cssStyle="color: red;"></form:errors>
              <form:input cssClass="form-control weight" path="weight" id="weight-activities" placeholder="Weight" disabled="true"></form:input>
            </div>
            <div class="col-lg-2">
              <label for="weight"><spring:message code="label.weight.health" /></label>&nbsp;<form:errors path="weight" cssStyle="color: red;"></form:errors>
              <form:input cssClass="form-control weight" path="weight" id="weight-health" placeholder="Weight" disabled="true"></form:input>
            </div>
            <div class="col-lg-2">
              <label for="weight"><spring:message code="label.weight.learning" /></label>&nbsp;<form:errors path="weight" cssStyle="color: red;"></form:errors>
              <form:input cssClass="form-control weight" path="weight" id="weight-learning" placeholder="Weight" disabled="true"></form:input>
            </div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-lg-6">
							<label for="shortDesc1"><spring:message code="label.short.description" /></label>&nbsp;<form:errors path="shortDesc1" cssStyle="color: red;"></form:errors>
							<form:textarea cssClass="form-control" path="shortDesc1" id="shortDesc1" placeholder="Short Description" rows="3" maxlength="250"></form:textarea>
						</div>
						<div class="col-lg-6">
							<label for="shortDesc2">&nbsp;</label>&nbsp;<form:errors path="shortDesc2" cssStyle="color: red;"></form:errors>
							<form:textarea cssClass="form-control" path="shortDesc2" id="shortDesc2" placeholder="Short Description" rows="3" maxlength="250"></form:textarea>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-lg-6">
							<label for="longDesc2"><spring:message code="label.long.description" /></label>
							<form:textarea cssClass="form-control" path="longDesc1" id="longDesc1" placeholder="Long Description" rows="5" maxlength="2000"></form:textarea>
						</div>
						<div class="col-lg-6">
							<label for="longDesc1">&nbsp;</label>
							<form:textarea cssClass="form-control" path="longDesc2" id="longDesc2" placeholder="Long Description" rows="5" maxlength="2000"></form:textarea>
						</div>
					</div>
				</div>

				<div class="section group">
					<div class="col span_1_of_3"></div>
					<div id="" class="col span_1_of_3" style="text-align: center">
            <img class="img-rounded" src="" />
					</div>
					<div class="col span_1_of_3"></div>
				</div>

				<div class="form-group">
					<label for="file"><spring:message code="label.image.location"/></label>
					<div class="input-group">
						<span class="input-group-addon"><form:radiobutton path="uploadType" id="uploadType0" value="0"/></span>
			            <form:input class="form-control" type="file" path="file" id="file" placeholder="Image Location" maxlength="500" accept="image/*" disabled="true"/>			
					</div>
				</div>
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon"><form:radiobutton path="uploadType" id="uploadType1" value="1"/></span>
						<form:input cssClass="form-control" path="image" id="image" placeholder="Image Location" maxlength="500" disabled="true"/>
					</div>
				</div>
<!--				<div class="form_save-control modal-footer">
					<div>
						<input type="submit" value='<spring:message code="label.save"/>' class="btn btn-primary" />
					</div>
				</div>-->
			</form:form>
			<form:form cssClass="form-horizontal" id="companyForm" role="form" method="POST" commandName="companyModelView" action="${contextPath}/admin/company/update">
<!--				<h3 class="modal-header">
					<spring:message code="label.title.company.edit"/>
				</h3>-->
				<form:hidden path="companyId" />
				<c:set var="companyId" value="${companyModelView.companyId}"></c:set>
<!--				<div class="form-group">
					<div class="col-lg-12"><form:errors path="locale1" cssStyle="color: red;"></form:errors></div>
					<div class="col-lg-6">
						<form:select cssClass="form-control" path="locale1" id="locale1" items="${locales}" itemValue="code" itemLabel="local_name"></form:select>
					</div>
					<div class="col-lg-6">
						<form:select cssClass="form-control" path="locale2" id="locale2" items="${locales}" itemValue="code" itemLabel="local_name"></form:select>
					</div>
				</div>-->
				<div class="form-group">
					<label for="city" class="col-lg-3 control-label"><spring:message code="label.city"/></label>
					<div class="col-lg-6">
						<form:select cssClass="form-control chosen-select" path="city" id="city" items="${cities}" itemLabel="name" itemValue="name" placeholder="City"></form:select>
					</div>
					<div>
					 <a class="btn btn-info btn-xs" data-toggle="modal" href="${contextPath}/admin/streetbind?type=city" data-target="#streetbind-create">
            <span class="glyphicon glyphicon-plus-sign" title="Add new city"></span>
           </a>
           <a class="btn btn-warning btn-xs" id="cityDel">
            <span class="glyphicon glyphicon-remove-sign" title="Delete city"></span>
           </a>
					</div>
				</div>
				<div class="form-group">
					<label for="district" class="col-lg-3 control-label"><spring:message code="label.district"/></label>
					<div class="col-lg-6">
						<form:select cssClass="form-control district-chosen-select" path="district" id="district" items="${districts}" itemLabel="district" itemValue="district" placeholder="District"/>
					</div>
					<div>
           <a class="btn btn-info btn-xs" data-toggle="modal" href="${contextPath}/admin/streetbind?type=district" data-target="#streetbind-create">
            <span class="glyphicon glyphicon-plus-sign" title="Add new district"></span>
           </a>
           <a class="btn btn-warning btn-xs" id="districtDel">
            <span class="glyphicon glyphicon-remove-sign" title="Delete district"></span>
           </a>
					</div>
				</div>
				<div class="form-group">
					<label for="ward" class="col-lg-3 control-label"><spring:message code="label.ward"/></label>
					<div class="col-lg-6">
						<form:select cssClass="form-control ward-chosen-select" path="ward" id="ward" items="${wards}" itemLabel="ward" itemValue="ward" placeholder="Ward"/>
					</div>
					<div>
           <a class="btn btn-info btn-xs" data-toggle="modal" href="${contextPath}/admin/streetbind?type=ward" data-target="#streetbind-create">
            <span class="glyphicon glyphicon-plus-sign" title="Add new ward"></span>
           </a>
           <a class="btn btn-warning btn-xs" id="wardDel">
            <span class="glyphicon glyphicon-remove-sign" title="Delete ward"></span>
           </a>
					</div>
				</div>
				<div class="form-group">
					<label for="street" class="col-lg-3 control-label"><spring:message code="label.street"/></label>
					<div class="col-lg-6">
						<form:select cssClass="form-control street-chosen-select" path="street" id="street" items="${streets}" itemLabel="street" itemValue="street" placeholder="Street"></form:select>
					</div>
					<div>
           <a class="btn btn-info btn-xs" data-toggle="modal" href="${contextPath}/admin/streetbind?type=street" data-target="#streetbind-create">
            <span class="glyphicon glyphicon-plus-sign" title="Add new street"></span>
           </a>
           <a class="btn btn-warning btn-xs" id="streetDel">
            <span class="glyphicon glyphicon-remove-sign" title="Delete street"></span>
           </a>
					</div>
				</div>
				<div class="form-group">
					<label for="addrNum" class="col-lg-3 control-label"><spring:message code="label.addrnum"/></label>
					<div class="col-lg-6">
						<form:input cssClass="form-control" path="addrNum" id="addrNum" placeholder="Address Number" maxlength="40"></form:input>
					</div>
				</div>
				<div class="form-group">
					<label for="titleEng" class="col-lg-3 control-label"><spring:message code="label.engTitle"/></label>
					&nbsp;<form:errors path="titleEng" cssStyle="color: red;"></form:errors>
					<div class="col-lg-6">
						<form:input cssClass="form-control" path="titleEng" id="titleEng" placeholder="Title English" maxlength="80"></form:input>
					</div>
				</div>
				<div class="form-group">
					<label for="titleLocal" class="col-lg-3 control-label"><spring:message code="label.localTitle"/></label>
					&nbsp;<form:errors path="titleLocal" cssStyle="color: red;"></form:errors>
					<div class="col-lg-6">
						<form:input cssClass="form-control" path="titleLocal" id="titleLocal" placeholder="Title Local" maxlength="80"></form:input>
					</div>
				</div>
				<div class="form-group">
					<label for="telephone1" class="col-lg-3 control-label"><spring:message code="label.telephone"/></label>
					<div class="col-lg-6">
						<form:input cssClass="form-control" path="telephone1" id="telephone1" placeholder="Telephone" maxlength="200"></form:input>
					</div>
				</div>
				<div class="form-group">
					<label for="telephone2" class="col-lg-3 control-label"><spring:message code="label.mobile"/>/<spring:message code="label.fax"/></label>
					<div class="col-lg-6">
						<form:input cssClass="form-control" path="telephone2" id="telephone2" placeholder="Mobile/Fax" maxlength="200"></form:input>
					</div>
				</div>
				<div class="form-group">
					<label for="email" class="col-lg-3 control-label"><spring:message code="label.email"/></label>
					<div class="col-lg-6">
						<form:input cssClass="form-control" path="email" id="email" placeholder="Email" maxlength="200"></form:input>
					</div>
				</div>
				<div class="form-group">
					<label for="contact" class="col-lg-3 control-label"><spring:message code="label.contact"/></label>
					<div class="col-lg-6">
						<form:input cssClass="form-control" path="contact" id="contact" placeholder="Contact" maxlength="200"></form:input>
					</div>
				</div>
				<div class="form-group">
					<label for="url" class="col-lg-3 control-label"><spring:message code="label.url"/></label>
					<div class="col-lg-6">
						<form:input cssClass="form-control" path="url" id="url" placeholder="URL" maxlength="200"></form:input>
					</div>
				</div>
				<div class="form_save-control modal-footer">
					<div>
						<input type="button" name="saveCompany" id="saveCompany" value='<spring:message code="label.save"/>' class="btn btn-primary" />
					</div>
				</div>
		</form:form>

            </div>
        </div>
        
        <!-- Modal Company-->
        <div class="modal fade" id="streetbind-create" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"></div>
        
        <!-- Modal items -->
    <div class="modal fade" id="add-subcat" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close"data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Add Subcategory</h4>
          </div>
          <div class="modal-body">
             <div class="row form-group">
                 <label for="addSub1">Subtitle - Language 1</label>
               <input type="text" class="form-control" id="addSub1" class="col-lg-4" placeholder="Primary Name"/>
             </div>     
             <div class="row form-group">
               <label for="addSub2">Subtitle - Language 2</label>
               <input type="text" class="form-control" id="addSub2" class="col-lg-4" placeholder="Secondary Name"/>
             </div>     
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" id="add-subcat-btn">Save</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
	<p id="back-top" style="display: block;"><a href="#top"><span></span></a></p>
  <div class="modal fade" id="pleaseWaitDialog" tabindex="-1" role="dialog" aria-hidden="true" data-keyboard="false"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><h3>Processing...</h3></div><div class="modal-body"><div class="progress progress-striped active"><div class="progress-bar progress-bar-info" role="progressbar" aria-valuetransitiongoal="100"></div></div></div></div></div></div>
<!-- Script -->    
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.3.1/jquery.cookie.min.js"></script>
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script src="${contextPath}/resources/js/bootbox.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap-progressbar.min.js"></script>
<%-- <script src="${contextPath}/resources/js/chosen/chosen.jquery.min.js"></script> --%>
<script src="//ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/jquery.dataTables.min.js"></script>
<%-- <script src="${contextPath}/resources/js/datatables/dataTables.fnReloadAjax.js"></script>
<script src="${contextPath}/resources/js/datatables/dataTables.fnGetSelected.js"></script> --%>
<script src="${contextPath}/resources/js/datatables.js"></script>

<script type="text/javascript">

// Sub-category
var subcatActionMode = "add";
var oldSubcat1 = "";
var oldSubcat2 = "";

var oTable;

//datatable ajax reload
function updateTable() {
  oTable.fnReloadAjax("${pageContext.request.contextPath}/ajax/admin/all?primLang=" + $("#locale1").val() + "&secLang=" + $("#locale2").val());
}

//Delete Company Location
function deleteLocation(type) {
    $("#" + type + "Del").on('click', function() {
        var name = $("#" + type).val();
        if (name == null || name == "") {
        	  //alert("Please select an item");
        	  bootbox.alert("Please select the " + type, function() {
       	      // todo
           	});
        	  return true;
        } else {
        	/* if (!confirm('Delete "' + name + '"?')) {
   		 		return;
  				} */
        	bootbox.confirm('Delete "' + name + '"?', function(result) {
        		if (result == true) {
        			$.post("${pageContext.request.contextPath}/admin/streetbind/ajax/delete",
 			        {
 			          "type" : type,
 			          "name" : name
 			        },
 			        function( response ) {
 			          if (response.status == "SUCCESS") {
 			              $("#" + type).children().filter(function(index, option) {
 			                  return option.value === name;
 			              }).remove();
 			              if (type == "city") {
 			            	  $('#city option').eq(1).prop('selected', true);
 			                  /* $('#city option')
 			                  .removeAttr('selected')
 			                  .filter('[value=Hồ Chí Minh]')
 			                      .attr('selected', true); */
 			              }
 			          } else if (response.status == "FAILED") {
 			        	    bootbox.alert("Has error occurred!", function() {
 			                    console.log("Has error occurred");
 			                });
 			          }
 			        });
         		}
       		});
        }
    });
}

//datatable ajax reload
function updateCompany(company1) {
//alert(company1);
//"companyTitle" : $("#company1").val(),
    $.post("${pageContext.request.contextPath}/ajax/admin/getCompanyByTitle",
				{
					"companyTitle" : company1,
					"primLang" : $("#locale1").val(),
					"secLang" : $("#locale2").val()
		   	},
				function( data ) {
					// Hide error messages
					$('#titleEng').removeClass('error');
					$('#titleLocal').removeClass('error');
					$('#com-display-success').hide();
					$('#com-display-error').hide();
					// Show data
					$('#companyId').val(data.companyId);
					$('#city').val(data.city);
					$('#district').val(data.district);
					$('#ward').val(data.ward);
					$('#street').val(data.street);
					$('#addrNum').val(data.addrNum);
					$('#titleEng').val(data.titleEng);
					$('#titleLocal').val(data.titleLocal);
					$('#telephone1').val(data.telephone1);
					$('#telephone2').val(data.telephone2);
					$('#email').val(data.email);
					$('#contact').val(data.contact);
					$('#url').val(data.url);
		   });
  
}

//update sub category base on category and both language
function updateSubCat(cat, locale1, locale2, subList1, subList2) {
    $.post("${pageContext.request.contextPath}/ajax/admin/subcats",
    {
      "cat" : cat,
      "primLang" : locale1,
      "secLang" : locale2,
    },
    function(data) {
      var $subcategory1 = $("#subcategory1").empty();
      var $subcategory2 = $("#subcategory2").empty();
      $(data[0]).each(function() {
        var newOption = '<option value="' + this + '">' + this + '</option>';
        $subcategory1.append(newOption);
      });
      $(data[1]).each(function() {
        var newOption = '<option value="' + this + '">' + this + '</option>';
        $subcategory2.append(newOption);
      });
      // Scroll to top
      $('#subcategory1').scrollTop(0);
      $("#subcategory1").val(subList1);
      $('#subcategory2').scrollTop(0);
      $("#subcategory2").val(subList2);
    });
}

//update company list and description when change language
function updateItemByLocale(itemId, locale, orderNo) {
    // Load company
    $.post("${pageContext.request.contextPath}/ajax/admin/companies",
    {
      "localLang" : locale
    },
    function(data) {
      var $company = $("#company" + orderNo).empty();
        $(data).each(function() {
          var newOption = '<option value="' + this + '">' + this + '</option>';
          $company.append(newOption);
        });
    });
    // Load description
    $.post("${pageContext.request.contextPath}/ajax/admin/desc",
    {
      "itemId" : itemId,
      "localLang" : locale
    },
    function(data) {
      var $shortDesc = $("#shortDesc" + orderNo).empty();
      var $longDesc = $("#longDesc" + orderNo).empty();
      $shortDesc.append(data[0]);
      $longDesc.append(data[1]);
    });
}

//sync scroll between 2 language sub category
function syncScrolling(parent, child) {
  $(parent).scroll(function () {
     $(child).scrollTop($(parent).scrollTop());
   });
 }

function checkImageAvailability(src, callbackSuccess, callbackFailure) {
    $("<img/>").attr("src", src).load(function() {
        callbackSuccess(src);
    }).error(function() {
        callbackFailure(src);
    });
}

$(document).ready(function() {
	  var loadingImage = null;
	  loadingImage = loadingImage || (function () {
	    return {
	      showPleaseWait: function() {
	    	  $('.progress-bar').css("width","0%");
	        $('.progress-bar').progressbar();
	        $('#pleaseWaitDialog').modal('show');
	      },
	      hidePleaseWait: function () {
	        setTimeout(function() { $('#pleaseWaitDialog').modal('hide'); }, 500);
	      },
	    };
		})();
	// hide #back-top first
	$("#back-top").hide();
	// fade in #back-top
	$(function () {
		$(window).scroll(function () {
			if ($(this).scrollTop() > 100) {
				$('#back-top').fadeIn();
			} else {
				$('#back-top').fadeOut();
			}
		});
		// scroll body to 0px on click
		$('#back-top a').click(function () {
			$('body,html').animate({
				scrollTop: 0
			}, 1000);
			return false;
		});
	});
    	  // --- Open location popup ---//
    $('#streetbind-create').on('hidden.bs.modal', function() {
        $('#streetbind-create').removeData('bs.modal');
    });
	  // Delete city
	  deleteLocation("city");
	  // Delete district
	  deleteLocation("district");
	  // Delete ward
	  deleteLocation("ward");
	  // Delete street
	  deleteLocation("street");
    // Get default page length in cookie
    var defaultDisplayPageLength = localStorage.getItem("kidzsaigonCompanyDisplayLength");
    if (defaultDisplayPageLength != null) {
    	defaultDisplayPageLength = parseInt(defaultDisplayPageLength);
		}

      // Locale1 onChange
//    $("#locale1").change(function() {
//      //reload table data
//      setTimeout(updateTable, 50);
//    });
//    // Locale2 onChange
//    $("#locale2").change(function() {
//      //reload table data
//      setTimeout(updateTable, 50);
//    });
		// Hide error messages
    $('#titleEng').removeClass('error');
    $('#titleLocal').removeClass('error');
    $('#com-display-success').hide();
    $('#com-display-error').hide();
    $('#saveCompany').on('click', function() {
        $('#titleEng').removeClass('error');
        $('#titleLocal').removeClass('error');
        $('#com-display-success').hide();
        $('#com-display-error').hide();
        var companyId = $('#companyId').val();
        if (companyId == 0) {
            bootbox.alert("Please select the company!", function() {
                // todo
            });
            return true;
        }

        var city = $('#city').val();
        var district = $('#district').val();
        var ward = $('#ward').val();
        var street = $('#street').val();
        var addrNum = $('#addrNum').val();
        var titleEng = $('#titleEng').val();
        var titleLocal = $('#titleLocal').val();
//        alert("locale1" + $("#locale1").val() +
//        	"locale2" + $("#locale2").val()+
//          "companyId" + $('#companyId').val());
        $.post("${pageContext.request.contextPath}/admin/company/ajax/check-input",
        {
        	"locale1" : $("#locale1").val(),
        	"locale2" : $("#locale2").val(),
          "companyId" : companyId,
          "city" : city,
          "district" : district,
          "ward" : ward,
          "street" : street,
          "addrNum" : addrNum,
          "titleEng" : titleEng, 
          "titleLocal" : titleLocal 
        },
        function( response ) {
          if (response.status == "OK") {
        	    $('#com-display-success').hide();
        	    $('#com-display-error').hide();
              //alert("")
              $.post("${pageContext.request.contextPath}/admin/company/ajax/update",
              {
              	"locale1" : $("#locale1").val(),
              	"locale2" : $("#locale2").val(),
                "companyId" : companyId,
                "city" : city,
                "district" : district,
                "ward" : ward,
                "street" : street,
                "addrNum" : addrNum,
                "titleEng" : titleEng, 
                "titleLocal" : titleLocal 
              },
              function( response ) {
                if (response.status == "OK") {
              	    $('#com-display-success').hide();
              	    $('#com-display-error').hide();
                    //Goto Save Item Next
                    $('#itemForm').submit();
                } else if (response.status == "INPUT_FAILED") {
                  errorInfo = "";
                  for(var i = 0 ; i < response.result.length ; i++) {
                    errorInfo += (i + 1) + ". " + response.result[i].code + "<br>";
                    $('div.' + response.result[i].field).html(response.result[i].code);
                    $('#' + response.result[i].field).addClass('error');
                  }
                  $('#com-display-error').html(errorInfo);
                  $('#com-display-error').show('slow');
                }
              });
          //
          //$('#companyForm').submit();
          } else if (response.status == "INPUT_FAILED") {
            errorInfo = "";
            for(var i = 0 ; i < response.result.length ; i++) {
              errorInfo += (i + 1) + ". " + response.result[i].code + "<br>";
              $('div.' + response.result[i].field).html(response.result[i].code);
              $('#' + response.result[i].field).addClass('error');
            }
            $('#com-display-error').html(errorInfo);
            $('#com-display-error').show('slow');
          }
        });
    });
    
  // Subcategory scrolling
  syncScrolling("#subcategory1", "#subcategory2");
  syncScrolling("#subcategory2", "#subcategory1");

	//Get default page length in cookie
  var defaultDisplayPageLength = localStorage.getItem("kidzsaigonItemDisplayLength");
  if (defaultDisplayPageLength != null) {
  	defaultDisplayPageLength = parseInt(defaultDisplayPageLength);
		}
  //load table
  oTable = $('#kidzsaigonTable').dataTable( {
    "fnDrawCallback": function( oSettings ) {
      // Save cookie
      var displayPageLength = oSettings._iDisplayLength;
	    localStorage.setItem("kidzsaigonItemDisplayLength", displayPageLength);
    },
    "iDisplayLength": defaultDisplayPageLength == null ? 50 : defaultDisplayPageLength,
    "sPaginationType": "bs_full",
    "bProcessing": true,
    "aaSorting": [[ 0, "desc" ]],
    "bSortClasses": false,
    "aoColumns": [
        {"sTitle": 'Item' },
        { "sTitle": 'Company', "sWidth" : '50%' },
        {"sTitle": "All" },
        <c:forEach items='${categories}' var='cat'>
        { "sTitle": '${cat.title}' },
        </c:forEach>
        { "sTitle": 'SD1', "sClass": 'center' },
        { "sTitle": 'SD2', "sClass": 'center' },
        { "sTitle": 'D1', "sClass": 'center' },
        { "sTitle": 'D2', "sClass": 'center' },
        { "sTitle": 'Vis', "sClass": 'center display' },
        { "sTitle": 'Img', "sClass": 'center' },
        { "sTitle": 'Cont', "sClass": 'center' },
        { "sTitle": 'Reorder', "sClass": 'center', "bSortable": false },
        { "sTitle": 'Del', "sClass": 'center', "bSortable": false }
    ],
    "sAjaxSource": "${pageContext.request.contextPath}/ajax/admin/all"
  });

  //change visible columen
  $("#kidzsaigonTable td.display").click(function() {
    var _this = this;

    if ($(_this).text() != "") {
      var itemId = $(_this).prevAll(':first').text();

      alert(itemId);
    }
  });
  
  //change item select
  $("#kidzsaigonTable tbody").click(function(event) {
    $(oTable.fnSettings().aoData).each(function (){
      $(this.nTr).removeClass('highlighted');
    });

    $(event.target).parents('tr').addClass('highlighted');
    var id = $(event.target).parents('tr').children(':first').text();

    //update item visible status
    if ($(event.target).parents('td').hasClass('display')) {
      $.get("${contextPath}/admin/item/delete/" + id);
      if ($(event.target).hasClass('glyphicon-ok')) {
        $(event.target).removeClass('glyphicon-ok green').addClass('glyphicon-remove red');
      } else { 
        $(event.target).removeClass('glyphicon-remove red').addClass('glyphicon-ok green');
      }
      bootbox.alert("Update item visible status ok.", function() {
	      console.log("Update item visible status ok");
      });
      return true;
      //return alert("Update item visible status ok.");
    }

    //increase item weight
    if ($(event.target).hasClass('glyphicon-arrow-up')) {
      var currRow = $(event.target).parents('tr');
      var nextRow = $(currRow).prev();
      var compare = null;

      do {
        compare = compare2row(currRow, nextRow); 
        if (compare == -1) {
          swap2row(currRow, nextRow);
          //return alert("Weight's increased");
          bootbox.alert("Weight's decreased.", function() {
    	      console.log("Weight's decreased");
          });
          return true;
        }
        nextRow = $(nextRow).prev();
      } while (compare == 0);
        
      //return alert('This record is not eligible to increase weight.');
      bootbox.alert("This record is not eligible to increase weight.", function() {
 	      // todo
     	});
      return true;
    }
    
    //decrease item weight
    if ($(event.target).hasClass('glyphicon-arrow-down')) {
      var currRow = $(event.target).parents('tr');
      var nextRow = $(currRow).next();
      var compare = null;

      do {
        compare = compare2row(currRow, nextRow); 
        if (compare == 1) {
          swap2row(currRow, nextRow);
          //return alert("Weight's decreased");
          bootbox.alert("Weight's decreased.", function() {
    	      console.log("Weight's decreased");
          });
          return true;
        }
        nextRow = $(nextRow).next();
      } while (compare == 0);

      bootbox.alert("This record is not eligible to decrease weight.", function() {
	      console.log("This record is not eligible to decrease weight");
      });
      return true;
      //return alert('This record is not eligible to decrease weight.');
    }
    
    //remove item permanent
    if ($(event.target).hasClass('glyphicon-trash')) {
      bootbox.confirm('This will remove your item permanent. Are you sure?', function(result) {
        if (result) {
            $.get("${contextPath}/admin/item/remove/" + id);
            oTable.fnDeleteRow($(event.target).parents('tr')[0]);
            
            bootbox.alert("Item has been removed successfully.", function() {
                console.log("Item has been removed successfully.");
            });
        }
      });
      
      return true;
    }

    //if (id == $('#itemId').val()) {
	  //  return;
		//}
    $('#itemId').val(id);
    $('.weight').val("");
    $('.weight').prop('disabled', true);
    //var company;
    //alert(company);
    var callbackOnSuccess = function(src) {
      loadingImage.showPleaseWait();
      // Here you can do whatever you want with your flickr images. Lets change the src and alt tags
      $(".img-rounded").attr("src", src);
      $(".img-rounded").attr("alt", src);
      // Lets change the parents href to #
      $(".img-rounded").parent().removeAttr("href");
      $('.img-rounded').load();
      loadingImage.hidePleaseWait();
      return false;
    };
    var callbackOnFailure = function(src) {
      loadingImage.showPleaseWait();
      // Here you can do whatever you want with your flickr images. Lets change the src and alt tags
      $(".img-rounded").attr("src", "${pageContext.request.contextPath}/resources/images/image_not_found.jpg");
      $(".img-rounded").attr("alt", "Image is blocked");
      // Lets change the parents href to #
      $(".img-rounded").parent().removeAttr("href");
      $('.img-rounded').load();
      loadingImage.hidePleaseWait();
      return false;
    };

    $.post("${pageContext.request.contextPath}/ajax/admin/getItem",
        { "itemId" : id, "primLang" : $("#locale1").val(), "secLang" : $("#locale2").val()},
        function( data ) {
            //company = data.company1;
            var category = data.category.toLowerCase();
            $('#weight-'+ category).val(data.weight);
            $('#weight-'+ category).prop('disabled', false);
            $('#cost').val(data.cost);
            $('#shortDesc1').val(data.shortDesc1);
            $('#shortDesc2').val(data.shortDesc2);
            $('#longDesc1').val(data.longDesc1);
            $('#longDesc2').val(data.longDesc2);
            $('#category').val(data.category);
            $('#company1').val(data.company1);
            $('#company2').val(data.company2);
            if (data.image != null && data.image.indexOf("http") !=-1) {
              checkImageAvailability(data.image, callbackOnSuccess, callbackOnFailure);
            } else {
            	checkImageAvailability("${pageContext.request.contextPath}/item/loadImage/" + data.image, callbackOnSuccess, callbackOnFailure);
            }
            $('#image').val(data.image);
            updateSubCat(data.category, $("#locale1").val(), $("#locale2").val(), data.subCategory1, data.subCategory2);
            updateCompany(data.company1);
        });
       // alert(company);
        
        
        //updateCompany(company);
        //setTimeout(updateCompany, 500);
        //Populate Company Fields
//        var companyId = $(event.target).parents('tr').children(':first').text();
        //var companyId = 89;
	    //if (companyId == $('#companyId').val()) {
		    //return;
		  //}
	    // Get company id
	    //$('#companyId').val(companyId);
            //alert($("#company1").val());
//	    $.post("${pageContext.request.contextPath}/ajax/admin/getCompanyByTitle",
//				{
//					"companyTitle" : $("#company1"),
//					"primLang" : $("#locale1").val(),
//					"secLang" : $("#locale2").val()
//		   	},
//				function( data ) {
//					// Hide error messages
//					$('#titleEng').removeClass('error');
//					$('#titleLocal').removeClass('error');
//					$('#com-display-success').hide();
//					$('#com-display-error').hide();
//					// Show data
//					$('#companyId').val(companyId);
//					$('#city').val(data.city);
//					$('#district').val(data.district);
//					$('#ward').val(data.ward);
//					$('#street').val(data.street);
//					$('#addrNum').val(data.addrNum);
//					$('#titleEng').val(data.titleEng);
//					$('#titleLocal').val(data.titleLocal);
//					$('#telephone1').val(data.telephone1);
//					$('#telephone2').val(data.telephone2);
//					$('#email').val(data.email);
//					$('#contact').val(data.contact);
//					$('#url').val(data.url);
//		   });
      
  });
    
  // Category onChange
  $(".category").change(function() {
    var select = $(this);
    var cat = "";
    
    $('option:selected', select).each(function(index, value) {
        if (index > 0) cat += "|";
        cat += $(value).val();
    });
    updateSubCat(cat, $("#locale1").val(), $("#locale2").val());
  });
  
  // Locale1 onChange
  $("#locale1").change(function() {
    var itemId = "${itemId}";
    var select = $(this);
    var locale = $('option:selected', select).val();

    updateItemByLocale(itemId, locale, 10);
    //reload table data
    setTimeout(updateTable, 10);
  });

  // Locale2 onChange
  $("#locale2").change(function() {
    var itemId = "${itemId}";
    var select = $(this);
    var locale = $('option:selected', select).val();
    updateItemByLocale(itemId, locale, 2);
    //reload table data
    setTimeout(updateTable, 10);
  });
   
  // Update company 1
  $('#company1').change(function() {
    var index = $(this).find('option:selected').index();
    $('#company2 option').eq(index).prop('selected', true);
    updateCompany($("#company1").val());
    //setTimeout(updateCompany, 10);
  });

  // Update company 2
  $('#company2').change(function() {
    var index = $(this).find('option:selected').index();
    $('#company1 option').eq(index).prop('selected', true);
    updateCompany($("#company1").val());
  });

  // Update sub category 1
  $('#subcategory1').change(function() {
  	$("#subcategory2 option:selected").removeAttr("selected");
  	$('#subcategory1 option:selected').each(function() {
  		var index = $(this).index();
  		$('#subcategory2 option').eq(index).prop('selected', true);
  	});
  });

  // Update sub category 2
  $('#subcategory2').change(function() {
  	$("#subcategory1 option:selected").removeAttr("selected");
  	$('#subcategory2 option:selected').each(function() {
  		var index = $(this).index();
  		$('#subcategory1 option').eq(index).prop('selected', true);
  	});
  });

  // Update upload type
  $('#uploadType0').change(function() {
  	if ($('#uploadType0').val() == 0) {
  		$("#file").removeAttr("disabled"); 
  		$("#image").attr("disabled","disabled");
  	}
  });
  
  //Update upload type
  $('#uploadType1').change(function() {
	if ($('#uploadType1').val() == 1) {
		$("#image").removeAttr("disabled"); 
		$("#file").attr("disabled","disabled");
	}
  });

  //reload popup when click on add new subcat
  $('#add-subcat').on('hidden.bs.modal', function() {
    $('#add-subcat').removeData('bs.modal');
    //$('#add-subcat').removeData("modal").modal({backdrop: 'static', keyboard: false})
  });
   
  //add new subcat
  $('#add-subcat-btn').click(function() {
	  $('#addSub1').removeClass('error');
		$('#addSub2').removeClass('error');
		var cat_titles = $('#category').val();
		var subCat1 = $('#addSub1').val();
		var subCat2 = $('#addSub2').val();

	  if (cat_titles == null) {
			//alert("Please select the category");
			bootbox.alert("Please select the category.", function() {
 	      // todo
     	});
			return true;
		}
		var hasError = false;
		if (subCat1 == '') {
			$('#addSub1').addClass('error');
			hasError = true;
		}
		if (subCat2 == '') {
			$('#addSub2').addClass('error');
			hasError = true;
		}
		if (hasError == true) {
			return;
		}
		var cats = "";
		for (var index = 0; index < cat_titles.length; index++) {
			if (index > 0) cats += "|";
			cats += cat_titles[index];
		}
		$.post("${pageContext.request.contextPath}/ajax/admin/addsubcat",
		{
			"cats" : cats,
			"lang1" : $('#locale1').val(),
			"subCat1" :  subCat1,
			"lang2" : $('#locale2').val(),
			"subCat2" :  subCat2,
			"actionMod" : subcatActionMode,
			"oldSubCat1" : oldSubcat1,
			"oldSubCat2" : oldSubcat2
		}, function (response) {
			updateSubCat(cats, $('#locale1').val(), $('#locale2').val());
			$('#add-subcat').modal("hide");
			$('#addSub1').val("");
			$('#addSub2').val("");
			subcatActionMode = "add";
			oldSubcat1 = "";
			oldSubcat2 = "";
		});
  });
	$('#btn-add-subcat').click(function() {
		$('#addSub1').val("");
		$('#addSub2').val("");
		subcatActionMode = "add";
		oldSubcat1 = "";
		oldSubcat2 = "";
	});
	$('#subcategory1').dblclick(function() {
		var sub_cat_title = $('#subcategory1').val();
		if (sub_cat_title == null) {
			return;
		}
		$('#add-subcat').modal("show");
		subcatActionMode = "edit";
		oldSubcat1 = $('#subcategory1').val().toString();
		oldSubcat2 = $('#subcategory2').val().toString();
		$('#addSub1').val($('#subcategory1').val().toString());
		$('#addSub2').val($('#subcategory2').val().toString());
	});
});

function swap2row(row1, row2) {

  var cell1 = $(row1).children(":eq(3)");
  var cell2 = $(row2).children(":eq(3)"); 

  for (var i = 0; i < 5; i++) {
    if(cell1.text() != "" && cell2.text() != "") {
      var weight1 = $(cell1).text();
      var weight2 = $(cell2).text();
      $(cell1).text(weight2);
      $(cell2).text(weight1);
      $($(row1).children(":eq(2)")).text(weight2);
      $($(row2).children(":eq(2)")).text(weight1);

      $.post("${pageContext.request.contextPath}/ajax/admin/update_weight",
      {
        "item" : $(row1).children(":first").text(),
        "category" : $('#kidzsaigonTable thead tr').children(":eq(" + (i+3) + ")").text(),
        "weight" : weight2,
      });
      $.post("${pageContext.request.contextPath}/ajax/admin/update_weight",
      {
        "item" : $(row2).children(":first").text(),
        "category" : $('#kidzsaigonTable thead tr').children(":eq(" + (i+3) + ")").text(),
        "weight" : weight1,
      });

      var html = $(row1).html();
      $(row1).html($(row2).html());
      $(row2).html(html);

      return compare2cell(cell1, cell2); 
    } 

    cell1 = $(cell1).next();
    cell2 = $(cell2).next();
  };
}

//row1 > row2 return 1
//row1 < row2 return -1
//row1 = row2 return 0
//cannot compare return -9
function compare2row(row1, row2) {
  if (row1 == null || row2 == null) return -9;

  var cell1 = $(row1).children(":eq(2)");
  var cell2 = $(row2).children(":eq(2)"); 

  for (var i = 0; i < 5; i++) {
    if(cell1.text() != "" && cell2.text() != "") {
      return compare2cell(cell1, cell2); 
    } 

    cell1 = $(cell1).next();
    cell2 = $(cell2).next();
  }

  return -9; 
};

function compare2cell(cell1, cell2) {
  var val1 = parseInt(cell1.text());
  var val2 = parseInt(cell2.text());
  
  if (val1 == val2) return 0;
  if (val1 > val2) return 1;
  if (val1 < val2) return -1;
  return -9;
};
</script>

</body>
</html>
