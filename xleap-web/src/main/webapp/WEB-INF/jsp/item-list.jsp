<%-- 
    Document   : item-list
    Created on : Oct 4, 2013, 5:38:44 AM
    Author     : 3piece
--%>
<%@ include file="taglibs.jsp"%>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="contextPath" value="<%=request.getContextPath()%>"></c:set>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" class="no-js" lang="en">
<head>
<meta charset="utf-8">
<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame -->
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Kidz Saigon - ${currCat}</title>
<link rel="icon" type="image/png" href="${contextPath}/resources/images/header/logo_face_bw.png">
    <meta name="description" content=<c:choose><c:when 
          test="${currCat == 'Eateries'}">"<spring:message code="description.page.brief.eateries"/>"</c:when><c:when 
          test="${currCat == 'Shopping'}">"<spring:message code="description.page.brief.shopping"/>"</c:when><c:when 
          test="${currCat == 'Activities'}">"<spring:message code="description.page.brief.activities"/>"</c:when><c:when 
          test="${currCat == 'Health'}">"<spring:message code="description.page.brief.health"/>"</c:when><c:when 
          test="${currCat == 'Learning'}">"<spring:message code="description.page.brief.learning"/>"</c:when><c:otherwise
              >"<spring:message code="description.page.brief.overall"/>"</c:otherwise></c:choose>>
<meta name="keywords" content="fun, kidz, children, excitment, things to do, saigon, vietnam, ho chi minh city">
<meta name="author" content="xaphe.com">
<meta http-equiv="cleartype" content="on">
<link rel="shortcut icon" href="/favicon.ico">
<!-- Responsive and mobile friendly stuff -->
<meta name="HandheldFriendly" content="True">
<meta name="MobileOptimized" content="640">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Stylesheets -->
<link rel="stylesheet" href="${contextPath}/resources/rgs/css/html5reset.css" media="all">
<link rel="stylesheet" href="${contextPath}/resources/rgs/css/col.css" media="all">
<script src="${contextPath}/resources/rgs/js/modernizr-2.5.3-min.js"></script>
<link rel="stylesheet" href="${contextPath}/resources/css/item-list.css" media="all">
<script>
  var clinks;
  var lang_set = 0;
  function initialize() {
       var downloadLink = document.getElementById('ad_bb_subway_wow_campaign');
addListener(downloadLink, 'click', function() {
  ga('send', 'event', 'Adverts', 'Click bb', 'Subway - WOW campaign');
  linkSelected(downloadLink.getAttribute('id'));
});
var facebooklink = document.getElementById('lnk_fbb_facebook');
addListener(facebooklink, 'click', function() {
  ga('send', 'event', 'Links', 'Click fbb', 'Facebook');
  linkSelected(facebooklink.getAttribute('id'));
});

var cat_home_button = document.getElementById("home_button");
addListener(cat_home_button, 'click', function() {
    ga('send', 'pageview', {'page': '/item', 'title': 'Kidz Saigon' });
 //linkSelected(link_${category.title}.getAttribute("${category.title}"));
 });


  <c:forEach var="category" items="${categories}">
      var cat_${category.title} = document.getElementById("${category.title}");
addListener(cat_${category.title}, 'click', function() {
    ga('send', 'pageview', {'page': '/item?cat=${category.title}', 'title': 'Kidz Saigon - ${category.title}' });
  ga('send', 'event', 'Pages', 'Categories', '${category.title}');
 //linkSelected(link_${category.title}.getAttribute("${category.title}"));
 });
  </c:forEach>

<c:forEach var="item" items="${itemList}">
      var link_${item.id} = document.getElementById("${item.url}");
addListener(link_${item.id}, 'click', function() {
  ga('send', 'event', 'Links', 'Click more_info', '${item.url}');
 linkSelected(link_${item.id}.getAttribute("${item.url}"));
 });
  </c:forEach>
  
  var topLeftBanner = document.getElementById('ad_tl_foodpanda');
addListener(topLeftBanner, 'click', function() {
  ga('send', 'event', 'Adverts', 'Click tl', 'Foodpanda');
  linkSelected(topLeftBanner.getAttribute('id'));
});

var topLeftBanner_home = document.getElementById('ad_tl_subway');
addListener(topLeftBanner, 'click', function() {
  ga('send', 'event', 'Adverts', 'Click tl', 'subway_wow');
  linkSelected(topLeftBanner.getAttribute('id'));
});

var topRightBanner = document.getElementById('ad_tr_taembe');
addListener(topRightBanner, 'click', function() {
  ga('send', 'event', 'Adverts', 'Click tr', 'TaEmBe - Diapers');
  linkSelected(topRightBanner.getAttribute('id'));
});
  
//  ga('send', 'pageview', {
//  'page': '/my-overridden-page?id=1',
//  'title': 'my overridden page'
//});
      
//  document.getElementById('simple-slider').setAttribute('x', '1');
//  Dragdealer('simple-slider', {x: 1});
      
    if (localStorage.skidz) {

    } else {
      localStorage.skidz = "";
    }
    if (localStorage.skidz_devid) {
      //TODO welcome back
      //localStorage.clickcount=Number(localStorage.clickcount)+1;
    } else {
      //TODO get new user from database
      localStorage.skidz_devid = "getIDFromDatabase";
    }
    // Check if langauge is set
    if (sessionStorage.skidz_lang) {
      if (sessionStorage.skidz_lang == 'en_US') {
          lang_set=0;
//        document.getElementById("Toggle_lang").checked = true;

      } else if (sessionStorage.skidz_lang == 'vi_VN') {
          lang_set=1;
//        document.getElementById("Toggle_lang").checked = false;
      }
    } else {
      sessionStorage.skidz_lang = 'en_US';
      lang_set=0;
//      document.getElementById("Toggle_lang").checked = true;
    }
//    var id_div = 'help_bubble';
//    var hb = document.getElementById(id_div);
//    if (localStorage.skidz_welcome) {
//        if (hb.style.display == 'block') {        
//            hb.style.display = 'none';
//            ga('send', 'event', 'welcome_popup', 'closed', id_div, localStorage.skidz_welcome);
//            //localStorage.skidz_welcome = parseInt(localStorage.skidz_welcome) + 1;
//    }
//        
//    } else {
//        localStorage.skidz_welcome = 1;
//        hb.style.display = 'block';
//        ga('send', 'event', 'welcome_popup', 'displayed', id_div);
// }
    
    
    
      //hideaway('help_bubble');
//    new Dragdealer('simple-slider',
//      {
//      steps: 2,
//      x: lang_set,
////      snap: true,
//      callback: function(x,y) {
////          alert(x);
//         if (x == 1) {
//             tog_lang('vi_VN');
//         }          
//         else {
//             tog_lang('en_US');
//         }
//      }
//      }
//  );
    //clinks = new Array();
  }
  function siteStats() {
    if (typeof (Storage) !== "undefined") {
      if (localStorage.clickcount) {
        localStorage.clickcount = Number(localStorage.clickcount) + 1;
      } else {
        localStorage.clickcount = 1;
      }
      document.getElementById("result2").innerHTML = "You have clicked the button "
          + localStorage.clickcount + " time(s).";
    } else {
      //      TODO send info back to database
      //document.getElementById("result2").innerHTML="Sorry, your browser does not support web storage...";
    }
  }
  
</script>
 <script>
 function JSEncode(title) {
     return 'Hi there';
 }
 </script>
     
     
<script>
  function clickCounterLocal() {
    if (typeof (Storage) !== "undefined") {
      if (localStorage.clickcount) {
        localStorage.clickcount = Number(localStorage.clickcount) + 1;
      } else {
        localStorage.clickcount = 1;
      }
      document.getElementById("result2").innerHTML = "You have clicked the button "
          + localStorage.clickcount + " time(s).";
    } else {
      document.getElementById("result2").innerHTML = "Sorry, your browser does not support web storage...";
    }
  }
</script>
<script>
  function clickCounter() {
    if (typeof (Storage) !== "undefined") {
      if (sessionStorage.clickcount) {
        sessionStorage.clickcount = Number(sessionStorage.clickcount) + 1;
      } else {
        sessionStorage.clickcount = 1;
      }
      document.getElementById("result").innerHTML = "You have clicked the button "
          + sessionStorage.clickcount + " time(s) in this session.";
    } else {
      document.getElementById("result").innerHTML = "Sorry, your browser does not support web storage...";
    }
  }
</script>
<script type="text/javascript">
  window.onbeforeunload = confirmExit;
  function confirmExit() {
    var clinks2 = new Array();//JSON.parse(sessionStorage["skidz_clinks"]);
    //   alert(clinks2.toString());
    Object.keys(sessionStorage).forEach(function(key) {
      if (/^skidz.clinks/.test(key)) {
        clinks2[key] = sessionStorage[key];
        sessionStorage.removeItem(key);
      }
    });

    var vals = Object.keys(clinks2).map(function(key) {
      return clinks2[key];
    });

    // return "Hi there "+vals.toString() +"Keys:"+ Object.keys(clinks2).toString() ;//clinks.toString(); //"You have attempted to leave this page.  If you have made any changes to the fields without clicking the Save button, your changes will be lost.  Are you sure you want to exit this page?";
  }
</script>
<script src="${contextPath}/resources/js/userstats.js"></script>
<script>
    
    
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-44650069-1', 'kidzsaigon.com');
  ga('send', 'pageview');
</script>
<script>
   
//var downloadLink = document.getElementById('ad bb subway wow campaign');
//addListener(downloadLink, 'click', function() {
//  ga('send', 'event', 'button', 'click', 'nav-buttons');
//});

//onclick="linkSelected(this.id);_gaq.push(['_trackEvent', 'Adverts', 'Click bb', 'Subway - WOW campaign']);"
//onclick="_gaq.push(['_trackEvent', 'Links', 'Click fbb', 'Facebook']);linkSelected(this.id);"


/**
 * Utility to wrap the different behaviors between W3C-compliant browsers
 * and IE when adding event handlers.
 *
 * @param {Object} element Object on which to attach the event listener.
 * @param {string} type A string representing the event type to listen for
 *     (e.g. load, click, etc.).
 * @param {function()} callback The function that receives the notification.
 */
function addListener(element, type, callback) {
 if (element.addEventListener) element.addEventListener(type, callback);
 else if (element.attachEvent) element.attachEvent('on' + type, callback);
}
</script>

<script type="text/javascript">
  function trackOutboundLink(link, category, action) {

    try {
      _gaq.push([ '_trackEvent', category, action ]);
    } catch (err) {
    }

    setTimeout(function() {
      document.location.href = link.href;
    }, 100);
  }
</script>
<script type="text/javascript">
  function tog_lang() {

    //document.getElementById("Toggle_lang").checked=true;
    if (sessionStorage.skidz_lang == 'vi_VN') {
      sessionStorage.skidz_lang = 'en_US';
      //if (sessionStorage.skidz.lang)
      //alert("Toggle is checked")
      //
      //window.location.href = "?lang=en_US";
    } else if (sessionStorage.skidz_lang == 'en_US') {
      sessionStorage.skidz_lang = 'vi_VN';
      //alert("Toggle is unchecked")
      // document.getElementById("Toggle_lang").checked=true;
      //window.location.href = "?lang=vi_VN";
    }
    //alert("Toggle switch Function Fired!")
    setTimeout(function() {
//      <%
        String ext2 = "";
        if (request.getParameter("cat") != null) {
          ext2 += "&cat=" + request.getParameter("cat");
	    }
		if (request.getParameter("page") != null) {
		  ext2 += "&page=" + request.getParameter("page");
		}
      %>//
//
//      window.location.href = "<%=request.getAttribute("javax.servlet.forward.request_uri")%>?lang=" + sessionStorage.skidz_lang + '<%=ext2%>';

        }, 300);
    //  location.reload();        
  }
  
  function tog_lang(lang) {
    sessionStorage.skidz_lang = lang;
    ga('send', 'pageview', {'page': '/?lang=' + lang + getURL()});
    setTimeout(function() {
      window.location.href = "<%=request.getAttribute("javax.servlet.forward.request_uri")%>?lang=" + lang + getURL();
        }, 300);
    //  location.reload();        
  }
  
  function getURL(){
  <%
        String ext = "";
        if (request.getParameter("cat") != null) {
          ext += "&cat=" + request.getParameter("cat");
	    }
		if (request.getParameter("page") != null) {
		  ext += "&page=" + request.getParameter("page");
		}
      %>
              return '<%=ext%>';
  }
  
</script>
      <script>
          function expandme(div_id,rowcount, currpage) { //'| item: ${rowCounter.count} | page: ${currPage}'
              var div_title = document.getElementById(div_id).getAttribute('title') + ' | item: ' + rowcount + ' | page: ' + currpage;
//              alert('ok');
//              alert(div_id + ' clicked!');
                var elem = document.getElementById(div_id+'_rollout');
//                            alert(elem.style.display);
//                        elem.style.visibility = 'visible';
//                        alert(elem.style.display + " : " + div_id);
                        if ( elem.style.display == 'block') {
                             elem.style.display = 'none';
                             ga('send', 'event', 'Items', 'closed', div_title);
                             
//                             elem.style.visibility = 'hidden';
                        } else {
                            
//                             elem.className = elem.className + " slideDown";
//                             setTimeout(function() {
//                        alert(div_title);
                                elem.style.display = 'block';
                                window.location.hash = div_id;
                                ga('send', 'event', 'Items', 'Expanded', div_title);
//                            }, 1500);
                              
                        }
                        
                         
                     hideaway('help_bubble');
                        //hidme2("help_bubble");
              
              //ga('send', 'event', 'Items', 'Expanded', div_id);
               //linkSelected(downloadLink.getAttribute('id'));
//              document.getElementById('accordian2').style.visibility = 'hidden';
          }
          
//          document.getElementById("spy-on-me").onmousedown = function () {
//    console.log("User moused down");
//    return true; // Not needed, as long as you don't return false
//};

function hideaway(id_div) {
    var hb = document.getElementById(id_div);
//    if (localStorage.skidz_welcome) {
        if (hb.style.display == 'block') {        
            hb.style.display = 'none';
            ga('send', 'event', 'welcome_popup', 'closed', id_div, localStorage.skidz_welcome);
            localStorage.skidz_welcome = parseInt(localStorage.skidz_welcome) + 1;
//    }
        
    } 
//    else {
//        localStorage.skidz_welcome = 1;
//        hb.style.display = 'block';
//        ga('send', 'event', 'welcome_popup', 'displayed', id_div);
// }
 }

      </script>
  <!--<style type="text/css" media="all">@import url("http://dimovie.vn/sites/all/modules/feedjs/feed_integration.css");</style>-->
  <script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js" type="text/javascript"></script>
<script src="http://dimovie.vn/sites/all/modules/feedjs/feed_integration.js" type="text/javascript"></script>
<!--<style type="text/css" media="all">@import url("http://dimovie.vn/sites/all/modules/feedjs/feed_integration.css");</style>-->
<script type="text/javascript">
jQuery(document).ready(function() {diMovieFeeds('#dimovie-feeds');});


</script>
       </head>
<body class="" onload="initialize()">
       <div class="container960" style="background: #FFFFFF;">
  <sec:authorize access="isAuthenticated()">
      <div class="section group" style="text-align: center;">
          <div class="col span_1_of_3">
              Welcome,
      <sec:authentication property="principal.username" />
          </div>
          <div class="col span_1_of_3 del_btn">
              ADMIN MODE
          </div>
          <div class="col span_1_of_3 pagination" style="color: #FFFFFF;">
              <a href="${contextPath}/j_spring_security_logout">Logout</a>
          </div>      
    </div>
    <div class="section group">
        <br/>
        <div class="menu">
            <h2><!--<a href="${contextPath}/item">--><spring:message code="label.menu.admin.website"/><!--</a>-->
	 | <a href="${contextPath}/admin/item"><spring:message code="label.menu.admin.new"/></a>
         | <a href="${contextPath}/admin/edit"><spring:message code="label.menu.admin.edit"/></a>
	 | <!--<a href="${contextPath}/admin/banner/list">--><spring:message code="label.menu.admin.banners"/><!--</a>-->
<!--	 | <a href="${contextPath}/admin/banner">Add banner</a>-->
	 | <a href="${contextPath}/admin/list/update"><spring:message code="label.menu.admin.edit_items"/></a>
	 | <a href="${contextPath}/admin/company/edit"><spring:message code="label.menu.admin.edit_companies"/></a>
	</h2>         
            <br/>
        </div>
    </div>
    <div style="display: block; color: #000"></sec:authorize>
        <sec:authorize access="isAnonymous()"><div style="display: none;"></sec:authorize>
            <c:choose><c:when 
          test="${currCat == 'Eateries'}">"<spring:message code="description.page.full.eateries"/>"</c:when><c:when 
          test="${currCat == 'Shopping'}">"<spring:message code="description.page.full.shopping"/>"</c:when><c:when 
          test="${currCat == 'Activities'}">"<spring:message code="description.page.full.activities"/>"</c:when><c:when 
          test="${currCat == 'Health'}">"<spring:message code="description.page.full.health"/>"</c:when><c:when 
          test="${currCat == 'Learning'}">"<spring:message code="description.page.full.learning"/>"</c:when><c:otherwise
              >"<spring:message code="description.page.full.overall"/>"</c:otherwise></c:choose>
        </div>

  <!--Advertising / Logo Header Block-->
  <!--<div class="container960" style="background: #FFFFFF;">-->
    <div class="section group el_heights_cont">
        
      <div class="col span_4_of_11 last header_ads el_heights_col">
          <c:choose><c:when 
                  test="${currCat == 'Eateries'}"><a id="ad_tl_foodpanda" href="http://foodpanda.vn/"  target="_blank">
             <img src="${contextPath}/resources/images/ads/foodpanda/<spring:message code="image.advert.foodpanda_4k_300x250"></spring:message>" style="width: 100%" />
            </a></c:when><c:when 
          test="${currCat == 'Shopping'}"><a id="ad_tl_foodpanda" href="http://foodpanda.vn/"  target="_blank">
             <img src="${contextPath}/resources/images/ads/foodpanda/<spring:message code="image.advert.foodpanda_4k_300x250"></spring:message>" style="width: 100%" />
            </a></c:when><c:when 
          test="${currCat == 'Activities'}"><a id="ad_tl_foodpanda" href="http://foodpanda.vn/"  target="_blank">
             <img src="${contextPath}/resources/images/ads/foodpanda/<spring:message code="image.advert.foodpanda_4k_300x250"></spring:message>" style="width: 100%" />
            </a></c:when><c:when 
          test="${currCat == 'Health'}"><a id="ad_tl_foodpanda" href="http://foodpanda.vn/"  target="_blank">
             <img src="${contextPath}/resources/images/ads/foodpanda/<spring:message code="image.advert.foodpanda_4k_300x250"></spring:message>" style="width: 100%" />
            </a></c:when><c:when 
          test="${currCat == 'Learning'}"><a id="ad_tl_foodpanda" href="http://foodpanda.vn/"  target="_blank">
             <img src="${contextPath}/resources/images/ads/foodpanda/<spring:message code="image.advert.foodpanda_4k_300x250"></spring:message>" style="width: 100%" />
            </a></c:when><c:otherwise
          ><a id="ad_tl_subway" href="https://www.facebook.com/subwayinvietnam"  target="_blank">
             <img src="${contextPath}/resources/images/ads/subway/bnweb300x250.jpg" style="width: 100%" />
            </a></c:otherwise></c:choose>
                    
      </div>
      <!--span_2_of_3-->
      <div class="col span_3_of_11 el_heights_col ">
        <!--<div class="col span_6_of_7">-->
        <!--<div style="height: 3em;"></div>-->
        
        <img src="${contextPath}/resources/images/header/spacer_300x34.jpg" />
        <img src="${contextPath}/resources/images/header/logo_face_bw_smll.png" alt="<c:choose><c:when 
          test="${currCat == 'Eateries'}"><spring:message code="description.page.brief.eateries"/></c:when><c:when 
          test="${currCat == 'Shopping'}"><spring:message code="description.page.brief.shopping"/></c:when><c:when 
          test="${currCat == 'Activities'}"><spring:message code="description.page.brief.activities"/></c:when><c:when 
          test="${currCat == 'Health'}"><spring:message code="description.page.brief.health"/></c:when><c:when 
          test="${currCat == 'Learning'}"><spring:message code="description.page.brief.learning"/></c:when><c:otherwise
              ><spring:message code="description.page.full.overall"/></c:otherwise></c:choose>"/>
        <!--</div>span_3_of_4-->
      </div>
      <!--span_3_of_3-->
        <div class="col span_4_of_11 header_ads el_heights_col">
              <a id="ad_tr_taembe" href="http://taembe.vn/"  target="_blank">
             <img src="${contextPath}/resources/images/ads/foodpanda/KidzSaigon-Taembe.jpg" style="width: 100%" />
            </a>
      </div>
      <!--span_3_of_3-->
    </div>
      
  
    <div class="section group ">
        
        <div class="col span_3_of_8" style="text-align: right; height: 100%" >
            <img class="img img-rounded" src="${contextPath}/resources/images/logos/ml_kidz_2.png" title="Kidz" alt="Kidz"/>
        </div>
      <!--        <div class="col span_5_of_11" style="background:#FFFFFF;"></div>-->
      
      <div class="col span_5_of_8 " style="text-align: center">
          
          <div class="section group">
              <div class="col span_2_of_5" style="text-align: center" >

                  <div class="section group el_heights_cont">
                       <img class="img img-rounded" src="${contextPath}/resources/images/logos/ksgn_plane.png" title="Kids Saigon Paper Airplane" alt="Kids Saigon Paper Plane" style="text-align: right;"/>

                  </div>

              </div>
              <div class="col span_3_of_5">
                  <img class="img img-rounded" src="${contextPath}/resources/images/logos/ml_sgn_2.png" title="Saigon" alt="Saigon" style="text-align: right;"/>
              </div>
          </div>
         
      </div>

    </div>
              
              <div class="section group">
                      
<!--<form class="searchform" >
	<input class="searchfield" type="text" value="Search..." onfocus="if (this.value == 'Search...') {this.value = '';}" onblur="if (this.value == '') {this.value = 'Search...';}" />
	<input class="searchbutton" type="button" value="  " />
</form>-->
<div  class="col span_1_of_8">
    <img class="img img-rounded" src="${contextPath}/resources/images/flags/flag_en.png" title="English" alt="English" onclick="tog_lang('en_US');"/>
</div>
    <!--<div>-->                 
        <div  class="col span_6_of_8">
            
<form:form action="${contextPath}/item/doSearch" method="get" class="searchform" >
<!--<input class="searchfield" type="text" name="cat"  value="Looking for something?" onfocus="if (this.value == 'Looking for something?') {this.value = '';}" onblur="if (this.value == '') {this.value = 'Looking for something?';}" />    -->
    <input class="searchfield" type="text" name="cat"  value='<spring:message code="label.searchBox" />' 
           onfocus="if (this.value == '<spring:message code="label.searchBox" />') {this.value = '';}" 
           onblur="if (this.value == '') {this.value = '<spring:message code="label.searchBox" />';}" /> 
	<input class="searchbutton" type="submit" value="  " />
        
</form:form>
        </div>
        
    <div  class="col span_1_of_8" style="vertical-align: bottom;">
        <img class="img img-rounded" src="${contextPath}/resources/images/flags/flag_vn.png" title="Tieng Viet" alt="Tieng Viet" onclick="tog_lang('vi_VN');" style="float: right;"/>
</div>
                  </div>
  <!--</div>-->
 
  <!--<div class="container960">-->
    <div class="section group">
      <div class="icn_rnd_crnr col span_1_of_6 catColor-home">
        <a id="home_button" href="${contextPath}/item"> 
            <img class="img" src="${contextPath}/resources/images/icons/home.png" title="Home" alt="All Categories" />
        </a>
      </div>
      <!--span_1_of_6-->
      <c:forEach var="category" items="${categories}">
        <div class="icn_rnd_crnr catColor_nl-${category.title} col span_1_of_6">
          <%-- <a href="<%=request.getAttribute("javax.servlet.forward.request_uri")%>?cat=${category.title}">  --%>
          <a id="${category.title}" href="${contextPath}/item?cat=${category.title}">
              <!--<object-->
                <img class="img img-rounded" src="${contextPath}/${category.filepath}" title="${category.title}" alt="${category.title}" />
          </a>
        </div>
        <!--span_1_of_6-->
      </c:forEach>
    </div>
            
    <!--section group-->
    
    <!--Speech Bubble-->
    <div class="section group" style="">
        <div class="col span_4_of_12"></div>
        <div class="col span_6_of_12">
            
    <div id="help_bubble" class="bubble" style="position:fixed; margin-top: 30px; text-align: center; display: block; color:#2C6896; min-height: 50px; padding: 10px; text" onclick="hideaway(this.id);">
        <h2 style="margin-top: 10px;"><spring:message code="message.welcome.select_box" /></h2>
    </div>
        </div>
        <div class="col span_2_of_12"></div>
        </div>
    
    <!--End Speech Bubble-->

      <!--ITEMS-->
      <c:if test="${!empty itemList}">
          <div class="section group">
              <c:forEach var="item" items="${itemList}" varStatus="rowCounter">
                  <div class="actlink" >
                      <div id="${item.id}" title="${item.title}" class="actbox_header section group catColor-${item.catTitle}" 
                           data-url="${contextPath}/item/${item.id}" onclick="expandme(${item.id},${rowCounter.count},<c:choose><c:when 
                                   test="${empty currPage}">'Search'</c:when><c:otherwise>${currPage}</c:otherwise></c:choose>);" >
                          
                      <!--<div class="actbox section group ">-->
                      <div class=" col span_1_of_5 " >
                          <img class="actpic icn_rnd_crnr catColor_nl-${item.catTitle}" src="${contextPath}${item.catIcon}"/>
                          </div>
                          <div class="col span_4_of_5 font_blackish " >
                              <h2>${item.title}</h2>
                              <hr />
                              
                              <div id="box1" class="section group ">
                                    <div class="col span_11_of_12">
                                        <h5>${item.subcats}</h5>
                                    </div>
                                    <div class="col span_1_of_12" >
<!--                                        <div  onclick="alert('Hi');expandme(${item.id}+'_rollout';">Test</div>-->
                                        <img src="${contextPath}/resources/images/buttons/roll_open.png" data-dismiss="modal" />
                                    </div>                                  
                                </div>
                              <p>${item.shrtDesc}</p>
                          </div>
                      </div>
                         <!--END of Header Box-->
  <!--RolloOut-->
                          <div id="${item.id}_rollout" class="actbox_rollout catColor-${item.catTitle}" style="display: none;" >
                    <div id="" class="font_blackish">     
                        <div class="section group ">
                            <div class="col span_1_of_12"></div>
                            <div class="col span_10_of_12 popupbgrnd">
                                <div class="col span_11_of_12 "></div>
                               
                                
                                <!--end of section-->
                                <div class="section group">
                                    <div class="col span_1_of_12"></div>
                                    <div id="desctable" class="col span_10_of_12">
                                        <p>${item.description}</p>
                                    </div>
                                    <div class="col span_1_of_12"></div>
                                </div>
                                    <!--end of section-->
                                <div class="section group">
                                    <div class="col span_1_of_3"></div>
                                    <div id="" class="col span_1_of_3" style="text-align: center">
                                        <c:choose><c:when test="${empty item.itm_img}"> 
                                                <!-- No Image Associated-->
                                                <%-- <img class="img-rounded" src="${contextPath}${item.cat_icon}" /> --%>
                                            </c:when><c:otherwise><c:choose><c:when test="${fn:containsIgnoreCase(item.itm_img, 'http')}"
                                                ><img class="img-rounded" src="${item.itm_img}" /></c:when><c:otherwise
                                                    ><img class="img-rounded" src="${contextPath}/item/loadImage/${item.itm_img}" /></c:otherwise></c:choose></c:otherwise></c:choose>
                                                    <br/>
                                    </div>
                                    <div class="col span_1_of_3"></div>
                                </div>
                                    
                                    <div class="section group">
                                    <!--<div class="col span_3_of_3">-->
                                    <div class="col span_1_of_12"></div>
                                    <div id="addbox" class="col span_10_of_12">
                                        <!-- <h4>Ho Chi Minh City, Viet Nam</h4> -->
                                        <c:if test="${!empty item.street || !empty item.addr_num}"
                                              ><h4>${item.addr_num} ${item.street}</h4></c:if
                                        ><c:if test="${!empty item.ward || !empty item.district}"><h4>${item.ward}<c:if test="${!empty item.ward && !empty item.district}">, </c:if>${item.district}</h4></c:if
                                        ><c:if test="${!empty item.city}"><h4>${item.city}</h4></c:if>
                                    </div>
                                    <div class="col span_1_of_12"></div>
                                </div>
                                    
                                <!--end of section-->
                                <c:if test="${!empty item.con4val}">
                                    <div class="section group nodecor">
                                        <div class="col span_1_of_12 "></div>
                                        <div class="col span_10_of_12 contactbox_left"><c:choose><c:when test="${fn:containsIgnoreCase(item.con4val, 'http')}"
                                                  ><span class="link">
                                              <a class="value nodecor" href="${item.con4val}" target="_blank" >Contact</a>
                                              </span></c:when><c:otherwise
                                                  ><div>${item.con4val}</div></c:otherwise></c:choose>
                                        </div>
                                        <div class="col span_1_of_12 "></div>
                                    </div>                                    
                                </c:if>
                                <c:if test="${!empty item.con2val}">
                                <div class="section group nodecor">
                                    <div class="col span_1_of_12 "></div>
                                    <div class="col span_10_of_12 contactbox_left">
                                        <span class="tel">
                                            <a class="value nodecor" href="tel:${item.con2val}">${item.con2val}</a>
                                        </span>
                                    </div>
                                </div>                                     
                                </c:if>
                                <c:if test="${!empty item.con1val}">
                                <div class="section group nodecor">
                                    <div class="col span_1_of_12 "></div>
                                    <div class="col span_10_of_12 contactbox_left">
                                        <span class="tel">
                                            <a class="value nodecor" href="tel:${item.con1val}">${item.con1val}</a>
                                        </span>
                                    </div>
                                    <div class="col span_1_of_12 "></div>
                                </div>                                 
                                </c:if>
                                <c:if test="${!empty item.con1val}">
                                <div class="section group">
                                    <div class="col span_1_of_12 "></div>
                                    <div class="col span_10_of_12 contactbox_left">
                                        <div><a class="nodecor" href="mailto:${item.con3val}">${item.con3val}</a></div>
                                    </div>
                                    <div class="col span_1_of_12 "></div>
                                </div>      
                                </c:if>
                                <!--end of section-->
                                <div class="section group ">
                                    <div class="col span_1_of_12 "></div>
                                   
                                    <div class="col span_6_of_12" >
                                         <div style="padding: 1% 0% 0% 0%;"> </div>
                                         <c:choose><c:when test="${fn:containsIgnoreCase(item.url, 'http')}"
                                         ><a id="${item.url}" href="${item.url}" target="_blank"></c:when><c:otherwise
                                                  ><a id="http://${item.url}" href="http://${item.url}" target="_blank"></c:otherwise></c:choose>
                                        <img class="img img-rounded web_button " src="${contextPath}/resources/images/buttons/moreinfo60.png" title="More Info" 
                                             alt="More Info" />
                                        </a>
                                        <!--<a href="${item.url}" target="_blank" style="position: absolute; bottom:  -1950px;">Please visit our website</a>-->
                                    </div>                                    
                                    <div class="col span_2_of_12 "></div>
                                    <div class="col span_2_of_12 ">
                                        <img src="${contextPath}/resources/images/moneybags/moneybag${item.cost}.png"
                                             style="float: right; margin-right: 10px;" />
                                    </div>
                                    <div class="col span_1_of_12 "></div>
                                </div>


                            </div>
                            <div class="col span_1_of_12">
                            </div>                    
                        </div>                
                    </div>
                </div>  
                  <!--End of link-->
                  </div>
                         
        <sec:authorize access="isAuthenticated()">
            <div class="section group" style="text-align: center; color: #FFFFFF;">
            <div class="col span_1_of_3 pagination">
                <a href="${contextPath}/admin/item?cat=${item.catTitle}&itemId=${item.id}"><div style="width: 100%">Edit</div></a>
            </div>
            <div class="col span_1_of_3 pagination">
                <a href="${contextPath}/admin/item" ><div style="width: 100%">Add New Item</div></a>
            </div>
            <div class="col span_1_of_3 del_btn">
            <a href="${contextPath}/admin/item/delete/${item.id}"><div style="width: 100%;">Delete Item</div></a>
            </div>
            
        </div>
        </sec:authorize>
      <!--</div>-->
      <!--container960-->
    </c:forEach>
      </div>
      <!--Section Group-->
    
      <!--Pagination section-->
      <!--padding: 1% 0 1% 0;-->
      <div id="pagination_grp" class="section group el_heights_cont" style="margin-bottom:1%; margin-top: 1%; text-align: center;">
          <div class="col span_1_of_5 el_heights_col">
              <c:if test="${!empty prevPage}">
                  <a href="<%=request.getAttribute("javax.servlet.forward.request_uri")%>?cat=${param.cat}&page=${prevPage}"
                     class="pagination" style="width: 100%">
                  <div class="pagination">Prev</div></a>
              </c:if>
          </div>
          <div class="col span_3_of_5 el_heights_col pag_cen" >
<% //calculate pagelist for paging
  List<String> listPage = new ArrayList<String>();
  int curpage = 0;
  if (request.getParameter("page") != null) {
		  curpage = Integer.parseInt(request.getParameter("page"));
 }
  int totalPage = (Integer)request.getAttribute("totalPage");
  if (totalPage <= 6) {
    for (int i = 0; i < totalPage; i++) {
      listPage.add((String.valueOf(i + 1))); 
    }
  } else {
	listPage.add("1");
	int currPage = request.getAttribute("nextPage") != null ? (Integer)request.getAttribute("nextPage") - 1 : totalPage;
    if (currPage > 4) {
      listPage.add("...");
      if (currPage > totalPage - 3) {
    	listPage.add(String.valueOf(totalPage - 4));
    	listPage.add(String.valueOf(totalPage - 3));
    	listPage.add(String.valueOf(totalPage - 2));
    	listPage.add(String.valueOf(totalPage - 1));
        listPage.add(String.valueOf(totalPage));
      } else {
      	listPage.add(String.valueOf(currPage - 2));
      	listPage.add(String.valueOf(currPage - 1));
      	listPage.add(String.valueOf(currPage));
      	listPage.add(String.valueOf(currPage + 1));
      	listPage.add(String.valueOf(currPage + 2));
        listPage.add("...");
        listPage.add(String.valueOf(totalPage));
      }
    } else {
      listPage.add("2");
      listPage.add("3");
      listPage.add("4");
      listPage.add("5");
      listPage.add("...");
      listPage.add(String.valueOf(totalPage));
    }
  
  }
%>
             <c:forEach var="page" items="<%=listPage%>">
                <c:choose>
                  <c:when test="${page == '...'}">
                      <span>${page}</span>
                  </c:when>
                  <c:when test="${(page eq param.page) or (empty param.page and page eq '1')}">
                      <span class="pagination_numbers_active"><a class="pagination_numbers_active" href="<%=request.getAttribute("javax.servlet.forward.request_uri")%>?cat=${param.cat}&page=${page}">${page}</a></span>
                  </c:when>
                  <c:otherwise>
                    <span class="pagination_numbers"><a class="pagination_numbers" href="<%=request.getAttribute("javax.servlet.forward.request_uri")%>?cat=${param.cat}&page=${page}">${page}</a></span>
                  </c:otherwise>
                </c:choose>
                  
             </c:forEach>
          </div>
          <div class="col span_1_of_5 el_heights_col">
              <c:if test="${!empty nextPage}">
                  <a href="<%=request.getAttribute("javax.servlet.forward.request_uri")%>?cat=${param.cat}&page=${nextPage}"
                     class="pagination" style="width: 100%"><div class="pagination">Next</div></a>
              </c:if>
          </div>
      </div>
      <!--section group-->
 </c:if>

  <!--<div class="container960">-->
    <div id="facebook" class=" section group">
        <a id="lnk_fbb_facebook" href="https://www.facebook.com/kidzsgn" target="_blank" >
      <img src="${contextPath}/resources/images/buttons/facebook_button.png" />
      </a>
    </div>
    <!--section group-->
  <!--</div>-->
  <!--container960-->
  <div style="margin-top: 1%; margin-bottom: 1%;">
      
 
       
            <c:choose><c:when test="${currCat == 'Eateries'}">
                    <div class="yellow section group bottomaddvertising" style="height: 100%;">
        <a id="ad_bb_subway_wow_campaign" href="https://www.facebook.com/subwayinvietnam"  target="_blank">
            <img src="${contextPath}/resources/images/ads/subway/subway_wow_720x300.jpg" width="100%;"/></a>
    </div></c:when>
                <c:when test="${currCat == 'Health'}">
                    <div class="yellow section group bottomaddvertising" style="height: 100%;">
        <a id="ad_bb_subway_wow_campaign" href="https://www.facebook.com/subwayinvietnam"  target="_blank">
            <img src="${contextPath}/resources/images/ads/subway/subway_wow_720x300.jpg" width="100%;"/></a>
    </div>
                </c:when>
                <c:otherwise>
                    <div class="movie-box container350">
                
          <div class="movie-box dimovie-titlebox">Movies in S�i G�n</div>
          <div id="dimovie-feeds" class="brown movie-box"></div>
                
            </div></c:otherwise>
            </c:choose>
    
    <!--section group-->
  </div>
  <!--container960-->
  
</body>
    
        </html>