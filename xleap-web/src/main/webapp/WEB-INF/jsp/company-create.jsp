<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="taglibs.jsp" %>
<c:set var="contextPath" value="<%=request.getContextPath()%>"></c:set>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script>
	function expandme(div_id) {
		var elem = document.getElementById(div_id + '_rollout');
		if (elem.style.display == 'block') {
			//$('#' + div_id + '_rollout').hide('slow');
			elem.style.display = 'none';
		} else {
			//$('#' + div_id + '_rollout').show('slow');
			elem.style.display = 'block';
		}
	}
</script>
</head>
<body>
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4 class="modal-title"><spring:message code="label.title.company.create" /></h4>
		</div>
		<div class="modal-body">
			<div id="com-display-error" class="display-error"></div>
			<div id="com-display-success" class="display-success"></div>
			<br>
			<form:form cssClass="form-horizontal" role="form" id="companyForm" data-target="#modal-dialog" method="POST" commandName="companyModelView" action="${contextPath}/admin/company/add">
				<div class="form-group">
					<label for="city" class="col-lg-4 control-label"><spring:message code="label.city"/></label>
					<div class="col-lg-7">
						<form:select cssClass="form-control chosen-select" path="city" id="city" items="${cities}" itemLabel="name" itemValue="name" placeholder="City"></form:select>
					</div>
					<!-- Add new city >>>-->
					<div>
					<a class="btn btn-info btn-xs" onclick="expandme('city');">
						<span class="glyphicon glyphicon-plus-sign" title="Add new city"></span>
					</a>
					</div>
					<div id="city_rollout" class="well col-sm-offset-4 col-sm-7" style="display:none;">
						<div class="col-lg-11">
							<input class="form-control" name="newcity" id="newcity" maxlength="200"></input>
						</div>
						<button type="button" id="savecity" class="btn btn-xs btn-primary">
							<span class="glyphicon glyphicon-ok"></span>
						</button>
					</div>
					<!-- Add new city <<<-->
				</div>
				<div class="form-group">
					<label for="district" class="col-lg-4 control-label"><spring:message code="label.district"/></label>
					<div class="col-lg-7">
						<form:select cssClass="form-control district-chosen-select" path="district" id="district" items="${districts}" itemLabel="district" itemValue="district" placeholder="District"/>
					</div>
					<!-- Add new district >>>-->
					<div>
					<a class="btn btn-info btn-xs" onclick="expandme('district');">
						<span class="glyphicon glyphicon-plus-sign" title="Add new district"></span>
					</a>
					</div>
					<div id="district_rollout" class="well col-sm-offset-4 col-sm-7" style="display:none;">
						<div class="col-lg-11">
							<input class="form-control" name="newdistrict" id="newdistrict" maxlength="200"></input>
						</div>
						<button type="button" id="savedistrict" class="btn btn-xs btn-primary">
							<span class="glyphicon glyphicon-ok"></span>
						</button>
					</div>
					<!-- Add new district <<<-->
				</div>
				<div class="form-group">
					<label for="ward" class="col-lg-4 control-label"><spring:message code="label.ward"/></label>
					<div class="col-lg-7">
						<form:select cssClass="form-control ward-chosen-select" path="ward" id="ward" items="${wards}" itemLabel="ward" itemValue="ward" placeholder="Ward"/>
					</div>
					<!-- Add new ward >>>-->
					<div>
					<a class="btn btn-info btn-xs" onclick="expandme('ward');">
						<span class="glyphicon glyphicon-plus-sign" title="Add new ward"></span>
					</a>
					</div>
					<div id="ward_rollout" class="well col-sm-offset-4 col-sm-7" style="display:none;">
						<div class="col-lg-11">
							<input class="form-control" name="newward" id="newward" maxlength="200"></input>
						</div>
						<button type="button" id="saveward" class="btn btn-xs btn-primary">
							<span class="glyphicon glyphicon-ok"></span>
						</button>
					</div>
					<!-- Add new ward <<<-->
				</div>
				<div class="form-group">
					<label for="street" class="col-lg-4 control-label"><spring:message code="label.street"/></label>
					<div class="col-lg-7">
						<form:select cssClass="form-control street-chosen-select" path="street" id="street" items="${streets}" itemLabel="street" itemValue="street" placeholder="Street"></form:select>
					</div>
					<!-- Add new street >>>-->
					<div>
					<a class="btn btn-info btn-xs" onclick="expandme('street');">
						<span class="glyphicon glyphicon-plus-sign" title="Add new street"></span>
					</a>
					</div>
					<div id="street_rollout" class="well col-sm-offset-4 col-sm-7" style="display:none;">
						<div class="col-lg-11">
							<input class="form-control" name="newstreet" id="newstreet" maxlength="200"></input>
						</div>
						<button type="button" id="savestreet" class="btn btn-xs btn-primary">
							<span class="glyphicon glyphicon-ok"></span>
						</button>
					</div>
					<!-- Add new street <<<-->
				</div>
				<div class="form-group">
					<label for="addrNum" class="col-lg-4 control-label"><spring:message code="label.addrnum"/></label>
					<div class="col-lg-8">
						<form:input cssClass="form-control" path="addrNum" id="addrNum" placeholder="Address Number" maxlength="40"></form:input>
					</div>
				</div>
				<div class="form-group">
					<label for="titleEng" class="col-lg-4 control-label"><spring:message code="label.engTitle"/></label>
					<div class="col-lg-8">
						<form:input cssClass="form-control" path="titleEng" id="titleEng" placeholder="Title English" maxlength="80"></form:input>
					</div>
				</div>
				<div class="form-group">
					<label for="titleLocal" class="col-lg-4 control-label"><spring:message code="label.localTitle"/></label>
					<div class="col-lg-8">
						<form:input cssClass="form-control" path="titleLocal" id="titleLocal" placeholder="Title Local" maxlength="80"></form:input>
					</div>
				</div>
				<div class="form-group">
					<label for="telephone1" class="col-lg-4 control-label"><spring:message code="label.telephone"/></label>
					<div class="col-lg-8">
						<form:input cssClass="form-control" path="telephone1" id="telephone1" placeholder="Telephone" maxlength="200"></form:input>
					</div>
				</div>
				<div class="form-group">
					<label for="telephone2" class="col-lg-4 control-label"><spring:message code="label.mobile"/>/<spring:message code="label.fax"/></label>
					<div class="col-lg-8">
						<form:input cssClass="form-control" path="telephone2" id="telephone2" placeholder="Mobile/Fax" maxlength="200"></form:input>
					</div>
				</div>
				<div class="form-group">
					<label for="email" class="col-lg-4 control-label"><spring:message code="label.email"/></label>
					<div class="col-lg-8">
						<form:input cssClass="form-control" path="email" id="email" placeholder="Email" maxlength="200"></form:input>
					</div>
				</div>
				<div class="form-group">
					<label for="contact" class="col-lg-4 control-label"><spring:message code="label.contact"/></label>
					<div class="col-lg-8">
						<form:input cssClass="form-control" path="contact" id="contact" placeholder="Contact" maxlength="200"></form:input>
					</div>
				</div>
				<div class="form-group">
					<label for="url" class="col-lg-4 control-label"><spring:message code="label.url"/></label>
					<div class="col-lg-8">
						<form:input cssClass="form-control" path="url" id="url" placeholder="URL" maxlength="200"></form:input>
					</div>
				</div>
			</form:form>
		</div>
		<div class="modal-footer">
			<button type="button" id="saveCompany" class="btn btn-primary"><spring:message code="label.save"/></button>
			<button type="button" class="btn btn-default" data-dismiss="modal"><spring:message code="label.close"/></button>
		</div>
	</div>
	<!-- /.modal-content -->
</div>
<script type="text/javascript">
	function saveStreetEvent(type) {
		$("#save" + type).on('click', function() {
			var name = $("#new" + type).val();
			var shortcode = "";
			// var shortcode = $("#shortcode").val();
			$.post("${pageContext.request.contextPath}/admin/streetbind/ajax/save",
			{
				"type" : type,
				"name" : name,
				"shortcode" : shortcode
			},
			function( response ) {
				if (response.status == "SUCCESS") {
					// Display message
					// Add new item to city selection
					var optionItem = '<option value="' + name + '" selected="selected">' + name + '</option>';
					$('#' + type).append(optionItem);
					$("#new" + type).val('');
					expandme(type);
				} else if (response.status == "FAILED") {
					errorInfo = "";
					for(var i = 0 ; i < response.result.length ; i++) {
						errorInfo += (i + 1) + ". " + response.result[i].code;
					}
					//alert(errorInfo);
					bootbox.alert(errorInfo, function() {
		      	// todo
		     	});
				} else if (response.status == "INPUT_FAILED") {
					errorInfo = "";
					for(var i = 0 ; i < response.result.length ; i++) {
						errorInfo += (i + 1) + ". " + response.result[i].code;
					}
					//alert(errorInfo);
					bootbox.alert(errorInfo, function() {
		      	// todo
		     	});
				}
			});
		});
	}
</script>
<script type="text/javascript">
	$(document).ready(function() {
		// Hide error message
		$('#titleEng').removeClass('error');
		$('#titleLocal').removeClass('error');
		$('#com-display-success').hide();
		$('#com-display-error').hide();
		// Set default value
		//$('#city').val("Hồ Chí Minh");
		//$('#district').val("Quận 1");
		//$('#ward').val("Bến Nghé");
		saveStreetEvent("city");
		saveStreetEvent("district");
		saveStreetEvent("ward");
		saveStreetEvent("street");
		//$(".district-chosen-select").chosen({ width : "100%"});
		//$(".ward-chosen-select").chosen({ width : "100%"});
		//$(".street-chosen-select").chosen({ width : "100%"});
		$('#saveCompany').on('click', function() {
        $('#titleEng').removeClass('error');
        $('#titleLocal').removeClass('error');
        $('#com-display-success').hide();
        $('#com-display-error').hide();
				var primaryLang = $("#locale1").val();
				var secondLang = $("#locale2").val();
				var city = $('#city').val();
				var district = $('#district').val();
				var ward = $('#ward').val();
				var street = $('#street').val();
				var addrNum = $('#addrNum').val();
				var titleEng = $('#titleEng').val();
				var titleLocal = $('#titleLocal').val();
				var telephone1 = $('#telephone1').val();
				var telephone2 = $('#telephone2').val();
				var email = $('#email').val();
				var contact = $('#contact').val();
				var url = $('#url').val();
				$.post("${pageContext.request.contextPath}/admin/company/ajax/save",
				{
					"locale1" : primaryLang,
					"locale2" : secondLang,
					"city" : city,
					"district" : district,
					"ward" : ward,
					"street" : street,
					"addrNum" : addrNum,
					"titleEng" : titleEng, 
					"titleLocal" : titleLocal, 
					"telephone1" : telephone1,
					"telephone2" : telephone2,
					"email" : email,
					"contact" : contact,
					"url" : url
				},
				function( response ) {
					if (response.status == "SUCCESS") {
						// Add new item to city selection
						var optionItemL1 = '<option value="' + titleEng + '" selected="selected">' + titleEng + '</option>';
						var optionItemL2 = '<option value="' + titleLocal + '" selected="selected">' + titleLocal + '</option>';
						$('#company1').append(optionItemL1);
						$('#company2').append(optionItemL2);
						// Reset value
						$('#city').val("Hồ Chí Minh");
						$('#district').val("Quận 1");
						$('#ward').val("Bến Nghé");
						$('#street').val("");
						$('#addrNum').val("");
						$('#titleEng').val("");
						$('#titleLocal').val("");
						$('#telephone1').val("");
						$('#telephone2').val("");
						$('#email').val("");
						$('#contact').val("");
						$('#url').val("");
						// Display message
						$('#com-display-success').html("Save successful.");
						$('#com-display-success').show('slow');
						$('#company-info').animate({ scrollTop: 30 }, 'slow');
            setTimeout(function() { $('#company-info').hide('slow'); }, 1000);
            setTimeout(function() { $('#company-info').modal('hide'); }, 1500);
					} else if (response.status == "FAILED") {
						$('#com-display-error').html("Save failed.");
						$('#com-display-error').show('slow');
						$('#company-info').animate({ scrollTop: 30 }, 'slow');
					} else if (response.status == "INPUT_FAILED") {
						errorInfo = "";
						for(var i = 0 ; i < response.result.length ; i++) {
							errorInfo += (i + 1) + ". " + response.result[i].code + "<br>";
							$('div.' + response.result[i].field).html(response.result[i].code);
							$('#' + response.result[i].field).addClass('error');
						}
						$('#com-display-error').html(errorInfo);
						$('#com-display-error').show('slow');
						$('#company-info').animate({ scrollTop: 30 }, 'slow');
					}
				});
		});
	});
</script>

</body>
</html>