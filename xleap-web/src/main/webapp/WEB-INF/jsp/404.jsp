<%@ page language="java" isErrorPage="true" session="false" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<title>Error page</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<body>
	<h1>Resource is not available</h1>
	<p>Sorry, an error occurred. Error code : 404</p>
</body>
</html>