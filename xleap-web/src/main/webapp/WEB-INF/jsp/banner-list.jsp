<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="taglibs.jsp" %>
<c:set var="contextPath" value="<%=request.getContextPath()%>"></c:set>
<c:set var="isUpdate" value="false"></c:set>
<c:if test="${actionMode eq 'update'}">
	<c:set var="isUpdate" value="true"></c:set>
</c:if>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all" />
<link href="//ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/css/jquery.dataTables.css" rel="stylesheet" type="text/css" media="all" />
<link href="${contextPath}/resources/css/item_admin.css" rel="stylesheet" type="text/css" media="all" />
<title>Banner Manager</title>
<style type="text/css">
#kidzsaigonTable tbody tr.highlighted { background-color: rgb(236, 255, 216);}
#kidzsaigonTable tbody tr:HOVER { cursor: pointer;}
</style>
</head>
<body>
<div class="container">
	<h3><a href="${contextPath}/item">Return to item view</a>
	 | <a href="${contextPath}/admin/item">Add item</a>
	 | <a href="${contextPath}/admin/list/update">Edit all items</a>
	 | <a href="${contextPath}/admin/company/edit">Edit all Companies</a>
	 | <a href="${contextPath}/admin/banner">Add banner</a>
	</h3>
	<div class="row">
		<c:forEach var="message" items="${messages}">
			<div id="display-${message.key}" class="display-${message.key}"><spring:message code="${message.value}" /></div>
		</c:forEach>
		<form:form cssClass="form-horizontal" role="form" method="POST" enctype="multipart/form-data" modelAttribute="bannerModelView" action="${contextPath}/admin/banner/list">
			<h3 class="modal-header">
			<c:if test="${isUpdate == false}">
				<spring:message code="label.title.banner.create"></spring:message>
			</c:if>
			<c:if test="${isUpdate == true}">
				<spring:message code="label.title.banner.edit"></spring:message>
			</c:if>
			</h3>
			<div class="form-group">
				<div class="col-lg-12">
					<table class="bordered-table zebra-striped dataTable" id="kidzsaigonTable">
						<thead>
							<tr>
								<th width="5%">ID</th>
								<th width="5%">Visible</th>
								<th width="15%">Client</th>
								<th width="10%">Category</th>
								<th width="10%">Image</th>
								<th width="15%">URL</th>
								<th width="10%">Weight</th>
								<th width="15%">Start Date</th>
								<th width="15%">End Date</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</form:form>
	</div>
</div>

<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script src="//ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/jquery.dataTables.min.js"></script>
<script src="${contextPath}/resources/js/datatables/dataTables.fnReloadAjax.js"></script>
<script src="${contextPath}/resources/js/datatables/dataTables.fnGetSelected.js"></script>
<script type="text/javascript">
	var oTable;
	$(document).ready(function() {
		// Get default page length in cookie
		var defaultDisplayPageLength = localStorage.getItem("kidzsaigonBannerDisplayLength");
		if (defaultDisplayPageLength != null) {
			defaultDisplayPageLength = parseInt(defaultDisplayPageLength);
		}
		oTable = $('#kidzsaigonTable').dataTable( {
				"fnDrawCallback": function( oSettings ) {
					// Save cookie
					var displayPageLength = oSettings._iDisplayLength;
					localStorage.setItem("kidzsaigonBannerDisplayLength", displayPageLength);
				},
				"iDisplayLength": defaultDisplayPageLength == null ? 10 : defaultDisplayPageLength,
				"bPaginate": true,
				"bSortClasses": false,
				"sScrollX": "100%",
				"sScrollXInner": "110%",
				"bScrollCollapse": true,
				"sAjaxSource": "${pageContext.request.contextPath}/admin/banner/all",
				"aoColumnDefs":[{
					"aTargets": [ 4 ],
					"bSortable": false,
					"mRender": function ( img, type, full ) {
						return '<img src="${pageContext.request.contextPath}/admin/banner/loadImage/' + img + '" style="width: 50px; height: 50px;" />';
					}
				},{
					"aTargets": [ 5 ],
					"bSortable": false,
					"mRender": function ( url, type, full )  {
						return  '<a target="_blank" href="' + url + '">' + full[2] + '</a>';
					}
				},{
					"aTargets":[ 7 ],
					"sType": "date",
					"mRender": function(date, type, full) {
						return new Date(date).toDateString();
					}
				}]
		});
		$('#kidzsaigonTable').on('click', 'tr', function(){
			var oData = oTable.fnGetData(this);
			console.log(oData);
		});
		//change item select
		$("#kidzsaigonTable tbody").click(function(event) {
			$(oTable.fnSettings().aoData).each(function (){
				$(this.nTr).removeClass('highlighted');
			});
			$(event.target).parents('tr').addClass('highlighted');
		});
	});
</script>

</body>
</html>