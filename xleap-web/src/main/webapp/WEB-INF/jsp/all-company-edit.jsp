<%@ include file="taglibs.jsp"%>
<c:set var="contextPath" value="<%=request.getContextPath()%>"></c:set>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="//ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/css/jquery.dataTables.css" rel="stylesheet" type="text/css" media="all" />
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all" />
<!-- <link href="//cdnjs.cloudflare.com/ajax/libs/chosen/1.0/chosen.min.css" rel="stylesheet" type="text/css" media="all" /> -->
<link href="${contextPath}/resources/css/item_admin.css" rel="stylesheet" type="text/css" media="all" />
<title>Admin Company</title>
<style type="text/css">
#kidzsaigonTable tbody tr.highlighted { background-color: rgb(236, 255, 216);}
#kidzsaigonTable tbody tr:HOVER { cursor: pointer;}
</style>
</head>
<body>
	<div>
		Welcome,
		<sec:authentication var="user" property="principal.username" />
	</div>
	<a href="${contextPath}/j_spring_security_logout">Logout</a>
	<div class="container">
            
	<h3><a href="${contextPath}/item"><spring:message code="label.menu.admin.website"/></a>
	 | <a href="${contextPath}/admin/item"><spring:message code="label.menu.admin.new"/></a>
         | <a href="${contextPath}/admin/edit"><spring:message code="label.menu.admin.edit"/></a>
	 | <!--<a href="${contextPath}/admin/banner/list">--><spring:message code="label.menu.admin.banners"/><!--</a>-->
<!--	 | <a href="${contextPath}/admin/banner">Add banner</a>-->
	 | <a href="${contextPath}/admin/list/update"><spring:message code="label.menu.admin.edit_items"/></a>
	 | <!--<a href="${contextPath}/admin/company/edit">--><spring:message code="label.menu.admin.edit_companies"/><!--</a>-->
	</h3>
  
  <!--<iframe src="http://webchat.freenode.net?nick=james_4k&channels=%234kdz&uio=d4" width="647" height="400"></iframe>-->
<!--  <iframe src="https://kiwiirc.com/client/irc.freenode.com/?nick=<c:choose><c:when 
          test="${user == 'wicksvn@gmail.com'}">bryan_4k</c:when><c:when
          test="${user == 'markzazula@yahoo.com'}">mark_4k</c:when><c:when 
          test="${user == 'craig.buckie@gmail.com'}">buckie_4k</c:when><c:when
          test="${user == 'dr.3piece@gmail.com'}">james_4k</c:when><c:otherwise>piece3</c:otherwise></c:choose>#4kdz" style="border:0; width:100%; height:450px;"></iframe>-->
		<div class="row">
			<c:forEach var="message" items="${messages}">
        <div id="display-${message.key}" class="display-${message.key}"><spring:message code="${message.value}" /></div>
      </c:forEach>
      <div id="com-display-error" class="display-error"></div>
      <div id="com-display-success" class="display-success"></div>
			<form:form cssClass="form-horizontal" id="companyForm" role="form" method="POST" commandName="companyModelView" action="${contextPath}/admin/company/update">
				<h3 class="modal-header">
					<spring:message code="label.title.company.edit"/>
				</h3>
				<form:hidden path="companyId" />
				<c:set var="companyId" value="${companyModelView.companyId}"></c:set>
				<div class="form-group">
					<div class="col-lg-12"><form:errors path="locale1" cssStyle="color: red;"></form:errors></div>
					<div class="col-lg-6">
						<form:select cssClass="form-control" path="locale1" id="locale1" items="${locales}" itemValue="code" itemLabel="local_name"></form:select>
					</div>
					<div class="col-lg-6">
						<form:select cssClass="form-control" path="locale2" id="locale2" items="${locales}" itemValue="code" itemLabel="local_name"></form:select>
					</div>
				</div>
				<div class="form-group">
					<div class="col-lg-12">
					<table class="display" id="kidzsaigonTable">
						<thead>
							<tr>
								<th width="5%">ID</th>
								<th width="20%">Company L1</th>
								<th width="20%">Company L2</th>
								<th width="10%">City</th>
								<th width="10%">District</th>
								<th width="10%">Ward</th>
								<th width="15%">Street</th>
								<th width="10%">Address Number</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
					</div>
				</div>
				<div class="form-group">
					<label for="city" class="col-lg-3 control-label"><spring:message code="label.city"/></label>
					<div class="col-lg-6">
						<form:select cssClass="form-control chosen-select" path="city" id="city" items="${cities}" itemLabel="name" itemValue="name" placeholder="City"></form:select>
					</div>
					<div>
					 <a class="btn btn-info btn-xs" data-toggle="modal" href="${contextPath}/admin/streetbind?type=city" data-target="#streetbind-create">
            <span class="glyphicon glyphicon-plus-sign" title="Add new city"></span>
           </a>
           <a class="btn btn-warning btn-xs" id="cityDel">
            <span class="glyphicon glyphicon-remove-sign" title="Delete city"></span>
           </a>
					</div>
				</div>
				<div class="form-group">
					<label for="district" class="col-lg-3 control-label"><spring:message code="label.district"/></label>
					<div class="col-lg-6">
						<form:select cssClass="form-control district-chosen-select" path="district" id="district" items="${districts}" itemLabel="district" itemValue="district" placeholder="District"/>
					</div>
					<div>
           <a class="btn btn-info btn-xs" data-toggle="modal" href="${contextPath}/admin/streetbind?type=district" data-target="#streetbind-create">
            <span class="glyphicon glyphicon-plus-sign" title="Add new district"></span>
           </a>
           <a class="btn btn-warning btn-xs" id="districtDel">
            <span class="glyphicon glyphicon-remove-sign" title="Delete district"></span>
           </a>
					</div>
				</div>
				<div class="form-group">
					<label for="ward" class="col-lg-3 control-label"><spring:message code="label.ward"/></label>
					<div class="col-lg-6">
						<form:select cssClass="form-control ward-chosen-select" path="ward" id="ward" items="${wards}" itemLabel="ward" itemValue="ward" placeholder="Ward"/>
					</div>
					<div>
           <a class="btn btn-info btn-xs" data-toggle="modal" href="${contextPath}/admin/streetbind?type=ward" data-target="#streetbind-create">
            <span class="glyphicon glyphicon-plus-sign" title="Add new ward"></span>
           </a>
           <a class="btn btn-warning btn-xs" id="wardDel">
            <span class="glyphicon glyphicon-remove-sign" title="Delete ward"></span>
           </a>
					</div>
				</div>
				<div class="form-group">
					<label for="street" class="col-lg-3 control-label"><spring:message code="label.street"/></label>
					<div class="col-lg-6">
						<form:select cssClass="form-control street-chosen-select" path="street" id="street" items="${streets}" itemLabel="street" itemValue="street" placeholder="Street"></form:select>
					</div>
					<div>
           <a class="btn btn-info btn-xs" data-toggle="modal" href="${contextPath}/admin/streetbind?type=street" data-target="#streetbind-create">
            <span class="glyphicon glyphicon-plus-sign" title="Add new street"></span>
           </a>
           <a class="btn btn-warning btn-xs" id="streetDel">
            <span class="glyphicon glyphicon-remove-sign" title="Delete street"></span>
           </a>
					</div>
				</div>
				<div class="form-group">
					<label for="addrNum" class="col-lg-3 control-label"><spring:message code="label.addrnum"/></label>
					<div class="col-lg-6">
						<form:input cssClass="form-control" path="addrNum" id="addrNum" placeholder="Address Number" maxlength="40"></form:input>
					</div>
				</div>
				<div class="form-group">
					<label for="titleEng" class="col-lg-3 control-label"><spring:message code="label.engTitle"/></label>
					&nbsp;<form:errors path="titleEng" cssStyle="color: red;"></form:errors>
					<div class="col-lg-6">
						<form:input cssClass="form-control" path="titleEng" id="titleEng" placeholder="Title English" maxlength="80"></form:input>
					</div>
				</div>
				<div class="form-group">
					<label for="titleLocal" class="col-lg-3 control-label"><spring:message code="label.localTitle"/></label>
					&nbsp;<form:errors path="titleLocal" cssStyle="color: red;"></form:errors>
					<div class="col-lg-6">
						<form:input cssClass="form-control" path="titleLocal" id="titleLocal" placeholder="Title Local" maxlength="80"></form:input>
					</div>
				</div>
				<div class="form-group">
					<label for="telephone1" class="col-lg-3 control-label"><spring:message code="label.telephone"/></label>
					<div class="col-lg-6">
						<form:input cssClass="form-control" path="telephone1" id="telephone1" placeholder="Telephone" maxlength="200"></form:input>
					</div>
				</div>
				<div class="form-group">
					<label for="telephone2" class="col-lg-3 control-label"><spring:message code="label.mobile"/>/<spring:message code="label.fax"/></label>
					<div class="col-lg-6">
						<form:input cssClass="form-control" path="telephone2" id="telephone2" placeholder="Mobile/Fax" maxlength="200"></form:input>
					</div>
				</div>
				<div class="form-group">
					<label for="email" class="col-lg-3 control-label"><spring:message code="label.email"/></label>
					<div class="col-lg-6">
						<form:input cssClass="form-control" path="email" id="email" placeholder="Email" maxlength="200"></form:input>
					</div>
				</div>
				<div class="form-group">
					<label for="contact" class="col-lg-3 control-label"><spring:message code="label.contact"/></label>
					<div class="col-lg-6">
						<form:input cssClass="form-control" path="contact" id="contact" placeholder="Contact" maxlength="200"></form:input>
					</div>
				</div>
				<div class="form-group">
					<label for="url" class="col-lg-3 control-label"><spring:message code="label.url"/></label>
					<div class="col-lg-6">
						<form:input cssClass="form-control" path="url" id="url" placeholder="URL" maxlength="200"></form:input>
					</div>
				</div>
				<div class="form_save-control modal-footer">
					<div>
						<input type="button" name="saveCompany" id="saveCompany" value='<spring:message code="label.save"/>' class="btn btn-primary" />
					</div>
				</div>
			</form:form>
		</div>
	</div>
	 <!-- Modal -->
  <div class="modal fade" id="streetbind-create" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"></div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script src="${contextPath}/resources/js/chosen/chosen.jquery.min.js"></script>
<script src="//ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/jquery.dataTables.min.js"></script>
<script src="${contextPath}/resources/js/datatables/dataTables.fnReloadAjax.js"></script>
<script src="${contextPath}/resources/js/datatables/dataTables.fnGetSelected.js"></script>
<script type="text/javascript">
var oTable;

function updateTable() {
	oTable.fnReloadAjax("${pageContext.request.contextPath}/ajax/admin/company/all?primLang=" + $("#locale1").val() + "&secLang=" + $("#locale2").val());
}

function deleteLocation(type) {
    $("#" + type + "Del").on('click', function() {
        var name = $("#" + type).val();
        if (name == null || name == "") {
        	  alert("Please select an item");
        	  return;
        } else {
        	if (!confirm('Delete "' + name + '"?')) {
        		 return;
       		}
        }
        $.post("${pageContext.request.contextPath}/admin/streetbind/ajax/delete",
        {
          "type" : type,
          "name" : name
        },
        function( response ) {
          if (response.status == "SUCCESS") {
              $("#" + type).children().filter(function(index, option) {
                  return option.value === name;
              }).remove();
              if (type == "city") {
            	  $('#city option').eq(1).prop('selected', true);
                  /* $('#city option')
                  .removeAttr('selected')
                  .filter('[value=Hồ Chí Minh]')
                      .attr('selected', true); */
              }
          } else if (response.status == "FAILED") {
        	    alert("Has error occurred");
          }
        });
    });
}

$(document).ready(function() {
	  // --- Open location popup ---//
    $('#streetbind-create').on('hidden.bs.modal', function() {
        $('#streetbind-create').removeData('bs.modal');
    });
	  // Delete city
	  deleteLocation("city");
	  // Delete district
	  deleteLocation("district");
	  // Delete ward
	  deleteLocation("ward");
	  // Delete street
	  deleteLocation("street");
    // Get default page length in cookie
    var defaultDisplayPageLength = localStorage.getItem("kidzsaigonCompanyDisplayLength");
    if (defaultDisplayPageLength != null) {
    	defaultDisplayPageLength = parseInt(defaultDisplayPageLength);
		}
    oTable = $('#kidzsaigonTable').dataTable( {
    		"fnDrawCallback": function( oSettings ) {
    			  // Save cookie
    		    var displayPageLength = oSettings._iDisplayLength;
    		    localStorage.setItem("kidzsaigonCompanyDisplayLength", displayPageLength);
    	  },
        "iDisplayLength": defaultDisplayPageLength == null ? 10 : defaultDisplayPageLength,
        "bPaginate": true,
        "bSortClasses": false,
        "sScrollX": "100%",
        "sScrollXInner": "110%",
        "bScrollCollapse": true,
        "sAjaxSource": "${pageContext.request.contextPath}/ajax/admin/company/all"
    });
    // Datatable click event
    $("#kidzsaigonTable tbody").click(function(event) {
		  $(oTable.fnSettings().aoData).each(function (){
	    	  $(this.nTr).removeClass('highlighted');
	    });
		  //$(event.target.parentNode).addClass('highlighted');
		  //var companyId = $(event.target.parentNode).children('td:first').text();
		  $(event.target).parents('tr').addClass('highlighted');
		  var companyId = $(event.target).parents('tr').children(':first').text();
	    //if (companyId == $('#companyId').val()) {
		    //return;
		  //}
	    // Get company id
	    $('#companyId').val(companyId);
	    $.post("${pageContext.request.contextPath}/ajax/admin/getCompany",
				{
					"companyId" : companyId,
					"primLang" : $("#locale1").val(),
					"secLang" : $("#locale2").val()
		   	},
				function( data ) {
					// Hide error messages
					$('#titleEng').removeClass('error');
					$('#titleLocal').removeClass('error');
					$('#com-display-success').hide();
					$('#com-display-error').hide();
					// Show data
					$('#companyId').val(companyId);
					$('#city').val(data.city);
					$('#district').val(data.district);
					$('#ward').val(data.ward);
					$('#street').val(data.street);
					$('#addrNum').val(data.addrNum);
					$('#titleEng').val(data.titleEng);
					$('#titleLocal').val(data.titleLocal);
					$('#telephone1').val(data.telephone1);
					$('#telephone2').val(data.telephone2);
					$('#email').val(data.email);
					$('#contact').val(data.contact);
					$('#url').val(data.url);
		   });
    });
    // Locale1 onChange
    $("#locale1").change(function() {
      //reload table data
      setTimeout(updateTable, 50);
    });
    // Locale2 onChange
    $("#locale2").change(function() {
      //reload table data
      setTimeout(updateTable, 50);
    });
		// Hide error messages
    $('#titleEng').removeClass('error');
    $('#titleLocal').removeClass('error');
    $('#com-display-success').hide();
    $('#com-display-error').hide();
    $('#saveCompany').on('click', function() {
        $('#titleEng').removeClass('error');
        $('#titleLocal').removeClass('error');
        $('#com-display-success').hide();
        $('#com-display-error').hide();
        var companyId = $('#companyId').val();
        if (companyId == 0) {
            alert("Please select the company");
            return;
        }
        var city = $('#city').val();
        var district = $('#district').val();
        var ward = $('#ward').val();
        var street = $('#street').val();
        var addrNum = $('#addrNum').val();
        var titleEng = $('#titleEng').val();
        var titleLocal = $('#titleLocal').val();
        $.post("${pageContext.request.contextPath}/admin/company/ajax/check-input",
        {
        	"locale1" : $("#locale1").val(),
        	"locale2" : $("#locale2").val(),
          "companyId" : companyId,
          "city" : city,
          "district" : district,
          "ward" : ward,
          "street" : street,
          "addrNum" : addrNum,
          "titleEng" : titleEng, 
          "titleLocal" : titleLocal 
        },
        function( response ) {
          if (response.status == "OK") {
        	    $('#com-display-success').hide();
        	    $('#com-display-error').hide();
        	    //$('#companyForm').submit();
          } else if (response.status == "INPUT_FAILED") {
            errorInfo = "";
            for(var i = 0 ; i < response.result.length ; i++) {
              errorInfo += (i + 1) + ". " + response.result[i].code + "<br>";
              $('div.' + response.result[i].field).html(response.result[i].code);
              $('#' + response.result[i].field).addClass('error');
            }
            $('#com-display-error').html(errorInfo);
            $('#com-display-error').show('slow');
          }
        });
    });
  });
</script>
</body>
</html>