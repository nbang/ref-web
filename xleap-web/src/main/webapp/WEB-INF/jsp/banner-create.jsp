<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="taglibs.jsp" %>
<c:set var="contextPath" value="<%=request.getContextPath()%>"></c:set>
<c:set var="isUpdate" value="false"></c:set>
<c:if test="${actionMode eq 'update'}">
	<c:set var="isUpdate" value="true"></c:set>
</c:if>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all" />
<link href="${contextPath}/resources/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" media="all" />
<link href="${contextPath}/resources/css/item_admin.css" rel="stylesheet" type="text/css" media="all" />
<link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.1/css/font-awesome.css" rel="stylesheet">
<title>Banner Manager</title>
</head>
<body>
<div class="container">
	<h3><a href="${contextPath}/item">Return to item view</a>
	 | <a href="${contextPath}/admin/item">Add item</a>
	 | <a href="${contextPath}/admin/list/update">Edit all items</a>
	 | <a href="${contextPath}/admin/company/edit">Edit all Companies</a>
	 | <a href="${contextPath}/admin/banner/list">All banners</a>
	</h3>
	<div class="row">
		<c:forEach var="message" items="${messages}">
			<div id="display-${message.key}" class="display-${message.key}"><spring:message code="${message.value}" /></div>
		</c:forEach>
		<form:form role="form" method="POST" enctype="multipart/form-data" modelAttribute="bannerModelView" action="${contextPath}/admin/banner/${actionMode}">
			<h3 class="modal-header">
			<c:if test="${isUpdate == false}">
				<spring:message code="label.title.banner.create"></spring:message>
			</c:if>
			<c:if test="${isUpdate == true}">
				<spring:message code="label.title.banner.edit"></spring:message>
			</c:if>
			</h3>
			<form:hidden path="id" />
			<c:set var="id" value="${bannerModelView.id}"></c:set>
			<div class="form-group">
				<div class="row">
					<div class="col-lg-5">
						<label for="category"><spring:message code="label.category" /></label>&nbsp;<form:errors path="category" cssStyle="color: red;"></form:errors>
						<form:select cssClass="form-control" path="category" multiple="true" size="5">
							<form:options items="${categories}" itemLabel="title" itemValue="title"/>
						</form:select>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-lg-5">
						<label for="pageLocation"><spring:message code="label.banner.pageLoc" /></label>&nbsp;<form:errors path="pageLocation" cssStyle="color: red;"></form:errors>
						<form:select cssClass="form-control" path="pageLocation" multiple="false" size="1">
							<form:options items="${pageLocs}" itemLabel="pageloc" itemValue="pageloc"/>
						</form:select>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-lg-5">
						<label for="clientName"><spring:message code="label.banner.client" /></label>&nbsp;<form:errors path="clientName" cssStyle="color: red;"></form:errors>
						<input class="form-control" type="text" name="clientName" id="clientName" value="${bannerModelView.clientName}" maxlength="50"></input>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-lg-5">
						<label for="image"><spring:message code="label.banner.image" /></label>&nbsp;<form:errors path="image" cssStyle="color: red;"></form:errors>
						<input class="form-control" type="file" name="image" id="image" value="${bannerModelView.image}" maxlength="250"></input>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-lg-5">
						<label for="url"><spring:message code="label.banner.url" /></label>&nbsp;<form:errors path="url" cssStyle="color: red;"></form:errors>
						<input class="form-control" type="text" name="url" id="url" value="${bannerModelView.url}" maxlength="500"></input>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-lg-2">
						<label for="width"><spring:message code="label.banner.width" /></label>&nbsp;<form:errors path="width" cssStyle="color: red;"></form:errors>
						<form:input class="form-control" path="width"/>
					</div>
					<div class="col-lg-2">
						<label for="height"><spring:message code="label.banner.height" /></label>&nbsp;<form:errors path="height" cssStyle="color: red;"></form:errors>
						<form:input class="form-control" path="height"/>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-lg-2">
						<label for="weight"><spring:message code="label.banner.weight" /></label>&nbsp;<form:errors path="weight" cssStyle="color: red;"></form:errors>
						<input class="form-control" type="text" name="weight" id="weight" value="${bannerModelView.weight}" maxlength="500"></input>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-lg-2">
						<label for="startDate"><spring:message code="label.banner.startDate" /></label>&nbsp;<form:errors path="startDate" cssStyle="color: red;"></form:errors>
						<form:input class="form-control" path="startDate" data-date-format="yyyy-mm-dd" readonly="false" />
					</div>
					<div class="col-lg-2">
						<label for="endDate"><spring:message code="label.banner.endDate" /></label>&nbsp;<form:errors path="endDate" cssStyle="color: red;"></form:errors>
						<form:input class="form-control" path="endDate" data-date-format="yyyy-mm-dd" readonly="false" />
					</div>
				</div>
			</div>
			<!-- input-group -->
			<div class="form_save-control modal-footer">
				<div>
					<input type="submit" value='<spring:message code="label.save"/>' class="btn btn-primary" />
				</div>
			</div>
		</form:form>
	</div>
</div>

<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<script src="${contextPath}/resources/js/moment.min.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap-datetimepicker.js"></script>

<script type="text/javascript">
	$(function () {
		$('#startDate').datetimepicker({
			weekStart: 1,
			todayBtn:  1,
			autoclose: 1,
			todayHighlight: 1,
			startView: 2,
			minView: 2,
			forceParse: 1
		});
		$('#endDate').datetimepicker({
			weekStart: 1,
			todayBtn:  1,
			autoclose: 1,
			todayHighlight: 1,
			startView: 2,
			minView: 2,
			forceParse: 1
		});
	});
	$(document).ready(function() {

	});
</script>

</body>
</html>