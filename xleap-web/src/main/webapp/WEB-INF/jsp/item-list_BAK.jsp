<%-- 
    Document   : item-list
    Created on : Oct 4, 2013, 5:38:44 AM
    Author     : 3piece
--%>
<%@ include file="taglibs.jsp"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" class="no-js" lang="en">
    <head>
        <!-- HTML5 Mobile Boilerplate -->
        <!--[if IEMobile 7]><html class="no-js iem7"><![endif]-->
        <!--[if (gt IEMobile 7)|!(IEMobile)]><!-->
        <!--<![endif]-->
        <!-- HTML5 Boilerplate -->
        <!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
        <!--[if (IE 7)&!(IEMobile)]><html class="no-js lt-ie9 lt-ie8" lang="en"><![endif]-->
        <!--[if (IE 8)&!(IEMobile)]><html class="no-js lt-ie9" lang="en"><![endif]-->
        <!--[if gt IE 8]><!-->
        <!--<![endif]-->
        <meta charset="utf-8"></meta>
        <!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"></meta>
        <title>Kidz Saigon</title>
        <meta name="description"
              content="This is Kidz Saigon, the place to find fun things to do in Ho Chi Minh City."></meta>
        <meta name="keywords" content="fun, exciting, saigon, vietnam viet nam ho chi minh city"> </meta>
        <meta name="author" content="xaphe.com"> </meta>
        <meta http-equiv="cleartype" content="on"></meta>
        <link rel="shortcut icon" href="/favicon.ico"></link>
        <!-- Responsive and mobile friendly stuff -->
        <meta name="HandheldFriendly" content="True"></meta>
        <meta name="MobileOptimized" content="320"></meta>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"></meta>
        <!-- Stylesheets -->
        <!--<link rel="stylesheet" href="<%=request.getContextPath()%>/resources/rgs/css/html5reset.css" media="all"></link>-->
        <!--<link rel="stylesheet" href="<%=request.getContextPath()%>/resources/rgs/css/responsivegridsystem.css" media="all"></link>-->
        <link rel="stylesheet" href="<%=request.getContextPath()%>/resources/rgs/css/col.css" media="all"></link>
        <!--<link rel="stylesheet" href="<%=request.getContextPath()%>/resources/rgs/css/2cols.css" media="all">-->
        <!--<link rel="stylesheet" href="css/3cols.css" media="all">
        <link rel="stylesheet" href="css/4cols.css" media="all">
        <!-- <link rel="stylesheet" href="css/5cols.css" media="all">
        <!-- <link rel="stylesheet" href="css/6cols.css" media="all">
        <!-- <link rel="stylesheet" href="css/7cols.css" media="all"> -->
        <!--<link rel="stylesheet" href="<%=request.getContextPath()%>/resources/rgs/css/3cols.css" media="all"></link>-->
        <!--<link rel="stylesheet" href="<%=request.getContextPath()%>/resources/rgs/css/5cols.css" media="all"></link>-->
        <!--<link rel="stylesheet" href="<%=request.getContextPath()%>/resources/rgs/css/6cols.css" media="all"></link>-->
        <!--<link rel="stylesheet" href="<%=request.getContextPath()%>/resources/rgs/css/8cols.css" media="all"></link>-->
        <!--<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" type="text/css"
          media="all" />-->
        <!-- All JavaScript at the bottom, except for Modernizr which enables HTML5 elements and feature detects -->
        <!--<script src="<%=request.getContextPath()%>/resources/rgs/js/modernizr-2.5.3-min.js"></script>-->
        <!--<link rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/item-list.css" media="all"></link>-->
        <style type="text/css">
            
root { 
    display: block;
}
body
{
    /*Why Does this change the padding on bottom of images?*/
    font-family:Arial, Helvetica, sans-serif;
    /*background:f00*/
}


/*Equal Hieghts code*/
.el_heights_cont {

    overflow: hidden; 
}
.el_heights_col {
    float: left; background: #ccc; width: 33%; margin-bottom: -2000px; padding-bottom: 2000px
}

/*Container 960 class to hold all items within a max width of 960px*/
.container960
{
    max-width:960px;
    /*background:#;*/
    margin:auto;
}

/*Horizontal Rule*/
hr
{
    max-width:960px;
    margin:10px auto;
    border:none;
    border-bottom:1px solid #c7c7c7;
    margin-top:20px;
    margin-bottom:20px;
}

/*Styling*/
/*
    .img
    {
        width: 100%;
        height: 100%;
    }*/

/*Rounded corners of background boxes for categories*/
.icn_rnd_crnr {
    border-radius: 10%; 
    -moz-border-radius: 10%; 
    -webkit-border-radius: 10%; 
}

/*Colour classes*/
/*General colours*/

.blue
{
    background: #0033ff;
}
.red
{
    background: #F00;
}
.bluegreen
{
    background: #476364;
}
.green
{
    background: #DEF379;
}
.yellow{
    background:#EFD355;
}
.pink{
    background:#FFCDC1;
}
.yellow_light{
    background:#FFFFCC;
}
/*Category Colours*/
.catColor-home
{
    background: #f6f6f6;
    /*background: #FFE680;*/
}    
.catColor-Eateries
{
    background: #59a2be;
    /*background: #FFE680;*/
}
.catColor-Shopping
{
    background: #ad62a7;
    /*background: #E9AFAF;*/
}
.catColor-Health
{
    background: #67bf74;
    /*background: #E5FF80;*/
}
.catColor-Activities{
    background: #f16364;
    /*background:#C8BEA7;*/
}
.catColor-Learning{
    background: #e4c62e;
    /*background:#99CCFF;*/
}

.actbox
    {
        margin-bottom:2%;
        -webkit-box-shadow: 2px 2px 4px 0px rgba(0,0,0,0.7);
        -moz-box-shadow: 2px 2px 4px 0px rgba(0,0,0,0.7);
        box-shadow: 2px 2px 4px 0px rgba(0,0,0,0.7);
    }
/*    .actlink
    {
        color:#000;
    }
    .actlink:hover
    {
        color:#f9f9f9;
    }
    .actlink:visited {
        color:#dddddd;
    }*/

/* RESPONSIVE GRID SYSTEM =============================================================================  */

/* IMAGES ============================================================================= */

img {
    /*border : 0;*/
    max-width: 100%;
    /*height: auto;*/
    width: auto\9; /* ie8 */
}
/*
img.floatleft { float: left; margin: 0 10px 0 0; }
img.floatright { float: right; margin: 0 0 0 10px; }*/

/*  GRID OF THREE   ============================================================================= */
    .span_3_of_3 {
        width: 100%; 
    }
    .span_2_of_3 {
        width: 66.13%; 
    }
    .span_1_of_3 {
        width: 32.26%; 
    }
    
    
    /*  GRID OF FIVE   ============================================================================= */
    .span_5_of_5 {
        width: 100%;
    }
    .span_4_of_5 {
        width: 79.68%; 
    }
    .span_3_of_5 {
        width: 59.36%; 
    }
    .span_2_of_5 {
        width: 39.04%;
    }
    .span_1_of_5 {
        width: 18.72%;
    }
    
    /*  GO FULL WIDTH AT LESS THAN 480 PIXELS */
    .span_6_of_6 {
        width: 100%;
    }
    .span_5_of_6 {
        width: 83.06%;
    }
    .span_4_of_6 {
        width: 66.13%;
    }
    .span_3_of_6 {
        width: 49.2%;
    }
    .span_2_of_6 {
        width: 32.26%;
    }
    .span_1_of_6 {
        width: 15.33%;
    }

/*  GRID OF EIGHT   ============================================================================= */
.span_8_of_8 {
	width: 100%;
}

.span_7_of_8 {
	width: 87.3%; 
}

.span_6_of_8 {
	width: 74.6%; 
}

.span_5_of_8 {
	width: 61.9%; 
}

.span_4_of_8 {
	width: 49.2%; 
}

.span_3_of_8 {
	width: 36.5%;
}

.span_2_of_8 {
	width: 23.8%; 
}

.span_1_of_8 {
	width: 11.1%; 
}


label {
	color: transparent;
	background: url(<%=request.getContextPath()%>/resources/images/buttons/flags.png) -40px 0  no-repeat;
	border-radius: 14px;
	box-shadow: 0 1px 2px #888, 0 0px 3px #777, inset 0 -1px 5px #333;
	display: block;
	position: relative;
	text-indent: 100%;
	width: 65px; height: 29px;
	-webkit-transition: background-position .3s ease;
	-moz-transition: background-position .3s ease;
	cursor: pointer;
	font-size: .01em;
	float: left;
}

input[type=checkbox] {
	display: none;
}

label span {
	background :url(<%=request.getContextPath()%>/resources/images/buttons/flags.png) -1px -30px no-repeat;
	border: 0px solid transparent;
	border-radius: 14px;
	box-shadow: 0 1px 3px #000, 0 2px 13px #000;
	content: "";
	display: block;
	position: absolute; top: 0; left: -1px;
	width: 28px; height: 28px;
	-webkit-transition: left .3s ease;
	-moz-transition: left .3s ease;
}

input[type=checkbox]:checked + label {
	background-position: 0 0;
}

input[type=checkbox]:checked + label span {
	left: 40px;
}


        </style>
        <script>
            var clinks;
            function initialize() {
                if (localStorage.skidz) {

                } else {
                    localStorage.skidz = "";
                }
                if (localStorage.skidz.devid) {
                    //TODO welcome back
                    //localStorage.clickcount=Number(localStorage.clickcount)+1;
                } else {
                    //TODO get new user from database
                    localStorage.skidz.devid = "getIDFromDatabase";
                }
                // Check if langauge is set
                if (sessionStorage.skidz_lang) {
                    if (sessionStorage.skidz_lang == 'en_US') {
                        document.getElementById("Toggle_lang").checked = true;

                    } else if (sessionStorage.skidz_lang == 'vi_VN') {
                        document.getElementById("Toggle_lang").checked = false;
                    }
                } else {
                    sessionStorage.skidz_lang = 'en_US';
                    document.getElementById("Toggle_lang").checked = true;
                }
                //clinks = new Array();
            }
            function siteStats() {
                if (typeof (Storage) !== "undefined") {
                    if (localStorage.clickcount) {
                        localStorage.clickcount = Number(localStorage.clickcount) + 1;
                    } else {
                        localStorage.clickcount = 1;
                    }
                    document.getElementById("result2").innerHTML = "You have clicked the button "
                            + localStorage.clickcount + " time(s).";
                } else {
                    //      TODO send info back to database
                    //document.getElementById("result2").innerHTML="Sorry, your browser does not support web storage...";
                }
            }
        </script>
        <script>
            function clickCounterLocal() {
                if (typeof (Storage) !== "undefined") {
                    if (localStorage.clickcount) {
                        localStorage.clickcount = Number(localStorage.clickcount) + 1;
                    } else {
                        localStorage.clickcount = 1;
                    }
                    document.getElementById("result2").innerHTML = "You have clicked the button "
                            + localStorage.clickcount + " time(s).";
                } else {
                    document.getElementById("result2").innerHTML = "Sorry, your browser does not support web storage...";
                }
            }
        </script>
        <script>
            function clickCounter() {
                if (typeof (Storage) !== "undefined") {
                    if (sessionStorage.clickcount) {
                        sessionStorage.clickcount = Number(sessionStorage.clickcount) + 1;
                    } else {
                        sessionStorage.clickcount = 1;
                    }
                    document.getElementById("result").innerHTML = "You have clicked the button "
                            + sessionStorage.clickcount + " time(s) in this session.";
                } else {
                    document.getElementById("result").innerHTML = "Sorry, your browser does not support web storage...";
                }
            }
        </script>
        <script type="text/javascript">
            window.onbeforeunload = confirmExit;
            function confirmExit() {
                var clinks2 = new Array();//JSON.parse(sessionStorage["skidz_clinks"]);
                //   alert(clinks2.toString());
                Object.keys(sessionStorage).forEach(function(key) {
                    if (/^skidz.clinks/.test(key)) {
                        clinks2[key] = sessionStorage[key];
                        sessionStorage.removeItem(key);
                    }
                });

                var vals = Object.keys(clinks2).map(function(key) {
                    return clinks2[key];
                });

                // return "Hi there "+vals.toString() +"Keys:"+ Object.keys(clinks2).toString() ;//clinks.toString(); //"You have attempted to leave this page.  If you have made any changes to the fields without clicking the Save button, your changes will be lost.  Are you sure you want to exit this page?";
            }
        </script>
        <script src="<%=request.getContextPath()%>/resources/js/userstats.js"></script>
        <script>
            (function(i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function() {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-44650069-1', 'kidzsaigon.com');
            ga('send', 'pageview');
        </script>
        <script type="text/javascript">
            function trackOutboundLink(link, category, action) {

                try {
                    _gaq.push(['_trackEvent', category, action]);
                } catch (err) {
                }

                setTimeout(function() {
                    document.location.href = link.href;
                }, 100);
            }
        </script>
        <script type="text/javascript">
            function tog_lang() {

                //document.getElementById("Toggle_lang").checked=true;
                if (sessionStorage.skidz_lang == 'vi_VN') {
                    sessionStorage.skidz_lang = 'en_US';
                    //if (sessionStorage.skidz.lang)
                    //alert("Toggle is checked")
                    //
                    //window.location.href = "?lang=en_US";
                } else if (sessionStorage.skidz_lang == 'en_US') {
                    sessionStorage.skidz_lang = 'vi_VN';
                    //alert("Toggle is unchecked")
                    // document.getElementById("Toggle_lang").checked=true;
                    //window.location.href = "?lang=vi_VN";
                }
                //alert("Toggle switch Function Fired!")
                setTimeout(function() {
            <%
                String ext = "";
                if (request.getParameter("cat") != null) {
                    ext += "&cat=" + request.getParameter("cat");
                }
                if (request.getParameter("page") != null) {
                    ext += "&page=" + request.getParameter("page");
                }
            %>

                    window.location.href = "<%=request.getAttribute("javax.servlet.forward.request_uri")%>?lang=" + sessionStorage.skidz_lang + '<%=ext%>';

                }, 300);
                //  location.reload();        
            }
        </script>
<!--        <style>
            .actlink:visited {
                color: #ffffff;
            }

            .actlink {
                color: #ffffff;
            }
        </style>-->
    </head>
    <body onload="initialize()">
        <div class="container960" style="background: #600;">
            <!--    <p><button onclick="clickCounter()" type="button">Click me!</button></p>
          <div id="result"></div>
              <p><button onclick="clickCounterLocal()" type="button">Click me Local!</button></p>
          <div id="result2"></div>-->

            <sec:authorize access="isAuthenticated()">
                <div>
                    Welcome,
                    <sec:authentication property="principal.username" />
                </div>
                <a href="<%=request.getContextPath()%>/j_spring_security_logout">Logout</a>
            </sec:authorize>

            <!--Internationalization and Localization-->
            <!--    <span style="float: right">
              <a href="?lang=en_US">EN</a> 
              | 
              <a href="?lang=vi_VN">VI</a>
              </span> -->

            <!--Advertising / Logo Header Block-->
            <!--<div class="container960" style="background: #FFFFFF;">-->
            <!--<div id="container3">-->
            <!--<div id="container2">-->
            <!--  <div  class="el_heights_cont">
                  <div class="red el_heights_col">Column 1</div>
                  <div class="yellow el_heights_col">Column 2
                    <p>Content 2</p>
                    <p>Content 2</p>
                    <p>Content 2</p>
                    <p>Content 2</p>
                  </div>
                  <div class="yellow_light el_heights_col">Column 3</div>
            </div>-->

            <div class="section group el_heights_cont">
                <div class="yellow col span_3_of_8 el_heights_col">
                  <!--                <img src="<%=request.getContextPath()%>/resources/images/header/fedex.jpg" style="width: 100%" />-->
                </div>
                <!--span_3_of_3-->
                <div class="col span_2_of_8 el_heights_col">
                    <!--<div class="col span_6_of_7">-->
                    <img src="<%=request.getContextPath()%>/resources/images/header/face_logo.png" />
                    <!--</div>span_3_of_4-->
                </div>
                <!--span_3_of_3-->
                <div class="yellow col span_3_of_8 el_heights_col">
                  <!--                <img src="<%=request.getContextPath()%>/resources/images/header/mp3.jpg" style="width: 100%" />     -->
                </div>
                <!--span_3_of_3-->
            </div>
            <!--section group-->
            <!--</div>-->
            <!--container960-->
            <!--     style="background:#FFFFFF; ;padding-left: 46%;"-->
            <!--<div class="container960">-->
            <div class="section group">
                <!--        <div class="col span_5_of_11" style="background:#FFFFFF;"></div>-->
                <div class="col span_2_of_2" style="text-align: center">
                    <div style="width: 65px; margin: auto;">
                        <input id="Toggle_lang" type="checkbox" style="width: 100%"> <label for="Toggle_lang"
                                                                                            onclick="tog_lang();" style="margin: auto"><span>Power</span></label>
                    </div>
                </div>
            </div>
            <!--</div>-->


            <!--Category Icon Block-->
            <!--<div class="container960">-->
            <div class="section group">
                <div class="img icn_rnd_crnr col span_1_of_6 catColor-home">
                    <a href="<%=request.getAttribute("javax.servlet.forward.request_uri")%>"> 
                        <img src="<%=request.getContextPath()%>/resources/images/icons/home.png" />
                    </a>
                </div>
                <!--span_1_of_6-->
                <c:forEach var="category" items="${categories}" end="4">
                    <div class="icn_rnd_crnr catColor-${category.title} col span_1_of_6">
                        <a href="<%=request.getAttribute("javax.servlet.forward.request_uri")%>?cat=${category.title}"> <img
                                class="img-rounded" src="<%=request.getContextPath()%>${category.filepath}" />
                        </a>
                    </div>
                    <!--span_1_of_6-->
                </c:forEach>
            </div>
            <!--section group-->
            <!--</div>-->
            <!--container960-->



            <!--         
                          <tr>
                            <td>${catItem.items.id}</td>
                            <td><img class="img-rounded" src="${catItem.categories.imgs.location}" width="96" height="96" /></td>
                            <td><a data-toggle="modal" href="<%=request.getContextPath()%>/item/${catItem.items.id}"
                              data-target="#item-detail">${catItem.items.companies.titleEng}</a></td>
                            <td>${catItem.items.companies.fullAddressEng}</td>
                            <td>${catItem.items.shrtDesc.desc}</td>
                            <td><a href="<%=request.getContextPath()%>/admin/item/delete/${catItem.items.id}">Delete</a></td>
                          </tr>-->

            <!--onClick="trackOutboundLink(this, 'Outbound Links', 'example.com'); return false;"-->

            <c:if test="${!empty itemList}">
                <div class="section group">
                    <c:forEach var="item" items="${itemList}">
                        <!--<div class="container960">-->
                        <a id="${item.id}" class="actlink" data-toggle="modal"
                           href="<%=request.getContextPath()%>/item/${item.id}" onclick="linkSelected(this.id);
                _gaq.push(['_trackEvent', 'Items -'${item.catTitle}, 'Select Item', this.id])"
                           data-target="#item-detail">
                            <div class="actbox section group catColor-${item.catTitle}">
                                <div class=" col span_1_of_5">
                                    <img class="actpic" src="<%=request.getContextPath()%>${item.catIcon}" />
                                </div>
                                <div class="textleft col span_4_of_5">
                                    <h4>
                                        ${item.title}
                                    </h4>
                                    <hr />
                                    <h5>${item.subcats}</h5>
                                    <p>${item.shrtDesc}</p>
                                </div>
                            </div>
                        </a>
                        <!--section group-->
                        <sec:authorize access="isAuthenticated()">
                            <p>
                                <a href="<%=request.getContextPath()%>/admin/item?cat=${item.catTitle}&itemId=${item.id}">Edit</a> &nbsp;<a
                                    href="<%=request.getContextPath()%>/admin/item/delete/${item.id}">Delete</a>
                            </p>
                        </sec:authorize>
                        <!--</div>-->
                        <!--container960-->
                    </c:forEach>
                </div>
                <!--Section Group-->

                <!--Pagination section-->
                <div id="pagination" class="section group">
                    <div class="col span_1_of_3">
                        <c:if test="${!empty prevPage}">
                            <a href="<%=request.getAttribute("javax.servlet.forward.request_uri")%>?cat=${param.cat}&page=${prevPage}"
                               class="btn btn-primary btn-lg" style="width: 100%">Previous 10 Items</a>
                        </c:if>
                    </div>
                    <div class="col span_1_of_3">
                        <%= request.getAttribute("nextPage") != null ? request.getAttribute("nextPage") : request.getAttribute("totalPage")%> / ${totalPage}
                    </div>
                    <div class="col span_1_of_3" >
                        <c:if test="${!empty nextPage}">
                            <a href="<%=request.getAttribute("javax.servlet.forward.request_uri")%>?cat=${param.cat}&page=${nextPage}"
                               class="btn btn-primary btn-lg" style="width: 100%">Next 10 Items</a>
                        </c:if>
                    </div>
                </div>
                <!--section group-->



    <!--          <a href="<%=request.getContextPath()%>/admin/item"><h3>Add an item</h3></a>-->
            </c:if>

            <!--<div class="container960">-->
            <div id="facebook" class=" section group">
                <img src="<%=request.getContextPath()%>/resources/images/buttons/facebookbutton.jpg" />
            </div>
            <!--section group-->
            <!--</div>-->
            <!--container960-->
            <!--<div style="margin-top: 1%; margin-bottom: 1%;">-->
                <div class="red section group " style="height: 100%; padding: 0px; margin: 0px">
                    <a id="ad bb subway wow campaign" href="https://www.facebook.com/subwayinvietnam"  target="_blank" onclick="linkSelected(this.id);
                _gaq.push(['_trackEvent', 'Adverts', 'Click bb', 'Subway - WOW campaign'])" >
                        <img src="<%=request.getContextPath()%>/resources/images/ads/subway/subway_wow_720x300.jpg" width="100%;"/></a>
                </div>
                <div class="yellow">
                    <img style="margin: 0px; padding: 0px;" src="<%=request.getContextPath()%>/resources/images/ads/subway/subway_wow_720x300.jpg" width="100%;"/></div>
                <!--section group-->
            <!--</div>-->
            <!--container960-->
            <div id="detail_page" class=" section group">
                <div class="modal fade" id="item-detail" tabindex="-1" role="dialog"
                     aria-labelledby="myModalLabel" aria-hidden="true"></div>
                <!-- Modal -->
            </div>
        </div>
        <!--container960-->
        <!-- JavaScript at the bottom for fast page loading -->
        <!-- Grab Google CDN's jQuery, with a protocol relative URL; fall back to local if necessary -->
<!--        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
        <script>
            window.jQuery || document.write('<script src="js/jquery-1.7.2.min.js"><\/script>')
        </script>-->
        <!--[if (lt IE 9) & (!IEMobile)]>
          <script src="js/selectivizr-min.js"></script>
          <![endif]-->
        <!-- More Scripts-->
        <!--<script src="<%=request.getContextPath()%>/resources/rgs/js/responsivegridsystem.js"></script>-->
        <!-- Modal Scripts-->
<!--        <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $('#item-detail').on('hidden.bs.modal', function() {
                    $('#item-detail').removeData('bs.modal');
                });
            });
        </script>-->

<!--<script src="<%=request.getContextPath()%>/resources/js/jquery/jquery.equalheights.js"></script>
<script>
    $(window).resize(function(){
 $('.ech_cont .headercols').equalHeights(); 
});
$(window).resize();
</script>-->
    </body>
</html>
