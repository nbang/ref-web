<%@ include file="taglibs.jsp"%>
<c:set var="contextPath" value="<%=request.getContextPath()%>"></c:set>
<c:set var="isUpdate" value="false"></c:set>
<c:if test="${actionMode eq 'update'}">
  <c:set var="isUpdate" value="true"></c:set>
</c:if>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all" />
<link href="${contextPath}/resources/css/item_admin.css" rel="stylesheet" type="text/css" media="all" />
<title>Admin Item</title>
</head>
<body>
  <div>
    Welcome,
    <sec:authentication property="principal.username" />
  </div>
  <a href="${contextPath}/j_spring_security_logout">Logout</a>
  <div class="container">
    <h3><a href="${contextPath}/item"><spring:message code="label.menu.admin.website"/></a>
   | <!--<a href="${contextPath}/admin/item">--><spring:message code="label.menu.admin.new"/><!--</a>-->
         | <a href="${contextPath}/admin/edit"><spring:message code="label.menu.admin.edit"/></a>
   | <!--<a href="${contextPath}/admin/banner/list">--><spring:message code="label.menu.admin.banners"/><!--</a>-->
<!--   | <a href="${contextPath}/admin/banner">Add banner</a>-->
   | <a href="${contextPath}/admin/list/update"><spring:message code="label.menu.admin.edit_items"/></a>
   | <a href="${contextPath}/admin/company/edit"><spring:message code="label.menu.admin.edit_companies"/></a>
  </h3>
    <div class="row">
      <c:forEach var="message" items="${messages}">
        <div id="display-${message.key}" class="display-${message.key}"><spring:message code="${message.value}" /></div>
      </c:forEach>
      <form:form role="form" method="POST" enctype="multipart/form-data" modelAttribute="itemModelView" action="${contextPath}/admin/item/${actionMode}">
        <h3 class="modal-header">
        <c:if test="${isUpdate == false}">
          <spring:message code="label.title.item.create"></spring:message>
        </c:if>
        <c:if test="${isUpdate == true}">
          <spring:message code="label.title.item.edit"></spring:message>
        </c:if>
        </h3>
        <form:hidden path="itemId" />
        <c:set var="itemId" value="${itemModelView.itemId}"></c:set>
        <div class="form-group">
          <div class="row">
            <div class="col-lg-12"><form:errors path="locale1" cssStyle="color: red;"></form:errors></div>
          </div>
          <div class="row">
            <div class="col-lg-6">
              <c:set value="${itemModelView.locale1}" var="tmpLocale1"></c:set>
              <form:select cssClass="form-control" path="locale1">
                <c:forEach items="${locales}" var="locale">
                  <c:choose>
                    <c:when test="${locale.code eq tmpLocale1}">
                      <form:option value="${locale.code}" label="${locale.local_name}"></form:option>
                    </c:when>
                    <c:otherwise>
                      <form:option value="${locale.code}" label="${locale.local_name}" disabled="true"></form:option>
                    </c:otherwise>
                  </c:choose>
                </c:forEach>
              </form:select>
            </div>
            <div class="col-lg-6">
              <%-- <form:select cssClass="form-control" path="locale2" id="locale2" items="${locales}" itemValue="code" itemLabel="local_name" disabled="true"></form:select> --%>
              <c:set value="${itemModelView.locale2}" var="tmpLocale2"></c:set>
              <form:select cssClass="form-control" path="locale2">
                <c:forEach items="${locales}" var="locale">
                  <c:choose>
                    <c:when test="${locale.code eq tmpLocale2}">
                      <form:option value="${locale.code}" label="${locale.local_name}"></form:option>
                    </c:when>
                    <c:otherwise>
                      <form:option value="${locale.code}" label="${locale.local_name}" disabled="true"></form:option>
                    </c:otherwise>
                  </c:choose>
                </c:forEach>
              </form:select>
            </div>
          </div>
        </div>
        <div class="form-group">
          <div class="row">
            <div class="col-lg-6">
              <label for="company1"><spring:message code="label.company" /></label>
              <a class="btn btn-info btn-xs" data-toggle="modal" href="${contextPath}/admin/company" data-target="#company-info">
                <span class="glyphicon glyphicon-plus-sign" title="Add new company information"></span>
              </a>
              &nbsp;<form:errors path="company1" cssStyle="color: red;"></form:errors>
              <form:select cssClass="form-control company-chosen-select" path="company1" id="company1">
                <c:if test="${!empty companies1}">
                <c:forEach items="${companies1}" var="comp">
                  <form:option value="${comp.title}" label="${comp.title}"></form:option>
                </c:forEach>
                </c:if>
              </form:select>
            </div>
            <div class="col-lg-6">
              <label for="company2">&nbsp;</label>
              &nbsp;<form:errors path="company2" cssStyle="color: red;"></form:errors>
              <form:select cssClass="form-control company-chosen-select" path="company2" id="company2">
                <c:if test="${!empty companies2}">
                <c:forEach items="${companies2}" var="comp">
                  <form:option value="${comp.title}" label="${comp.title}"></form:option>
                </c:forEach>
                </c:if>
              </form:select>
            </div>
          </div>
        </div>
        <div class="form-group">
          <div class="row">
            <div class="col-lg-4">
              <label for="category"><spring:message code="label.category" /></label>&nbsp;<form:errors path="category" cssStyle="color: red;"></form:errors>
              <form:select cssClass="form-control category" id="category" path="category" multiple="true" size="10">
                <c:if test="${!empty categories}">
                <c:forEach items="${categories}" var="cat">
                  <form:option value="${cat.title}" label="${cat.title}" title="${cat.catdesc}"></form:option>
                </c:forEach>
                </c:if>
              </form:select>
            </div>
            <div class="col-lg-4">
              <label for="subcategory1"><spring:message code="label.subcategory" /></label>
              <a class="btn btn-info btn-xs" id="btn-add-subcat" data-toggle="modal" data-target="#add-subcat">
                <span class="glyphicon glyphicon-plus-sign" title="Add new subcategory"></span>
              </a>
              <form:select cssClass="form-control" id="subcategory1" path="subCategory1" multiple="true" size="10">
                <c:if test="${!empty subCategories1}">
                <c:forEach items="${subCategories1}" var="subcat">
                  <form:option value="${subcat.subname}" label="${subcat.subname}"></form:option>
                </c:forEach>
                </c:if>
              </form:select>
            </div>
            <div class="col-lg-4">
              <label for="subcategory2">&nbsp;</label>
              <form:select cssClass="form-control" id="subcategory2" path="subCategory2" multiple="true" size="10">
                <c:if test="${!empty subCategories2}">
                <c:forEach items="${subCategories2}" var="subcat">
                  <form:option value="${subcat.subname}" label="${subcat.subname}"></form:option>
                </c:forEach>
                </c:if>
              </form:select>
            </div>
          </div>
        </div>
        <div class="form-group">
          <div class="row">
            <div class="col-lg-2">
              <label for="cost"><spring:message code="label.cost" /></label>&nbsp;
              <form:select cssClass="form-control" path="cost" id="cost">
                <c:forEach items="${costList}" var="cost">
                  <form:option value="${cost.key}" label="${cost.value}"></form:option>
                </c:forEach>
              </form:select>
            </div>
          </div>
        </div>
        <div class="form-group">
          <div class="row">
            <div class="col-lg-2">
              <label for="weight-eateries"><spring:message code="label.weight.eateries" /></label><br><form:errors path="weight" cssStyle="color: red;"></form:errors>
              <form:input cssClass="form-control weight" path="weight" id="weight-eateries" placeholder="Weight" disabled="true"></form:input>
            </div>
            <div class="col-lg-2">
              <label for="weight-shopping"><spring:message code="label.weight.shopping" /></label><br><form:errors path="weight" cssStyle="color: red;"></form:errors>
              <form:input cssClass="form-control weight" path="weight" id="weight-shopping" placeholder="Weight" disabled="true"></form:input>
            </div>
            <div class="col-lg-2">
              <label for="weight-activities"><spring:message code="label.weight.activities" /></label><br><form:errors path="weight" cssStyle="color: red;"></form:errors>
              <form:input cssClass="form-control weight" path="weight" id="weight-activities" placeholder="Weight" disabled="true"></form:input>
            </div>
            <div class="col-lg-2">
              <label for="weight-health"><spring:message code="label.weight.health" /></label><br><form:errors path="weight" cssStyle="color: red;"></form:errors>
              <form:input cssClass="form-control weight" path="weight" id="weight-health" placeholder="Weight" disabled="true"></form:input>
            </div>
            <div class="col-lg-2">
              <label for="weight-learning"><spring:message code="label.weight.learning" /></label><br><form:errors path="weight" cssStyle="color: red;"></form:errors>
              <form:input cssClass="form-control weight" path="weight" id="weight-learning" placeholder="Weight" disabled="true"></form:input>
            </div>
          </div>
        </div>
        <div class="form-group">
          <div class="row">
            <div class="col-lg-6">
              <label for="shortDesc1"><spring:message code="label.short.description" /></label>&nbsp;<form:errors path="shortDesc1" cssStyle="color: red;"></form:errors>
              <textarea class="form-control" name="shortDesc1" id="shortDesc1" placeholder='<spring:message code="label.short.description" />' rows="3" maxlength="250">${itemModelView.shortDesc1}</textarea>
            </div>
            <div class="col-lg-6">
              <label for="shortDesc2">&nbsp;</label>&nbsp;<form:errors path="shortDesc2" cssStyle="color: red;"></form:errors>
              <textarea class="form-control" name="shortDesc2" id="shortDesc2" placeholder='<spring:message code="label.short.description" />' rows="3" maxlength="250">${itemModelView.shortDesc2}</textarea>
            </div>
          </div>
        </div>
        <div class="form-group">
          <div class="row">
            <div class="col-lg-6">
              <label for="longDesc2"><spring:message code="label.long.description" /></label>
              <textarea class="form-control" name="longDesc1" id="longDesc1" placeholder='<spring:message code="label.long.description" />' rows="5" maxlength="2000">${itemModelView.longDesc1}</textarea>
            </div>
            <div class="col-lg-6">
              <label for="longDesc1">&nbsp;</label>
              <textarea class="form-control" name="longDesc2" id="longDesc2" placeholder='<spring:message code="label.long.description" />' rows="5" maxlength="2000">${itemModelView.longDesc2}</textarea>
            </div>
          </div>
        </div>
        <div class="form-group">
          <label for="file"><spring:message code="label.image.location" /></label>
          <div class="input-group">
            <span class="input-group-addon"><input type="radio" name="uploadType" id="uploadType0" value="0"></span>
            <input class="form-control" type="file" name="file" id="file" value="${itemModelView.file}" placeholder='<spring:message code="label.image.location" />' maxlength="500" accept="image/*"/>
          </div>
        </div>
        <div class="form-group">
          <div class="input-group">
            <span class="input-group-addon"><input type="radio" name="uploadType" id="uploadType1" value="1"></span>
            <input class="form-control" name="image" id="image" value="${itemModelView.image}" placeholder='<spring:message code="label.image.location" />' maxlength="500"></input>
          </div>
        </div>
        <!-- /input-group -->
        <div class="form_save-control modal-footer">
          <div>
            <input type="submit" value='<spring:message code="label.save"/>' class="btn btn-primary" />
          </div>
        </div>
      </form:form>
    </div>
  </div>
  <!-- Modal -->
  <div class="modal fade" id="add-subcat" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close"data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="myModalLabel">Add Subcategory</h4>
        </div>
        <div class="modal-body">
           <div class="row form-group">
               <label for="addSub1">Subtitle - Language 1</label>
             <input type="text" class="form-control" id="addSub1" class="col-lg-4" placeholder="Primary Name"/>
           </div>     
           <div class="row form-group">
             <label for="addSub2">subtitle - Language 2</label>
             <input type="text" class="form-control" id="addSub2" class="col-lg-4" placeholder="Secondary Name"/>
           </div>     
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" id="add-subcat-btn">Save changes</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- Modal -->
  <div class="modal fade" id="company-info" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static"></div>

<!-- Script -->    
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script src="${contextPath}/resources/js/bootbox.min.js"></script>

<script type="text/javascript">
  // Sub-category
  var subcatActionMode = "add";
  var oldSubcat1 = "";
  var oldSubcat2 = "";
  
  var initCategory = '${itemModelView.category}'.toLowerCase();
  var initCategoris = initCategory.split(',', 5);
  var initWeight = '${itemModelView.weight}';
  var initWeights = initWeight.split(',');
  for (var i = 0; i < initCategoris.length; i++) {
    $('#weight-' + initCategoris[i]).prop('disabled', false);
    $('#weight-' + initCategoris[i]).val(initWeights[i]);
  }
</script>

<script type="text/javascript">
  function syncScrolling(parent, child) {
    $(parent).scroll(function () {
        $(child).scrollTop($(parent).scrollTop());
    });
  }
  function updateSubCat(cat, locale1, locale2) {
    $.post("${pageContext.request.contextPath}/ajax/admin/subcats",
    {
      "cat" : cat,
      "primLang" : locale1,
      "secLang" : locale2,
    },
    function(data) {
      var $subcategory1 = $("#subcategory1").empty();
      var $subcategory2 = $("#subcategory2").empty();
      $(data[0]).each(function() {
        var newOption = '<option value="' + this + '">' + this + '</option>';
        $subcategory1.append(newOption);
      });
      $(data[1]).each(function() {
        var newOption = '<option value="' + this + '">' + this + '</option>';
        $subcategory2.append(newOption);
      });
      // Scroll to top
      $('#subcategory1').scrollTop(0);
      $('#subcategory2').scrollTop(0);
    });
  }

  function updateItemByLocale(itemId, locale, orderNo) {
    // Load company
    $.post("${pageContext.request.contextPath}/ajax/admin/companies",
    {
      "localLang" : locale
    },
    function(data) {
      var $company = $("#company" + orderNo).empty();
        $(data).each(function() {
          var newOption = '<option value="' + this + '">' + this + '</option>';
          $company.append(newOption);
        });
    });
    // Load description
    <c:if test="${isUpdate == true}">
      $.post("${pageContext.request.contextPath}/ajax/admin/desc",
      {
        "itemId" : itemId,
        "localLang" : locale
      },
      function(data) {
        var $shortDesc = $("#shortDesc" + orderNo).empty();
        var $longDesc = $("#longDesc" + orderNo).empty();
        $shortDesc.append(data[0]);
        $longDesc.append(data[1]);
      });
    </c:if>
  }
  var categories = new Array("eateries", "shopping", "activities", "health", "learning");
  function setWeightInUnableStatus(status) {
    for (var i = 0; i < categories.length; i++) {
      $('#weight-' + categories[i]).prop('disabled', status);
    }
  }
  function updateWeightStatus(category, status) {
    $('#weight-'+ category).prop('disabled', status);
    for (var i = 0; i < categories.length; i++) {
      //var isDisable = $('#weight-' + categories[i]).prop('disabled');
      //if (isDisable) {
      //  $('#weight-' + categories[i]).val('');
      //}
    }
  }
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $("#file").attr("disabled","disabled");
    $("#image").attr("disabled","disabled");
    // Category onChange
    $(".category").change(function() {
      setWeightInUnableStatus(true);
      var select = $(this);
      var cat = "";
      $('option:selected', select).each(function(index, value) {
        if (index > 0) cat += "|";
        cat += $(value).val();
        // Update weight of category
        var category = $(value).val().toLowerCase();
        updateWeightStatus(category, false);
      });
      updateSubCat(cat, $("#locale1").val(), $("#locale2").val());
    });
    // Subcategory scrolling
    syncScrolling("#subcategory1", "#subcategory2");
    syncScrolling("#subcategory2", "#subcategory1");
    // Locale1 onChange
    $("#locale1").change(function() {
      var itemId = "${itemId}";
      var select = $(this);
      var locale = $('option:selected', select).val();
      updateItemByLocale(itemId, locale, 1);
    });
    // Locale2 onChange
    $("#locale2").change(function() {
      var itemId = "${itemId}";
      var select = $(this);
      var locale = $('option:selected', select).val();
      updateItemByLocale(itemId, locale, 2);
    });
    // Update company 1
    $('#company1').change(function() {
      var index = $(this).find('option:selected').index();
      $('#company2 option').eq(index).prop('selected', true);
    });
    // Update company 2
    $('#company2').change(function() {
      var index = $(this).find('option:selected').index();
      $('#company1 option').eq(index).prop('selected', true);
    });
    // Update sub category 1
    $('#subcategory1').change(function() {
      $("#subcategory2 option:selected").removeAttr("selected");
      $('#subcategory1 option:selected').each(function() {
        var index = $(this).index();
        $('#subcategory2 option').eq(index).prop('selected', true);
      });
    });
    // Update sub category 2
    $('#subcategory2').change(function() {
      $("#subcategory1 option:selected").removeAttr("selected");
      $('#subcategory2 option:selected').each(function() {
        var index = $(this).index();
        $('#subcategory1 option').eq(index).prop('selected', true);
      });
    });
    // Update upload type
    $('#uploadType0').change(function() {
      if ($('#uploadType0').val() == 0) {
        $("#file").removeAttr("disabled"); 
        $("#image").attr("disabled","disabled");
      }
    });
    $('#uploadType1').change(function() {
      if ($('#uploadType1').val() == 1) {
        $("#image").removeAttr("disabled"); 
        $("#file").attr("disabled","disabled");
      }
    });
    $('#company-info').on('hidden.bs.modal', function() {
      $('#company-info').removeData('bs.modal');
    });

    //add new subcat
    $('#add-subcat-btn').click(function() {
      $('#addSub1').removeClass('error');
      $('#addSub2').removeClass('error');
      var cat_titles = $('#category').val();
      var subCat1 = $('#addSub1').val();
      var subCat2 = $('#addSub2').val();
  
      if (cat_titles == null) {
        //alert("Please select category");
        bootbox.alert("Please select the category.", function() {
          // todo
         });
        return true;
      }
      var hasError = false;
      if (subCat1 == '') {
        $('#addSub1').addClass('error');
        hasError = true;
      }
      if (subCat2 == '') {
        $('#addSub2').addClass('error');
        hasError = true;
      }
      if (hasError == true) {
        return;
      }
      var cats = "";
      for (var index = 0; index < cat_titles.length; index++) {
        if (index > 0) cats += "|";
        cats += cat_titles[index];
      }
      $.post("${pageContext.request.contextPath}/ajax/admin/addsubcat",
      {
        "cats" : cats,
        "lang1" : $('#locale1').val(),
        "subCat1" :  subCat1,
        "lang2" : $('#locale2').val(),
        "subCat2" :  subCat2,
        "actionMod" : subcatActionMode,
        "oldSubCat1" : oldSubcat1,
        "oldSubCat2" : oldSubcat2
      }, function (response) {
        updateSubCat(cats, $('#locale1').val(), $('#locale2').val());
        $('#add-subcat').modal("hide");
        $('#addSub1').val("");
        $('#addSub2').val("");
        subcatActionMode = "add";
        oldSubcat1 = "";
        oldSubcat2 = "";
      });
    });

    $('#btn-add-subcat').click(function() {
      $('#addSub1').val("");
      $('#addSub2').val("");
      subcatActionMode = "add";
      oldSubcat1 = "";
      oldSubcat2 = "";
    });
    $('#subcategory1').dblclick(function() {
      var sub_cat_title = $('#subcategory1').val();
      if (sub_cat_title == null) {
        return;
      }
      $('#add-subcat').modal("show");
      subcatActionMode = "edit";
      oldSubcat1 = $('#subcategory1').val().toString();
      oldSubcat2 = $('#subcategory2').val().toString();
      $('#addSub1').val($('#subcategory1').val().toString());
      $('#addSub2').val($('#subcategory2').val().toString());
    });
  });
</script>
</body>
</html>