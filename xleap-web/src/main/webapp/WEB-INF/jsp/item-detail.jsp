<%@ include file="taglibs.jsp"%>
<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!-- <!DOCTYPE html> -->
<!-- <html xmlns="http://www.w3.org/1999/xhtml"> -->
<!-- <head> -->
<!-- <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> -->
<!-- <title>Untitled Document</title> -->
<!-- <!--</head> -->
<!-- <body>--> 
<!--<div id="tdiv" style="width: 100px; height: 100px; background: f00; margin: auto;">
        </div>-->

<!--    <div class=" group ">
        
    </div>-->


<div id="popupbox" class="font_blackish">
    <div class="section group ">
        <div class="col span_1_of_8"></div>
        <div class="col span_6_of_8 popupbgrnd">
    <div id="box1" class="section group ">
        <div id="imagebox" class="col span_2_of_8">
            <c:choose>
                <c:when test="${empty item.itm_img}">
                    <img class="img-rounded" src="<%=request.getContextPath()%>${item.cat_icon}" />
                </c:when>
                <c:otherwise>
                    <img class="img-rounded" src="${item.itm_img}" />
                </c:otherwise>
            </c:choose>
        </div>
        <div id="titlebox" class="col span_5_of_8">
            <div class="section group">
                <div class="col span_3_of_3">
                    <h1>${item.title}</h1>
                    <!--style="padding-right: 30px;"-->
                </div>
            </div>
                    <hr />
            <!--<div id="testbutt" >close</div>-->

            <div class="section group">
                <!--<div class="col span_3_of_3">-->
                <div id="addbox" class="col span_3_of_3">
                    <h2>${item.addr_num} ${item.street} ${item.ward} ${item.district} ${item.city}</h2>
                </div>
            </div>
        </div>
        <div id="crossbox" class="col span_1_of_8" >
            <img src="<%=request.getContextPath()%>/resources/images/closeX1.png" data-dismiss="modal" />
        </div>
    </div>
        <!--end of section-->
    <div class="section group">
        <div id="desctable" class="col span_3_of_3">
           <p>${item.description}</p>
        </div>
    </div>
           <!--end of section-->
    <div class="section group">
        <div class="col span_4_of_5"></div>
        <div class="col span_1_of_5">
          <img src="<%=request.getContextPath()%>/resources/images/moneybags/moneybag${item.cost}.png"
                             style="float: right; margin-right: 10px;" />
        </div>
    </div>
    <!--end of section-->
    <div class="section group">
        <div class="col span_3_of_3 ">
             <a href="${item.url}" target="_blank">Please visit our website</a>
        </div>
    </div>
        </div>
        <div class="col span_1_of_8"></div>
    </div>
               
               
                        <!--<br />
               <div class="cost-${item.cost}"><spring:message code="label.cost"/>: ${item.cost}</div>-->
                        
              
<!--    <div style="text-align: center;">
      
        <img src="images/Visit-Website-Button.png" height="30" />
    </div>-->
</div>
<!--popupbox-->
<!--    </body>
</html>-->



<!--<div class="modal-dialog">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      
<c:choose>
    <c:when test="${empty item.itm_img}">
      <img class="img-rounded" src="${item.cat_icon}" width="96" height="96" />
    </c:when>
    <c:otherwise>
      <img class="img-rounded" src="${item.itm_img}" width="96" height="96" />
    </c:otherwise>
</c:choose>
<h4 class="modal-title">${item.title}</h4>
<div class="cost-${item.cost}"><spring:message code="label.cost"/>: ${item.cost}</div>
</div>
<div class="modal-body">
<div>
<spring:message code="label.address"/>: ${item.addr_num} 
<c:choose>
    <c:when test="${item.lang == 'vi_VN'}">
        <spring:message code="label.street"/> ${item.street}
        <spring:message code="label.ward"/> ${item.ward} 
        <spring:message code="label.district"/> ${item.district} 
        <spring:message code="label.city"/> ${item.city}
    </c:when> 
    <c:otherwise>
        ${item.street} <spring:message code="label.street"/>,
        ${item.ward} <spring:message code="label.ward"/>, 
        <spring:message code="label.district"/> ${item.district}, 
        ${item.city} <spring:message code="label.city"/>
    </c:otherwise>
</c:choose>
</div>
<div><spring:message code="label.long.description"/>: ${item.description}</div>
<div><spring:message code="label.url"/>: ${item.url}</div>
onClick="trackOutboundLink(this, 'Outbound Links', 'example.com'); return false;"
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>
</div>
/.modal-content 
</div>-->
<script>

//    $(document).ready(function() {
//        $('#popupbox').center();
//    });
//               $('#testbutt').click(function() {
//          alert('cross clicked');
//      });
    $('#crossbox').click(function(e) {
        $('#detail_page').fadeOut(100, function() {
            $("#overlay").fadeOut(100);
            $('#detail_page').hide();
        });
    });

//    $('#popupbox').center();
</script>
