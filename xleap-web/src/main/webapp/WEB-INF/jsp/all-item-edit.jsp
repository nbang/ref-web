<%@ include file="taglibs.jsp"%>
<c:set var="contextPath" value="<%=request.getContextPath()%>"></c:set>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="//ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/css/jquery.dataTables.css" rel="stylesheet" type="text/css" media="all" />
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all" />
<link href="${contextPath}/resources/css/chosen/chosen.min.css" rel="stylesheet" type="text/css" media="all" />
<link href="${contextPath}/resources/css/item_admin.css" rel="stylesheet" type="text/css" media="all" />
<title>Admin Item</title>
<style type="text/css">
.error {
	width: 100%;
	/* border: 1px solid #D8D8D8; */
	padding: 5px;
	border-radius: 5px;
	/* font-family: Arial;
	font-size: 11px;
	text-transform: uppercase;
	background-color: rgb(255, 249, 242); */
	color: rgb(211, 0, 0);
}

#display-error {
	width: 400px;
	border: 1px solid #D8D8D8;
	padding: 5px;
	border-radius: 5px;
	/* font-family: Arial;
	font-size: 11px; */
	text-transform: uppercase;
	background-color: rgb(255, 249, 242);
	color: rgb(211, 0, 0);
	text-align: center;
}

#display-error span {
	float: left;
}

#display-success {
	width: 400px;
	border: 1px solid #D8D8D8;
	padding: 10px;
	border-radius: 5px;
	/* font-family: Arial;
	font-size: 11px; */
	text-transform: uppercase;
	background-color: rgb(236, 255, 216);
	color: green;
	text-align: center;
	margin-top: 30px;
}

#display-success img {
	position: relative;
	bottom: 5px;
}

#kidzsaigonTable tbody tr.highlighted { background-color: rgb(236, 255, 216);}
#kidzsaigonTable tbody tr:HOVER { cursor: pointer;}

</style>
</head>
<body>
	<div>
		Welcome,
		<sec:authentication property="principal.username" />
	</div>
	<a href="${contextPath}/j_spring_security_logout">Logout</a>
	<div class="container">
	<h3><a href="${contextPath}/item"><spring:message code="label.menu.admin.website"/></a>
	 | <a href="${contextPath}/admin/item"><spring:message code="label.menu.admin.new"/></a>
         | <a href="${contextPath}/admin/edit"><spring:message code="label.menu.admin.edit"/></a>
	 | <!--<a href="${contextPath}/admin/banner/list">--><spring:message code="label.menu.admin.banners"/><!--</a>-->
<!--	 | <a href="${contextPath}/admin/banner">Add banner</a>-->
	 | <!--<a href="${contextPath}/admin/list/update">--><spring:message code="label.menu.admin.edit_items"/><!--</a>-->
	 | <a href="${contextPath}/admin/company/edit"><spring:message code="label.menu.admin.edit_companies"/></a>
	</h3>
		<div class="row">
			<c:forEach var="message" items="${messages}">
				<div id="display-error"><span class="glyphicon glyphicon-warning-sign"></span><spring:message code="${message.value}" /></div>
			</c:forEach>
			<form:form role="form" method="POST" commandName="itemModelView" action="${contextPath}/admin/item/update?src=all-item-edit" enctype="multipart/form-data">
				<h3 class="modal-header">
					<spring:message code="label.title.item.edit"/>
				</h3>
				<form:hidden path="itemId" />
				<c:set var="itemId" value="${itemModelView.itemId}"></c:set>
				<div class="form-group">
					<div class="row">
						<div class="col-lg-12"><form:errors path="locale1" cssStyle="color: red;"></form:errors></div>
					</div>
					<div class="row">
						<div class="col-lg-6">
							<form:select cssClass="form-control" path="locale1" id="locale1" items="${locales}" itemValue="code" itemLabel="local_name"></form:select>
						</div>
						<div class="col-lg-6">
							<form:select cssClass="form-control" path="locale2" id="locale2" items="${locales}" itemValue="code" itemLabel="local_name"></form:select>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-lg-12">
							<table class="display" id="kidzsaigonTable"></table>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-lg-6">
							<label for="company1"><spring:message code="label.company" /></label>&nbsp;<form:errors path="company1" cssStyle="color: red;"></form:errors>
							<form:select cssClass="form-control" path="company1" id="company1">
								<c:if test="${!empty companies1}">
								<c:forEach items="${companies1}" var="comp">
									<form:option value="${comp.title}" label="${comp.title}"></form:option>
								</c:forEach>
								</c:if>
							</form:select>
						</div>
						<div class="col-lg-6">
							<label for="company2">&nbsp;</label>&nbsp;<form:errors path="company2" cssStyle="color: red;"></form:errors>
							<form:select cssClass="form-control" path="company2" id="company2">
								<c:if test="${!empty companies2}">
								<c:forEach items="${companies2}" var="comp">
									<form:option value="${comp.title}" label="${comp.title}"></form:option>
								</c:forEach>
								</c:if>
							</form:select>
						</div>
					</div>
				</div>
<!-- 				<div class="form-group"> -->
<%-- 					<a class="btn btn-info" data-toggle="modal" href="${contextPath}/admin/company" data-target="#company-info"> --%>
<%-- 						<spring:message code="label.company.add" /> --%>
<!-- 					</a> -->
<!-- 				</div> -->
				<div class="form-group">
					<div class="row">
						<div class="col-lg-4">
							<label for="category"><spring:message code="label.category" /></label>&nbsp;<form:errors path="category" cssStyle="color: red;"></form:errors>
							<form:select cssClass="form-control category" id="category" path="category" multiple="true" size="10">
								<c:if test="${!empty categories}">
								<c:forEach items="${categories}" var="cat">
									<form:option value="${cat.title}" label="${cat.title}" title="${cat.catdesc}"></form:option>
								</c:forEach>
								</c:if>
							</form:select>
						</div>
						<div class="col-lg-4">
							<label for="subcategory1"><spring:message code="label.subcategory" /></label>
              <a class="btn btn-info btn-xs" id="btn-add-subcat" data-toggle="modal" data-target="#add-subcat">
                 <span class="glyphicon glyphicon-plus-sign" title="Add new subcategory"></span>
              </a>
							<form:select cssClass="form-control" id="subcategory1" path="subCategory1" multiple="true" size="10">
								<c:if test="${!empty subCategories1}">
								<c:forEach items="${subCategories1}" var="subcat">
									<form:option value="${subcat.subname}" label="${subcat.subname}"></form:option>
								</c:forEach>
								</c:if>
							</form:select>
						</div>
						<div class="col-lg-4">
							<label for="subcategory2">&nbsp;</label>
							<form:select cssClass="form-control" id="subcategory2" path="subCategory2" multiple="true" size="10">
								<c:if test="${!empty subCategories2}">
								<c:forEach items="${subCategories2}" var="subcat">
									<form:option value="${subcat.subname}" label="${subcat.subname}"></form:option>
								</c:forEach>
								</c:if>
							</form:select>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
                        <div class="col-lg-2">
                          <label for="cost"><spring:message code="label.cost" /></label>&nbsp;<form:errors path="cost" cssStyle="color: red;"></form:errors>
                            <form:select cssClass="form-control" path="cost" id="cost">
                                <option value="9" label="<spring:message code="cost.undefined" />"/>
                                <option value="0" label="<spring:message code="cost.free" />"/>
                                <option value="1" label="<spring:message code="cost.cheap" />"/>
                                <option value="3" label="<spring:message code="cost.medium" />"/>
                                <option value="5" label="<spring:message code="cost.premium" />"/>
                            </form:select>
                        </div>
						<div class="col-lg-2">
							<label for="weight"><spring:message code="label.weight.eateries" /></label>&nbsp;<form:errors path="weight" cssStyle="color: red;"></form:errors>
							<form:input cssClass="form-control weight" path="weight" id="weight-eateries" placeholder="Weight" disabled="true"></form:input>
						</div>
            <div class="col-lg-2">
              <label for="weight"><spring:message code="label.weight.shopping" /></label>&nbsp;<form:errors path="weight" cssStyle="color: red;"></form:errors>
              <form:input cssClass="form-control weight" path="weight" id="weight-shopping" placeholder="Weight" disabled="true"></form:input>
            </div>
            <div class="col-lg-2">
              <label for="weight"><spring:message code="label.weight.activities" /></label>&nbsp;<form:errors path="weight" cssStyle="color: red;"></form:errors>
              <form:input cssClass="form-control weight" path="weight" id="weight-activities" placeholder="Weight" disabled="true"></form:input>
            </div>
            <div class="col-lg-2">
              <label for="weight"><spring:message code="label.weight.health" /></label>&nbsp;<form:errors path="weight" cssStyle="color: red;"></form:errors>
              <form:input cssClass="form-control weight" path="weight" id="weight-health" placeholder="Weight" disabled="true"></form:input>
            </div>
            <div class="col-lg-2">
              <label for="weight"><spring:message code="label.weight.learning" /></label>&nbsp;<form:errors path="weight" cssStyle="color: red;"></form:errors>
              <form:input cssClass="form-control weight" path="weight" id="weight-learning" placeholder="Weight" disabled="true"></form:input>
            </div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-lg-6">
							<label for="shortDesc1"><spring:message code="label.short.description" /></label>&nbsp;<form:errors path="shortDesc1" cssStyle="color: red;"></form:errors>
							<form:textarea cssClass="form-control" path="shortDesc1" id="shortDesc1" placeholder="Short Description" rows="3" maxlength="250"></form:textarea>
						</div>
						<div class="col-lg-6">
							<label for="shortDesc2">&nbsp;</label>&nbsp;<form:errors path="shortDesc2" cssStyle="color: red;"></form:errors>
							<form:textarea cssClass="form-control" path="shortDesc2" id="shortDesc2" placeholder="Short Description" rows="3" maxlength="250"></form:textarea>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-lg-6">
							<label for="longDesc2"><spring:message code="label.long.description" /></label>
							<form:textarea cssClass="form-control" path="longDesc1" id="longDesc1" placeholder="Long Description" rows="5" maxlength="2000"></form:textarea>
						</div>
						<div class="col-lg-6">
							<label for="longDesc1">&nbsp;</label>
							<form:textarea cssClass="form-control" path="longDesc2" id="longDesc2" placeholder="Long Description" rows="5" maxlength="2000"></form:textarea>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="file"><spring:message code="label.image.location"/></label>
					<div class="input-group">
						<span class="input-group-addon"><form:radiobutton path="uploadType" id="uploadType0" value="0"/></span>
			            <form:input class="form-control" type="file" path="file" id="file" placeholder="Image Location" maxlength="500" accept="image/*" disabled="true"/>			
					</div>
				</div>
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon"><form:radiobutton path="uploadType" id="uploadType1" value="1"/></span>
						<form:input cssClass="form-control" path="image" id="image" placeholder="Image Location" maxlength="500" disabled="true"/>
					</div>
				</div>
				<div class="form_save-control modal-footer">
					<div>
						<input type="submit" value='<spring:message code="label.save"/>' class="btn btn-primary" />
					</div>
				</div>
			</form:form>
		</div>
	</div>
	<!-- Modal -->
    <div class="modal fade" id="add-subcat" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close"data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Add Subcategory</h4>
          </div>
          <div class="modal-body">
             <div class="row form-group">
                 <label for="addSub1">Subtitle - Language 1</label>
               <input type="text" class="form-control" id="addSub1" class="col-lg-4" placeholder="Primary Name"/>
             </div>     
             <div class="row form-group">
               <label for="addSub2">subtitle - Language 2</label>
               <input type="text" class="form-control" id="addSub2" class="col-lg-4" placeholder="Secondary Name"/>
             </div>     
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" id="add-subcat-btn">Save changes</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    
<!-- Script -->    
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.3.1/jquery.cookie.min.js"></script>
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script src="${contextPath}/resources/js/chosen/chosen.jquery.min.js"></script>
<script src="//ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/jquery.dataTables.min.js"></script>
<script src="${contextPath}/resources/js/datatables/dataTables.fnReloadAjax.js"></script>
<script src="${contextPath}/resources/js/datatables/dataTables.fnGetSelected.js"></script>
<script type="text/javascript">
// Sub-category
var subcatActionMode = "add";
var oldSubcat1 = "";
var oldSubcat2 = "";

var oTable;

//datatable ajax reload
function updateTable() {
  oTable.fnReloadAjax("${pageContext.request.contextPath}/ajax/admin/all?primLang=" + $("#locale1").val() + "&secLang=" + $("#locale2").val());
}

//update sub category base on category and both language
function updateSubCat(cat, locale1, locale2, subList1, subList2) {
    $.post("${pageContext.request.contextPath}/ajax/admin/subcats",
    {
      "cat" : cat,
      "primLang" : locale1,
      "secLang" : locale2,
    },
    function(data) {
      var $subcategory1 = $("#subcategory1").empty();
      var $subcategory2 = $("#subcategory2").empty();
      $(data[0]).each(function() {
        var newOption = '<option value="' + this + '">' + this + '</option>';
        $subcategory1.append(newOption);
      });
      $(data[1]).each(function() {
        var newOption = '<option value="' + this + '">' + this + '</option>';
        $subcategory2.append(newOption);
      });
      // Scroll to top
      $('#subcategory1').scrollTop(0);
      $("#subcategory1").val(subList1);
      $('#subcategory2').scrollTop(0);
      $("#subcategory2").val(subList2);
    });
}

//update company list and description when change language
function updateItemByLocale(itemId, locale, orderNo) {
    // Load company
    $.post("${pageContext.request.contextPath}/ajax/admin/companies",
    {
      "localLang" : locale
    },
    function(data) {
      var $company = $("#company" + orderNo).empty();
        $(data).each(function() {
          var newOption = '<option value="' + this + '">' + this + '</option>';
          $company.append(newOption);
        });
    });
    // Load description
    $.post("${pageContext.request.contextPath}/ajax/admin/desc",
    {
      "itemId" : itemId,
      "localLang" : locale
    },
    function(data) {
      var $shortDesc = $("#shortDesc" + orderNo).empty();
      var $longDesc = $("#longDesc" + orderNo).empty();
      $shortDesc.append(data[0]);
      $longDesc.append(data[1]);
    });
}

//sync scroll between 2 language sub category
function syncScrolling(parent, child) {
  $(parent).scroll(function () {
     $(child).scrollTop($(parent).scrollTop());
   });
 }

$(document).ready(function() {
  // Subcategory scrolling
  syncScrolling("#subcategory1", "#subcategory2");
  syncScrolling("#subcategory2", "#subcategory1");

  // Get default page length in cookie
  var defaultDisplayPageLength = $.cookie('kidzsaigonItemDisplayLength');
  //load table
  oTable = $('#kidzsaigonTable').dataTable( {
    "fnDrawCallback": function( oSettings ) {
      // Save cookie
      var displayPageLength = oSettings._iDisplayLength;
      $.cookie('kidzsaigonItemDisplayLength', displayPageLength, { expires: 90 });
    },
    "iDisplayLength": defaultDisplayPageLength == null ? 25 : defaultDisplayPageLength,
    "bProcessing": true,
    "bSortClasses": false,
    "aoColumns": [
        {"sTitle": "Item" },
        { "sTitle": 'Company', "sWidth" : '50%' },
        <c:forEach items="${categories}" var="cat">
        { "sTitle": '${cat.title}' },
        </c:forEach>
        { "sTitle": 'SD_1', "sClass": 'center' },
        { "sTitle": 'SD_2', "sClass": 'center' },
        { "sTitle": 'D_1', "sClass": 'center' },
        { "sTitle": 'D_2', "sClass": 'center' },
        { "sTitle": 'Visible', "sClass": 'center display' },
        { "sTitle": 'Reorder', "sClass": 'center' }
    ],
    "sAjaxSource": "${pageContext.request.contextPath}/ajax/admin/all"
  });

  //change visible columen
  $("#kidzsaigonTable td.display").click(function() {
    var _this = this;

    if ($(_this).text() != "") {
      var itemId = $(_this).prevAll(':first').text();

      alert(itemId);
    }
  });
  
  //change item select
  $("#kidzsaigonTable tbody").click(function(event) {
    $(oTable.fnSettings().aoData).each(function (){
      $(this.nTr).removeClass('highlighted');
    });

    $(event.target).parents('tr').addClass('highlighted');
    var id = $(event.target).parents('tr').children(':first').text();

    //update item visible status
    if ($(event.target).parents('td').hasClass('display')) {
      $.get("${contextPath}/admin/item/delete/" + id);
      if ($(event.target).hasClass('glyphicon-ok')) {
        $(event.target).removeClass('glyphicon-ok').addClass('glyphicon-remove');
      } else { 
        $(event.target).removeClass('glyphicon-remove').addClass('glyphicon-ok');
      }
      
      return alert("Update item visible status ok.");
    }

    //increase item weight
    if ($(event.target).hasClass('glyphicon-arrow-up')) {
      var currRow = $(event.target).parents('tr');
      var nextRow = $(currRow).prev();
      var compare = null;

      do {
        compare = compare2row(currRow, nextRow); 
        if (compare == -1) {
          swap2row(currRow, nextRow);
          return alert("Weight's increased");
        }
        nextRow = $(nextRow).prev();
      } while (compare == 0);
        
      return alert('This record is not eligible to increase weight.');
    }
    
    //decrease item weight
    if ($(event.target).hasClass('glyphicon-arrow-down')) {
      var currRow = $(event.target).parents('tr');
      var nextRow = $(currRow).next();
      var compare = null;

      do {
        compare = compare2row(currRow, nextRow); 
        if (compare == 1) {
          swap2row(currRow, nextRow);
          return alert("Weight's decreased");
        }
        nextRow = $(nextRow).next();
      } while (compare == 0);
        
      return alert('This record is not eligible to decrease weight.');
    }

    if (id == $('#itemId').val()) {
	    return;
	}
    $('#itemId').val(id);
    $('.weight').val("");
    $('.weight').prop('disabled', true);
    $.post("${pageContext.request.contextPath}/ajax/admin/getItem",
        { "itemId" : id, "primLang" : $("#locale1").val(), "secLang" : $("#locale2").val()},
        function( data ) {
            var category = data.category.toLowerCase();
            $('#weight-'+ category).val(data.weight);
            $('#weight-'+ category).prop('disabled', false);
            $('#cost').val(data.cost);
            $('#shortDesc1').val(data.shortDesc1);
            $('#shortDesc2').val(data.shortDesc2);
            $('#longDesc1').val(data.longDesc1);
            $('#longDesc2').val(data.longDesc2);
            $('#category').val(data.category);
            $('#company1').val(data.company1);
            $('#company2').val(data.company2);
            updateSubCat(data.category, $("#locale1").val(), $("#locale2").val(), data.subCategory1, data.subCategory2);
        });
      
  });
    
  // Category onChange
  $(".category").change(function() {
    var select = $(this);
    var cat = "";
    
    $('option:selected', select).each(function(index, value) {
        if (index > 0) cat += "|";
        cat += $(value).val();
    });
    updateSubCat(cat, $("#locale1").val(), $("#locale2").val());
  });
  
  // Locale1 onChange
  $("#locale1").change(function() {
    var itemId = "${itemId}";
    var select = $(this);
    var locale = $('option:selected', select).val();

    updateItemByLocale(itemId, locale, 1);
    //reload table data
    setTimeout(updateTable, 50);
  });

  // Locale2 onChange
  $("#locale2").change(function() {
    var itemId = "${itemId}";
    var select = $(this);
    var locale = $('option:selected', select).val();
    updateItemByLocale(itemId, locale, 2);
    //reload table data
    setTimeout(updateTable, 50);
  });
   
  // Update company 1
  $('#company1').change(function() {
    var index = $(this).find('option:selected').index();
    $('#company2 option').eq(index).prop('selected', true);
  });

  // Update company 2
  $('#company2').change(function() {
    var index = $(this).find('option:selected').index();
    $('#company1 option').eq(index).prop('selected', true);
  });

  // Update sub category 1
  $('#subcategory1').change(function() {
  	$("#subcategory2 option:selected").removeAttr("selected");
  	$('#subcategory1 option:selected').each(function() {
  		var index = $(this).index();
  		$('#subcategory2 option').eq(index).prop('selected', true);
  	});
  });

  // Update sub category 2
  $('#subcategory2').change(function() {
  	$("#subcategory1 option:selected").removeAttr("selected");
  	$('#subcategory2 option:selected').each(function() {
  		var index = $(this).index();
  		$('#subcategory1 option').eq(index).prop('selected', true);
  	});
  });

  // Update upload type
  $('#uploadType0').change(function() {
  	if ($('#uploadType0').val() == 0) {
  		$("#file").removeAttr("disabled"); 
  		$("#image").attr("disabled","disabled");
  	}
  });
  
  //Update upload type
  $('#uploadType1').change(function() {
	if ($('#uploadType1').val() == 1) {
		$("#image").removeAttr("disabled"); 
		$("#file").attr("disabled","disabled");
	}
  });

  //reload popup when click on add new subcat
  $('#add-subcat').on('hidden.bs.modal', function() {
    $('#add-subcat').removeData('bs.modal');
    //$('#add-subcat').removeData("modal").modal({backdrop: 'static', keyboard: false})
  });
   
  //add new subcat
  $('#add-subcat-btn').click(function() {
	  $('#addSub1').removeClass('error');
		$('#addSub2').removeClass('error');
		var cat_titles = $('#category').val();
		var subCat1 = $('#addSub1').val();
		var subCat2 = $('#addSub2').val();

	  if (cat_titles == null) {
			alert("Please select category");
			return;
		}
		var hasError = false;
		if (subCat1 == '') {
			$('#addSub1').addClass('error');
			hasError = true;
		}
		if (subCat2 == '') {
			$('#addSub2').addClass('error');
			hasError = true;
		}
		if (hasError == true) {
			return;
		}
		var cats = "";
		for (var index = 0; index < cat_titles.length; index++) {
			if (index > 0) cats += "|";
			cats += cat_titles[index];
		}
		$.post("${pageContext.request.contextPath}/ajax/admin/addsubcat",
		{
			"cats" : cats,
			"lang1" : $('#locale1').val(),
			"subCat1" :  subCat1,
			"lang2" : $('#locale2').val(),
			"subCat2" :  subCat2,
			"actionMod" : subcatActionMode,
			"oldSubCat1" : oldSubcat1,
			"oldSubCat2" : oldSubcat2
		}, function (response) {
			updateSubCat(cats, $('#locale1').val(), $('#locale2').val());
			$('#add-subcat').modal("hide");
			$('#addSub1').val("");
			$('#addSub2').val("");
			subcatActionMode = "add";
			oldSubcat1 = "";
			oldSubcat2 = "";
		});
  });
	$('#btn-add-subcat').click(function() {
		$('#addSub1').val("");
		$('#addSub2').val("");
		subcatActionMode = "add";
		oldSubcat1 = "";
		oldSubcat2 = "";
	});
	$('#subcategory1').dblclick(function() {
		var sub_cat_title = $('#subcategory1').val();
		if (sub_cat_title == null) {
			return;
		}
		$('#add-subcat').modal("show");
		subcatActionMode = "edit";
		oldSubcat1 = $('#subcategory1').val().toString();
		oldSubcat2 = $('#subcategory2').val().toString();
		$('#addSub1').val($('#subcategory1').val().toString());
		$('#addSub2').val($('#subcategory2').val().toString());
	});
});

function swap2row(row1, row2) {

  var cell1 = $(row1).children(":eq(2)");
  var cell2 = $(row2).children(":eq(2)"); 

  for (var i = 0; i < 5; i++) {
    if(cell1.text() != "" && cell2.text() != "") {
      var weight1 = $(cell1).text();
      var weight2 = $(cell2).text();
      $(cell1).text(weight2);
      $(cell2).text(weight1);

      $.post("${pageContext.request.contextPath}/ajax/admin/update_weight",
      {
        "item" : $(row1).children(":first").text(),
        "category" : $('#kidzsaigonTable thead tr').children(":eq(" + (i+2) + ")").text(),
        "weight" : weight2,
      });
      $.post("${pageContext.request.contextPath}/ajax/admin/update_weight",
      {
        "item" : $(row2).children(":first").text(),
        "category" : $('#kidzsaigonTable thead tr').children(":eq(" + (i+2) + ")").text(),
        "weight" : weight1,
      });

      var html = $(row1).html();
      $(row1).html($(row2).html());
      $(row2).html(html);

      return compare2cell(cell1, cell2); 
    } 

    cell1 = $(cell1).next();
    cell2 = $(cell2).next();
  }
}

//row1 > row2 return 1
//row1 < row2 return -1
//row1 = row2 return 0
//cannot compare return -9
function compare2row(row1, row2) {
  if (row1 == null || row2 == null) return -9;

  var cell1 = $(row1).children(":eq(2)");
  var cell2 = $(row2).children(":eq(2)"); 

  for (var i = 0; i < 5; i++) {
    if(cell1.text() != "" && cell2.text() != "") {
      return compare2cell(cell1, cell2); 
    } 

    cell1 = $(cell1).next();
    cell2 = $(cell2).next();
  }

  return -9; 
};

function compare2cell(cell1, cell2) {
  var val1 = parseInt(cell1.text());
  var val2 = parseInt(cell2.text());
  
  if (val1 == val2) return 0;
  if (val1 > val2) return 1;
  if (val1 < val2) return -1;
  return -9;
}
</script>
</body>
</html>