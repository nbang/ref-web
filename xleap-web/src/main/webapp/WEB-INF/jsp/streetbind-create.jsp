<%@ include file="taglibs.jsp" %>
<c:set var="contextPath" value="<%=request.getContextPath()%>"></c:set>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4 class="modal-title"><spring:message code="label.title.streetbind.create" /></h4>
		</div>
		<div class="modal-body">
		  <div id="messages"></div>
			<br>
			<form:form cssClass="form-horizontal" role="form" id="streetbindForm" data-target="#modal-dialog" method="POST" commandName="streetbindModelView" action="${contextPath}/admin/streetbind/add">
			  <form:hidden path="type"/>
				<div class="form-group">
					<label for="name" class="col-lg-4 control-label"><spring:message code="${streetLabel}"/></label>
					<div class="col-lg-8">
					  <input class="form-control" name="name" id="name" placeholder='<spring:message code="${streetLabel}"/>' maxlength="200"></input>
					</div>
				</div>
				<c:if test='${streetbindModelView.type eq "city"}'>
				  <div class="form-group">
          <label for="name" class="col-lg-4 control-label"><spring:message code="label.shortcode"/></label>
          <div class="col-lg-2">
            <form:input cssStyle="text-transform: uppercase;" cssClass="form-control" path="shortcode" id="shortcode" maxlength="3"></form:input>
          </div>
          </div>
				</c:if>
			</form:form>
		</div>
		<div class="modal-footer">
			<button type="button" id="saveStreetbind" class="btn btn-primary"><spring:message code="label.save"/></button>
			<button type="button" class="btn btn-default" data-dismiss="modal"><spring:message code="label.close"/></button>
		</div>
	</div>
	<!-- /.modal-content -->
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$('#messages').hide();

		$("#saveStreetbind").on('click', function() {
			var type = $("#type").val();
			var name = $("#name").val();
			var shortcode = $("#shortcode").val();
			$.post("${pageContext.request.contextPath}/admin/streetbind/ajax/save",
			{
				"type" : type,
				"name" : name,
				"shortcode" : shortcode
			},
			function( response ) {
				if (response.status == "SUCCESS") {
					// Display message
          $('#messages').hide('slow');
          $('#messages').removeClass('display-error');
					$('#messages').html("Save successful.");
					$('#messages').addClass('display-success');
					$('#messages').show('slow');
					// Add new item to city selection
					var optionItem = '<option value="' + name + '" selected="selected">' + name + '</option>';
					switch(type) {
					case 'city':
						$('#city').append(optionItem);
						break;
					case 'district':
						$('#district').append(optionItem);
            break;
					case 'ward':
						$('#ward').append(optionItem);
            break;
					case 'street':
						$('#street').append(optionItem);
            break;
					}
				} else if (response.status == "FAILED") {
          $('#messages').hide('slow');
          $('#messages').removeClass('display-success');
          errorInfo = "Save failed.<br>";
          for(var i = 0 ; i < response.result.length ; i++) {
            errorInfo += (i + 1) + ". " + response.result[i].code + "<br>";
          }
          $('#messages').html(errorInfo);
          $('#messages').addClass('display-error');
          $('#messages').show('slow');
				} else if (response.status == "INPUT_FAILED") {
          $('#messages').hide('slow');
          $('#messages').removeClass('display-success');
					errorInfo = "";
					for(var i = 0 ; i < response.result.length ; i++) {
						errorInfo += (i + 1) + ". " + response.result[i].code + "<br>";
						$('#' + response.result[i].field).addClass('error');
					}
					$('#messages').html(errorInfo);
          $('#messages').addClass('display-error');
          $('#messages').show('slow');
				}
			});
			});
		});
</script>

</body>
</html>