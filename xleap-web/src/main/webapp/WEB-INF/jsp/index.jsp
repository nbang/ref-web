<!DOCTYPE html>
<html lang="en-US">
<head>
<meta charset="UTF-8">
<meta http-equiv="refresh" content="1;url=<%=request.getContextPath()%>/item">
<script type="text/javascript">
	window.location.href = "<%=request.getContextPath()%>/item";
</script>
<title>Page Redirection</title>
</head>
<body>
	If you are not redirected automatically, follow the
	<a href='<%=request.getContextPath()%>/item'>Item list</a>
</body>
</html>