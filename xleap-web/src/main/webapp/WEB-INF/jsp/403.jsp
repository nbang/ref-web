<%@ page language="java" isErrorPage="true" session="false" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<title>Error page</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<body>
  <h1>ERROR PAGE!</h1>
  <p>Sorry, an error occurred. Error code : 403</p>
</body>
</html>