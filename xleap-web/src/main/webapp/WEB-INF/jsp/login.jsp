<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<title>Login</title>
<script>
function loginGmail() {
	 document.getElementById('openid_identifier').value= "https://www.google.com/accounts/o8/id";
	 document.getElementById("frmLogin").submit();
	 return false;
}
function loginYHmail() {
	   document.getElementById('openid_identifier').value= "me.yahoo.com";
	   document.getElementById("frmLogin").submit();
	   return false;
	}
</script>
</head>
<body>

  <h1>Login... Surprise! You may already have an OpenID.</h1>
  <div id="login-error">${error}</div>

  <form id="frmLogin" action='<c:url value="/j_spring_openid_security_check"/>' method="post">
    <input type="hidden"  id="openid_identifier" name="openid_identifier" value="" />
    <!-- 
    <img src="../resources/logos/images/openid.png"></img> <label for="openid_identifier">OpenID Login</label>: <input
        id="openid_identifier" name="openid_identifier" type="text" value="https://www.google.com/accounts/o8/id" size="40"/><input type="submit" value="Login" /> -->
      Sign In With<br><a href="#" onclick="loginGmail();"><img src="../resources/logos/images/gmail-icon-small.jpg" /></a>
      <br>
      Sign In With<br><a href="#" onclick="loginYHmail();"><img src="../resources/logos/images/yhmail-icon-small.png" /></a>
  </form>

</body>
</html>
