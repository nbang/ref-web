package com.xaphe.xleap.enums;

public enum ContactTypesEnum {
	TEL(1),
	FAX(2),
	MOBILE(3),
	EMAIL(4),
	CONTACT(5);

	private int types;

	private ContactTypesEnum(int types) {
		this.types = types;
	}

	/**
	 * @return the types
	 */
	public int getTypes() {
		return types;
	}

	/**
	 * @param types
	 *            the types to set
	 */
	public void setTypes(int types) {
		this.types = types;
	}
}
