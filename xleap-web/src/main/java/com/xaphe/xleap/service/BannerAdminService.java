package com.xaphe.xleap.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xaphe.xleap.dao.localize.LocalizeBannerDao;
import com.xaphe.xleap.dto.LocalizeBannerDto;
import com.xaphe.xleap.dto.PageLocDto;
import com.xaphe.xleap.model.BannerModelView;
import com.xaphe.xleap.utils.StringUtil;

@Service
public class BannerAdminService {
	private static Logger logger = LoggerFactory.getLogger(BannerAdminService.class);
	@Autowired
	private LocalizeBannerDao localizeBannerDao;

	/**
	 * Get list of banner information
	 * @return List<LocalizeBannerDao>
	 */
	public List<LocalizeBannerDto> getBannerList() {
		logger.info("Get list of banner information");
		return localizeBannerDao.getLocalizeBannerList();
	}

	/**
	 * Create banner information
	 * 
	 * @param bannerModelView
	 */
	public void create(final BannerModelView bannerModelView)
			throws Exception {
		logger.info("Create banner info --- BEGIN");
		LocalizeBannerDto localizeBannerDto = getBannerDto(bannerModelView);
		localizeBannerDao.createBanner(localizeBannerDto);
		logger.info("Update banner info --- END");
	}

	/**
	 * Update banner information
	 * 
	 * @param bannerModelView
	 */
	public void update(final BannerModelView bannerModelView)
			throws Exception {
		logger.info("Update banner info --- BEGIN");
		LocalizeBannerDto localizeBannerDto = getBannerDto(bannerModelView);
		localizeBannerDao.updateBanner(localizeBannerDto);
		logger.info("Update banner info --- END");
	}

	/**
	 * Delete banner (set visible = false)
	 * @param id
	 */
	public void delete(int id) {
		logger.info("Delete banner info --- BEGIN");
		localizeBannerDao.deleteBanner(id);
		logger.info("Delete banner info --- END");
	}

	/**
	 * @param bannerModelView
	 * @return
	 */
	private LocalizeBannerDto getBannerDto(final BannerModelView bannerModelView) {
		LocalizeBannerDto localizeBannerDto = new LocalizeBannerDto();
		localizeBannerDto.setName(bannerModelView.getName());
		localizeBannerDto.setPageloc(bannerModelView.getPageLocation());
		localizeBannerDto.setClientname(bannerModelView.getClientName());
		if (StringUtil.hasLength(bannerModelView.getCategory())) {
			localizeBannerDto.setCattitle(bannerModelView.getCategory().replace(',', '|'));
		}
		localizeBannerDto.setImage(bannerModelView.getImage().getOriginalFilename());
		localizeBannerDto.setUrl(bannerModelView.getUrl());
		localizeBannerDto.setWidth(Integer.parseInt(bannerModelView.getWidth()));
		localizeBannerDto.setHeight(Integer.parseInt(bannerModelView.getHeight()));
		localizeBannerDto.setVisible(true);
		localizeBannerDto.setWeight(Integer.parseInt(bannerModelView.getWeight()));
		localizeBannerDto.setStartdate(bannerModelView.getStartDate());
		localizeBannerDto.setEnddate(bannerModelView.getEndDate());
		return localizeBannerDto;
	}

	public List<PageLocDto> getPageLocs() {
		logger.info("Get list of page location");
		return localizeBannerDao.getPageLocs();
	}
}
