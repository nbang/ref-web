package com.xaphe.xleap.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.openid.OpenIDAttribute;
import org.springframework.security.openid.OpenIDAuthenticationToken;
import org.springframework.stereotype.Service;

@Service
public class CustomUserDetailsService implements UserDetailsService,
		AuthenticationUserDetailsService<OpenIDAuthenticationToken> {
	private static final Logger logger = LoggerFactory.getLogger(CustomUserDetailsService.class);

	public UserDetails loadUserByUsername(String arg0) throws UsernameNotFoundException, DataAccessException {

		try {
			logger.debug("Username: {}", arg0);
			return new User(arg0, "", true, true, true, true, getAuthorities(arg0));
		} catch (Exception e) {
			throw new UsernameNotFoundException("Username is invalid");
		}
	}

	private Collection<GrantedAuthority> getAuthorities(String username) {
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();

		// Simple OpenId authentication for google and yahoo email
		if ("wicksvn@gmail.com|dr.3piece@gmail.com|thaonguyen.tran1995@gmail.com|markzazula@yahoo.com|daibang1102@gmail.com|vdlamson3009@gmail.com|phandangquang@gmail.com|vietnamvisacenter@gmail.com|craig.buckie@gmail.com"
				.contains(username)) {
			authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
			authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
//                        String ircNick;
//                        switch(username) {
//                            case "wicksvn@gmail.com": ircNick = "bryan_4k";
//                                break;
//                            case "dr.3piece@gmail.com": ircNick = "james_4k";
//                                break;
//                            case "markzazula@yahoo.com": ircNick = "mark_4k";
//                                break;
//                            case "craig.buckie@gmail.com": ircNick = "buckie_4k";
//                                break;
//                            default: ircNick = "";
//                                break;
//                        }
//                        User user = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//                        
//                        authorities.add(new )
		}

		return authorities;
	}

	@Override
	public UserDetails loadUserDetails(OpenIDAuthenticationToken token) throws UsernameNotFoundException {
		for (OpenIDAttribute attr : token.getAttributes()) {
			logger.debug(attr.getName() + " " + attr.getValues().get(0));

			if (attr.getName().equals("email")) {
				return new User(attr.getValues().get(0), "", true, true, true, true, getAuthorities(attr.getValues()
						.get(0)));
			}
		}

		throw new UsernameNotFoundException("Username is invalid");
	}
}
