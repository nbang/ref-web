package com.xaphe.xleap.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xaphe.xleap.dao.localize.LocalizeSubCategoriesDao;

@Service
@Transactional
public class SubCategoryService {
	private static Logger logger = LoggerFactory.getLogger(SubCategoryService.class);
	@Autowired
	private LocalizeSubCategoriesDao subCategoriesDao;

	/**
	 * Create sub-category
	 * 
	 * @param cat
	 * @param lang1
	 * @param subCat1
	 * @param lang2
	 * @param subCat2
	 * @throws Exception
	 */
	public void createLocalizeSubCategory(
			String cat, String lang1, String lang2, String subCat1, String subCat2) throws Exception {
		logger.info("Create Sub-Category --- BEGIN");
		logger.info("Create Sub-Category {} [{}] --- BEGIN", subCat1, lang1);
		int id = subCategoriesDao.createLocalizeSubCategory(cat, lang1, subCat1);
		logger.info("Create Sub-Category {} [{}] --- END", subCat1, lang1);
		if (id > 0) {
			logger.info("Create Sub-Category {} [{}] --- BEGIN", subCat2, lang2);
			subCategoriesDao.createLocalizeSubCategory(id, cat, lang2, subCat2);
			logger.info("Create Sub-Category {} [{}] --- END", subCat2, lang2);
		}
		logger.info("Create Sub-Category --- END");
	}

	/**
	 * Edit sub category
	 * 
	 * @param cat
	 * @param lang1
	 * @param newSubCat1
	 * @param lang2
	 * @param newSubCat2
	 * @param oldSubCat1
	 * @param oldSubCat2
	 * @throws Exception
	 */
	public void updateLocalizeSubCategory(String cat, String lang1, String lang2, 
			String newSubCat1, String newSubCat2, String oldSubCat1, String oldSubCat2) throws Exception {
		logger.info("Edit Sub-Category --- BEGIN");
		logger.info("Update Sub-Category {} [{}] --- BEGIN", newSubCat1, lang1);
		int id = subCategoriesDao.updateLocalizeSubCategory(cat, lang1, oldSubCat1, newSubCat1);
		logger.info("Update Sub-Category {} [{}] --- END", newSubCat1, lang1);
		if (id > 0) {
			logger.info("Update Sub-Category {} [{}] --- BEGIN", newSubCat2, lang2);
			subCategoriesDao.updateLocalizeSubCategory(cat, lang2, oldSubCat2, newSubCat2);
			logger.info("Update Sub-Category {} [{}] --- END", newSubCat2, lang2);
		}
		logger.info("Edit Sub-Category --- END");
	}

	/**
	 * Delete sub-category
	 * 
	 * @param cat
	 * @param lang1
	 * @param subCat1
	 * @param lang2
	 * @param subCat2
	 * @throws Exception
	 */
	public void deleteLocalizeSubCategory(
			String cat, String lang1, String lang2, String subCat1, String subCat2) throws Exception {
		logger.info("Delete Sub-Category --- BEGIN");
		logger.info("Delete Sub-Category {} [{}] --- BEGIN", subCat1, lang1);
		subCategoriesDao.deleteLocalizeSubCategory(cat, lang1, subCat1);
		logger.info("Delete Sub-Category {} [{}] --- END", subCat1, lang1);

		logger.info("Delete Sub-Category {} [{}] --- BEGIN", subCat2, lang2);
		subCategoriesDao.deleteLocalizeSubCategory(cat, lang2, subCat2);
		logger.info("Delete Sub-Category {} [{}] --- END", subCat2, lang2);

		logger.info("Delete Sub-Category --- END");
	}
}
