package com.xaphe.xleap.service;

import java.math.BigDecimal;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xaphe.xleap.dao.localize.LocalizeItemsDao;
import com.xaphe.xleap.dto.LocalizeItemDto;
import com.xaphe.xleap.model.ItemModelView;
import com.xaphe.xleap.utils.StringUtil;

@Service
public class ItemAdminService {
	private static Logger logger = LoggerFactory.getLogger(ItemAdminService.class);
	@Autowired
	private LocalizeItemsDao localizeItemsDao;

	/**
	 * Get LocalizeItemDto
	 * 
	 * @param itemModelView
	 * @return LocalizeItemDto
	 */
	private LocalizeItemDto getItemEntityFromModelView(final ItemModelView itemModelView) {
		LocalizeItemDto localizeItemDto = new LocalizeItemDto();
		localizeItemDto.setCatTitle(itemModelView.getCategory());
		if (StringUtil.isNumber(itemModelView.getWeight())) {
			localizeItemDto.setWeight(Integer.parseInt(itemModelView.getWeight()));
		}
		localizeItemDto.setCost(new BigDecimal(itemModelView.getCost()));
		localizeItemDto.setShrtDesc(itemModelView.getShortDesc1());
		localizeItemDto.setDescription(itemModelView.getLongDesc1());
		localizeItemDto.setItmImg(itemModelView.getImage());
		localizeItemDto.setVisible(itemModelView.isVisible());
		return localizeItemDto;
	}

	/**
	 * Get sub category string
	 * @param subCategories
	 * @return String
	 */
	private String getSubCategory(final List<String> subCategories) {
		if (subCategories != null && subCategories.size() > 0) {
			StringBuilder sbSubCategories = new StringBuilder();
			for (String subCategory : subCategories) {
				if (sbSubCategories.length() > 0) {
					sbSubCategories.append("|");
				}
				sbSubCategories.append(subCategory);
			}
			return sbSubCategories.toString();
		}
		return null;
	}

	/**
	 * Insert item
	 * 
	 * @param lang
	 * @param itemModelView
	 */
	public void createItem(final String lang, final ItemModelView itemModelView)
			throws Exception {
		logger.info("Create item --- BEGIN");
		LocalizeItemDto localizeItemDto = getItemEntityFromModelView(itemModelView);
		logger.info("Create item [{}] --- BEGIN", itemModelView.getLocale1());
		localizeItemDto.setTitle(itemModelView.getCompany1());
		localizeItemDto.setLang(itemModelView.getLocale1());
		localizeItemDto.setSubcats(getSubCategory(itemModelView.getSubCategory1()));
		localizeItemDto.setShrtDesc(itemModelView.getShortDesc1());
		localizeItemDto.setDescription(itemModelView.getLongDesc1());

		int id = localizeItemsDao.createLocalizeItem(localizeItemDto);
		logger.info("Create item [{}] --- END", itemModelView.getLocale1());
		if (id > 0) {
			logger.info("Create item {} [{}] --- BEGIN", id, itemModelView.getLocale1());
			localizeItemDto.setId(id);

			localizeItemDto.setTitle(itemModelView.getCompany2());
			localizeItemDto.setLang(itemModelView.getLocale2());
			localizeItemDto.setSubcats(getSubCategory(itemModelView.getSubCategory2()));
			localizeItemDto.setShrtDesc(itemModelView.getShortDesc2());
			localizeItemDto.setDescription(itemModelView.getLongDesc2());
                        localizeItemDto.setVisible(itemModelView.isVisible());

			localizeItemsDao.createLocalizeItemWithId(localizeItemDto);
			logger.info("Create item {} [{}] --- END", id, itemModelView.getLocale1());
		}
		logger.info("Create item --- END");
	}

	/**
	 * Update item
	 * 
	 * @param lang
	 * @param itemModelView
	 */
	public void updateItem(final String lang, final ItemModelView itemModelView)
			throws Exception {
		logger.info("Update item --- BEGIN");
		logger.info("Delete sub category --- BEGIN");
		localizeItemsDao.deleteSubCategoryByItemId(itemModelView.getItemId());
		logger.info("Delete sub category --- END");
		// Update item with locale1
		logger.info("Update item {} [{}] --- BEGIN", itemModelView.getItemId(), itemModelView.getLocale1());
		LocalizeItemDto localizeItemDto = getItemEntityFromModelView(itemModelView);
		localizeItemDto.setId(itemModelView.getItemId());
		localizeItemDto.setTitle(itemModelView.getCompany1());
		localizeItemDto.setLang(itemModelView.getLocale1());
		localizeItemDto.setSubcats(getSubCategory(itemModelView.getSubCategory1()));
		localizeItemDto.setShrtDesc(itemModelView.getShortDesc1());
		localizeItemDto.setDescription(itemModelView.getLongDesc1());
		localizeItemsDao.updateLocalizeItem(localizeItemDto);
		logger.info("Update item {} [{}] --- END", itemModelView.getItemId(), itemModelView.getLocale1());
		// Update item with locale2
		logger.info("Update item {} [{}] --- BEGIN", itemModelView.getItemId(), itemModelView.getLocale2());
		localizeItemDto.setTitle(itemModelView.getCompany2());
		localizeItemDto.setLang(itemModelView.getLocale2());
		localizeItemDto.setSubcats(getSubCategory(itemModelView.getSubCategory2()));
		localizeItemDto.setShrtDesc(itemModelView.getShortDesc2());
		localizeItemDto.setDescription(itemModelView.getLongDesc2());
		localizeItemDto.setVisible(itemModelView.isVisible());
		localizeItemsDao.updateLocalizeItem(localizeItemDto);
		logger.info("Update item {} [{}] --- END", itemModelView.getItemId(), itemModelView.getLocale2());
		logger.info("Update item --- END");
	}
}
