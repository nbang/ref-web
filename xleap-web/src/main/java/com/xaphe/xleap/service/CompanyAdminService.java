package com.xaphe.xleap.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xaphe.xleap.dao.localize.LocalizeCompaniesDao;
import com.xaphe.xleap.dto.LocalizeCompanyDto;
import com.xaphe.xleap.enums.ContactTypesEnum;
import com.xaphe.xleap.model.CompanyModelView;

@Service
@Transactional
public class CompanyAdminService {
	private static Logger logger = LoggerFactory.getLogger(CompanyAdminService.class);
	@Autowired
	private LocalizeCompaniesDao companyDao;

	/**
	 * Get LocalizeCompanyDto
	 * 
	 * @param companyModelView
	 * @return LocalizeCompanyDto
	 */
	private LocalizeCompanyDto getCompanyEntityFromModelView(
			final CompanyModelView companyModelView) {
		LocalizeCompanyDto companyDto = new LocalizeCompanyDto();
		companyDto.setAddrnum(companyModelView.getAddrNum());
		companyDto.setStreet(companyModelView.getStreet());
		companyDto.setWard(companyModelView.getWard());
		companyDto.setDistrict(companyModelView.getDistrict());
		companyDto.setCity(companyModelView.getCity());
		companyDto.setProvince("");
		companyDto.setGeomarker(null);
		companyDto.setTitleLocal(companyModelView.getTitleLocal());
		companyDto.setTitleEng(companyModelView.getTitleEng());
		companyDto.setCon1typ(ContactTypesEnum.TEL.getTypes());
		companyDto.setCon1val(companyModelView.getTelephone1());
		companyDto.setCon2typ(ContactTypesEnum.MOBILE.getTypes());
		companyDto.setCon2val(companyModelView.getTelephone2());
		companyDto.setCon3typ(ContactTypesEnum.EMAIL.getTypes());
		companyDto.setCon3val(companyModelView.getEmail());
		companyDto.setCon4typ(ContactTypesEnum.CONTACT.getTypes());
		companyDto.setCon4val(companyModelView.getContact());
		companyDto.setUrl(companyModelView.getUrl());
		companyDto.setUrldesc("URL...");
		return companyDto;
	}

	/**
	 * Create company
	 * 
	 * @param companyModelView
	 */
	public void createLocalizeCompany(
			final CompanyModelView companyModelView) throws Exception {
		logger.info("Create Company --- BEGIN");
		logger.info("Create Company {} [{}] --- BEGIN", companyModelView.getCompanyId(), companyModelView.getLocale1());
		LocalizeCompanyDto companyDto = getCompanyEntityFromModelView(companyModelView);
		companyDto.setLang(companyModelView.getLocale1());
		companyDto.setTitle(companyModelView.getTitleEng());
		int id = companyDao.createLocalizeCompany(companyDto);
		logger.info("Create Company {} [{}] --- END", companyModelView.getCompanyId(), companyModelView.getLocale1());
		if (id > 0) {
			logger.info("Create Company {} [{}] --- BEGIN", companyModelView.getCompanyId(), companyModelView.getLocale2());
			companyDto.setId(id);
			companyDto.setLang(companyModelView.getLocale2());
			companyDto.setTitle(companyModelView.getTitleLocal());
			companyDao.updateLocalizeCompany(companyDto);
			logger.info("Create Company {} [{}] --- END", companyModelView.getCompanyId(), companyModelView.getLocale2());
		}
		logger.info("Create Company --- END");
	}

	/**
	 * Create company
	 * 
	 * @param companyModelView
	 */
	public void updateLocalizeCompany(
			final CompanyModelView companyModelView) throws Exception {
		logger.info("Updae Company --- BEGIN");
		logger.info("Update Company {} [{}] --- BEGIN", companyModelView.getCompanyId(), companyModelView.getLocale1());
		LocalizeCompanyDto companyDto = getCompanyEntityFromModelView(companyModelView);
		companyDto.setId(companyModelView.getCompanyId());
		companyDto.setLang(companyModelView.getLocale1());
		companyDto.setTitle(companyModelView.getTitleEng());
		companyDao.updateLocalizeCompany(companyDto);
		logger.info("Update Company {} [{}] --- END", companyModelView.getCompanyId(), companyModelView.getLocale1());

		logger.info("Update Company {} [{}] --- BEGIN", companyModelView.getCompanyId(), companyModelView.getLocale2());
		companyDto.setLang(companyModelView.getLocale2());
		companyDto.setTitle(companyModelView.getTitleLocal());
		companyDao.updateLocalizeCompany(companyDto);
		logger.info("Update Company {} [{}] --- END", companyModelView.getCompanyId(), companyModelView.getLocale2());
		logger.info("Update Company --- END");
	}
}
