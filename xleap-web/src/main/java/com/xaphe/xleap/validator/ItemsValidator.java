package com.xaphe.xleap.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.xaphe.xleap.model.ItemModelView;
import com.xaphe.xleap.utils.StringUtil;

/**
 * @author donavo
 */
public class ItemsValidator implements Validator {
	
	@Override
	public boolean supports(Class<?> clazz) {
		return ItemModelView.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		ItemModelView itemModelView = (ItemModelView) target;
		if (!StringUtil.hasLength(itemModelView.getCategory())) {
			errors.rejectValue("category", "category.input.required");
		}
		if (!StringUtil.hasLength(itemModelView.getCompany1())) {
			errors.rejectValue("company1", "company.input.required");
		}
		if (!StringUtil.hasLength(itemModelView.getCompany2())) {
			errors.rejectValue("company2", "company.input.required");
		}
		if (!StringUtil.hasLength(itemModelView.getWeight())) {
			errors.rejectValue("weight", "weight.input.required");
		} else if (!StringUtil.isNumber(itemModelView.getWeight())) {
			errors.rejectValue("weight", "error.input.validate");
		}
		/*if (!StringUtil.hasLength(itemModelView.getCost())) {
			errors.rejectValue("cost", "cost.input.required");
		} else if (!StringUtil.isNumber(itemModelView.getCost())) {
			errors.rejectValue("cost", "error.input.validate");
		}*/
		if (itemModelView.getLocale1().equals(itemModelView.getLocale2())) {
			errors.rejectValue("locale1", "locale.input.notsame");
		}
		if (!StringUtil.hasLength(itemModelView.getShortDesc1())
				&& !StringUtil.hasLength(itemModelView.getShortDesc2())) {
			errors.rejectValue("shortDesc1", "shortDesc.input.required");
		}
	}

}
