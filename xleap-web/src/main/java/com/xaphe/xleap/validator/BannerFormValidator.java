package com.xaphe.xleap.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.xaphe.xleap.model.BannerModelView;
import com.xaphe.xleap.utils.StringUtil;

/**
 * @author donavo
 */
public class BannerFormValidator implements Validator {
	
	@Override
	public boolean supports(Class<?> clazz) {
		return BannerModelView.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		BannerModelView modelView = (BannerModelView) target;
		if (!StringUtil.hasLength(modelView.getCategory())) {
			errors.rejectValue("category", "banner.cat.input.required");
		}
		if (!StringUtil.hasLength(modelView.getUrl())) {
			errors.rejectValue("url", "banner.url.input.required");
		}
		if (modelView.getImage() == null || modelView.getImage().isEmpty()) {
			errors.rejectValue("image", "banner.image.input.required");
		}
		if (!StringUtil.hasLength(modelView.getWeight())) {
			errors.rejectValue("weight", "banner.weight.input.required");
		} else if (!StringUtil.isNumber(modelView.getWeight())) {
			errors.rejectValue("weight", "banner.weight.input.validate");
		}
		if (!StringUtil.isNumber(modelView.getWidth())) {
			errors.rejectValue("width", "banner.width.input.validate");
		}
		if (!StringUtil.isNumber(modelView.getHeight())) {
			errors.rejectValue("height", "banner.height.input.validate");
		}
	}

}
