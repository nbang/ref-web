package com.xaphe.xleap.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.xaphe.xleap.model.CompanyModelView;
import com.xaphe.xleap.utils.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author donavo
 */
public class CompaniesValidator implements Validator {
	private static Logger logger = LoggerFactory.getLogger(CompaniesValidator.class);

	@Override
	public boolean supports(Class<?> clazz) {
		return CompanyModelView.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
            logger.debug("Validating Company, Target: {}, Errors: {}", target.toString(), errors.toString());
		CompanyModelView companyModelView = (CompanyModelView) target;
		/*if (!StringUtil.hasLength(companyModelView.getCity())) {
			errors.rejectValue("city", "error.empty");
		}
		if (!StringUtil.hasLength(companyModelView.getDistrict())) {
			errors.rejectValue("district", "error.empty");
		}
		if (!StringUtil.hasLength(companyModelView.getWard())) {
			errors.rejectValue("ward", "error.empty");
		}
		if (!StringUtil.hasLength(companyModelView.getStreet())) {
			errors.rejectValue("street", "error.empty");
		}*/
		if (!StringUtil.hasLength(companyModelView.getTitleEng())) {
                    logger.debug("User Input Error - English Title Value Empty");
			errors.rejectValue("titleEng", "error.empty");
		}
		if (!StringUtil.hasLength(companyModelView.getTitleLocal())) {
                    logger.debug("User Input Error - Local Title Value Empty");
			errors.rejectValue("titleLocal", "error.empty");
		}
	}

}
