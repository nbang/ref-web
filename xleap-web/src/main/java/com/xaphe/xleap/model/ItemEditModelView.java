/**
 * 
 */
package com.xaphe.xleap.model;


/**
 * Item form model
 * 
 * @author donavo
 */
public class ItemEditModelView {
	private int id;
	private String title;
	private int weight;
	private boolean hasShrtDescEN;
	private boolean hasLongDescEN;
	private boolean hasShrtDescVN;
	private boolean hasLongDescVN;
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * @return the weight
	 */
	public int getWeight() {
		return weight;
	}
	/**
	 * @param weight the weight to set
	 */
	public void setWeight(int weight) {
		this.weight = weight;
	}
	/**
	 * @return the hasShrtDescEN
	 */
	public boolean isHasShrtDescEN() {
		return hasShrtDescEN;
	}
	/**
	 * @param hasShrtDescEN the hasShrtDescEN to set
	 */
	public void setHasShrtDescEN(boolean hasShrtDescEN) {
		this.hasShrtDescEN = hasShrtDescEN;
	}
	/**
	 * @return the hasLongDescEN
	 */
	public boolean isHasLongDescEN() {
		return hasLongDescEN;
	}
	/**
	 * @param hasLongDescEN the hasLongDescEN to set
	 */
	public void setHasLongDescEN(boolean hasLongDescEN) {
		this.hasLongDescEN = hasLongDescEN;
	}
	/**
	 * @return the hasShrtDescVN
	 */
	public boolean isHasShrtDescVN() {
		return hasShrtDescVN;
	}
	/**
	 * @param hasShrtDescVN the hasShrtDescVN to set
	 */
	public void setHasShrtDescVN(boolean hasShrtDescVN) {
		this.hasShrtDescVN = hasShrtDescVN;
	}
	/**
	 * @return the hasLongDescVN
	 */
	public boolean isHasLongDescVN() {
		return hasLongDescVN;
	}
	/**
	 * @param hasLongDescVN the hasLongDescVN to set
	 */
	public void setHasLongDescVN(boolean hasLongDescVN) {
		this.hasLongDescVN = hasLongDescVN;
	}

}
