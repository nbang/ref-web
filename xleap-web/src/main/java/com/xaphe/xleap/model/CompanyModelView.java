/**
 * 
 */
package com.xaphe.xleap.model;

/**
 * Company form model
 * 
 * @author donavo
 */
public class CompanyModelView {
	private String locale1;
	private String locale2;
	private int companyId;
	private int cityId;
	private String city;
	private int districtId;
	private String district;
	private int wardId;
	private String ward;
	private int streetId;
	private String street;
	private String addrNum;
	private String titleLocal;
	private String titleEng;
	private int telephone1Id;
	private String telephone1;
	private int telephone2Id;
	private String telephone2;
	private int emailId;
	private String email;
	private int contactId;
	private String contact;
	private int urlId;
	private String url;

	/**
	 * @return the locale1
	 */
	public String getLocale1() {
		return locale1;
	}

	/**
	 * @param locale1
	 *            the locale1 to set
	 */
	public void setLocale1(String locale1) {
		this.locale1 = locale1;
	}

	/**
	 * @return the locale2
	 */
	public String getLocale2() {
		return locale2;
	}

	/**
	 * @param locale2
	 *            the locale2 to set
	 */
	public void setLocale2(String locale2) {
		this.locale2 = locale2;
	}

	/**
	 * @return the companyId
	 */
	public int getCompanyId() {
		return companyId;
	}

	/**
	 * @param companyId
	 *            the companyId to set
	 */
	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	/**
	 * @return the cityId
	 */
	public int getCityId() {
		return cityId;
	}

	/**
	 * @param cityId
	 *            the cityId to set
	 */
	public void setCityId(int cityId) {
		this.cityId = cityId;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city
	 *            the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the districtId
	 */
	public int getDistrictId() {
		return districtId;
	}

	/**
	 * @param districtId
	 *            the districtId to set
	 */
	public void setDistrictId(int districtId) {
		this.districtId = districtId;
	}

	/**
	 * @return the district
	 */
	public String getDistrict() {
		return district;
	}

	/**
	 * @param district
	 *            the district to set
	 */
	public void setDistrict(String district) {
		this.district = district;
	}

	/**
	 * @return the wardId
	 */
	public int getWardId() {
		return wardId;
	}

	/**
	 * @param wardId
	 *            the wardId to set
	 */
	public void setWardId(int wardId) {
		this.wardId = wardId;
	}

	/**
	 * @return the ward
	 */
	public String getWard() {
		return ward;
	}

	/**
	 * @param ward
	 *            the ward to set
	 */
	public void setWard(String ward) {
		this.ward = ward;
	}

	/**
	 * @return the streetId
	 */
	public int getStreetId() {
		return streetId;
	}

	/**
	 * @param streetId
	 *            the streetId to set
	 */
	public void setStreetId(int streetId) {
		this.streetId = streetId;
	}

	/**
	 * @return the street
	 */
	public String getStreet() {
		return street;
	}

	/**
	 * @param street
	 *            the street to set
	 */
	public void setStreet(String street) {
		this.street = street;
	}

	/**
	 * @return the addrNum
	 */
	public String getAddrNum() {
		return addrNum;
	}

	/**
	 * @param addrNum
	 *            the addrNum to set
	 */
	public void setAddrNum(String addrNum) {
		this.addrNum = addrNum;
	}

	/**
	 * @return the titleLocal
	 */
	public String getTitleLocal() {
		return titleLocal;
	}

	/**
	 * @param titleLocal
	 *            the titleLocal to set
	 */
	public void setTitleLocal(String titleLocal) {
		this.titleLocal = titleLocal;
	}

	/**
	 * @return the titleEng
	 */
	public String getTitleEng() {
		return titleEng;
	}

	/**
	 * @param titleEng
	 *            the titleEng to set
	 */
	public void setTitleEng(String titleEng) {
		this.titleEng = titleEng;
	}

	/**
	 * @return the telephone1Id
	 */
	public int getTelephone1Id() {
		return telephone1Id;
	}

	/**
	 * @param telephone1Id
	 *            the telephone1Id to set
	 */
	public void setTelephone1Id(int telephone1Id) {
		this.telephone1Id = telephone1Id;
	}

	/**
	 * @return the telephone1
	 */
	public String getTelephone1() {
		return telephone1;
	}

	/**
	 * @param telephone1
	 *            the telephone1 to set
	 */
	public void setTelephone1(String telephone1) {
		this.telephone1 = telephone1;
	}

	/**
	 * @return the telephone2Id
	 */
	public int getTelephone2Id() {
		return telephone2Id;
	}

	/**
	 * @param telephone2Id
	 *            the telephone2Id to set
	 */
	public void setTelephone2Id(int telephone2Id) {
		this.telephone2Id = telephone2Id;
	}

	/**
	 * @return the telephone2
	 */
	public String getTelephone2() {
		return telephone2;
	}

	/**
	 * @param telephone2
	 *            the telephone2 to set
	 */
	public void setTelephone2(String telephone2) {
		this.telephone2 = telephone2;
	}

	/**
	 * @return the emailId
	 */
	public int getEmailId() {
		return emailId;
	}

	/**
	 * @param emailId
	 *            the emailId to set
	 */
	public void setEmailId(int emailId) {
		this.emailId = emailId;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the contactId
	 */
	public int getContactId() {
		return contactId;
	}

	/**
	 * @param contactId
	 *            the contactId to set
	 */
	public void setContactId(int contactId) {
		this.contactId = contactId;
	}

	/**
	 * @return the contact
	 */
	public String getContact() {
		return contact;
	}

	/**
	 * @param contact
	 *            the contact to set
	 */
	public void setContact(String contact) {
		this.contact = contact;
	}

	/**
	 * @return the urlId
	 */
	public int getUrlId() {
		return urlId;
	}

	/**
	 * @param urlId
	 *            the urlId to set
	 */
	public void setUrlId(int urlId) {
		this.urlId = urlId;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url
	 *            the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}
}
