/**
 * 
 */
package com.xaphe.xleap.model;

/**
 * Streetbind form model
 * 
 * @author donavo
 */
public class StreetbindModelView {
	private String type;
	private String province;
	private String shortcode;
	private String name;

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the province
	 */
	public String getProvince() {
		return province;
	}

	/**
	 * @param province
	 *            the province to set
	 */
	public void setProvince(String province) {
		this.province = province;
	}

	/**
	 * @return the shortcode
	 */
	public String getShortcode() {
		return shortcode;
	}

	/**
	 * @param shortcode
	 *            the shortcode to set
	 */
	public void setShortcode(String shortcode) {
		this.shortcode = shortcode;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

}
