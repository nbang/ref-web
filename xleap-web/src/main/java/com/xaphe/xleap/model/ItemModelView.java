/**
 * 
 */
package com.xaphe.xleap.model;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

/**
 * Item form model
 * 
 * @author donavo
 */
public class ItemModelView {
	private String category;
	private List<String> subCategory1;
	private List<String> subCategory2;
	private int itemId;
	private String company1;
	private String company2;
	private String cost;
	private String weight;
	private String shortDesc1;
	private String shortDesc2;
	private String longDesc1;
	private String longDesc2;
	private String locale1;
	private String locale2;
	private String uploadType;
	private MultipartFile file;
	private String image;
        private boolean visible;

	/**
	 * @return the category
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * @param category
	 *            the category to set
	 */
	public void setCategory(String category) {
		this.category = category;
	}

	/**
	 * @return the subCategory1
	 */
	public List<String> getSubCategory1() {
		return subCategory1;
	}

	/**
	 * @param subCategory1
	 *            the subCategory1 to set
	 */
	public void setSubCategory1(List<String> subCategory1) {
		this.subCategory1 = subCategory1;
	}

	/**
	 * @return the subCategory2
	 */
	public List<String> getSubCategory2() {
		return subCategory2;
	}

	/**
	 * @param subCategory2
	 *            the subCategory2 to set
	 */
	public void setSubCategory2(List<String> subCategory2) {
		this.subCategory2 = subCategory2;
	}

	/**
	 * @return the itemId
	 */
	public int getItemId() {
		return itemId;
	}

	/**
	 * @param itemId
	 *            the itemId to set
	 */
	public void setItemId(int itemId) {
		this.itemId = itemId;
	}

	/**
	 * @return the company1
	 */
	public String getCompany1() {
		return company1;
	}

	/**
	 * @param company1
	 *            the company1 to set
	 */
	public void setCompany1(String company1) {
		this.company1 = company1;
	}

	/**
	 * @return the company2
	 */
	public String getCompany2() {
		return company2;
	}

	/**
	 * @param company2
	 *            the company2 to set
	 */
	public void setCompany2(String company2) {
		this.company2 = company2;
	}

	/**
	 * @return the cost
	 */
	public String getCost() {
		return cost;
	}

	/**
	 * @param cost
	 *            the cost to set
	 */
	public void setCost(String cost) {
		this.cost = cost;
	}

	/**
	 * @return the weight
	 */
	public String getWeight() {
		return weight;
	}

	/**
	 * @param weight
	 *            the weight to set
	 */
	public void setWeight(String weight) {
		this.weight = weight;
	}

	/**
	 * @return the shortDesc1
	 */
	public String getShortDesc1() {
		return shortDesc1;
	}

	/**
	 * @param shortDesc1
	 *            the shortDesc1 to set
	 */
	public void setShortDesc1(String shortDesc1) {
		this.shortDesc1 = shortDesc1;
	}

	/**
	 * @return the shortDesc2
	 */
	public String getShortDesc2() {
		return shortDesc2;
	}

	/**
	 * @param shortDesc2
	 *            the shortDesc2 to set
	 */
	public void setShortDesc2(String shortDesc2) {
		this.shortDesc2 = shortDesc2;
	}

	/**
	 * @return the longDesc1
	 */
	public String getLongDesc1() {
		return longDesc1;
	}

	/**
	 * @param longDesc1
	 *            the longDesc1 to set
	 */
	public void setLongDesc1(String longDesc1) {
		this.longDesc1 = longDesc1;
	}

	/**
	 * @return the longDesc2
	 */
	public String getLongDesc2() {
		return longDesc2;
	}

	/**
	 * @param longDesc2
	 *            the longDesc2 to set
	 */
	public void setLongDesc2(String longDesc2) {
		this.longDesc2 = longDesc2;
	}

	/**
	 * @return the image
	 */
	public String getImage() {
		return image;
	}

	/**
	 * @param image
	 *            the image to set
	 */
	public void setImage(String image) {
		this.image = image;
	}

	/**
	 * @return the locale1
	 */
	public String getLocale1() {
		return locale1;
	}

	/**
	 * @param locale1
	 *            the locale1 to set
	 */
	public void setLocale1(String locale1) {
		this.locale1 = locale1;
	}

	/**
	 * @return the locale2
	 */
	public String getLocale2() {
		return locale2;
	}

	/**
	 * @param locale2
	 *            the locale2 to set
	 */
	public void setLocale2(String locale2) {
		this.locale2 = locale2;
	}

	/**
	 * @return the file
	 */
	public MultipartFile getFile() {
		return file;
	}

	/**
	 * @param file
	 *            the file to set
	 */
	public void setFile(MultipartFile file) {
		this.file = file;
	}

	/**
	 * @return the uploadType
	 */
	public String getUploadType() {
		return uploadType;
	}

	/**
	 * @param uploadType the uploadType to set
	 */
	public void setUploadType(String uploadType) {
		this.uploadType = uploadType;
	}

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }
        
        
}
