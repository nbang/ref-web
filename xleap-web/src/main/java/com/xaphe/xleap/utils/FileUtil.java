package com.xaphe.xleap.utils;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;

import javax.imageio.ImageIO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

public class FileUtil {
	private static Logger logger = LoggerFactory.getLogger(FileUtil.class);
//	public static String UPLOAD_IMAGES_LOC = "/var/www/ksgn/images/";
//	public static String UPLOAD_IMAGES_LOC = "z:/xleap_data/sgnkidz/images/";
	public static final String PNG_EXT = ".png";
	public static final String JPG_EXT = ".jpg";
	public static final int BUFFER_SIZE = 4096;

	/**
	 * Write file from user upload
	 * @param file
	 * @param path
	 * @param fullFileName
	 */
	public static String writeUploadedFile(MultipartFile file, String path, String imageDestName) {
            logger.debug("WriteUploadFile called");
		InputStream inputStream = null;
		OutputStream outputStream = null;
		String fullFileName = path + imageDestName;
		try {
			inputStream = file.getInputStream();
			File newFile = new File(fullFileName);
			if (!newFile.exists()) {
				newFile.createNewFile();
			}
			outputStream = new FileOutputStream(newFile);
			int read = 0;
			byte[] bytes = new byte[1024];

			while ((read = inputStream.read(bytes)) != -1) {
				outputStream.write(bytes, 0, read);
			}
		} catch (FileNotFoundException e) {
			logger.error("Cannot upload file due to File not found exception. Error: {}", e.getMessage());
		} catch (IOException e) {
			logger.error("Cannot upload file due to IO exception. Error: {}", e.getMessage());
		} finally {
			try {
				if (inputStream != null) {
					inputStream.close();
				}
				if (outputStream != null) {
					outputStream.close();
				}
			} catch (IOException e) {
				logger.error(e.getMessage(), e);
			}
		}
		return fullFileName;
	}

	/**
	 * Download image from URL and write down user local
	 * @param imageUrl
	 * @param path
	 * @param imageDestName
	 */
	public static void downloadPNGImageFromUrl(String imageUrl, String path, String imageDestName) {
		BufferedImage image = null;
		try {
			URL url = new URL(imageUrl);
			// read the url
			image = ImageIO.read(url);
			// for png
			ImageIO.write(image, "png", new File(path + imageDestName + PNG_EXT));
		} catch (IOException e) {
			logger.error("Cannot upload file due to IO exception. Error: {}", e.getMessage());
		} finally {
			if (image != null) {
				image.flush();
			}
		}
	}

	/**
	 * Download image from URL and write down user local
	 * @param imageUrl
	 * @param path
	 * @param imageDestName
	 */
	public static void downloadImageFromUrl(String imageUrl, String path, String imageDestName) {
		BufferedImage image = null;
		try {
			URL url = new URL(imageUrl);
                        logger.info("URL of Image: {}", url);
			// read the url
			image = ImageIO.read(url);
			// for jpg
			ImageIO.write(image, "jpg", new File(path + imageDestName + JPG_EXT));
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		} finally {
			if (image != null) {
				image.flush();
			}
		}
	}
}
