package com.xaphe.xleap.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class StringUtil {
	public static final String DATE_FORMAT_NOW = "yyyyMMddHHmmss";

	/**
	 * Check string is null or empty
	 * 
	 * @param str
	 * @return boolean
	 */
	public static boolean hasLength(String str) {
		if (str == null || str.length() == 0) {
			return false;
		}
		return true;
	}

	public static boolean isNumber(String str) {
		if (!hasLength(str)) {
			return false;
		}
		return str.matches("-?\\d+(\\.\\d+)?");
	}

	public static String getItemImageName() {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
		return "ITEM_" + sdf.format(cal.getTime());
	}

	public static String getBannerImageName() {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
		return "BANNER_" + sdf.format(cal.getTime());
	}
}
