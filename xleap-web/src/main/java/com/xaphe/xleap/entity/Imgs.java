package com.xaphe.xleap.entity;

// Generated Oct 8, 2013 9:12:19 PM by Hibernate Tools 4.0.0

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Imgs generated by hbm2java
 */
@Entity
@Table(name = "imgs", schema = "els")
public class Imgs implements java.io.Serializable {

	private int id;
	private String filePath;
	private Set<Items> itemses = new HashSet<Items>(0);
	private Set<Categories> categorieses = new HashSet<Categories>(0);

	public Imgs() {
	}

	public Imgs(int id, String filePath) {
		this.id = id;
		this.filePath = filePath;
	}

	public Imgs(int id, String filePath, Set<Items> itemses,
			Set<Categories> categorieses) {
		this.id = id;
		this.filePath = filePath;
		this.itemses = itemses;
		this.categorieses = categorieses;
	}

	@Id
	@Column(name = "id", unique = true, nullable = false)
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "file_path", nullable = false, length = 500)
	public String getFilePath() {
		return this.filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "imgs")
	public Set<Items> getItemses() {
		return this.itemses;
	}

	public void setItemses(Set<Items> itemses) {
		this.itemses = itemses;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "imgs")
	public Set<Categories> getCategorieses() {
		return this.categorieses;
	}

	public void setCategorieses(Set<Categories> categorieses) {
		this.categorieses = categorieses;
	}

}
