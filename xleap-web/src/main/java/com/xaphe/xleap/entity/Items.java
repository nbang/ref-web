package com.xaphe.xleap.entity;

// Generated Oct 8, 2013 9:12:19 PM by Hibernate Tools 4.0.0

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Items generated by hbm2java
 */
@Entity
@Table(name = "items", schema = "vndata")
public class Items implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6214097474666666490L;

	private int id;
	private ShrtDescId shrtDescId;
	private DescId descId;
	private Companies companies;
	private Imgs imgs;
	private int cost;
	private Boolean visible;
	private Set<LnkCatItm> lnkCatItms = new HashSet<LnkCatItm>(0);
	private Set<SubcatId> subcatIds = new HashSet<SubcatId>(0);
	private Set<ItmXtraContacts> itmXtraContactses = new HashSet<ItmXtraContacts>(0);
	private Set<Tags> tagses = new HashSet<Tags>(0);
	private Set<Posfix> posfixes = new HashSet<Posfix>(0);

	public Items() {
	}

	public Items(int id, Companies companies) {
		this.id = id;
		this.companies = companies;
	}

	public Items(int id, ShrtDescId shrtDescId, DescId descId, Companies companies, Imgs imgs, int cost,
			Boolean visible, Set<LnkCatItm> lnkCatItms, Set<SubcatId> subcatIds,
			Set<ItmXtraContacts> itmXtraContactses, Set<Tags> tagses, Set<Posfix> posfixes) {
		this.id = id;
		this.shrtDescId = shrtDescId;
		this.descId = descId;
		this.companies = companies;
		this.imgs = imgs;
		this.cost = cost;
		this.visible = visible;
		this.lnkCatItms = lnkCatItms;
		this.subcatIds = subcatIds;
		this.itmXtraContactses = itmXtraContactses;
		this.tagses = tagses;
		this.posfixes = posfixes;
	}

	@Id
	@Column(name = "id", unique = true, nullable = false)
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "short_desc")
	public ShrtDescId getShrtDescId() {
		return this.shrtDescId;
	}

	public void setShrtDescId(ShrtDescId shrtDescId) {
		this.shrtDescId = shrtDescId;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "long_desc")
	public DescId getDescId() {
		return this.descId;
	}

	public void setDescId(DescId descId) {
		this.descId = descId;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "compnay_id", nullable = false)
	public Companies getCompanies() {
		return this.companies;
	}

	public void setCompanies(Companies companies) {
		this.companies = companies;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "primary_img")
	public Imgs getImgs() {
		return this.imgs;
	}

	public void setImgs(Imgs imgs) {
		this.imgs = imgs;
	}

	@Column(name = "cost", precision = 1, scale = 0)
	public int getCost() {
		return this.cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

	@Column(name = "visible")
	public Boolean getVisible() {
		return this.visible;
	}

	public void setVisible(Boolean visible) {
		this.visible = visible;
	}

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "items")
	public Set<LnkCatItm> getLnkCatItms() {
		return this.lnkCatItms;
	}

	public void setLnkCatItms(Set<LnkCatItm> lnkCatItms) {
		this.lnkCatItms = lnkCatItms;
	}

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "lnk_scat_itm", schema = "vndata", joinColumns = { @JoinColumn(name = "item", nullable = false, updatable = false) }, inverseJoinColumns = { @JoinColumn(name = "subcat", nullable = false, updatable = false) })
	public Set<SubcatId> getSubcatIds() {
		return this.subcatIds;
	}

	public void setSubcatIds(Set<SubcatId> subcatIds) {
		this.subcatIds = subcatIds;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "items")
	public Set<ItmXtraContacts> getItmXtraContactses() {
		return this.itmXtraContactses;
	}

	public void setItmXtraContactses(Set<ItmXtraContacts> itmXtraContactses) {
		this.itmXtraContactses = itmXtraContactses;
	}

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "lnk_tag_itm", schema = "vndata", joinColumns = { @JoinColumn(name = "item", nullable = false, updatable = false) }, inverseJoinColumns = { @JoinColumn(name = "tag", nullable = false, updatable = false) })
	public Set<Tags> getTagses() {
		return this.tagses;
	}

	public void setTagses(Set<Tags> tagses) {
		this.tagses = tagses;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "items")
	public Set<Posfix> getPosfixes() {
		return this.posfixes;
	}

	public void setPosfixes(Set<Posfix> posfixes) {
		this.posfixes = posfixes;
	}

}
