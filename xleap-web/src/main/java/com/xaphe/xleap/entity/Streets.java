package com.xaphe.xleap.entity;

// Generated Oct 8, 2013 9:12:19 PM by Hibernate Tools 4.0.0

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * Streets generated by hbm2java
 */
@Entity
@Table(name = "streets", schema = "vnlocs", uniqueConstraints = @UniqueConstraint(columnNames = "street"))
public class Streets implements java.io.Serializable {

	private int id;
	private String street;
	private Set<Streetbind> streetbinds = new HashSet<Streetbind>(0);

	public Streets() {
	}

	public Streets(int id, String street) {
		this.id = id;
		this.street = street;
	}

	public Streets(int id, String street, Set<Streetbind> streetbinds) {
		this.id = id;
		this.street = street;
		this.streetbinds = streetbinds;
	}

	@Id
	@Column(name = "id", unique = true, nullable = false)
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "street", unique = true, nullable = false, length = 40)
	public String getStreet() {
		return this.street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "streets")
	public Set<Streetbind> getStreetbinds() {
		return this.streetbinds;
	}

	public void setStreetbinds(Set<Streetbind> streetbinds) {
		this.streetbinds = streetbinds;
	}

}
