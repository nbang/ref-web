/**
 * 
 */
package com.xaphe.xleap.dao.localize;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.xaphe.xleap.dto.LocalizeDistrictsDto;

/**
 * @author son-vo
 * 
 */
@Repository
public class LocalizeDistrictsDao extends
		AbstractLocalizeDao<LocalizeDistrictsDto> {
	private static final Logger logger = LoggerFactory
			.getLogger(LocalizeDistrictsDao.class);

	public LocalizeDistrictsDao() {
		super(LocalizeDistrictsDto.class, "vnlocs.districts");
	}

	/**
	 * Create new district
	 * 
	 * @param districts
	 * @return number of record insert
	 */
	public int createDistrict(LocalizeDistrictsDto districts) {
		logger.info("Create new district");
		StringBuilder insertSQL = new StringBuilder();
		insertSQL.append("INSERT INTO vnlocs.districts");
		insertSQL.append(" (district)");
		insertSQL.append(" VALUES (:name)");
		Query query = getCurrentSession().createSQLQuery(insertSQL.toString())
				.setParameter("name", districts.getDistrict());
		logger.debug("Create new district: \tQuery: {}", query.toString());
		return query.executeUpdate();
	}

	/**
	 * Delete district
	 * 
	 * @param name
	 * @return number of record delete
	 */
	public int deleteDistrict(String name) {
		logger.info("Delete district");
		StringBuilder insertSQL = new StringBuilder();
		insertSQL.append("DELETE FROM vnlocs.districts");
		insertSQL.append(" WHERE district = :name");
		Query query = getCurrentSession().createSQLQuery(insertSQL.toString())
				.setParameter("name", name);
		logger.debug("Delete district: \tQuery: {}", query.toString());
		return query.executeUpdate();
	}

	/**
	 * Get districts
	 * 
	 * @return List<LocalizeDistrictsDto>
	 */
	@SuppressWarnings("unchecked")
	public List<LocalizeDistrictsDto> getDistricts() {
		StringBuilder querySQL = new StringBuilder();
		querySQL.append("SELECT id, district");
		querySQL.append(" FROM vnlocs.districts ");
		querySQL.append(" ORDER BY district, id");
		SQLQuery query = getCurrentSession()
				.createSQLQuery(querySQL.toString());

		query.setResultTransformer(Transformers.aliasToBean(LocalizeDistrictsDto.class));

		logger.debug("Get districts: \tQuery: {}", query.toString());

		return query.list();
	}
}
