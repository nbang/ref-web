package com.xaphe.xleap.dao;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public abstract class AbstractHibernateDAO<T extends Serializable> {
	protected final Class<T> clazz;
	@Autowired
	SessionFactory sessionFactory;

	public AbstractHibernateDAO(final Class<T> clazzToSet) {
		this.clazz = clazzToSet;
	}

	@SuppressWarnings("unchecked")
	public T findById(final int id) {
		return (T) this.getCurrentSession().get(this.clazz, id);
	}

	@SuppressWarnings("unchecked")
	public List<T> findAll() {
		return this.getCurrentSession()
				.createQuery("from " + this.clazz.getName()).list();
	}

	public void create(final T entity) {
		this.getCurrentSession().persist(entity);
	}

	public void update(final T entity) {
		this.getCurrentSession().merge(entity);
	}

	public void delete(final T entity) {
		this.getCurrentSession().delete(entity);
	}

	public void deleteById(final int entityId) {
		final T entity = this.findById(entityId);
		this.delete(entity);
	}

	public Integer getLocalizeMaxId(String locale) {
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append("select max(cl.id.id) ");
		sqlBuilder.append(" from ");
		sqlBuilder.append(this.clazz.getName());
		sqlBuilder.append(" as cl ");
		sqlBuilder.append(" inner join cl.locale as lc");
		sqlBuilder.append(" where lower(lc.code) = lower(:locale)");
		Query query = getCurrentSession().createQuery(sqlBuilder.toString());
		query.setParameter("locale", locale);
		@SuppressWarnings("unchecked")
		List<Integer> list = query.list();
		if (list != null && list.size() > 0) {
			return list.get(0);
		} else {
			return 0;
		}
	}

	public Long getMaxId() {
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(this.clazz).setProjection(
				Projections.max("id"));
		Object value = criteria.uniqueResult();
		if (value == null) {
			return 0L;
		}
		Long maxId = 0L;
		if (value instanceof Integer) {
			maxId = ((Integer) value).longValue();
		} else if (value instanceof Long) {
			maxId = (Long) value;
		} else {
			maxId = new BigDecimal(value.toString()).longValue();
		}
		return maxId == null ? 0L : maxId;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	protected final Session getCurrentSession() {
		return this.sessionFactory.getCurrentSession();
	}
	
	protected Class<T> getClazz() {
		return this.clazz;
	}
}
