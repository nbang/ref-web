package com.xaphe.xleap.dao.localize;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.xaphe.xleap.dto.LocalizeCompanyDto;

/**
 * @author son-vo
 * @param <T>
 */
@Repository
public class LocalizeCompaniesDao extends AbstractLocalizeDao<LocalizeCompanyDto> {
	private static final Logger logger = LoggerFactory.getLogger(LocalizeCompaniesDao.class);

	public LocalizeCompaniesDao() {
		super(LocalizeCompanyDto.class, "views.comps");
	}

	/**
	 * Check title is exist
	 * 
	 * @param title
	 * @return false : if exist else not exist
	 */
	public boolean validateCompanyTitle(String title) {
		SQLQuery query = getCurrentSession().createSQLQuery("select * from title_v where title ilike :title");
		query.setParameter("title", title);

		logger.info("Title: {},\tQuery: {}", title, query.toString());

		return query.list().isEmpty();
	}

	/**
	 * Check title is exist
	 * 
	 * @param companyId
	 * @param titleEng
	 * @return @return false : if exist else not exist
	 */
	public boolean validateCompanyTitle(int companyId, String title) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("select title_v.id from vndata.companies_v inner join els.title_v ON companies_v.title = title_v.id");
		sbQuery.append(" where companies_v.id != :companyId and title_v.title ilike :title");
		SQLQuery query = getCurrentSession().createSQLQuery(sbQuery.toString());
		query.setParameter("companyId", companyId);
		query.setParameter("title", title);

		logger.info("Title: {},\tQuery: {}", title, query.toString());

		return query.list().isEmpty();
	}

	@SuppressWarnings("unchecked")
	public List<LocalizeCompanyDto> getLocalizeCompanyListByLang(String lang) {
		StringBuilder querySQL = new StringBuilder();
		querySQL.append("SELECT ");
		querySQL.append(getCompsFields());
		querySQL.append(" FROM views.getcomps (:lang)");
		querySQL.append(" ORDER BY id");
		Query query = getCurrentSession().createSQLQuery(querySQL.toString()).setResultTransformer(
				Transformers.aliasToBean(LocalizeCompanyDto.class));
		query.setParameter("lang", lang);

		logger.debug("Lang: {},\tQuery: {}", lang, query.toString());

		return query.list();
	}

	@SuppressWarnings("unchecked")
	public List<String> getLocalizeCompaniesNameByLang(String lang) {
		StringBuilder querySQL = new StringBuilder();
		querySQL.append("SELECT ");
		querySQL.append("title");
		querySQL.append(" FROM views.getcomps (:lang)");
		querySQL.append(" ORDER BY id");
		Query query = getCurrentSession().createSQLQuery(querySQL.toString());
		query.setParameter("lang", lang);

		logger.debug("Lang: {},\tQuery: {}", lang, query.toString());

		return query.list();
	}

	/**
	 * Insert company
	 * 
	 * @param entity
	 * @return number of record insert
	 */
	public int createLocalizeCompany(LocalizeCompanyDto entity) {
		StringBuilder insertSQL = new StringBuilder();
		insertSQL.append("INSERT INTO " + view);
		insertSQL.append(" (lang, title, addr_num, street, ward, district, city, url, url_desc, con1typ, con1val, con2typ, con2val, con3typ, con3val, con4typ, con4val)");
		insertSQL.append(" VALUES (:lang, :title, :addr_num, :street, :ward, :district, :city, :url, :url_desc, :con1typ, :con1val, :con2typ, :con2val, :con3typ, :con3val, :con4typ, :con4val)");
		insertSQL.append(" RETURNING id;");
		Query query = getCurrentSession().createSQLQuery(insertSQL.toString()).setParameter("lang", entity.getLang())
				.setParameter("title", entity.getTitle())
				.setParameter("addr_num", entity.getAddrnum())
				.setParameter("street", entity.getStreet())
				.setParameter("ward", entity.getWard())
				.setParameter("district", entity.getDistrict())
				.setParameter("city", entity.getCity())
				.setParameter("url", entity.getUrl())
				.setParameter("url_desc", entity.getUrldesc())
				.setParameter("con1typ", entity.getCon1typ())
				.setParameter("con1val", entity.getCon1val())
				.setParameter("con2typ", entity.getCon2typ())
				.setParameter("con2val", entity.getCon2val())
				.setParameter("con3typ", entity.getCon3typ())
				.setParameter("con3val", entity.getCon3val())
				.setParameter("con4typ", entity.getCon4typ())
				.setParameter("con4val", entity.getCon4val());

		logger.debug(
				"Lang: {},\tTitle: {},\tAddNum: {},\tStreet: {},\tWard: {},\tDistrict: {},\tCity: {},\tUrl: {},\tDesc: {},\tQuery: {}",
				entity.getLang(), entity.getTitle(), entity.getAddrnum(), entity.getStreet(), entity.getWard(),
				entity.getDistrict(), entity.getCity(), entity.getUrl(), entity.getUrldesc(), query.toString());

		return (int) query.uniqueResult();
	}

	/**
	 * Insert company
	 * 
	 * @param entity
	 * @return number of record insert
	 */
	public int createLocalizeCompanyWithId(LocalizeCompanyDto entity) {
		StringBuilder insertSQL = new StringBuilder();
		insertSQL.append("INSERT INTO " + view);
		insertSQL.append(" (id, lang, title, addr_num, street, ward, district, city, url, url_desc, con1typ, con1val, con2typ, con2val, con3typ, con3val, con4typ, con4val)");
		insertSQL.append(" VALUES (:id, :lang, :title, :addr_num, :street, :ward, :district, :city, :con1typ, :con1val, :con2typ, :con2val, :con3typ, :con3val, :con4typ, :con4val)");
		Query query = getCurrentSession().createSQLQuery(insertSQL.toString()).setParameter("id", entity.getId())
				.setParameter("lang", entity.getLang())
				.setParameter("title", entity.getTitle())
				.setParameter("addr_num", entity.getAddrnum())
				.setParameter("street", entity.getStreet())
				.setParameter("ward", entity.getWard())
				.setParameter("district", entity.getDistrict())
				.setParameter("city", entity.getCity())
				.setParameter("url", entity.getUrl())
				.setParameter("url_desc", entity.getUrldesc())
				.setParameter("con1typ", entity.getCon1typ())
				.setParameter("con1val", entity.getCon1val())
				.setParameter("con2typ", entity.getCon2typ())
				.setParameter("con2val", entity.getCon2val())
				.setParameter("con3typ", entity.getCon3typ())
				.setParameter("con3val", entity.getCon3val())
				.setParameter("con4typ", entity.getCon4typ())
				.setParameter("con4val", entity.getCon4val());

		logger.debug(
				"Lang: {},\tTitle: {},\tAddNum: {},\tStreet: {},\tWard: {},\tDistrict: {},\tCity: {},\tUrl: {},\tDesc: {},\tQuery: {}",
				entity.getLang(), entity.getTitle(), entity.getAddrnum(), entity.getStreet(), entity.getWard(),
				entity.getDistrict(), entity.getCity(), entity.getUrl(), entity.getUrldesc(), query.toString());

		return query.executeUpdate();
	}

	/**
	 * Update company
	 * 
	 * @param entity
	 * @return number of record update
	 */
	public int updateLocalizeCompany(LocalizeCompanyDto entity) {
		StringBuilder updateSQL = new StringBuilder();
		updateSQL.append("UPDATE " + view);
		updateSQL.append(" SET ");
		updateSQL.append("lang = :lang");
		updateSQL.append(",title = :title");
		updateSQL.append(",addr_num = :addr_num");
		updateSQL.append(",street = :street");
		updateSQL.append(",ward = :ward");
		updateSQL.append(",district = :district");
		updateSQL.append(",city = :city");
		updateSQL.append(",url = :url");
		updateSQL.append(",url_desc = :url_desc");
		updateSQL.append(",con1typ = :con1typ");
		updateSQL.append(",con1val = :con1val");
		updateSQL.append(",con2typ = :con2typ");
		updateSQL.append(",con2val = :con2val");
		updateSQL.append(",con3typ = :con3typ");
		updateSQL.append(",con3val = :con3val");
		updateSQL.append(",con4typ = :con4typ");
		updateSQL.append(",con4val = :con4val");
		updateSQL.append(" WHERE id = :id");
		//updateSQL.append(" AND lang = :lang");
		Query query = getCurrentSession().createSQLQuery(updateSQL.toString()).setParameter("lang", entity.getLang())
				.setParameter("title", entity.getTitle())
				.setParameter("addr_num", entity.getAddrnum())
				.setParameter("street", entity.getStreet())
				.setParameter("ward", entity.getWard())
				.setParameter("district", entity.getDistrict())
				.setParameter("city", entity.getCity())
				.setParameter("url", entity.getUrl())
				.setParameter("url_desc", entity.getUrldesc())
				.setParameter("con1typ", entity.getCon1typ())
				.setParameter("con1val", entity.getCon1val())
				.setParameter("con2typ", entity.getCon2typ())
				.setParameter("con2val", entity.getCon2val())
				.setParameter("con3typ", entity.getCon3typ())
				.setParameter("con3val", entity.getCon3val())
				.setParameter("con4typ", entity.getCon4typ())
				.setParameter("con4val", entity.getCon4val())
				.setParameter("id", entity.getId());

		logger.debug(
				"Lang: {},\tTitle: {},\tAddNum: {},\tStreet: {},\tWard: {},\tDistrict: {},\tCity: {},\tUrl: {},\tDesc: {},\tId: {},\tQuery: {}",
				entity.getLang(), entity.getTitle(), entity.getAddrnum(), entity.getStreet(), entity.getWard(),
				entity.getDistrict(), entity.getCity(), entity.getUrl(), entity.getUrldesc(), entity.getId(),
				query.toString());

		return query.executeUpdate();
	}

	@SuppressWarnings("unchecked")
	public LocalizeCompanyDto getLocalizeCompanyById(int companyId, String lang) {
		StringBuilder querySQL = new StringBuilder();
		querySQL.append("SELECT ");
		querySQL.append(getCompsFields());
		querySQL.append(" FROM views.getcomps (:lang)");
		querySQL.append(" WHERE id = :id");
		Query query = getCurrentSession().createSQLQuery(querySQL.toString()).setResultTransformer(
				Transformers.aliasToBean(LocalizeCompanyDto.class));
		query.setParameter("lang", lang);
		query.setParameter("id", companyId);

		List<LocalizeCompanyDto> list = query.list();
		if (list != null && list.size() > 0) {
			return list.get(0);
		}

		logger.debug("Lang: {},\tId: {},\tQuery: {}", lang, companyId, query.toString());

		return new LocalizeCompanyDto();
	}
        
        @SuppressWarnings("unchecked")
	public LocalizeCompanyDto getLocalizeCompanyByTitle(String companyTitle, String lang) {
                logger.info("Getting Company by with Title: {}, Lang: {}", companyTitle, lang);
		StringBuilder querySQL = new StringBuilder();
		querySQL.append("SELECT ");
		querySQL.append(getCompsFields());
		querySQL.append(" FROM views.getcomps (:lang)");
		querySQL.append(" WHERE title = :title");
		Query query = getCurrentSession().createSQLQuery(querySQL.toString()).setResultTransformer(
				Transformers.aliasToBean(LocalizeCompanyDto.class));
		query.setParameter("lang", lang);
		query.setParameter("title", companyTitle);

		List<LocalizeCompanyDto> list = query.list();
		if (list != null && list.size() > 0) {
			return list.get(0);
		}

		logger.debug("Lang: {},\tId: {},\tQuery: {}", lang, companyTitle, query.toString());

		return new LocalizeCompanyDto();
	}

	/**
	 * Get fields of companies
	 * 
	 * @return String
	 */
	private String getCompsFields() {
		StringBuilder querySQL = new StringBuilder();
		querySQL.append("id,");
		querySQL.append("lang,");
		querySQL.append("title,");
		querySQL.append("addr_num AS addrnum,");
		querySQL.append("street,");
		querySQL.append("ward,");
		querySQL.append("district,");
		querySQL.append("city,");
		querySQL.append("url,");
		querySQL.append("url_desc AS urldesc,");
		querySQL.append("con1typ,");
		querySQL.append("con1val,");
		querySQL.append("con2typ,");
		querySQL.append("con2val,");
		querySQL.append("con3typ,");
		querySQL.append("con3val,");
		querySQL.append("con4typ,");
		querySQL.append("con4val");
		return querySQL.toString();
	}
}
