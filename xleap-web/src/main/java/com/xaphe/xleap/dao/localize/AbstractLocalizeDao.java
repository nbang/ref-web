/**
 * 
 */
package com.xaphe.xleap.dao.localize;

import java.io.Serializable;
import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Repository;

import com.xaphe.xleap.dao.AbstractHibernateDAO;


/**
 * @author son-vo
 * 
 */
@Repository
public abstract class AbstractLocalizeDao<T extends Serializable> extends AbstractHibernateDAO<T> {

	protected final String view;
	
	public AbstractLocalizeDao(Class<T> clazzToSet, String view) {
		super(clazzToSet);
		this.view = view;
	}

	@SuppressWarnings({ "hiding", "unchecked", "rawtypes"})
	public <T> T getLocalizeText(int id, String localize) {
		SQLQuery query = getCurrentSession()
				.createSQLQuery("SELECT t.* FROM " + view + " t WHERE t.id= :id and t.lang like :lang");
				
		query.setResultTransformer(Transformers.aliasToBean(clazz));
		
		query.setParameter("id", id);
		query.setParameter("lang", localize);
		
		List result = query.list();
		
		return (result != null && result.size() > 0) ? (T) result.get(0) : null;
	}
	
	
}
