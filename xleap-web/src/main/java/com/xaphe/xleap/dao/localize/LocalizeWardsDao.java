/**
 * 
 */
package com.xaphe.xleap.dao.localize;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.xaphe.xleap.dto.LocalizeWardsDto;

/**
 * @author son-vo
 * 
 */
@Repository
public class LocalizeWardsDao extends AbstractLocalizeDao<LocalizeWardsDto> {
	private static final Logger logger = LoggerFactory.getLogger(LocalizeWardsDao.class);

	public LocalizeWardsDao() {
		super(LocalizeWardsDto.class, "vnlocs.wards");
	}

	/**
	 * Create new ward
	 * 
	 * @param wards
	 * @return number of record insert
	 */
	public int createWard(LocalizeWardsDto wards) {
		logger.info("Create new ward");
		StringBuilder insertSQL = new StringBuilder();
		insertSQL.append("INSERT INTO vnlocs.wards");
		insertSQL.append(" (ward)");
		insertSQL.append(" VALUES (:name)");
		Query query = getCurrentSession().createSQLQuery(insertSQL.toString())
				.setParameter("name", wards.getWard());
		logger.debug("Create new ward: \tQuery: {}", query.toString());
		return query.executeUpdate();
	}

	/**
	 * Delete ward
	 * 
	 * @param name
	 * @return number of record delete
	 */
	public int deleteWard(String name) {
		logger.info("Delete ward");
		StringBuilder insertSQL = new StringBuilder();
		insertSQL.append("DELETE FROM vnlocs.wards");
		insertSQL.append(" WHERE ward = :name");
		Query query = getCurrentSession().createSQLQuery(insertSQL.toString())
				.setParameter("name", name);
		logger.debug("Delete ward: \tQuery: {}", query.toString());
		return query.executeUpdate();
	}

	/**
	 * Get wards
	 * 
	 * @return List<LocalizeWardsDto>
	 */
	@SuppressWarnings("unchecked")
	public List<LocalizeWardsDto> getWards() {
		StringBuilder querySQL = new StringBuilder();
		querySQL.append("SELECT id, ward");
		querySQL.append(" FROM vnlocs.wards ");
		querySQL.append(" ORDER BY ward, id");
		SQLQuery query = getCurrentSession()
				.createSQLQuery(querySQL.toString());

		query.setResultTransformer(Transformers.aliasToBean(LocalizeWardsDto.class));

		logger.debug("Get wards: \tQuery: {}", query.toString());

		return query.list();
	}
}
