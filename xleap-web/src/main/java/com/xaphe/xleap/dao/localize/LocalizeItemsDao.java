package com.xaphe.xleap.dao.localize;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.xaphe.xleap.dto.LocalizeItemDto;

/**
 * @author son-vo
 * @param <T>
 */
@Repository
public class LocalizeItemsDao extends AbstractLocalizeDao<LocalizeItemDto> {
	private static final Logger logger = LoggerFactory.getLogger(LocalizeItemsDao.class);

	public LocalizeItemsDao() {
		super(LocalizeItemDto.class, "views.items");
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> getItems(String localize, String searchString, int firstResult, int maxResult) {
		SQLQuery query = getCurrentSession()
				.createSQLQuery(
						createSeclectString()
								+ " WHERE id IN (SELECT id FROM els.descs, to_tsquery(:searchString) query WHERE query @@ desc_val_tsv_gin)");

		query.setResultTransformer(Transformers.aliasToBean(clazz));
		query.setParameter("lang", localize);
		query.setParameter("searchString", searchString);
		query.setFirstResult(firstResult);
		query.setMaxResults(maxResult);

		logger.debug("Lang: {},\tSearch: {},\tFirst: {},\tMax: {},\tQuery: {}", localize, searchString, firstResult,
				maxResult, query.toString());

		return query.list();
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> getAllItem(String localize, int firstResult, int maxResult) {
		SQLQuery query = getCurrentSession().createSQLQuery(createSeclectString() + " WHERE visible = true");

		query.setResultTransformer(Transformers.aliasToBean(clazz));
		query.setParameter("lang", localize);
		query.setFirstResult(firstResult);
		query.setMaxResults(maxResult);

		logger.debug("Lang: {},\tFirst: {},\tMax: {},\tQuery: {}", localize, firstResult, maxResult, query.toString());

		return query.list();
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> getAllItemForEdit(String localize, int firstResult, int maxResult) {
		SQLQuery query = getCurrentSession().createSQLQuery(createSeclectString());

		query.setResultTransformer(Transformers.aliasToBean(clazz));
		query.setParameter("lang", localize);
		query.setFirstResult(firstResult);
		query.setMaxResults(maxResult);

		logger.debug("Lang: {},\tFirst: {},\tMax: {},\tQuery: {}", localize, firstResult, maxResult, query.toString());

		return query.list();
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> getAllItemByCat(String localize, String category, int firstResult, int maxResult) {
		SQLQuery query = getCurrentSession().createSQLQuery(
				createSeclectString() + " WHERE cat_title like :category and visible = true");

		query.setResultTransformer(Transformers.aliasToBean(clazz));
		query.setParameter("lang", localize);
		query.setParameter("category", category);
		query.setFirstResult(firstResult);
		query.setMaxResults(maxResult);

		logger.debug("Lang: {},\tCategory: {},\tFirst: {},\tMax: {},\tQuery: {}", localize, category, firstResult,
				maxResult, query.toString());

		return query.list();
	}

	public int getAllItemCount(String localize) {
		SQLQuery query = getCurrentSession().createSQLQuery(
				"SELECT count(*) FROM views.getitems (:lang) " + " WHERE visible = true");

		query.setParameter("lang", localize);

		String result = query.uniqueResult().toString();

		logger.debug("Lang: {},\tQuery: {}", localize, query.toString());

		return Integer.valueOf(result);
	}

	public int getAllItemCountByCategory(String localize, String category) {
		SQLQuery query = getCurrentSession().createSQLQuery(
				"SELECT count(*) FROM views.getitems (:lang) " + " WHERE visible = true and cat_title like :category");

		query.setParameter("lang", localize);
		query.setParameter("category", category);

		String result = query.uniqueResult().toString();

		logger.debug("Lang: {},\tCategory: {},\tQuery: {}", localize, category, query.toString());

		return Integer.valueOf(result);
	}

	private String createSeclectString() {
		StringBuilder sb = new StringBuilder();

		sb.append("SELECT id");
		sb.append(", lang");
		sb.append(", title");
		sb.append(", cat_title");
		sb.append(", cat_icon");
		sb.append(", weight");
		sb.append(", subcats");
		sb.append(", cost");
		sb.append(", shrt_desc");
		sb.append(", description");
		sb.append(", itm_img");
		sb.append(", visible");
		sb.append(" FROM ");
		sb.append(" views.getitems (:lang) ");

		return sb.toString();
	}

	@SuppressWarnings("unchecked")
	public <T> T getItemByIdAndLocale(int id, String localize) {
		SQLQuery query = getCurrentSession().createSQLQuery(createSeclectString() + " WHERE id = :id");

		query.setResultTransformer(Transformers.aliasToBean(clazz));
		query.setParameter("lang", localize);
		query.setParameter("id", id);

		List<T> result = query.list();

		logger.debug("Lang: {},\tId: {},\tQuery: {}", localize, id, query.toString());

		return result.size() > 0 ? result.get(0) : null;
	}

	/**
	 * Insert item
	 * 
	 * @param entity
	 * @return number of record insert
	 */
	public int createLocalizeItem(LocalizeItemDto entity) {
		StringBuilder insertSQL = new StringBuilder();
		insertSQL.append("INSERT INTO " + view);
		insertSQL.append(" (lang, cat_title, subcats, title, weight, cost, shrt_desc, description, itm_img, visible)");
		insertSQL
				.append(" VALUES (:lang, :cat_title, :subcats, :title, :weight, :cost, :shrt_desc, :description, :itm_img, :visible)");
		insertSQL.append(" RETURNING id;");
		Query query = getCurrentSession().createSQLQuery(insertSQL.toString()).setParameter("lang", entity.getLang())
				.setParameter("cat_title", entity.getCatTitle()).setParameter("subcats", entity.getSubcats())
				.setParameter("title", entity.getTitle()).setParameter("weight", entity.getWeight())
				.setParameter("cost", entity.getCost()).setParameter("shrt_desc", entity.getShrtDesc())
				.setParameter("description", entity.getDescription()).setParameter("itm_img", entity.getItmImg())
				.setParameter("visible", entity.isVisible());

		logger.debug(
				"Lang: {},\tCatTitle: {},\tSubCats: {},\tTitle: {},\tWeight: {},\tCost: {},\tShortDesc: {},\tDesc: {},\tImg: {},\tVisible: {},\tQuery: {}",
				entity.getLang(), entity.getCatTitle(), entity.getSubcats(), entity.getTitle(), entity.getWeight(),
				entity.getCost(), entity.getShrtDesc(), entity.getDescription(), entity.getItmImg(),
				entity.isVisible(), query.toString());

		return (int) query.uniqueResult();
	}

	/**
	 * Insert item
	 * 
	 * @param entity
	 * @return number of record insert
	 */
	public int createLocalizeItemWithId(LocalizeItemDto entity) {
		StringBuilder insertSQL = new StringBuilder();
		insertSQL.append("INSERT INTO " + view);
		insertSQL
				.append(" (id, lang, cat_title, subcats, title, weight, cost, shrt_desc, description, itm_img, visible)");
		insertSQL
				.append(" VALUES (:id, :lang, :cat_title, :subcats, :title, :weight, :cost, :shrt_desc, :description, :itm_img, :visible)");
		Query query = getCurrentSession().createSQLQuery(insertSQL.toString()).setParameter("id", entity.getId())
				.setParameter("lang", entity.getLang()).setParameter("cat_title", entity.getCatTitle())
				.setParameter("weight", entity.getWeight()).setParameter("subcats", entity.getSubcats())
				.setParameter("title", entity.getTitle()).setParameter("cost", entity.getCost())
				.setParameter("shrt_desc", entity.getShrtDesc()).setParameter("description", entity.getDescription())
				.setParameter("itm_img", entity.getItmImg()).setParameter("visible", entity.isVisible());

		logger.debug(
				"Lang: {},\tId: {}\tCatTitle: {},\tSubCats: {},\tTitle: {},\tWeight: {},\tCost: {},\tShortDesc: {},\tDesc: {},\tImg: {},\tVisible: {},\tQuery: {}",
				entity.getLang(), entity.getId(), entity.getCatTitle(), entity.getSubcats(), entity.getTitle(),
				entity.getWeight(), entity.getCost(), entity.getShrtDesc(), entity.getDescription(),
				entity.getItmImg(), entity.isVisible(), query.toString());

		return query.executeUpdate();
	}

	/**
	 * Update item
	 * 
	 * @param entity
	 * @return number of record update
	 */
	public int updateLocalizeItem(LocalizeItemDto entity) {
		StringBuilder insertSQL = new StringBuilder();
		insertSQL.append("INSERT INTO " + view);
		insertSQL
				.append(" (id, lang, cat_title, subcats, title, weight, cost, shrt_desc, description, itm_img, visible)");
		insertSQL
				.append(" VALUES (:id, :lang, :cat_title, :subcats, :title, :weight, :cost, :shrt_desc, :description, :itm_img, :visible)");
		Query query = getCurrentSession().createSQLQuery(insertSQL.toString()).setParameter("id", entity.getId())
				.setParameter("lang", entity.getLang()).setParameter("cat_title", entity.getCatTitle())
				.setParameter("subcats", entity.getSubcats()).setParameter("title", entity.getTitle())
				.setParameter("weight", entity.getWeight()).setParameter("cost", entity.getCost())
				.setParameter("shrt_desc", entity.getShrtDesc()).setParameter("description", entity.getDescription())
				.setParameter("itm_img", entity.getItmImg()).setParameter("visible", entity.isVisible());

		logger.debug(
				"Lang: {},\tId: {}\tCatTitle: {},\tSubCats: {},\tTitle: {},\tWeight: {},\tCost: {},\tShortDesc: {},\tDesc: {},\tImg: {},\tVisible: {},\tQuery: {}",
				entity.getLang(), entity.getId(), entity.getCatTitle(), entity.getSubcats(), entity.getTitle(),
				entity.getWeight(), entity.getCost(), entity.getShrtDesc(), entity.getDescription(),
				entity.getItmImg(), entity.isVisible(), query.toString());

		return query.executeUpdate();
	}

	/**
	 * Delete sub category by item id
	 * 
	 * @param itemId
	 * @return record deleted
	 */
	public int deleteSubCategoryByItemId(int itemId) {
		StringBuilder executeSQL = new StringBuilder();
		executeSQL.append("DELETE FROM utils.itm_scats_grp WHERE id = :itemId");
		Query query = getCurrentSession().createSQLQuery(executeSQL.toString()).setParameter("itemId", itemId);

		logger.debug("Id: {},\tQuery: {}", itemId, query.toString());

		return query.executeUpdate();
	}

	public int updateItemWeight(int itemId, String category, int weight) {
		StringBuilder updateSQL = new StringBuilder();
		updateSQL.append("UPDATE " + "vndata.lnk_cat_itm");
		updateSQL.append(" SET weight = :weight");
		updateSQL
				.append(" WHERE item = :id AND category IN (SELECT id FROM vndata.categories where title like :category)");
		Query query = getCurrentSession().createSQLQuery(updateSQL.toString()).setParameter("weight", weight)
				.setParameter("id", itemId).setParameter("category", category);

		logger.debug("Id: {}\tCategory: {},\tWeight: {},\tQuery: {}", itemId, category, weight, query.toString());

		return query.executeUpdate();
	}
}
