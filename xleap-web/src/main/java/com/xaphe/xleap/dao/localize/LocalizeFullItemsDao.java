package com.xaphe.xleap.dao.localize;

import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.xaphe.xleap.dto.LocalizeFullItemDto;

/**
 * @author son-vo
 * @param <T>
 * 
 */
@Repository
public class LocalizeFullItemsDao extends AbstractLocalizeDao<LocalizeFullItemDto> {
	private static final Logger logger = LoggerFactory.getLogger(LocalizeFullItemsDao.class);
	
	public LocalizeFullItemsDao() {
		super(LocalizeFullItemDto.class, "views.all");
	}

	private String createSeclectString() {
		StringBuilder sb = new StringBuilder();

		sb.append("SELECT id");
		sb.append(",lang");
		sb.append(",title");
		sb.append(",addr_num");
		sb.append(",street");
		sb.append(",ward");
		sb.append(",district");
		sb.append(",city");
		sb.append(",con1val");
		sb.append(",con2val");
		sb.append(",con3val");
		sb.append(",con4val");
		sb.append(",url");
		sb.append(",url_desc");
		sb.append(",cat_title");
		sb.append(",cat_icon");
		sb.append(",subcats");
		sb.append(",cost");
		sb.append(",shrt_desc");
		sb.append(",description");
		sb.append(",itm_img");
		sb.append(",visible ");
		sb.append(",weight");
		sb.append(" FROM views.getall (:lang)");

		return sb.toString();
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> getItems(String localize, String searchString, int firstResult, int maxResult) {
		SQLQuery query = getCurrentSession()
				.createSQLQuery(
						createSeclectString()
								+ " WHERE id IN (SELECT id FROM els.descs, to_tsquery(:searchString) query WHERE query @@ desc_val_tsv_gin)");

		query.setResultTransformer(Transformers.aliasToBean(clazz));
		query.setParameter("lang", localize);
		query.setParameter("searchString", searchString);
		query.setFirstResult(firstResult);
		query.setMaxResults(maxResult);

		logger.debug("Lang: {},\tSearch: {},\tFirst: {},\tMax: {},\tQuery: {}", localize, searchString, firstResult,
				maxResult, query.toString());

		return query.list();
	}

	public int countItemBySearchCondition(String localize, String searchString) {
		SQLQuery query = getCurrentSession()
				.createSQLQuery(
						"SELECT COUNT(*) FROM views.getall (:lang) WHERE id IN (SELECT id FROM els.descs, to_tsquery(:searchString) query WHERE query @@ desc_val_tsv_gin)");

		query.setParameter("lang", localize);
		query.setParameter("searchString", searchString);

		String result = query.uniqueResult().toString();

		return Integer.valueOf(result);
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> getAllItemForEdit(String localize, int firstResult, int maxResult) {
		SQLQuery query = getCurrentSession().createSQLQuery(createSeclectString());

		query.setResultTransformer(Transformers.aliasToBean(clazz));
		query.setParameter("lang", localize);
		query.setFirstResult(firstResult);
		query.setMaxResults(maxResult);

		logger.debug("Lang: {},\tFirst: {},\tMax: {},\tQuery: {}", localize, firstResult,
				maxResult, query.toString());

		return query.list();
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> getAllItem(String localize, int firstResult, int maxResult) {
		SQLQuery query = getCurrentSession().createSQLQuery(createSeclectString() + " WHERE visible = true");

		query.setResultTransformer(Transformers.aliasToBean(clazz));
		query.setParameter("lang", localize);
		query.setFirstResult(firstResult);
		query.setMaxResults(maxResult);

		logger.debug("Lang: {},\tFirst: {},\tMax: {},\tQuery: {}", localize, firstResult,
				maxResult, query.toString());

		return query.list();
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> getAllItemByCat(String localize, String category, int firstResult, int maxResult) {
		SQLQuery query = getCurrentSession().createSQLQuery(
				createSeclectString() + " WHERE cat_title like :category and visible = true");

		query.setResultTransformer(Transformers.aliasToBean(clazz));
		query.setParameter("lang", localize);
		query.setParameter("category", category);
		query.setFirstResult(firstResult);
		query.setMaxResults(maxResult);

		logger.debug("Lang: {},\tCategory: {},\tFirst: {},\tMax: {},\tQuery: {}", localize, category, firstResult,
				maxResult, query.toString());
		
		return query.list();
	}

	@SuppressWarnings("unchecked")
	public <T> T getItemById(int id, String localize) {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT *");
		sb.append(" FROM ");
		sb.append(" views.getall (:lang) ");
		sb.append(" WHERE id = :id");
		SQLQuery query = getCurrentSession().createSQLQuery(sb.toString());

		query.setResultTransformer(Transformers.aliasToBean(clazz));
		query.setParameter("id", id);
		query.setParameter("lang", localize);

		logger.debug("Lang: {},\tId: {},\tQuery: {}", localize, id, query.toString());

		List<T> result = query.list();

		return result.size() > 0 ? result.get(0) : null;
	}
}
