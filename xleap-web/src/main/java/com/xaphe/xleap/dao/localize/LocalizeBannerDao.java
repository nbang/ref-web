package com.xaphe.xleap.dao.localize;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.xaphe.xleap.dto.LocalizeBannerDto;
import com.xaphe.xleap.dto.PageLocDto;

/**
 * @author son-vo
 */
@Repository
public class LocalizeBannerDao extends AbstractLocalizeDao<LocalizeBannerDto> {
	private static final Logger logger = LoggerFactory
			.getLogger(LocalizeBannerDao.class);

	public LocalizeBannerDao() {
		super(LocalizeBannerDto.class, "views.banners");
	}

	@SuppressWarnings("unchecked")
	public List<LocalizeBannerDto> getLocalizeBannerList() {
		StringBuilder querySQL = new StringBuilder();
		querySQL.append("SELECT ");
		querySQL.append("id,");
		querySQL.append("client_name as clientname,");
		querySQL.append("cat_title as cattitle,");
		querySQL.append("image,");
		querySQL.append("url,");
		querySQL.append("width,");
		querySQL.append("height,");
		querySQL.append("page_loc as pageloc,");
		querySQL.append("visible,");
		querySQL.append("weight,");
		querySQL.append("start_date as startdate,");
		querySQL.append("end_date as enddate");
		querySQL.append(" FROM ");
		querySQL.append(view);
		Query query = getCurrentSession().createSQLQuery(
				querySQL.toString()).setResultTransformer(
				Transformers.aliasToBean(LocalizeBannerDto.class));

		logger.debug("Query: {}", query.toString());

		return query.list();
	}

	/**
	 * Insert banner
	 * 
	 * @param entity
	 * @return number of record insert
	 */
	public void createBanner(LocalizeBannerDto entity) {
		StringBuilder execSQL = new StringBuilder();
		execSQL.append("INSERT INTO ");
		execSQL.append(view);
		execSQL.append(" (client_name, cat_title, image, url, width, height, page_loc, visible, weight, start_date, end_date) ");
		execSQL.append("VALUES (:client_name, :cat_title, :image, :url, :width, :height, :page_loc, :visible, :weight, :start_date, :end_date)");
		SQLQuery sqlQuery = getCurrentSession().createSQLQuery(execSQL.toString());
		Query query = sqlQuery.setParameter("client_name", entity.getClientname());
		query.setParameter("cat_title", entity.getCattitle());
		query.setParameter("image", entity.getImage());
		query.setParameter("url", entity.getUrl());
		query.setParameter("width", entity.getWidth());
		query.setParameter("height", entity.getHeight());
		query.setParameter("page_loc", entity.getPageloc());
		query.setParameter("visible", entity.isVisible());
		query.setParameter("weight", entity.getWeight());
		query.setParameter("start_date", entity.getStartdate());
		query.setParameter("end_date", entity.getEnddate());

		logger.debug("Client Name: {},\tCat Title: {},\tImage: {},\tURL: {},\tWidth: {},\tHeight: {},\tPage Location: {},\tVisible: {},\tWeight: {},\tStart Date: {},\tEnd Date: {},\tQuery: {}",
				entity.getClientname(), entity.getCattitle(), entity.getImage(), entity.getUrl(), entity.getWidth(), entity.getHeight(), 
				entity.getPageloc(), entity.isVisible(), entity.getWeight(), entity.getStartdate(),entity.getEnddate(), query.toString());

		query.executeUpdate();
	}

	/**
	 * Update banner
	 * 
	 * @param entity
	 */
	public void updateBanner(LocalizeBannerDto entity) {
		StringBuilder execSQL = new StringBuilder();
		execSQL.append("UPDATE ");
		execSQL.append(view);
		execSQL.append(" SET ");
		execSQL.append("client_name=:client_name,");
		execSQL.append("cat_title=:cat_title,");
		execSQL.append("image=:image,");
		execSQL.append("url=:url,");
		execSQL.append("width=:width,");
		execSQL.append("height=:height,");
		execSQL.append("page_loc=:page_loc,");
		execSQL.append("visible=:visible,");
		execSQL.append("weight=:weight,");
		execSQL.append("start_date=:start_date,");
		execSQL.append("end_date=:end_date");
		execSQL.append(" WHERE ");
		execSQL.append("id=:id");

		SQLQuery sqlQuery = getCurrentSession().createSQLQuery(execSQL.toString());
		Query query = sqlQuery.setParameter("client_name", entity.getClientname());
		query.setParameter("cat_title", entity.getCattitle());
		query.setParameter("image", entity.getImage());
		query.setParameter("url", entity.getUrl());
		query.setParameter("width", entity.getWidth());
		query.setParameter("height", entity.getHeight());
		query.setParameter("page_loc", entity.getPageloc());
		query.setParameter("visible", entity.isVisible());
		query.setParameter("weight", entity.getWeight());
		query.setParameter("start_date", entity.getStartdate());
		query.setParameter("end_date", entity.getEnddate());
		query.setParameter("id", entity.getId());

		logger.debug("Client Name: {},\tCat Title: {},\tImage: {},\tURL: {},\tWidth: {},\tHeight: {},\tPage Location: {},\tVisible: {},\tWeight: {},\tStart Date: {},\tEnd Date: {},\tQuery: {}",
				entity.getClientname(), entity.getCattitle(), entity.getImage(), entity.getUrl(), entity.getWidth(), entity.getHeight(), 
				entity.getPageloc(), entity.isVisible(), entity.getWeight(), entity.getStartdate(),entity.getEnddate(), query.toString());

		query.executeUpdate();
	}

	/**
	 * Delete banner (set visible = false)
	 * 
	 * @param id
	 */
	public void deleteBanner(int id) {
		StringBuilder execSQL = new StringBuilder();
		execSQL.append("UPDATE ");
		execSQL.append(view);
		execSQL.append(" SET ");
		execSQL.append("visible=false");
		execSQL.append(" WHERE ");
		execSQL.append("id=:id");

		Query query = getCurrentSession().createSQLQuery(execSQL.toString())
				.setParameter("id", id);

		logger.debug("ID: {},\tQuery: {}", id, query.toString());

		query.executeUpdate();
	}

	/**
	 * Get list of page locations
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<PageLocDto> getPageLocs() {
		StringBuilder querySQL = new StringBuilder();
		querySQL.append("SELECT ");
		querySQL.append("id,");
		querySQL.append("page_loc as pageloc");
		querySQL.append(" FROM els.page_locs_v");
		Query query = getCurrentSession().createSQLQuery(
				querySQL.toString()).setResultTransformer(
				Transformers.aliasToBean(PageLocDto.class));

		logger.debug("Query: {}", query.toString());

		return query.list();
	}
}
