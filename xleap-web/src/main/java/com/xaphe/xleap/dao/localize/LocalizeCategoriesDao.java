package com.xaphe.xleap.dao.localize;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.transform.Transformers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.xaphe.xleap.dto.LocalizeCategoriesDto;

/**
 * @author son-vo
 * 
 */
@Repository
public class LocalizeCategoriesDao extends AbstractLocalizeDao<LocalizeCategoriesDto> {
	private static final Logger logger = LoggerFactory.getLogger(LocalizeCategoriesDao.class);
	private static List<LocalizeCategoriesDto> listCat = null;

	public LocalizeCategoriesDao() {
		super(LocalizeCategoriesDto.class, "views.cats");
	}

	
	@SuppressWarnings("unchecked")
	public List<LocalizeCategoriesDto> getLocalizeCategoryList() {
		if (listCat == null || listCat.size() == 0) {
			StringBuilder querySQL = new StringBuilder();
			querySQL.append("SELECT ");
			querySQL.append("id,");
			querySQL.append("title,");
			querySQL.append("cat_desc as catdesc,");
			querySQL.append("file_path as filepath");
			querySQL.append(" FROM ");
			querySQL.append(view);
			Query query = getCurrentSession().createSQLQuery(querySQL.toString()).setResultTransformer(
					Transformers.aliasToBean(LocalizeCategoriesDto.class));
			listCat = query.list();

			logger.debug("Query: {}", query.toString());
		}

		return listCat;
	}
}
