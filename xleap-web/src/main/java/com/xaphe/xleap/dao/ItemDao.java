/**
 * 
 */
package com.xaphe.xleap.dao;

import java.util.List;

import org.hibernate.SQLQuery;
import org.springframework.stereotype.Repository;

import com.xaphe.xleap.entity.Items;

/**
 * @author son-vo
 * 
 */
@Repository
public class ItemDao extends AbstractHibernateDAO<Items> {
	public ItemDao() {
		super(Items.class);
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> getItemByCategory(int categoryId, int pageNumber, int pageSize) {
		SQLQuery query = getCurrentSession().createSQLQuery("select i.* from itm_cat_v i i.category = :categoryId");
		query.setParameter("categoryId", categoryId);
		query.setFirstResult(pageNumber * pageSize);
		query.setMaxResults(pageSize);
		
		return query.list();
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> getPaging(int pageNumber, int pageSize) {
		SQLQuery query = getCurrentSession().createSQLQuery("select i.* from itm_cat_v i");
		query.setFirstResult(pageNumber * pageSize);
		query.setMaxResults(pageSize);
		
		return query.list();
	}
}
