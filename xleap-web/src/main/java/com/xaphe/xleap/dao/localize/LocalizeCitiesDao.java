/**
 * 
 */
package com.xaphe.xleap.dao.localize;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.xaphe.xleap.dto.LocalizeCitiesDto;

/**
 * @author son-vo
 * 
 */
@Repository
public class LocalizeCitiesDao extends AbstractLocalizeDao<LocalizeCitiesDto> {
	private static final Logger logger = LoggerFactory
			.getLogger(LocalizeCitiesDao.class);

	public LocalizeCitiesDao() {
		super(LocalizeCitiesDto.class, "vnlocs.cities");
	}

	/**
	 * Create new city
	 * 
	 * @param cities
	 * @return number of record insert
	 */
	public int createCity(LocalizeCitiesDto cities) {
		logger.info("Create new city");
		StringBuilder insertSQL = new StringBuilder();
		insertSQL.append("INSERT INTO vnlocs.cities");
		insertSQL.append(" (name, province, shortcode)");
		insertSQL.append(" VALUES (:name, :province, :shortcode)");
		Query query = getCurrentSession().createSQLQuery(insertSQL.toString())
				.setParameter("name", cities.getName())
				.setParameter("province", 0)
				.setParameter("shortcode", cities.getShortcode());
		logger.debug("Create new city: \tQuery: {}", query.toString());
		return query.executeUpdate();
	}

	/**
	 * Delete city
	 * @param name
	 * @return number of record delete
	 */
	public int deleteCity(String name) {
		logger.info("Delete city");
		StringBuilder insertSQL = new StringBuilder();
		insertSQL.append("DELETE FROM vnlocs.cities");
		insertSQL.append(" WHERE name = :name");
		Query query = getCurrentSession().createSQLQuery(insertSQL.toString())
				.setParameter("name", name);
		logger.debug("Delete city: \tQuery: {}", query.toString());
		return query.executeUpdate();
	}

	/**
	 * Get cities
	 * 
	 * @return List<LocalizeCitiesDto>
	 */
	@SuppressWarnings("unchecked")
	public List<LocalizeCitiesDto> getCities() {
		StringBuilder querySQL = new StringBuilder();
		querySQL.append("SELECT id, name, province, shortcode");
		querySQL.append(" FROM vnlocs.cities ");
		querySQL.append(" ORDER BY name, id ");
		SQLQuery query = getCurrentSession()
				.createSQLQuery(querySQL.toString());

		query.setResultTransformer(Transformers.aliasToBean(LocalizeCitiesDto.class));

		logger.debug("Get cities: \tQuery: {}", query.toString());

		return query.list();
	}
}
