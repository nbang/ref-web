/**
 * 
 */
package com.xaphe.xleap.dao;

import org.springframework.stereotype.Repository;

import com.xaphe.xleap.entity.Titles;

/**
 * @author son-vo
 * 
 */
@Repository
public class TitlesDao extends AbstractHibernateDAO<Titles> {
	public TitlesDao() {
		super(Titles.class);
	}
}
