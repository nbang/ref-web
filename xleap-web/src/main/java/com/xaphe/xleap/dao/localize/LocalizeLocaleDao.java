/**
 * 
 */
package com.xaphe.xleap.dao.localize;

import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Repository;

import com.xaphe.xleap.dto.LocalizeLocaleDto;

/**
 * @author son-vo
 * 
 */
@Repository
public class LocalizeLocaleDao extends AbstractLocalizeDao<LocalizeLocaleDto> {

	public LocalizeLocaleDao() {
		super(LocalizeLocaleDto.class, "locale_v");
	}

	@SuppressWarnings("unchecked")
	public List<LocalizeLocaleDto> getAllLocale() {
		SQLQuery query = getCurrentSession().createSQLQuery(
				"SELECT * FROM locale_v ORDER BY id");
		query.setResultTransformer(Transformers.aliasToBean(clazz));
		return query.list();
	}
}
