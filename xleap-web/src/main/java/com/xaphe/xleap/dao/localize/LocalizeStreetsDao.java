/**
 * 
 */
package com.xaphe.xleap.dao.localize;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.xaphe.xleap.dto.LocalizeStreetsDto;

/**
 * @author son-vo
 * 
 */
@Repository
public class LocalizeStreetsDao extends AbstractLocalizeDao<LocalizeStreetsDto> {
	private static final Logger logger = LoggerFactory
			.getLogger(LocalizeStreetsDao.class);

	public LocalizeStreetsDao() {
		super(LocalizeStreetsDto.class, "vnlocs.streets");
	}

	/**
	 * Create new streets
	 * 
	 * @param streets
	 * @return number of record insert
	 */
	public int createStreet(LocalizeStreetsDto streets) {
		logger.info("Create new street");
		StringBuilder insertSQL = new StringBuilder();
		insertSQL.append("INSERT INTO vnlocs.streets");
		insertSQL.append(" (street)");
		insertSQL.append(" VALUES (:name)");
		Query query = getCurrentSession().createSQLQuery(insertSQL.toString())
				.setParameter("name", streets.getStreet());
		logger.debug("Create new street: \tQuery: {}", query.toString());
		return query.executeUpdate();
	}

	/**
	 * Delete street
	 * 
	 * @param name
	 * @return number of record delete
	 */
	public int deleteStreet(String name) {
		logger.info("Delete street");
		StringBuilder insertSQL = new StringBuilder();
		insertSQL.append("DELETE FROM vnlocs.streets");
		insertSQL.append(" WHERE street = :name");
		Query query = getCurrentSession().createSQLQuery(insertSQL.toString())
				.setParameter("name", name);
		logger.debug("Detele street: \tQuery: {}", query.toString());
		return query.executeUpdate();
	}

	/**
	 * Get streets
	 * 
	 * @return List<LocalizeStreetsDto>
	 */
	@SuppressWarnings("unchecked")
	public List<LocalizeStreetsDto> getStreets() {
		StringBuilder querySQL = new StringBuilder();
		querySQL.append("SELECT id, street");
		querySQL.append(" FROM vnlocs.streets ");
		querySQL.append(" ORDER BY street, id");
		SQLQuery query = getCurrentSession()
				.createSQLQuery(querySQL.toString());

		query.setResultTransformer(Transformers.aliasToBean(LocalizeStreetsDto.class));

		logger.debug("Get districts: \tQuery: {}", query.toString());

		return query.list();
	}

}
