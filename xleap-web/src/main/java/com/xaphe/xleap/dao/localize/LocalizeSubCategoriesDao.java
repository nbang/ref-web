package com.xaphe.xleap.dao.localize;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.transform.Transformers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.xaphe.xleap.dto.LocalizeSubCategoriesDto;

/**
 * @author son-vo
 * 
 */
@Repository
public class LocalizeSubCategoriesDao extends AbstractLocalizeDao<LocalizeSubCategoriesDto> {
	private static final Logger logger = LoggerFactory.getLogger(LocalizeSubCategoriesDao.class);
	
	public LocalizeSubCategoriesDao() {
		// super(LocalizeSubCategoriesDto.class, "utils.itm_scats");
		super(LocalizeSubCategoriesDto.class, "views.subcats");
	}

	@SuppressWarnings("unchecked")
	public List<LocalizeSubCategoriesDto> getLocalizeSubCategoryByLang(String lang) {
		StringBuilder querySQL = new StringBuilder();
		getSubCategorySQL(querySQL);
		querySQL.append(" WHERE lang like :lang");
		Query query = getCurrentSession().createSQLQuery(querySQL.toString()).setResultTransformer(
				Transformers.aliasToBean(clazz));
		query.setParameter("lang", lang);

		logger.debug("Lang: {},\tQuery: {}", lang, query.toString());
		
		return query.list();
	}

	@SuppressWarnings("unchecked")
	public List<LocalizeSubCategoriesDto> getLocalizeSubCategoryByCategoryName(String category, String lang) {
		StringBuilder querySQL = new StringBuilder();
		getSubCategorySQL(querySQL);
		querySQL.append(" WHERE parent_cat = :category AND lang like :lang");
		querySQL.append(" ORDER BY id");
		Query query = getCurrentSession().createSQLQuery(querySQL.toString()).setResultTransformer(
				Transformers.aliasToBean(clazz));
		query.setParameter("category", category);
		query.setParameter("lang", lang);
		
		logger.debug("Lang: {},\tCategory: {},\tQuery: {}", lang, category, query.toString());
		
		return query.list();
	}

        
	public int createSubcat(int item, String lang1, String subCat1, String lang2, String subCat2) {
		StringBuilder insertSQL = new StringBuilder();

		insertSQL.append("INSERT INTO utils.itm_scats");
		insertSQL.append(" (cat, lang1, subCat1, lang2, subCat2)");
		insertSQL.append(" VALUES (:cat, :lang1, :subCat1, :lang2, :subCat2)");
		insertSQL.append(" RETURNING item;");
		
		Query query = getCurrentSession().createSQLQuery(insertSQL.toString()).setParameter("cat", item)
				.setParameter("lang1", lang1).setParameter("subCat1", subCat1).setParameter("lang2", lang2)
				.setParameter("subCat2", subCat2);
		
		logger.debug("Lang1: {},\tLang2: {},\tQuery: {}", lang1, lang2, query.toString());
		
		return (int) query.uniqueResult();
	}

        
	/**
	 * Insert item
	 * 
     * @param cat
     * @param lang
     * @param subCat
     * @return 
	 */
        public int createLocalizeSubCategory(String cat, String lang, String subCat) {
            StringBuilder insertSQL = new StringBuilder();

		insertSQL.append("INSERT INTO els.subcats_comb_v");
		insertSQL.append(" (cat_title, lang, sub_name)");
		insertSQL.append(" VALUES (:cat_title, :lang, :subCat)");
		insertSQL.append(" RETURNING id;");
		
		Query query = getCurrentSession().createSQLQuery(insertSQL.toString()).setParameter("cat_title", cat)
				.setParameter("lang", lang).setParameter("subCat", subCat);
                //.setParameter("lang2", lang2).setParameter("subCat2", subCat2);
                
//                int subcatID = (int) query.uniqueResult();
//                
//                insertSQL = new StringBuilder();
//
//		insertSQL.append("INSERT INTO els.subcats_comb_v");
//		insertSQL.append(" (id, cat, lang, sub_name)");
//		insertSQL.append(" VALUES (:subCatID, :cat, :lang2, :subCat2)");
//		insertSQL.append(" RETURNING id;");
//		
//		query = getCurrentSession().createSQLQuery(insertSQL.toString()).setParameter("id", subcatID).setParameter("cat", cat)
//				.setParameter("lang2", lang2).setParameter("subCat2", subCat2);
//		
                logger.info("Adding Subcategory");
		logger.info("Cat: {},\tLang: {},\tQuery: {}",cat, lang, query.toString());
		
		return (int) query.uniqueResult();
//                return query.executeUpdate();
        }
        
        /**
	 * Insert item
	 *  
     * @param id
     * @param cat
     * @param lang
     * @param subCat
     * @return 
	 */
        public int createLocalizeSubCategory(int id, String cat, String lang, String subCat) {
            StringBuilder insertSQL = new StringBuilder();

		insertSQL.append("INSERT INTO els.subcats_comb_v");
		insertSQL.append(" (id, cat_title, lang, sub_name)");
		insertSQL.append(" VALUES (:id, :cat_title, :lang, :subCat)");
		insertSQL.append(" RETURNING id;");
		
		Query query = getCurrentSession().createSQLQuery(insertSQL.toString()).setParameter("id", id)
				.setParameter("cat_title", cat).setParameter("lang", lang).setParameter("subCat", subCat);
                
                logger.info("Adding Subcategory");
		logger.info("Cat: {},\tLang: {},\tQuery: {}",cat, lang, query.toString());
		
		return (int) query.uniqueResult();
//                return query.executeUpdate();
        }
        
	/**
	 * Insert item
	 * 
	 * @param item
	 * @param lang
	 * @param subCat
	 * @return
	 */
//	public int createLocalizeSubCategory(int item, String lang, String subCat) {
//		StringBuilder insertSQL = new StringBuilder();
//
//		insertSQL.append("INSERT INTO utils.itm_scats");
//		insertSQL.append(" (item, code, sub_name)");
//		insertSQL.append(" VALUES (:item, :code, :sub_name)");
//		
//		Query query = getCurrentSession().createSQLQuery(insertSQL.toString()).setParameter("item", item)
//				.setParameter("code", lang).setParameter("sub_name", subCat);
//		
//		logger.debug("Lang: {},\tQuery: {}", lang, query.toString());
//		
//		return query.executeUpdate();
//	}

	private void getSubCategorySQL(StringBuilder querySQL) {
		querySQL.append("SELECT ");
		querySQL.append("id,");
		querySQL.append("lang,");
		querySQL.append("parent_cat as parentcat,");
		querySQL.append("cat_icon as caticon,");
		querySQL.append("sub_name as subname,");
		querySQL.append("definition as definition");
		querySQL.append(" FROM ");
		querySQL.append(view);
	}

	/**
	 * Update sub category
	 * 
	 * @param cat
	 * @param lang
	 * @param oldSubCat
	 * @param newSubCat
	 * @return
	 */
	public int updateLocalizeSubCategory(String cat, String lang, String oldSubCat, String newSubCat) {
		StringBuilder execSQL = new StringBuilder();

		execSQL.append("UPDATE els.subcats_comb_v");
		execSQL.append(" SET sub_name = :new_sub_name");
		execSQL.append(" WHERE sub_name = :old_sub_name");
		execSQL.append(" AND lang = :lang");
		execSQL.append(" AND cat_title = :cat_title");

		Query query = getCurrentSession().createSQLQuery(execSQL.toString())
				.setParameter("new_sub_name", newSubCat)
				.setParameter("old_sub_name", oldSubCat)
				.setParameter("lang", lang)
				.setParameter("cat_title", cat);

		logger.info("Update Subcategory");
		logger.info("Old Sub Cat: {},\tNew Sub Cat: {},\tLang: {},\tQuery: {}", oldSubCat, newSubCat, lang, query.toString());

		return query.executeUpdate();
	}

	/**
	 * Delete sub-category
	 * @param cat
	 * @param lang
	 * @param subCat
	 * @return
	 */
	public int deleteLocalizeSubCategory(String cat, String lang, String subCat) {
		StringBuilder execSQL = new StringBuilder();

		execSQL.append("DELETE FROM els.subcats_comb_v");
		execSQL.append(" WHERE sub_name = :sub_name");
		execSQL.append(" AND lang = :lang");
		execSQL.append(" AND cat_title = :cat_title");

		Query query = getCurrentSession().createSQLQuery(execSQL.toString())
				.setParameter("sub_name", subCat)
				.setParameter("lang", lang)
				.setParameter("cat_title", cat);

		logger.info("Update Subcategory");
		logger.info("Sub Cat: {},\tLang: {},\tQuery: {}", subCat, lang, query.toString());

		return query.executeUpdate();
	}
}
