package com.xaphe.xleap.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.xaphe.xleap.dao.localize.LocalizeCategoriesDao;
import com.xaphe.xleap.dao.localize.LocalizeCompaniesDao;
import com.xaphe.xleap.dao.localize.LocalizeItemsDao;
import com.xaphe.xleap.dao.localize.LocalizeLocaleDao;
import com.xaphe.xleap.dao.localize.LocalizeSubCategoriesDao;
import com.xaphe.xleap.dto.LocalizeCategoriesDto;
import com.xaphe.xleap.dto.LocalizeCompanyDto;
import com.xaphe.xleap.dto.LocalizeLocaleDto;
import com.xaphe.xleap.dto.LocalizeSubCategoriesDto;
import com.xaphe.xleap.model.ItemModelView;
import com.xaphe.xleap.utils.StringUtil;

/**
 * @author bang nguyen
 */
@Controller
@RequestMapping("/admin/list")
public class ListItemAdminController {
	private static final String PRIMARY_LANG = "en_US";
	private static final String SECOND_LANG = "vi_VN";

	@Autowired
	private LocalizeCategoriesDao categoryDao;
	@Autowired
	private LocalizeSubCategoriesDao subCategoriesDao;
	@Autowired
	private LocalizeCompaniesDao companyDao;
	@Autowired
	private LocalizeItemsDao localizeItemsDao;
	@Autowired
	private LocalizeLocaleDao localizeLocaleDao;

	// all item edit page for admin
	@RequestMapping(value = "/update", method = RequestMethod.GET)
	public ModelAndView listAllItem(Locale locale, @ModelAttribute("itemModelView") ItemModelView itemModelView) {
		itemModelView.setLocale1(PRIMARY_LANG);
		itemModelView.setLocale2(SECOND_LANG);

		ModelAndView modelView = preparedData(itemModelView, locale, "all-item-edit");
		modelView.addObject("itemModelView", itemModelView);

		return modelView;
	}

	/**
	 * Prepared data
	 * 
	 * @param itemModelView
	 * @param viewName
	 * @return ModelAndView
	 */
	private ModelAndView preparedData(ItemModelView itemModelView, Locale locale, String viewName) {
		ModelAndView modelView = new ModelAndView(viewName);
		modelView.addObject("lang", locale.toString());

		List<LocalizeCategoriesDto> categories = categoryDao.getLocalizeCategoryList();
		List<LocalizeCompanyDto> companies1 = companyDao.getLocalizeCompanyListByLang(PRIMARY_LANG);
		List<LocalizeCompanyDto> companies2 = companyDao.getLocalizeCompanyListByLang(SECOND_LANG);
		List<LocalizeLocaleDto> locales = localizeLocaleDao.getAllLocale();

		List<LocalizeSubCategoriesDto> subCategories1 = new ArrayList<LocalizeSubCategoriesDto>();
		if (StringUtil.hasLength(itemModelView.getCategory())) {
			subCategories1 = subCategoriesDao.getLocalizeSubCategoryByCategoryName(itemModelView.getCategory(),
					PRIMARY_LANG);
		}
		List<LocalizeSubCategoriesDto> subCategories2 = new ArrayList<LocalizeSubCategoriesDto>();
		if (StringUtil.hasLength(itemModelView.getCategory())) {
			subCategories2 = subCategoriesDao.getLocalizeSubCategoryByCategoryName(itemModelView.getCategory(),
					SECOND_LANG);
		}

		modelView.addObject("companies1", companies1);
		modelView.addObject("companies2", companies2);
		modelView.addObject("categories", categories);
		modelView.addObject("subCategories1", subCategories1);
		modelView.addObject("subCategories2", subCategories2);
		modelView.addObject("locales", locales);

		return modelView;
	}
}