/**
 * 
 */
package com.xaphe.xleap.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.xaphe.xleap.dao.ItemDao;
import com.xaphe.xleap.dao.localize.LocalizeCategoriesDao;
import com.xaphe.xleap.dao.localize.LocalizeCompaniesDao;
import com.xaphe.xleap.dao.localize.LocalizeItemsDao;
import com.xaphe.xleap.dao.localize.LocalizeLocaleDao;
import com.xaphe.xleap.dao.localize.LocalizeSubCategoriesDao;
import com.xaphe.xleap.dto.LocalizeCategoriesDto;
import com.xaphe.xleap.dto.LocalizeCompanyDto;
import com.xaphe.xleap.dto.LocalizeItemDto;
import com.xaphe.xleap.dto.LocalizeLocaleDto;
import com.xaphe.xleap.dto.LocalizeSubCategoriesDto;
import com.xaphe.xleap.entity.Items;
import com.xaphe.xleap.model.ItemModelView;
import com.xaphe.xleap.service.ItemAdminService;
import com.xaphe.xleap.utils.FileUtil;
import com.xaphe.xleap.utils.StringUtil;
import com.xaphe.xleap.validator.ItemsValidator;

/**
 * Item administrator controller
 * 
 * @author son-vo
 */
@Controller
@RequestMapping("/admin/item")
public class ItemAdminController {
	private static final String PRIMARY_LANG = "en_US";
	private static final String SECOND_LANG = "vi_VN";
	private static final String ADMIN_ITEM_UPDATE = "update";
	private static final String ADMIN_ITEM_ADD = "add";
	private static final Logger logger = LoggerFactory.getLogger(ItemAdminController.class);
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private LocalizeCategoriesDao categoryDao;
	@Autowired
	private LocalizeSubCategoriesDao subCategoriesDao;
	@Autowired
	private LocalizeCompaniesDao companyDao;
	@Autowired
	private ItemDao itemDao;
	@Autowired
	private LocalizeItemsDao localizeItemsDao;
	@Autowired
	private ItemAdminService itemAdminService;
	@Autowired
	private LocalizeLocaleDao localizeLocaleDao;
	@Value("${upload.image.loc}")
	private String uploadImageLocation;

	/**
	 * Prepared data
	 * @param itemModelView 
	 * @param viewName
	 * @return ModelAndView
	 */
	private ModelAndView preparedData(ItemModelView itemModelView, Locale locale, String viewName) {
		ModelAndView modelView = new ModelAndView(viewName);
		modelView.addObject("lang", locale.toString());

		List<LocalizeCategoriesDto> categories = categoryDao.getLocalizeCategoryList();
		List<LocalizeCompanyDto> companies1 = companyDao.getLocalizeCompanyListByLang(PRIMARY_LANG);
		List<LocalizeCompanyDto> companies2 = companyDao.getLocalizeCompanyListByLang(SECOND_LANG);
		List<LocalizeLocaleDto> locales = localizeLocaleDao.getAllLocale();

		List<LocalizeSubCategoriesDto> subCategories1 = new ArrayList<LocalizeSubCategoriesDto>();
		if (StringUtil.hasLength(itemModelView.getCategory())) {
			subCategories1 = subCategoriesDao.getLocalizeSubCategoryByCategoryName(itemModelView.getCategory(), PRIMARY_LANG);
		}
		List<LocalizeSubCategoriesDto> subCategories2 = new ArrayList<LocalizeSubCategoriesDto>();
		if (StringUtil.hasLength(itemModelView.getCategory())) {
			subCategories2 = subCategoriesDao.getLocalizeSubCategoryByCategoryName(itemModelView.getCategory(), SECOND_LANG);
		}
		Map<String, String> cost = new LinkedHashMap<String, String>();
		cost.put("9", messageSource.getMessage("cost.undefined", new String[] { }, locale));
		cost.put("0", messageSource.getMessage("cost.free", new String[] { }, locale));
		cost.put("1", messageSource.getMessage("cost.cheap", new String[] { }, locale));
		cost.put("3", messageSource.getMessage("cost.medium", new String[] { }, locale));
		cost.put("5", messageSource.getMessage("cost.premium", new String[] { }, locale));

		modelView.addObject("companies1", companies1);
		modelView.addObject("companies2", companies2);
		modelView.addObject("categories", categories);
		modelView.addObject("subCategories1", subCategories1);
		modelView.addObject("subCategories2", subCategories2);
		modelView.addObject("locales", locales);
		modelView.addObject("costList", cost);

		return modelView;
	}

	@RequestMapping(value = "", method = RequestMethod.GET)
	private ModelAndView itemIndex(Locale locale,
			@ModelAttribute("itemModelView") ItemModelView itemModelView,
			@RequestParam(required = false, value = "cat", defaultValue = "0") String cat,
			@RequestParam(required = false, value = "itemId", defaultValue = "0") int itemId) {
		itemModelView.setLocale1(PRIMARY_LANG);
		itemModelView.setLocale2(SECOND_LANG);
		// Edit mode
		if (StringUtil.hasLength(cat) && itemId != 0) {
			// Get locale1 description
			LocalizeItemDto localizeItemDto = localizeItemsDao.getItemByIdAndLocale(itemId, itemModelView.getLocale1());
			if (localizeItemDto != null) {
				itemModelView.setItemId(itemId);
				itemModelView.setWeight(String.valueOf(localizeItemDto.getWeight()));
				itemModelView.setCost(localizeItemDto.getCost().toPlainString());
				itemModelView.setCategory(localizeItemDto.getCatTitle());
				String subcats = localizeItemDto.getSubcats();
				if (StringUtil.hasLength(subcats)) {
					List<String> subcatList = new ArrayList<String>();
					StringTokenizer tokenizer = new StringTokenizer(subcats, "\\|");
					while (tokenizer.hasMoreTokens()) {
						subcatList.add(tokenizer.nextToken().trim());
					}
					itemModelView.setSubCategory1(subcatList);
				}
				itemModelView.setCompany1(localizeItemDto.getTitle());
				itemModelView.setLocale1(localizeItemDto.getLang());
				itemModelView.setShortDesc1(localizeItemDto.getShrtDesc());
				itemModelView.setLongDesc1(localizeItemDto.getDescription());
				itemModelView.setImage(localizeItemDto.getItmImg());
			}
			// Get locale2 description
			localizeItemDto = localizeItemsDao.getItemByIdAndLocale(itemId, itemModelView.getLocale2());
			if (localizeItemDto != null) {
				String subcats = localizeItemDto.getSubcats();
				if (StringUtil.hasLength(subcats)) {
					List<String> subcatList = new ArrayList<String>();
					StringTokenizer tokenizer = new StringTokenizer(subcats, "\\|");
					while (tokenizer.hasMoreTokens()) {
						subcatList.add(tokenizer.nextToken().trim());
					}
					itemModelView.setSubCategory2(subcatList);
				}
				itemModelView.setCompany2(localizeItemDto.getTitle());
				itemModelView.setLocale2(localizeItemDto.getLang());
				itemModelView.setShortDesc2(localizeItemDto.getShrtDesc());
				itemModelView.setLongDesc2(localizeItemDto.getDescription());
			}
		}
		ModelAndView modelView = preparedData(itemModelView, locale, "item-create");
		if (StringUtil.hasLength(cat) && itemId != 0) {
			modelView.addObject("actionMode", ADMIN_ITEM_UPDATE);
		} else {
			modelView.addObject("actionMode", ADMIN_ITEM_ADD);
		}
		modelView.addObject("itemModelView", itemModelView);

		return modelView;
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	private ModelAndView addNewItem(Locale locale,
			@ModelAttribute("itemModelView") final ItemModelView itemModelView,
			BindingResult result) {
		Map<String, String> messages = new HashMap<String, String>();
		ItemsValidator itemsValidator = new ItemsValidator();
		itemsValidator.validate(itemModelView, result);
		if (result.hasErrors()) {
			ModelAndView modelView = preparedData(itemModelView, locale, "item-create");
			modelView.addObject("actionMode", ADMIN_ITEM_ADD);
			messages.put("error", "message.input.required");
			modelView.addObject("messages", messages);
			return modelView;
		}
		if (StringUtil.hasLength(itemModelView.getUploadType())) {
			if ("0".equals(itemModelView.getUploadType())) {
				if (itemModelView.getFile() != null && itemModelView.getFile().getSize() > 0) {
					uploadFile(itemModelView);
				}
			} else if ("1".equals(itemModelView.getUploadType())) {
				if (StringUtil.hasLength(itemModelView.getImage())) {
					String fileName = StringUtil.getItemImageName();
					FileUtil.downloadImageFromUrl(itemModelView.getImage(), uploadImageLocation, fileName);
					itemModelView.setImage(fileName + FileUtil.JPG_EXT);
				}
			}
		}
		// Insert item
		try {
			itemAdminService.createItem(locale.toString(), itemModelView);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			ModelAndView modelView = preparedData(itemModelView, locale, "item-create");
			modelView.addObject("actionMode", ADMIN_ITEM_ADD);
			messages.put("error", "message.error.save");
			modelView.addObject("messages", messages);
			return modelView;
		}
		ModelAndView modelView = preparedData(itemModelView, locale, "item-create");
		modelView.addObject("itemModelView", new ItemModelView());
		modelView.addObject("actionMode", ADMIN_ITEM_ADD);
		messages.put("success", "message.success.save");
		modelView.addObject("messages", messages);
		return modelView;
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	private ModelAndView updateNewItem(Locale locale,
			@ModelAttribute("itemModelView") ItemModelView itemModelView,
			BindingResult result, 
			@RequestParam(required = false, value = "src", defaultValue = "item-create") String srcPage) {
		Map<String, String> messages = new HashMap<String, String>();
		// Check input
		ItemsValidator itemsValidator = new ItemsValidator();
		itemsValidator.validate(itemModelView, result);
		if (result.hasErrors()) {
			ModelAndView modelView = preparedData(itemModelView, locale, srcPage);
			modelView.addObject("actionMode", ADMIN_ITEM_UPDATE);
			messages.put("error", "message.input.required");
			modelView.addObject("messages", messages);
			return modelView;
		}
		if (StringUtil.hasLength(itemModelView.getUploadType())) {
			if ("0".equals(itemModelView.getUploadType())) {
				if (itemModelView.getFile() != null && itemModelView.getFile().getSize() > 0) {
					uploadFile(itemModelView);
				}
			} else if ("1".equals(itemModelView.getUploadType())) {
				if (StringUtil.hasLength(itemModelView.getImage())) {
					String fileName = StringUtil.getItemImageName();
					FileUtil.downloadImageFromUrl(itemModelView.getImage(), uploadImageLocation, fileName);
					itemModelView.setImage(fileName + FileUtil.JPG_EXT);
				}
			}
		}
		// Update item
		try {
			itemAdminService.updateItem(locale.toString(), itemModelView);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			ModelAndView modelView = preparedData(itemModelView, locale, srcPage);
			modelView.addObject("actionMode", ADMIN_ITEM_UPDATE);
			messages.put("error", "message.error.save");
			modelView.addObject("messages", messages);
			return modelView;
		}
		ModelAndView modelView = new ModelAndView();
            switch (srcPage) {
                case "item-create":
                    modelView.setViewName("redirect:/item");
                    break;
                case "all-item-edit":
                    modelView.setViewName("redirect:/admin/list/update");
                    break;
                default:
                    modelView.setViewName("redirect:/admin/edit");
                    break;
            }
		
		modelView.addObject("itemModelView", new ItemModelView());
		messages.put("success", "message.success.save");
		modelView.addObject("messages", messages);
		return modelView;
	}
	
	//remove item by update visible status
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	public ModelAndView deleteItem(@PathVariable int id) {
		ModelAndView modelAndView = new ModelAndView("redirect:/item");
		Items item = itemDao.findById(id);
		if (item != null) {
			item.setVisible(!item.getVisible());
			itemDao.update(item);
		}

		String message = "Item was successfully deleted.";
		modelAndView.addObject("message", message);
		return modelAndView;
	}

	/**
	 * Update file
	 * @param file
	 */
	private void uploadFile(ItemModelView itemModelView) {
		MultipartFile file = itemModelView.getFile();
		String fileName = StringUtil.getItemImageName() + FileUtil.JPG_EXT;
		FileUtil.writeUploadedFile(file, uploadImageLocation, fileName/*file.getOriginalFilename()*/);
		itemModelView.setImage(fileName/*file.getOriginalFilename()*/);
	}

	@RequestMapping(value = { "/loadImage/{fileName:.+}",
			"/loadImage/{fileName}/" })
	public void getImage(HttpServletResponse response,
			@PathVariable String fileName) {
		String fullPath = uploadImageLocation;// FileUtil.UPLOAD_IMAGES_LOC;
		if (StringUtil.hasLength(fileName)) {
			//fullPath = FileUtil.UPLOAD_IMAGES_LOC + fileName;
			fullPath = uploadImageLocation + fileName;
		}
		FileInputStream inputStream = null;
		OutputStream outStream = null;
		try {
			File downloadFile = new File(fullPath);
			inputStream = new FileInputStream(downloadFile);

			response.setContentType("image/jpeg");
			response.setContentLength((int) downloadFile.length());
			// get output stream of the response

			outStream = response.getOutputStream();
			byte[] buffer = new byte[FileUtil.BUFFER_SIZE];
			int bytesRead = -1;

			// write bytes read from the input stream into the output stream
			while ((bytesRead = inputStream.read(buffer)) != -1) {
				outStream.write(buffer, 0, bytesRead);
			}
		} catch (IOException e) {
			logger.error("Cannot upload file due to IO exception. Error: {}", e.getMessage());
		} finally {
			try {
				if (inputStream != null) {
					inputStream.close();
				}
				if (outStream != null) {
					outStream.close();
				}
			} catch (IOException e) {
				logger.error(e.getMessage());
			}
		}
	}
	
	//delete item forever - use with caution
	@RequestMapping(value = "/remove/{id}", method = RequestMethod.GET)
	public ModelAndView removeItem(@PathVariable int id) {
		ModelAndView modelAndView = new ModelAndView("redirect:/item");
		
		itemDao.deleteById(id);

		String message = "Item was successfully removed.";
		modelAndView.addObject("message", message);
		return modelAndView;
	}
}
