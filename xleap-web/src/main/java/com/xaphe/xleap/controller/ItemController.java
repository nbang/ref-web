/**
 * 
 */
package com.xaphe.xleap.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.xaphe.xleap.dao.localize.LocalizeCategoriesDao;
import com.xaphe.xleap.dao.localize.LocalizeFullItemsDao;
import com.xaphe.xleap.dao.localize.LocalizeItemsDao;
import com.xaphe.xleap.dto.LocalizeCategoriesDto;
import com.xaphe.xleap.dto.LocalizeFullItemDto;
import com.xaphe.xleap.utils.FileUtil;
import com.xaphe.xleap.utils.StringUtil;

/**
 * @author bang nguyen
 * 
 */
@Controller
@RequestMapping("/item")
public class ItemController {
	private static Logger logger = LoggerFactory.getLogger(ItemController.class);
	
	@Autowired
	private LocalizeCategoriesDao categoryDao;
	@Autowired
	private LocalizeItemsDao localizeItemsDao;
	@Autowired
	private LocalizeFullItemsDao localizeFullItemsDao;
	@Value("${item.max_result}")
	private int maxResult;
	@Value("${upload.image.loc}")
	private String uploadImageLocation;
        
         private String currLang = "vi_VN";

	@RequestMapping(method = RequestMethod.GET)
	public String listItem(Locale locale, Model model,
			@RequestParam(required = false, value = "cat", defaultValue = "") String category,
			@RequestParam(required = false, value = "page", defaultValue = "1") int page) {

		logger.debug("Category = {},\tPage = {}" , category, page);
                
               
		
		if (page < 1) page = 1;
		
		if (!model.containsAttribute("categories")) {
			List<LocalizeCategoriesDto> categories = categoryDao.getLocalizeCategoryList();
			model.addAttribute("categories", categories);
		}

		List<LocalizeFullItemDto> itemList = null;
		int totalRecord = 0;

		if (StringUtil.hasLength(category)) {
			itemList = localizeFullItemsDao.getAllItemByCat(locale.toString(), category, (page - 1) * maxResult, maxResult);
			totalRecord = localizeItemsDao.getAllItemCountByCategory(locale.toString(), category);
		} else {
			itemList = localizeFullItemsDao.getAllItem(locale.toString(), (page - 1) * maxResult, maxResult);
			totalRecord = localizeItemsDao.getAllItemCount(locale.toString());
		}
		
		int totalPage = calculateTotalPage(totalRecord);

		logger.debug("Total Record Fetched = {},\tTotal Page = {}", totalRecord, totalPage);
		
		model.addAttribute("itemList", itemList);
		model.addAttribute("totalPage", totalPage);
                
                model.addAttribute("currLang", currLang);
                model.addAttribute("currCat", category);
                model.addAttribute("currPage", page);

		if (page < totalPage)
			model.addAttribute("nextPage", page + 1);
		if (page > 1)
			model.addAttribute("prevPage", page - 1);

		return "item-list";
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public String detailItem(Locale locale, Model model, @PathVariable int id) {
		LocalizeFullItemDto item = localizeFullItemsDao.getItemById(id, locale.toString());
		model.addAttribute("item", item);

		return "item-detail";
	}
	
	private int calculateTotalPage(int totalRecord) {
		return (totalRecord - 1) / maxResult + 1;
	}

	@RequestMapping(value = "doSearch", method = RequestMethod.GET, produces = "text/plain; charset=utf-8")
	public String searchItems(Locale locale, Model model,
			@RequestParam(required = false, value = "cat", defaultValue = "") String searchString,
			@RequestParam(required = false, value = "page", defaultValue = "1") int page) {

		logger.debug("Search String = {},\tPage = {}" , searchString, page);
		
		if (page < 1) page = 1;

		if (!model.containsAttribute("categories")) {
			List<LocalizeCategoriesDto> categories = categoryDao.getLocalizeCategoryList();
			model.addAttribute("categories", categories);
		}

		List<LocalizeFullItemDto> itemList = null;
		int totalRecord = 0;

		StringBuilder newSearchString = new StringBuilder();
		StringTokenizer tokenizer = new StringTokenizer(searchString, " ");
		while (tokenizer.hasMoreTokens()) {
			if (StringUtil.hasLength(newSearchString.toString())) {
				newSearchString.append("|");
			}
			newSearchString.append(tokenizer.nextToken().trim());
		}

		itemList = localizeFullItemsDao.getItems(locale.toString(), newSearchString.toString(), (page - 1) * maxResult, maxResult);
		totalRecord = localizeFullItemsDao.countItemBySearchCondition(locale.toString(), newSearchString.toString());

		int totalPage = calculateTotalPage(totalRecord);

		logger.debug("Total Record Fetched = {},\tTotal Page = {}", totalRecord, totalPage);
		
		
		model.addAttribute("itemList", itemList);
		model.addAttribute("totalPage", totalPage);
		
		if (page < totalPage)
			model.addAttribute("nextPage", page + 1);
		if (page > 1)
			model.addAttribute("prevPage", page - 1);

		return "item-list";
	}

	@RequestMapping(value = { "/loadImage/{fileName:.+}",
			"/loadImage/{fileName}/" })
	public void getImage(HttpServletResponse response,
			@PathVariable String fileName) {
		String fullPath = uploadImageLocation;// FileUtil.UPLOAD_IMAGES_LOC;
		if (StringUtil.hasLength(fileName)) {
			//fullPath = FileUtil.UPLOAD_IMAGES_LOC + fileName;
			fullPath = uploadImageLocation + fileName;
		}
		FileInputStream inputStream = null;
		OutputStream outStream = null;
		try {
			File downloadFile = new File(fullPath);
			inputStream = new FileInputStream(downloadFile);

			response.setContentType("image/jpeg");
			response.setContentLength((int) downloadFile.length());
			// get output stream of the response

			outStream = response.getOutputStream();
			byte[] buffer = new byte[FileUtil.BUFFER_SIZE];
			int bytesRead = -1;

			// write bytes read from the input stream into the output stream
			while ((bytesRead = inputStream.read(buffer)) != -1) {
				outStream.write(buffer, 0, bytesRead);
			}
		} catch (IOException e) {
			logger.error("Cannot upload file due to IO exception. Error: {}", e.getMessage());
		} finally {
			try {
				if (inputStream != null) {
					inputStream.close();
				}
				if (outStream != null) {
					outStream.close();
				}
			} catch (IOException e) {
				logger.error(e.getMessage());
			}
		}
	}
}
