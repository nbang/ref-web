/**
 * 
 */
package com.xaphe.xleap.controller;

import java.util.Locale;

import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.xaphe.xleap.dao.localize.LocalizeCitiesDao;
import com.xaphe.xleap.dao.localize.LocalizeDistrictsDao;
import com.xaphe.xleap.dao.localize.LocalizeStreetsDao;
import com.xaphe.xleap.dao.localize.LocalizeWardsDao;
import com.xaphe.xleap.dto.JsonResponse;
import com.xaphe.xleap.dto.LocalizeCitiesDto;
import com.xaphe.xleap.dto.LocalizeDistrictsDto;
import com.xaphe.xleap.dto.LocalizeStreetsDto;
import com.xaphe.xleap.dto.LocalizeWardsDto;
import com.xaphe.xleap.model.StreetbindModelView;
import com.xaphe.xleap.utils.StringUtil;

/**
 * Street administrator controller
 * 
 * @author son-vo
 */
@Controller
@RequestMapping("/admin/streetbind")
public class StreetbindAdminController {
	private static Logger logger = LoggerFactory.getLogger(StreetbindAdminController.class);
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private LocalizeCitiesDao citiesDao;
	@Autowired
	private LocalizeDistrictsDao districtsDao;
	@Autowired
	private LocalizeWardsDao wardsDao;
	@Autowired
	private LocalizeStreetsDao streetsDao;

	/**
	 * Prepared data
	 * @param viewName
	 * @return ModelAndView
	 */
	private ModelAndView preparedData(String viewName) {
		ModelAndView modelView = new ModelAndView();
		modelView.setViewName(viewName);
		StreetbindModelView streetbindModelView = new StreetbindModelView();
		modelView.addObject("streetbindModelView", streetbindModelView);
		return modelView;
	}

	@RequestMapping(value = "", method = RequestMethod.GET)
	public ModelAndView index(@RequestParam(required = false, value = "type", defaultValue = "city") String type) {
		ModelAndView modelView = preparedData("streetbind-create");
		if ("city".equals(type)) {
                    logger.warn("Adding City");
                    logger.info("Addding City..");
			modelView.addObject("streetLabel", "label.city");
		} else if ("district".equals(type)) {
			modelView.addObject("streetLabel", "label.district");
		} else if ("ward".equals(type)) {
			modelView.addObject("streetLabel", "label.ward");
		} else if ("street".equals(type)) {
			modelView.addObject("streetLabel", "label.street");
		}
		StreetbindModelView streetbindModelView = new StreetbindModelView();
		streetbindModelView.setType(type);
		modelView.addObject("streetbindModelView", streetbindModelView);
		return modelView;
	}

	@RequestMapping(value = "edit", method = RequestMethod.GET)
	public ModelAndView editCompany(
			@ModelAttribute("streetbindModelView") StreetbindModelView streetbindModelView) {
		ModelAndView modelView = preparedData("all-company-edit");
		return modelView;
	}

	@RequestMapping(value = "/ajax/save", method = RequestMethod.POST)
	@ResponseBody
	public JsonResponse addNewStreet(
			@ModelAttribute("streetbindModelView") StreetbindModelView streetbindModelView,
			Locale locale,
			BindingResult result) {
		// Check input
		JsonResponse res = new JsonResponse();
		if (!StringUtil.hasLength(streetbindModelView.getName())) {
			if ("city".equals(streetbindModelView.getType())) {
				ValidationUtils.rejectIfEmpty(result, "name", messageSource.getMessage("city.input.required", new String[] { }, locale));
			} else if ("district".equals(streetbindModelView.getType())) {
				ValidationUtils.rejectIfEmpty(result, "name", messageSource.getMessage("district.input.required", new String[] { }, locale));
			} else if ("ward".equals(streetbindModelView.getType())) {
				ValidationUtils.rejectIfEmpty(result, "name", messageSource.getMessage("ward.input.required", new String[] { }, locale));
			} else if ("street".equals(streetbindModelView.getType())) {
				ValidationUtils.rejectIfEmpty(result, "name", messageSource.getMessage("street.input.required", new String[] { }, locale));
			}
		}
		// Check input
		if (result.hasErrors()) {
			res.setStatus("INPUT_FAILED");
			res.setResult(result.getAllErrors());
		} else {
			try {
				if ("city".equals(streetbindModelView.getType())) {
					LocalizeCitiesDto cities = new LocalizeCitiesDto();
					cities.setName(streetbindModelView.getName());
					cities.setShortcode(streetbindModelView.getShortcode().toUpperCase());
					citiesDao.createCity(cities);
				} else if ("district".equals(streetbindModelView.getType())) {
					LocalizeDistrictsDto districts = new LocalizeDistrictsDto();
					districts.setDistrict(streetbindModelView.getName());
					districtsDao.createDistrict(districts);
				} else if ("ward".equals(streetbindModelView.getType())) {
					LocalizeWardsDto wards = new LocalizeWardsDto();
					wards.setWard(streetbindModelView.getName());
					wardsDao.createWard(wards);
				} else if ("street".equals(streetbindModelView.getType())) {
					LocalizeStreetsDto streets = new LocalizeStreetsDto();
					streets.setStreet(streetbindModelView.getName());
					streetsDao.createStreet(streets);
				}
				res.setStatus("SUCCESS");
			} catch (ConstraintViolationException e) {
				logger.error(e.getMessage(), e);
				res.setStatus("FAILED");
				String message = messageSource.getMessage("error.input.exist", new String[] { streetbindModelView.getName() }, locale);
				result.reject(message);
				res.setResult(result.getAllErrors());
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
				res.setStatus("FAILED");
				result.reject(e.getMessage());
				res.setResult(result.getAllErrors());
			}
		}

		return res;
	}

	@RequestMapping(value = "/ajax/delete", method = RequestMethod.POST)
	@ResponseBody
	public JsonResponse deleteLocation(
			@ModelAttribute("streetbindModelView") StreetbindModelView streetbindModelView,
			Locale locale,
			BindingResult result) {
		JsonResponse res = new JsonResponse();
		try {
			if ("city".equals(streetbindModelView.getType())) {
				citiesDao.deleteCity(streetbindModelView.getName());
			} else if ("district".equals(streetbindModelView.getType())) {
				districtsDao.deleteDistrict(streetbindModelView.getName());
			} else if ("ward".equals(streetbindModelView.getType())) {
				wardsDao.deleteWard(streetbindModelView.getName());
			} else if ("street".equals(streetbindModelView.getType())) {
				streetsDao.deleteStreet(streetbindModelView.getName());
			}
			res.setStatus("SUCCESS");
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			res.setStatus("FAILED");
			result.reject(e.getMessage());
			res.setResult(result.getAllErrors());
		}
		return res;
	}
}
