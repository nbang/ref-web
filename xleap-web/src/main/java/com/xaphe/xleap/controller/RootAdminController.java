package com.xaphe.xleap.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xaphe.xleap.dao.localize.LocalizeItemsDao;
import com.xaphe.xleap.dto.LocalizeItemDto;
import com.xaphe.xleap.model.ItemEditModelView;
import com.xaphe.xleap.utils.StringUtil;

/**
 * @author son-vo
 */
@Controller
@RequestMapping("/admin")
public class RootAdminController {
	@Autowired
	private LocalizeItemsDao localizeItemsDao;

	//auto redirect to create new item page
	@RequestMapping(value = "", method = RequestMethod.GET)
	public String index(Model model) {
		return "redirect:/admin/item";
	}
	
	//list item in admin mode
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public String listItem(Model model) {
		return "redirect:/";
	}

	//all item edit page for admin
	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public String listAllItem(Locale locale, Model model) {
		List<LocalizeItemDto> itemListEN = localizeItemsDao.getAllItem("en_US", 0, 100);
		List<LocalizeItemDto> itemListVN = localizeItemsDao.getAllItem("vi_VN", 0, 100);
		
		List<ItemEditModelView> itemList = new ArrayList<ItemEditModelView>(100);
		
		for (int i = 0; i < itemListEN.size(); i++) {
			ItemEditModelView item = new ItemEditModelView();
			item.setId(itemListEN.get(i).getId());
			item.setTitle(itemListEN.get(i).getTitle());
			item.setWeight(itemListEN.get(i).getWeight());
			item.setHasShrtDescEN(StringUtil.hasLength(itemListEN.get(i).getShrtDesc()));
			item.setHasLongDescEN(StringUtil.hasLength(itemListEN.get(i).getDescription()));
			item.setHasShrtDescVN(StringUtil.hasLength(itemListVN.get(i).getShrtDesc()));
			item.setHasLongDescVN(StringUtil.hasLength(itemListVN.get(i).getDescription()));
			
			itemList.add(item);
		}
		
		model.addAttribute("itemList", itemList);
		return "all-item-edit";
	}
}