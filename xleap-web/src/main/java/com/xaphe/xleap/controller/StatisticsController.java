/**
 * 
 */
package com.xaphe.xleap.controller;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author son-vo
 * 
 */
@Controller
@RequestMapping("/stats")
public class StatisticsController {
	@RequestMapping(method = RequestMethod.POST)
	@ResponseStatus(value=HttpStatus.OK)
	public void postStatus(Model model, @RequestParam String data) {
		//applied some code to post data here 
	}
}
