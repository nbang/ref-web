/**
 * 
 */
package com.xaphe.xleap.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.xaphe.xleap.dao.localize.LocalizeCitiesDao;
import com.xaphe.xleap.dao.localize.LocalizeCompaniesDao;
import com.xaphe.xleap.dao.localize.LocalizeDistrictsDao;
import com.xaphe.xleap.dao.localize.LocalizeLocaleDao;
import com.xaphe.xleap.dao.localize.LocalizeStreetsDao;
import com.xaphe.xleap.dao.localize.LocalizeWardsDao;
import com.xaphe.xleap.dto.JsonResponse;
import com.xaphe.xleap.dto.LocalizeCitiesDto;
import com.xaphe.xleap.dto.LocalizeDistrictsDto;
import com.xaphe.xleap.dto.LocalizeLocaleDto;
import com.xaphe.xleap.dto.LocalizeStreetsDto;
import com.xaphe.xleap.dto.LocalizeWardsDto;
import com.xaphe.xleap.model.CompanyModelView;
import com.xaphe.xleap.service.CompanyAdminService;
import com.xaphe.xleap.utils.StringUtil;
import com.xaphe.xleap.validator.CompaniesValidator;

/**
 * Item administrator controller
 * 
 * @author son-vo
 */
@Controller
@RequestMapping("/admin/company")
public class CompanyAdminController {
	private static Logger logger = LoggerFactory.getLogger(CompanyAdminController.class);
	private static final String CITY_DEFAULT = "Hồ Chí Minh";
	private static final String DISTRICT_DEFAULT = "Quận 1";
	private static final String PRIMARY_LANG = "en_US";
	private static final String SECOND_LANG = "vi_VN";
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private LocalizeCitiesDao citiesDao;
	@Autowired
	private LocalizeDistrictsDao districtsDao;
	@Autowired
	private LocalizeWardsDao wardsDao;
	@Autowired
	private LocalizeStreetsDao streetsDao;
	@Autowired
	private LocalizeCompaniesDao localizeCompaniesDao;
	@Autowired
	private LocalizeLocaleDao localizeLocaleDao;
	@Autowired
	private CompanyAdminService companyAdminService;

	/**
	 * Prepared data
	 * @param viewName
	 * @return ModelAndView
	 */
	private ModelAndView preparedData(String viewName, CompanyModelView companyModelView) {
		List<LocalizeCitiesDto> cities = citiesDao.getCities();
		List<LocalizeDistrictsDto> districts = districtsDao.getDistricts();
		List<LocalizeWardsDto> wards = wardsDao.getWards();
		List<LocalizeStreetsDto> streets = streetsDao.getStreets();
		List<LocalizeLocaleDto> locales = localizeLocaleDao.getAllLocale();

		ModelAndView modelView = new ModelAndView();
		modelView.setViewName(viewName);
		modelView.addObject("cities", cities);
		modelView.addObject("districts", districts);
		modelView.addObject("wards", wards);
		modelView.addObject("streets", streets);
		modelView.addObject("locales", locales);
		if (companyModelView == null) {
			companyModelView = new CompanyModelView();
		}
		companyModelView.setLocale1(PRIMARY_LANG);
		companyModelView.setLocale2(SECOND_LANG);
		companyModelView.setCity(CITY_DEFAULT);
		companyModelView.setDistrict(DISTRICT_DEFAULT);
		modelView.addObject("companyModelView", companyModelView);
		return modelView;
	}

	@RequestMapping(value = "", method = RequestMethod.GET)
	public ModelAndView companyIndex() {
            logger.info("Getting Company Model");
		ModelAndView modelView = preparedData("company-create", null);
		return modelView;
	}

	@RequestMapping(value = "edit", method = RequestMethod.GET)
	public ModelAndView editCompany(
			@ModelAttribute("companyModelView") CompanyModelView companyModelView) {
		ModelAndView modelView = preparedData("all-company-edit", companyModelView);
		return modelView;
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public ModelAndView addNewCompany(
			@ModelAttribute("companyModelView") CompanyModelView companyModelView,
			BindingResult result) {
		// Check input
		CompaniesValidator companiesValidator = new CompaniesValidator();
		companiesValidator.validate(companyModelView, result);
		if (result.hasErrors()) {
			//ModelAndView modelView = preparedData("company-create");
			//return modelView;
		}
		try {
			companyAdminService.createLocalizeCompany(companyModelView);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			ModelAndView modelView = preparedData("company-create", companyModelView);
			return modelView;
		}

		ModelAndView modelView = new ModelAndView("redirect:/admin/item");
		return modelView;
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ModelAndView updateCompany(
			@ModelAttribute("companyModelView") CompanyModelView companyModelView,
			Locale locale,
			BindingResult result) {
            logger.info("Updating Company");
                logger.debug("Company Update Called. Model: {}, Locale: {}, BindinResult {}", companyModelView.toString(), locale.toLanguageTag(), result.toString());
		Map<String, String> messages = new HashMap<String, String>();
		ModelAndView modelView = new ModelAndView("redirect:edit");
		logger.debug("Checking input...");
		CompaniesValidator companiesValidator = new CompaniesValidator();
		companiesValidator.validate(companyModelView, result);
		if (result.hasErrors()) {
                    logger.debug("Result has Errors!");
			modelView = preparedData("all-company-edit", companyModelView);
			//modelView.setViewName("all-company-edit");;
			//messages.put("error", "message.input.required");
		} else {
                    logger.debug("Result is ok!");
			try {
				companyAdminService.updateLocalizeCompany(companyModelView);
				messages.put("success", "message.success.save");
			} catch (ConstraintViolationException e) {
				modelView = preparedData("all-company-edit", companyModelView);
				logger.error(e.getMessage(), e);
				messages.put("error", "message.save.error.duplicate.value");
			} catch (Exception e) {
				modelView = preparedData("all-company-edit", companyModelView);
				logger.error(e.getMessage(), e);
				messages.put("error", "message.error.save");
			}
		}
		modelView.addObject("messages", messages);
		return modelView;
	}
        
        @RequestMapping(value = "/ajax/update", method = RequestMethod.POST)
	@ResponseBody
	public JsonResponse ajaxUpdateCompany(
			@ModelAttribute("companyModelView") CompanyModelView companyModelView,
			Locale locale,
			BindingResult result) {
            logger.info("Updating Company");
            JsonResponse res = new JsonResponse();
                logger.debug("Company Update Called. Model: {}, Locale: {}, BindinResult {}", companyModelView.toString(), locale.toLanguageTag(), result.toString());
		//Map<String, String> messages = new HashMap<String, String>();
		//ModelAndView modelView = new ModelAndView("redirect:edit");
		logger.debug("Checking input...");
		CompaniesValidator companiesValidator = new CompaniesValidator();
		companiesValidator.validate(companyModelView, result);
		if (result.hasErrors()) {
                    logger.debug("Result has Errors!");
                    res.setStatus("UPDATE_FAILED");
                    res.setResult(result.getAllErrors());
			//modelView = preparedData("all-company-edit", companyModelView);
			//modelView.setViewName("all-company-edit");;
			//messages.put("error", "message.input.required");
		} else {
                    logger.debug("Result is ok!");
			try {
				companyAdminService.updateLocalizeCompany(companyModelView);
				//messages.put("success", "message.success.save");
                                res.setStatus("OK");
                                logger.debug("Company Data updated!");
			} catch (ConstraintViolationException e) {
				//modelView = preparedData("all-company-edit", companyModelView);
                            res.setStatus("UPDATE_FAILED");
                                res.setResult("Company Update Failed - " + e.getMessage());
				logger.error(e.getMessage(), e);
				//messages.put("error", "message.save.error.duplicate.value");
			} catch (Exception e) {
                                 res.setStatus("UPDATE_FAILED");
                                res.setResult("Company Update Failed - " + e.getMessage());
				//modelView = preparedData("all-company-edit", companyModelView);
				logger.error(e.getMessage(), e);
				//messages.put("error", "message.error.save");
			}
		}
		//modelView.addObject("messages", messages);
//		return modelView;
		return res;
	}

	@RequestMapping(value = "/ajax/check-input", method = RequestMethod.POST)
	@ResponseBody
	public JsonResponse checkInputCompanyAjax(
			@ModelAttribute("companyModelView") CompanyModelView companyModelView,
			Locale locale,
			BindingResult result) {

		JsonResponse res = new JsonResponse();
		try {
			validateInputForUpdate(companyModelView, locale, result);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			String message = messageSource.getMessage( "message.error.save", new String[] { }, locale);
			result.reject(message);
		}
		// Check input
		if (result.hasErrors()) {
			res.setStatus("INPUT_FAILED");
			res.setResult(result.getAllErrors());
		} else {
			res.setStatus("OK");
		}
		return res;
	}

	@RequestMapping(value = "/ajax/save", method = RequestMethod.POST)
	@ResponseBody
	public JsonResponse addNewCompanyAjax(
			@ModelAttribute("companyModelView") CompanyModelView companyModelView,
			Locale locale,
			BindingResult result) {

		JsonResponse res = new JsonResponse();
		try {
			validateInputForInsert(companyModelView, locale, result);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			String message = messageSource.getMessage( "message.error.save", new String[] { }, locale);
			result.reject(message);
		}
		// Check input
		if (result.hasErrors()) {
			res.setStatus("INPUT_FAILED");
			res.setResult(result.getAllErrors());
		} else {
			try {
				companyAdminService.createLocalizeCompany(companyModelView);
				res.setStatus("SUCCESS");
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
				//ValidationUtils.rejectIfEmpty(result, "saveError", messageSource.getMessage("message.error.save", new String[] { }, locale));
				res.setStatus("FAILED");
				res.setResult(result.getAllErrors());
			}
		}

		return res;
	}

	/**
	 * Check input value is correct
	 * @param companyModelView
	 * @param locale
	 * @param result
	 */
	private void validateInputForInsert(CompanyModelView companyModelView,
			Locale locale, BindingResult result) {
		if (companyModelView.getLocale1().equals(companyModelView.getLocale2())) {
			String message = messageSource.getMessage( "locale.input.notsame", new String[] { }, locale);
			result.rejectValue("locale1", message);
		}
		if (!StringUtil.hasLength(companyModelView.getTitleEng())) {
			ValidationUtils.rejectIfEmpty(result, "titleEng", messageSource.getMessage("title.eng.input.required", new String[] { }, locale));
		} else {
			boolean isValidTitleEng = localizeCompaniesDao.validateCompanyTitle(companyModelView.getTitleEng());
			if (!isValidTitleEng) {
				String message = messageSource.getMessage("error.value.existing", new String[] { companyModelView.getTitleEng() }, locale);
				result.rejectValue("titleEng", message);
			}
		}
		if (!StringUtil.hasLength(companyModelView.getTitleLocal())) {
			ValidationUtils.rejectIfEmpty(result, "titleLocal", messageSource.getMessage("title.local.input.required", new String[] { }, locale));
		} else {
			boolean isValidTitleLocal = localizeCompaniesDao.validateCompanyTitle(companyModelView.getTitleLocal());
			if (!isValidTitleLocal) {
				String message = messageSource.getMessage( "error.value.existing", new String[] { companyModelView.getTitleLocal() }, locale);
				result.rejectValue("titleLocal", message);
			}
		}
	}

	/**
	 * Check input value is correct
	 * @param companyModelView
	 * @param locale
	 * @param result
	 */
	private void validateInputForUpdate(CompanyModelView companyModelView,
			Locale locale, BindingResult result) {
		if (companyModelView.getLocale1().equals(companyModelView.getLocale2())) {
			String message = messageSource.getMessage( "locale.input.notsame", new String[] { }, locale);
			result.rejectValue("locale1", message);
		}
		if (!StringUtil.hasLength(companyModelView.getTitleEng())) {
			ValidationUtils.rejectIfEmpty(result, "titleEng", messageSource.getMessage("title.eng.input.required", new String[] { }, locale));
		} else {
			boolean isValidTitleEng = localizeCompaniesDao.validateCompanyTitle(companyModelView.getCompanyId(), companyModelView.getTitleEng());
			if (!isValidTitleEng) {
				String message = messageSource.getMessage("error.value.existing", new String[] { companyModelView.getTitleEng() }, locale);
				result.rejectValue("titleEng", message);
			}
		}
		if (!StringUtil.hasLength(companyModelView.getTitleLocal())) {
			ValidationUtils.rejectIfEmpty(result, "titleLocal", messageSource.getMessage("title.local.input.required", new String[] { }, locale));
		} else {
			boolean isValidTitleLocal = localizeCompaniesDao.validateCompanyTitle(companyModelView.getCompanyId(), companyModelView.getTitleLocal());
			if (!isValidTitleLocal) {
				String message = messageSource.getMessage( "error.value.existing", new String[] { companyModelView.getTitleLocal() }, locale);
				result.rejectValue("titleLocal", message);
			}
		}
	}

}
