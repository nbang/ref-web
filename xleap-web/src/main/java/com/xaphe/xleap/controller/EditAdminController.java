/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.xaphe.xleap.controller;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.xaphe.xleap.dao.ItemDao;
import com.xaphe.xleap.dao.localize.LocalizeCategoriesDao;
import com.xaphe.xleap.dao.localize.LocalizeCitiesDao;
import com.xaphe.xleap.dao.localize.LocalizeCompaniesDao;
import com.xaphe.xleap.dao.localize.LocalizeDistrictsDao;
import com.xaphe.xleap.dao.localize.LocalizeItemsDao;
import com.xaphe.xleap.dao.localize.LocalizeLocaleDao;
import com.xaphe.xleap.dao.localize.LocalizeStreetsDao;
import com.xaphe.xleap.dao.localize.LocalizeSubCategoriesDao;
import com.xaphe.xleap.dao.localize.LocalizeWardsDao;
import com.xaphe.xleap.dto.JsonResponse;
import com.xaphe.xleap.dto.LocalizeCategoriesDto;
import com.xaphe.xleap.dto.LocalizeCitiesDto;
import com.xaphe.xleap.dto.LocalizeCompanyDto;
import com.xaphe.xleap.dto.LocalizeDistrictsDto;
import com.xaphe.xleap.dto.LocalizeItemDto;
import com.xaphe.xleap.dto.LocalizeLocaleDto;
import com.xaphe.xleap.dto.LocalizeStreetsDto;
import com.xaphe.xleap.dto.LocalizeSubCategoriesDto;
import com.xaphe.xleap.dto.LocalizeWardsDto;
import com.xaphe.xleap.model.CompanyModelView;
import com.xaphe.xleap.model.ItemModelView;
import com.xaphe.xleap.service.CompanyAdminService;
import com.xaphe.xleap.service.ItemAdminService;
import com.xaphe.xleap.utils.StringUtil;

/**
 * Edit administrator controller
 * 
 * @author piece3
 */
@Controller
@RequestMapping("/admin/edit")
public class EditAdminController {
	private static Logger logger = LoggerFactory.getLogger(EditAdminController.class);

	private static final String PRIMARY_LANG = "en_US";
	private static final String SECOND_LANG = "vi_VN";
	private static final String ADMIN_ITEM_UPDATE = "update";
	private static final String ADMIN_ITEM_ADD = "add";
	private static final String CITY_DEFAULT = "Hồ Chí Minh";
	private static final String DISTRICT_DEFAULT = "Quận 1";
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private LocalizeCitiesDao citiesDao;
	@Autowired
	private LocalizeDistrictsDao districtsDao;
	@Autowired
	private LocalizeWardsDao wardsDao;
	@Autowired
	private LocalizeStreetsDao streetsDao;
	@Autowired
	private LocalizeCompaniesDao localizeCompaniesDao;
	@Autowired
	private LocalizeLocaleDao localizeLocaleDao;
	@Autowired
	private CompanyAdminService companyAdminService;
	@Autowired
	private LocalizeCategoriesDao categoryDao;
	@Autowired
	private LocalizeSubCategoriesDao subCategoriesDao;
	@Autowired
	private LocalizeCompaniesDao companyDao;
	@Autowired
	private ItemDao itemDao;
	@Autowired
	private LocalizeItemsDao localizeItemsDao;
	@Autowired
	private ItemAdminService itemAdminService;
	@Value("${upload.image.loc}")
	private String uploadImageLocation;

	/**
	 * Prepared data
	 * 
	 * @param itemModelView
	 * @param companyModelView
	 * @param locale
	 * @param viewName
	 * @return ModelAndView
	 */
	private ModelAndView preparedData(ItemModelView itemModelView, CompanyModelView companyModelView, Locale locale,
			String viewName) {
		ModelAndView modelView = new ModelAndView(viewName);
		modelView.addObject("lang", locale.toString());

		List<LocalizeCategoriesDto> categories = categoryDao.getLocalizeCategoryList();
		List<LocalizeCompanyDto> companies1 = companyDao.getLocalizeCompanyListByLang(PRIMARY_LANG);
		List<LocalizeCompanyDto> companies2 = companyDao.getLocalizeCompanyListByLang(SECOND_LANG);
		List<LocalizeLocaleDto> locales = localizeLocaleDao.getAllLocale();

		List<LocalizeCitiesDto> cities = citiesDao.getCities();
		List<LocalizeDistrictsDto> districts = districtsDao.getDistricts();
		List<LocalizeWardsDto> wards = wardsDao.getWards();
		List<LocalizeStreetsDto> streets = streetsDao.getStreets();

		List<LocalizeSubCategoriesDto> subCategories1 = new ArrayList<LocalizeSubCategoriesDto>();
		if (StringUtil.hasLength(itemModelView.getCategory())) {
			subCategories1 = subCategoriesDao.getLocalizeSubCategoryByCategoryName(itemModelView.getCategory(),
					PRIMARY_LANG);
		}
		List<LocalizeSubCategoriesDto> subCategories2 = new ArrayList<LocalizeSubCategoriesDto>();
		if (StringUtil.hasLength(itemModelView.getCategory())) {
			subCategories2 = subCategoriesDao.getLocalizeSubCategoryByCategoryName(itemModelView.getCategory(),
					SECOND_LANG);
		}
		Map<String, String> cost = new LinkedHashMap<String, String>();
		cost.put("9", messageSource.getMessage("cost.undefined", new String[] {}, locale));
		cost.put("0", messageSource.getMessage("cost.free", new String[] {}, locale));
		cost.put("1", messageSource.getMessage("cost.cheap", new String[] {}, locale));
		cost.put("3", messageSource.getMessage("cost.medium", new String[] {}, locale));
		cost.put("5", messageSource.getMessage("cost.premium", new String[] {}, locale));

		modelView.addObject("companies1", companies1);
		modelView.addObject("companies2", companies2);
		modelView.addObject("categories", categories);
		modelView.addObject("subCategories1", subCategories1);
		modelView.addObject("subCategories2", subCategories2);
		modelView.addObject("locales", locales);
		modelView.addObject("costList", cost);

		modelView.setViewName(viewName);
		modelView.addObject("cities", cities);
		modelView.addObject("districts", districts);
		modelView.addObject("wards", wards);
		modelView.addObject("streets", streets);
		modelView.addObject("locales", locales);
		if (companyModelView == null) {
			companyModelView = new CompanyModelView();
		}
		companyModelView.setLocale1(PRIMARY_LANG);
		companyModelView.setLocale2(SECOND_LANG);
		companyModelView.setCity(CITY_DEFAULT);
		companyModelView.setDistrict(DISTRICT_DEFAULT);
		modelView.addObject("companyModelView", companyModelView);

		return modelView;
	}

	@RequestMapping(value = "", method = RequestMethod.GET)
	public ModelAndView itemIndex(Locale locale, @ModelAttribute("itemModelView") ItemModelView itemModelView,
			@ModelAttribute("companyModelView") CompanyModelView companyModelView,
			@RequestParam(required = false, value = "cat", defaultValue = "0") String cat,
			@RequestParam(required = false, value = "itemId", defaultValue = "0") int itemId) {
		logger.info("Getting Company Model");
		itemModelView.setLocale1(PRIMARY_LANG);
		itemModelView.setLocale2(SECOND_LANG);
		// Edit mode
		if (StringUtil.hasLength(cat) && itemId != 0) {
			// Get locale1 description
			LocalizeItemDto localizeItemDto = localizeItemsDao.getItemByIdAndLocale(itemId, itemModelView.getLocale1());
			if (localizeItemDto != null) {
				itemModelView.setItemId(itemId);
				itemModelView.setWeight(String.valueOf(localizeItemDto.getWeight()));
				itemModelView.setCost(localizeItemDto.getCost().toPlainString());
				itemModelView.setCategory(localizeItemDto.getCatTitle());
				String subcats = localizeItemDto.getSubcats();
				if (StringUtil.hasLength(subcats)) {
					List<String> subcatList = new ArrayList<String>();
					StringTokenizer tokenizer = new StringTokenizer(subcats, "\\|");
					while (tokenizer.hasMoreTokens()) {
						subcatList.add(tokenizer.nextToken().trim());
					}
					itemModelView.setSubCategory1(subcatList);
				}
				itemModelView.setCompany1(localizeItemDto.getTitle());
				itemModelView.setLocale1(localizeItemDto.getLang());
				itemModelView.setShortDesc1(localizeItemDto.getShrtDesc());
				itemModelView.setLongDesc1(localizeItemDto.getDescription());
				itemModelView.setImage(localizeItemDto.getItmImg());
				itemModelView.setVisible(localizeItemDto.isVisible());
			}
			// Get locale2 description
			localizeItemDto = localizeItemsDao.getItemByIdAndLocale(itemId, itemModelView.getLocale2());
			if (localizeItemDto != null) {
				String subcats = localizeItemDto.getSubcats();
				if (StringUtil.hasLength(subcats)) {
					List<String> subcatList = new ArrayList<String>();
					StringTokenizer tokenizer = new StringTokenizer(subcats, "\\|");
					while (tokenizer.hasMoreTokens()) {
						subcatList.add(tokenizer.nextToken().trim());
					}
					itemModelView.setSubCategory2(subcatList);
				}
				itemModelView.setCompany2(localizeItemDto.getTitle());
				itemModelView.setLocale2(localizeItemDto.getLang());
				itemModelView.setShortDesc2(localizeItemDto.getShrtDesc());
				itemModelView.setLongDesc2(localizeItemDto.getDescription());
			}
		}
		ModelAndView modelView = preparedData(itemModelView, companyModelView, locale, "edit-all");
		if (StringUtil.hasLength(cat) && itemId != 0) {
			modelView.addObject("actionMode", ADMIN_ITEM_UPDATE);
		} else {
			modelView.addObject("actionMode", ADMIN_ITEM_ADD);
		}
		modelView.addObject("itemModelView", itemModelView);
		return modelView;
	}

	@RequestMapping(value = "/ajax/check-input", method = RequestMethod.POST)
	@ResponseBody
	public JsonResponse checkInputCompanyAjax(@ModelAttribute("companyModelView") CompanyModelView companyModelView,
			Locale locale, BindingResult result) {

		JsonResponse res = new JsonResponse();
		try {
			validateInputForUpdate(companyModelView, locale, result);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			String message = messageSource.getMessage("message.error.save", new String[] {}, locale);
			result.reject(message);
		}
		// Check input
		if (result.hasErrors()) {
			res.setStatus("INPUT_FAILED");
			res.setResult(result.getAllErrors());
		} else {
			res.setStatus("OK");
		}
		return res;
	}

	/**
	 * Check input value is correct
	 * 
	 * @param companyModelView
	 * @param locale
	 * @param result
	 */
	private void validateInputForUpdate(CompanyModelView companyModelView, Locale locale, BindingResult result) {
		if (companyModelView.getLocale1().equals(companyModelView.getLocale2())) {
			String message = messageSource.getMessage("locale.input.notsame", new String[] {}, locale);
			result.rejectValue("locale1", message);
		}
		if (!StringUtil.hasLength(companyModelView.getTitleEng())) {
			ValidationUtils.rejectIfEmpty(result, "titleEng",
					messageSource.getMessage("title.eng.input.required", new String[] {}, locale));
		} else {
			boolean isValidTitleEng = localizeCompaniesDao.validateCompanyTitle(companyModelView.getCompanyId(),
					companyModelView.getTitleEng());
			if (!isValidTitleEng) {
				String message = messageSource.getMessage("error.value.existing",
						new String[] { companyModelView.getTitleEng() }, locale);
				result.rejectValue("titleEng", message);
			}
		}
		if (!StringUtil.hasLength(companyModelView.getTitleLocal())) {
			ValidationUtils.rejectIfEmpty(result, "titleLocal",
					messageSource.getMessage("title.local.input.required", new String[] {}, locale));
		} else {
			boolean isValidTitleLocal = localizeCompaniesDao.validateCompanyTitle(companyModelView.getCompanyId(),
					companyModelView.getTitleLocal());
			if (!isValidTitleLocal) {
				String message = messageSource.getMessage("error.value.existing",
						new String[] { companyModelView.getTitleLocal() }, locale);
				result.rejectValue("titleLocal", message);
			}
		}
	}

	//
	// public EditAdminController() {
	// }
	//
	// protected ModelAndView handleRequestInternal(
	// HttpServletRequest request,
	// HttpServletResponse response) throws Exception {
	// throw new UnsupportedOperationException("Not yet implemented");
	// }

}
