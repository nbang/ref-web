package com.xaphe.xleap.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author son-vo
 */
@Controller
@RequestMapping("")
public class RootIndexController {

	@RequestMapping(value = "", method = RequestMethod.GET)
	public String index(Model model) {
		return "index";
	}
}