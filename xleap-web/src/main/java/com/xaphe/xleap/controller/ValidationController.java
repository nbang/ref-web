/**
 * 
 */
package com.xaphe.xleap.controller;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xaphe.xleap.dao.localize.LocalizeCompaniesDao;

/**
 * @author son-vo
 */
@Controller
@RequestMapping("/val")
public class ValidationController {
	@Autowired
	private LocalizeCompaniesDao companyDao;
	@Autowired
	private MessageSource messageSource;

	@RequestMapping(value = "/company", method = RequestMethod.POST, produces = "text/plain; charset=utf-8")
	@ResponseBody
	public ResponseEntity<String> validateCompanyTitle(Model model, Locale locale,
			@RequestParam(required = false, value = "titleLocal", defaultValue = "") String titleLocal,
			@RequestParam(required = false, value = "titleEng", defaultValue = "") String titleEng) {

		boolean isValidTitleLocal = companyDao.validateCompanyTitle(titleLocal);
		boolean isValidTitleEng = companyDao.validateCompanyTitle(titleEng);
		StringBuilder sbRet = new StringBuilder();
		sbRet.append("{");
		sbRet.append("\"status\":" + ((isValidTitleLocal && isValidTitleEng) ? true : false));
		sbRet.append(",\"result\":{");
		if (!isValidTitleLocal) {
			sbRet.append("\"titleLocal\":\"" + messageSource.getMessage("error.value.existing", new String[] {titleLocal}, locale) + "\"");
		}
		if (!isValidTitleEng) {
			if (!isValidTitleLocal) {
				sbRet.append(",");
			}
			sbRet.append("\"titleEng\":\"" + messageSource.getMessage("error.value.existing", new String[] {titleEng}, locale) + "\"");
		}
		sbRet.append("}");
		sbRet.append("}");

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");
		return new ResponseEntity<String>(sbRet.toString(), headers, HttpStatus.OK);
	}
}
