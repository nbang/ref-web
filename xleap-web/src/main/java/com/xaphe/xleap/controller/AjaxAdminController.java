package com.xaphe.xleap.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xaphe.xleap.dao.localize.LocalizeCategoriesDao;
import com.xaphe.xleap.dao.localize.LocalizeCompaniesDao;
import com.xaphe.xleap.dao.localize.LocalizeFullItemsDao;
import com.xaphe.xleap.dao.localize.LocalizeItemsDao;
import com.xaphe.xleap.dao.localize.LocalizeSubCategoriesDao;
import com.xaphe.xleap.dto.LocalizeCategoriesDto;
import com.xaphe.xleap.dto.LocalizeCompanyDto;
import com.xaphe.xleap.dto.LocalizeFullItemDto;
import com.xaphe.xleap.dto.LocalizeItemDto;
import com.xaphe.xleap.dto.LocalizeSubCategoriesDto;
import com.xaphe.xleap.model.CompanyModelView;
import com.xaphe.xleap.model.ItemModelView;
import com.xaphe.xleap.service.SubCategoryService;
import com.xaphe.xleap.utils.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Controller
@RequestMapping("/ajax/admin")
public class AjaxAdminController {
	private static Logger logger = LoggerFactory.getLogger(AjaxAdminController.class);
	private static String HTML_CHECK = "<span class=\"glyphicon glyphicon-ok green\">&nbsp;</span>";
	private static String HTML_REMOVE = "<span class=\"glyphicon glyphicon-remove red\">&emsp;</span>";
	private static String HTML_SORT = "<span class=\"glyphicon glyphicon-arrow-up green\"></span><span class=\"glyphicon glyphicon-arrow-down red\"></span>";
	private static String HTML_DELETE = "<span class=\"glyphicon glyphicon-trash red\"></span>";

	@Autowired
	private LocalizeSubCategoriesDao subCategoriesDao;
	@Autowired
	private LocalizeFullItemsDao localizeFullItemsDao;
	@Autowired
	private LocalizeItemsDao localizeItemsDao;
	@Autowired
	private LocalizeCategoriesDao localizeCategoriesDao;
	@Autowired
	private LocalizeCompaniesDao localizeCompaniesDao;
	@Autowired
	private SubCategoryService subCategoryService;

	@RequestMapping(value = "/addsubcat", method = RequestMethod.POST)
	public ResponseEntity<String> getSubCategory(@RequestParam String cats, @RequestParam String subCat1,
			@RequestParam String lang1, @RequestParam String subCat2, @RequestParam String lang2,
			@RequestParam(required = false, value = "actionMod", defaultValue = "add") String actionMod,
			@RequestParam(required = false, value = "oldSubCat1", defaultValue = "") String oldSubCat1,
			@RequestParam(required = false, value = "oldSubCat2", defaultValue = "") String oldSubCat2) {
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");

		try {
			String[] catList = cats.split("\\|");
			for (final String cat : catList) {
				if ("add".equals(actionMod)) {
					subCategoryService.createLocalizeSubCategory(cat, lang1, lang2, subCat1, subCat2);
				} else if ("edit".equals(actionMod)) {
					subCategoryService.updateLocalizeSubCategory(cat, lang1, lang2, subCat1, subCat2, oldSubCat1,
							oldSubCat2);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<String>(String.valueOf(0), headers, HttpStatus.OK);
		}

		return new ResponseEntity<String>(String.valueOf(1), headers, HttpStatus.OK);
	}

	@RequestMapping(value = "/subcat", method = RequestMethod.POST)
	@ResponseBody
	public List<String> getSubCategoryJson(
			@RequestParam(required = false, value = "cat", defaultValue = "") String cat,
			@RequestParam(required = false, value = "localLang", defaultValue = "en_US") String localLang) {
		List<LocalizeSubCategoriesDto> subCategories = subCategoriesDao.getLocalizeSubCategoryByCategoryName(cat,
				localLang);
		List<String> result = new ArrayList<String>();

		for (LocalizeSubCategoriesDto item : subCategories) {
			result.add(item.getSubname());
		}

		return result;
	}

	@RequestMapping(value = "/subcats", method = RequestMethod.POST)
	@ResponseBody
	public List<List<String>> getSubCategoryTwoLangJson(
			@RequestParam(required = false, value = "cat", defaultValue = "") String cat,
			@RequestParam(required = false, value = "primLang", defaultValue = "en_US") String primLang,
			@RequestParam(required = false, value = "secLang", defaultValue = "vi_VN") String secLang) {

		String[] catList = cat.split("\\|");
		List<LocalizeSubCategoriesDto> subCategories1 = new ArrayList<LocalizeSubCategoriesDto>();
		List<LocalizeSubCategoriesDto> subCategories2 = new ArrayList<LocalizeSubCategoriesDto>();
		for (String itemCat : catList) {
			subCategories1.addAll(subCategoriesDao.getLocalizeSubCategoryByCategoryName(itemCat, primLang));
			subCategories2.addAll(subCategoriesDao.getLocalizeSubCategoryByCategoryName(itemCat, secLang));
		}
		// Sub category 1
		List<String> subCat1 = new ArrayList<String>();
		for (LocalizeSubCategoriesDto item : subCategories1) {
			// loop to check duplicated item
			boolean isFound = false;
			for (String sub : subCat1) {
				if (sub.equals(item.getSubname())) {
					isFound = true;
				}
			}

			if (!isFound)
				subCat1.add(item.getSubname());

		}
		// Sub category 2
		List<String> subCat2 = new ArrayList<String>();
		for (LocalizeSubCategoriesDto item : subCategories2) {
			// loop to check duplicated item
			boolean isFound = false;
			for (String sub : subCat2) {
				if (sub.equals(item.getSubname())) {
					isFound = true;
				}
			}

			if (!isFound)
				subCat2.add(item.getSubname());
		}
		List<List<String>> subCat = new ArrayList<List<String>>();
		subCat.add(subCat1);
		subCat.add(subCat2);

		return subCat;
	}

	@RequestMapping(value = "/desc", method = RequestMethod.POST)
	@ResponseBody
	public List<String> getLocalizeDescriptionJson(
			@RequestParam(required = false, value = "itemId", defaultValue = "0") int itemId,
			@RequestParam(required = false, value = "localLang", defaultValue = "en_US") String localLang) {
		List<String> result = new ArrayList<String>();
		LocalizeFullItemDto localizeFullItemDto = localizeFullItemsDao.getItemById(itemId, localLang);
		if (localizeFullItemDto != null) {
			result.add(localizeFullItemDto.getShrt_desc() == null ? "" : localizeFullItemDto.getShrt_desc());
			result.add(localizeFullItemDto.getDescription() == null ? "" : localizeFullItemDto.getDescription());
		}

		return result;
	}

	@RequestMapping(value = "/companies", method = RequestMethod.POST)
	@ResponseBody
	public List<String> getLocalizeCompaniesJson(
			@RequestParam(required = false, value = "localLang", defaultValue = "en_US") String localLang) {
		List<String> result = localizeCompaniesDao.getLocalizeCompaniesNameByLang(localLang);
		if (result == null) {
			result = new ArrayList<String>();
		}

		return result;
	}

	@RequestMapping(value = "/all", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, List<List<String>>> getAllItemJson(
			@RequestParam(required = false, value = "primLang", defaultValue = "en_US") String primLang,
			@RequestParam(required = false, value = "secLang", defaultValue = "vi_VN") String secLang) {

		if (primLang.equals(secLang)) {
			if (primLang.equals("en_US")) {
				secLang = "vi_VN";
			} else {
				primLang = "en_US";
			}
		}

		List<LocalizeFullItemDto> itemListPrim = localizeFullItemsDao.getAllItemForEdit(primLang, 0, 1000);
		List<LocalizeFullItemDto> itemListSec = localizeFullItemsDao.getAllItemForEdit(secLang, 0, 1000);

		List<List<String>> itemList = new ArrayList<List<String>>();
		List<LocalizeCategoriesDto> catList = localizeCategoriesDao.getLocalizeCategoryList();

		for (int i = 0; i < itemListPrim.size(); i++) {
			List<String> item = new ArrayList<String>();

			item.add(String.valueOf(itemListPrim.get(i).getId()));
			item.add(itemListPrim.get(i).getTitle());
			item.add(String.valueOf(itemListPrim.get(i).getWeight()));
			for (LocalizeCategoriesDto cat : catList) {
				if (cat.getTitle().equals(itemListPrim.get(i).getCatTitle())) {
					item.add(String.valueOf(itemListPrim.get(i).getWeight()));
				} else {
					item.add("");
				}
			}

			item.add(StringUtil.hasLength(itemListPrim.get(i).getShrtDesc()) ? HTML_CHECK : "");
			item.add(StringUtil.hasLength(itemListPrim.get(i).getDescription()) ? HTML_CHECK : "");
			item.add(StringUtil.hasLength(itemListSec.get(i).getShrtDesc()) ? HTML_CHECK : "");
			item.add(StringUtil.hasLength(itemListSec.get(i).getDescription()) ? HTML_CHECK : "");
			item.add(itemListPrim.get(i).isVisible() ? HTML_CHECK : HTML_REMOVE);
			item.add(StringUtil.hasLength(itemListPrim.get(i).getItmImg()) ? HTML_CHECK : "");
			boolean hasContactDetails = false;
			if (StringUtil.hasLength(itemListPrim.get(i).getAddr_num())
					|| StringUtil.hasLength(itemListPrim.get(i).getWard())
					|| StringUtil.hasLength(itemListPrim.get(i).getDistrict())
					|| StringUtil.hasLength(itemListPrim.get(i).getCity())) {
				hasContactDetails = true;
			}
			if (hasContactDetails == false) {
				if (StringUtil.hasLength(itemListPrim.get(i).getCon1val())
						|| StringUtil.hasLength(itemListPrim.get(i).getCon2val())
						|| StringUtil.hasLength(itemListPrim.get(i).getCon3val())
						|| StringUtil.hasLength(itemListPrim.get(i).getCon4val())
						|| StringUtil.hasLength(itemListPrim.get(i).getUrl())) {
					hasContactDetails = true;
				}
			}
			if (hasContactDetails) {
				item.add(HTML_CHECK);
			} else {
				item.add("");
			}

			item.add(HTML_SORT);
			item.add(HTML_DELETE);
			itemList.add(item);
		}

		Map<String, List<List<String>>> mapper = new HashMap<String, List<List<String>>>();
		mapper.put("aaData", itemList);

		return mapper;
	}

	@RequestMapping(value = "/getItem", method = RequestMethod.POST)
	@ResponseBody
	public ItemModelView getItemJson(int itemId,
			@RequestParam(required = false, value = "primLang", defaultValue = "en_US") String primLang,
			@RequestParam(required = false, value = "secLang", defaultValue = "vi_VN") String secLang) {

		ItemModelView item = new ItemModelView();

		LocalizeItemDto localizeItemDto = localizeItemsDao.getItemByIdAndLocale(itemId, primLang);
		if (localizeItemDto != null) {
			item.setItemId(itemId);
			item.setWeight(String.valueOf(localizeItemDto.getWeight()));
			item.setCost(localizeItemDto.getCost().toPlainString());
			item.setCategory(localizeItemDto.getCatTitle());
			String subcats = localizeItemDto.getSubcats();
			if (StringUtil.hasLength(subcats)) {
				List<String> subcatList = new ArrayList<String>();
				StringTokenizer tokenizer = new StringTokenizer(subcats, "\\|");
				while (tokenizer.hasMoreTokens()) {
					subcatList.add(tokenizer.nextToken().trim());
				}
				item.setSubCategory1(subcatList);
			}
			item.setCompany1(localizeItemDto.getTitle());
			item.setLocale1(localizeItemDto.getLang());
			item.setShortDesc1(localizeItemDto.getShrtDesc());
			item.setLongDesc1(localizeItemDto.getDescription());
			item.setImage(localizeItemDto.getItmImg());
		}
		// Get locale2 description
		localizeItemDto = localizeItemsDao.getItemByIdAndLocale(itemId, secLang);
		if (localizeItemDto != null) {
			String subcats = localizeItemDto.getSubcats();
			if (StringUtil.hasLength(subcats)) {
				List<String> subcatList = new ArrayList<String>();
				StringTokenizer tokenizer = new StringTokenizer(subcats, "\\|");
				while (tokenizer.hasMoreTokens()) {
					subcatList.add(tokenizer.nextToken().trim());
				}
				item.setSubCategory2(subcatList);
			}
			item.setCompany2(localizeItemDto.getTitle());
			item.setLocale2(localizeItemDto.getLang());
			item.setShortDesc2(localizeItemDto.getShrtDesc());
			item.setLongDesc2(localizeItemDto.getDescription());
		}

		return item;
	}

	@RequestMapping(value = "/company/all", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, List<List<String>>> getAllCompanyJson(
			@RequestParam(required = false, value = "primLang", defaultValue = "en_US") String primLang,
			@RequestParam(required = false, value = "secLang", defaultValue = "vi_VN") String secLang) {

		if (primLang.equals(secLang)) {
			if (primLang.equals("en_US")) {
				secLang = "vi_VN";
			} else {
				primLang = "en_US";
			}
		}
		List<LocalizeCompanyDto> companyListPrim = localizeCompaniesDao.getLocalizeCompanyListByLang(primLang);
		List<LocalizeCompanyDto> companyListSecd = localizeCompaniesDao.getLocalizeCompanyListByLang(secLang);

		List<List<String>> itemList = new ArrayList<List<String>>();

		for (int i = 0; i < companyListPrim.size(); i++) {
			List<String> item = new ArrayList<String>();

			item.add(String.valueOf(companyListPrim.get(i).getId()));
			item.add(companyListPrim.get(i).getTitle());
			item.add(companyListSecd.get(i).getTitle());
			item.add(companyListPrim.get(i).getCity());
			item.add(companyListPrim.get(i).getDistrict());
			item.add(companyListPrim.get(i).getWard());
			item.add(companyListPrim.get(i).getStreet());
			item.add(companyListPrim.get(i).getAddrnum());

			itemList.add(item);
		}

		Map<String, List<List<String>>> mapper = new HashMap<String, List<List<String>>>();
		mapper.put("aaData", itemList);

		return mapper;
	}

	@RequestMapping(value = "/getCompany", method = RequestMethod.POST)
	@ResponseBody
	public CompanyModelView getCompanyJson(int companyId,
			@RequestParam(required = false, value = "primLang", defaultValue = "en_US") String primLang,
			@RequestParam(required = false, value = "secLang", defaultValue = "vi_VN") String secLang) {

		if (primLang.equals(secLang)) {
			if (primLang.equals("en_US")) {
				secLang = "vi_VN";
			} else {
				primLang = "en_US";
			}
		}
		CompanyModelView company = new CompanyModelView();

		LocalizeCompanyDto localizeCompanyDto = localizeCompaniesDao.getLocalizeCompanyById(companyId, primLang);
		if (localizeCompanyDto != null) {
			company.setCompanyId(companyId);
			company.setCity(localizeCompanyDto.getCity());
			company.setDistrict(localizeCompanyDto.getDistrict());
			company.setWard(localizeCompanyDto.getWard());
			company.setStreet(localizeCompanyDto.getStreet());
			company.setAddrNum(localizeCompanyDto.getAddrnum());
			company.setTitleEng(localizeCompanyDto.getTitle());
			company.setTelephone1(localizeCompanyDto.getCon1val());
			company.setTelephone2(localizeCompanyDto.getCon2val());
			company.setEmail(localizeCompanyDto.getCon3val());
			company.setContact(localizeCompanyDto.getCon4val());
			company.setUrl(localizeCompanyDto.getUrl());
		}
		// Get locale2 description
		localizeCompanyDto = localizeCompaniesDao.getLocalizeCompanyById(companyId, secLang);
		if (localizeCompanyDto != null) {
			company.setTitleLocal(localizeCompanyDto.getTitle());
		}

		return company;
	}

	@RequestMapping(value = "/getCompanyByTitle", method = RequestMethod.POST)
	@ResponseBody
	public CompanyModelView getCompanyByTitleJson(String companyTitle,
			@RequestParam(required = false, value = "primLang", defaultValue = "en_US") String primLang,
			@RequestParam(required = false, value = "secLang", defaultValue = "vi_VN") String secLang) {

		if (primLang.equals(secLang)) {
			if (primLang.equals("en_US")) {
				secLang = "vi_VN";
			} else {
				primLang = "en_US";
			}
		}
		CompanyModelView company = new CompanyModelView();

		LocalizeCompanyDto localizeCompanyDto = localizeCompaniesDao.getLocalizeCompanyByTitle(companyTitle, primLang);
		if (localizeCompanyDto != null) {
			company.setCompanyId(localizeCompanyDto.getId());
			company.setCity(localizeCompanyDto.getCity());
			company.setDistrict(localizeCompanyDto.getDistrict());
			company.setWard(localizeCompanyDto.getWard());
			company.setStreet(localizeCompanyDto.getStreet());
			company.setAddrNum(localizeCompanyDto.getAddrnum());
			company.setTitleEng(localizeCompanyDto.getTitle());
			company.setTelephone1(localizeCompanyDto.getCon1val());
			company.setTelephone2(localizeCompanyDto.getCon2val());
			company.setEmail(localizeCompanyDto.getCon3val());
			company.setContact(localizeCompanyDto.getCon4val());
			company.setUrl(localizeCompanyDto.getUrl());
		}
		// Get locale2 description
		logger.info("Company ID is {} --- {}", localizeCompanyDto.getId(), company.getCompanyId());
		localizeCompanyDto = localizeCompaniesDao.getLocalizeCompanyById(localizeCompanyDto.getId(), secLang);
		if (localizeCompanyDto != null) {
			company.setTitleLocal(localizeCompanyDto.getTitle());
		}

		return company;
	}

	@RequestMapping(value = "/update_weight", method = RequestMethod.POST)
	public ResponseEntity<String> updateItemWeight(@RequestParam int item, @RequestParam String category,
			@RequestParam int weight) {
		int result = localizeItemsDao.updateItemWeight(item, category, weight);

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");
		return new ResponseEntity<String>(String.valueOf(result), headers, HttpStatus.OK);
	}
}
