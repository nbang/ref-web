/**
 * 
 */
package com.xaphe.xleap.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.xaphe.xleap.dao.localize.LocalizeCategoriesDao;
import com.xaphe.xleap.dto.LocalizeBannerDto;
import com.xaphe.xleap.model.BannerModelView;
import com.xaphe.xleap.service.BannerAdminService;
import com.xaphe.xleap.utils.FileUtil;
import com.xaphe.xleap.utils.StringUtil;
import com.xaphe.xleap.validator.BannerFormValidator;

/**
 * Item administrator controller
 * 
 * @author son-vo
 */
@Controller
@RequestMapping("/admin/banner")
public class BannerAdminController {
	private static final Logger logger = LoggerFactory.getLogger(BannerAdminController.class);
	@Autowired
	private LocalizeCategoriesDao categoryDao;
	@Autowired
	private BannerAdminService bannerAdminService;
	@Value("${upload.image.loc}")
	private String uploadImageLocation;

	private void prepare(ModelAndView modelView) {
		modelView.addObject("categories", categoryDao.getLocalizeCategoryList());
		modelView.addObject("pageLocs", bannerAdminService.getPageLocs());
	}

	@RequestMapping(value = "", method = RequestMethod.GET)
	private ModelAndView index(@ModelAttribute("bannerModelView") final BannerModelView bannerModelView) {
		
		ModelAndView modelView = new ModelAndView("banner-create");
		prepare(modelView);
		modelView.addObject("actionMode", "add");
		return modelView;
	}

	@RequestMapping(value = "/all", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, List<List<String>>> getAllBannerJson() {

		List<LocalizeBannerDto> localizeBannerDtoList = bannerAdminService.getBannerList();

		List<List<String>> itemList = new ArrayList<List<String>>();
		for (LocalizeBannerDto dto : localizeBannerDtoList) {
			List<String> item = new ArrayList<String>();

			item.add(String.valueOf(dto.getId()));
			item.add(dto.isVisible() ? "TRUE" : "FALSE");
			item.add(dto.getClientname());
			item.add(dto.getCattitle());
			item.add(dto.getImage());
			item.add(dto.getUrl());
			item.add(String.valueOf(dto.getWeight()));
			item.add(dto.getStartdate());
			item.add(dto.getEnddate());

			itemList.add(item);
		}

		Map<String, List<List<String>>> mapper = new HashMap<String, List<List<String>>>();
		mapper.put("aaData", itemList);

		return mapper;
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	private ModelAndView list() {
		ModelAndView modelView = new ModelAndView();
		modelView.setViewName("banner-list");
		//modelView.addObject("banners", bannerAdminService.getBannerList());
		return modelView;
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	private ModelAndView add(
			@ModelAttribute("bannerModelView") final BannerModelView bannerModelView,
			BindingResult result) {

		BannerFormValidator bfl = new BannerFormValidator();
		bfl.validate(bannerModelView, result);
		if (result.hasErrors()) {
			ModelAndView modelView = new ModelAndView("banner-create");
			prepare(modelView);
			modelView.addObject("actionMode", "add");
			Map<String, String> messages = new HashMap<String, String>();
			messages.put("error", "message.input.required");
			modelView.addObject("messages", messages);
			return modelView;
		}
		try {
			bannerAdminService.create(bannerModelView);
			uploadFile(bannerModelView);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		ModelAndView modelView = new ModelAndView("banner-list");
		return modelView;
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	private ModelAndView update(@ModelAttribute("bannerModelView") final BannerModelView bannerModelView,
			BindingResult result) {
		BannerFormValidator bfl = new BannerFormValidator();
		bfl.validate(bannerModelView, result);
		if (result.hasErrors()) {
			ModelAndView modelView = new ModelAndView("banner-create");
			prepare(modelView);
			modelView.addObject("actionMode", "update");
			Map<String, String> messages = new HashMap<String, String>();
			messages.put("error", "message.input.required");
			modelView.addObject("messages", messages);
			return modelView;
		}
		try {
			bannerAdminService.update(bannerModelView);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		ModelAndView modelView = new ModelAndView("banner-list");
		return modelView;
	}
	
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	public ModelAndView delete(@PathVariable int id) {
		try {
			bannerAdminService.delete(id);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		ModelAndView modelAndView = new ModelAndView("redirect:/banner-list");
		return modelAndView;
	}

	/**
	 * Update file
	 * @param file
	 */
	public void uploadFile(BannerModelView bannerModelView) {
		MultipartFile file = bannerModelView.getImage();
		String fileExt = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
		String fileName = StringUtil.getBannerImageName() + fileExt;
		logger.debug(fileName);

		FileUtil.writeUploadedFile(file, uploadImageLocation, file.getOriginalFilename());
	}

	@RequestMapping(value = { "/loadImage/{fileName:.+}",
			"/loadImage/{fileName}/" })
	public void getImage(HttpServletResponse response,
			@PathVariable String fileName) {
		String fullPath = uploadImageLocation;
		if (StringUtil.hasLength(fileName)) {
			fullPath = uploadImageLocation + fileName;
		}
		FileInputStream inputStream = null;
		OutputStream outStream = null;
		try {
			File downloadFile = new File(fullPath);
			inputStream = new FileInputStream(downloadFile);

			response.setContentType("image/jpeg");
			response.setContentLength((int) downloadFile.length());
			// get output stream of the response

			outStream = response.getOutputStream();
			byte[] buffer = new byte[FileUtil.BUFFER_SIZE];
			int bytesRead = -1;

			// write bytes read from the input stream into the output stream
			while ((bytesRead = inputStream.read(buffer)) != -1) {
				outStream.write(buffer, 0, bytesRead);
			}
		} catch (IOException e) {
			logger.error("Cannot load file due to IO exception. Error: {}", e.getMessage());
		} finally {
			try {
				if (inputStream != null) {
					inputStream.close();
				}
				if (outStream != null) {
					outStream.close();
				}
			} catch (IOException e) {
				logger.error(e.getMessage());
			}
		}
	}
}
