package com.xaphe.xleap.controller;

import org.springframework.web.bind.annotation.ExceptionHandler;

public class BaseController {
	@ExceptionHandler(Exception.class) 
    public void handleExceptions(Exception anExc) {
        anExc.printStackTrace(); // do something better than this ;)
    }
}
