package com.xaphe.xleap.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class LocalizeItemDto implements Serializable {

	private static final long serialVersionUID = -7384968215482526693L;

	private int id;
	private String lang;
	private String title;
	private String cat_title;
	private String cat_icon;
	private String subcats;
	private Integer weight;
	private BigDecimal cost;
	private String shrt_desc;
	private String description;
	private String itm_img;
	private boolean visible;

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the lang
	 */
	public String getLang() {
		return lang;
	}

	/**
	 * @param lang
	 *            the lang to set
	 */
	public void setLang(String lang) {
		this.lang = lang;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the catTitle
	 */
	public String getCatTitle() {
		return cat_title;
	}

	/**
	 * @param catTitle
	 *            the catTitle to set
	 */
	public void setCatTitle(String cat_title) {
		this.cat_title = cat_title;
	}

	/**
	 * @return the catIcon
	 */
	public String getCatIcon() {
		return cat_icon;
	}

	/**
	 * @param catIcon
	 *            the catIcon to set
	 */
	public void setCatIcon(String cat_icon) {
		this.cat_icon = cat_icon;
	}

	/**
	 * @return the subCats
	 */
	public String getSubcats() {
		return subcats;
	}

	/**
	 * @param subCats
	 *            the subCats to set
	 */
	public void setSubcats(String subcats) {
		this.subcats = subcats;
	}

	/**
	 * @return the weight
	 */
	public Integer getWeight() {
		return weight;
	}

	/**
	 * @param weight
	 *            the weight to set
	 */
	public void setWeight(Integer weight) {
		this.weight = weight;
	}

	/**
	 * @return the cost
	 */
	public BigDecimal getCost() {
		return cost;
	}

	/**
	 * @param cost
	 *            the cost to set
	 */
	public void setCost(BigDecimal cost) {
		this.cost = cost;
	}

	/**
	 * @return the shrtDesc
	 */
	public String getShrtDesc() {
		return shrt_desc;
	}

	/**
	 * @param shrtDesc
	 *            the shrtDesc to set
	 */
	public void setShrtDesc(String shrt_desc) {
		this.shrt_desc = shrt_desc;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the itmImg
	 */
	public String getItmImg() {
		return itm_img;
	}

	/**
	 * @param itmImg
	 *            the itmImg to set
	 */
	public void setItmImg(String itm_img) {
		this.itm_img = itm_img;
	}

	/**
	 * @return the visible
	 */
	public boolean isVisible() {
		return visible;
	}

	/**
	 * @param visible
	 *            the visible to set
	 */
	public void setVisible(boolean visible) {
		this.visible = visible;
	}

}
