package com.xaphe.xleap.dto;

import java.io.Serializable;

public class LocalizeCitiesDto implements Serializable {
	private static final long serialVersionUID = -8319600315980872705L;
	private int id;
	private String name;
	private String shortcode;
	private Integer province;

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the shortcode
	 */
	public String getShortcode() {
		return shortcode;
	}

	/**
	 * @param shortcode
	 *            the shortcode to set
	 */
	public void setShortcode(String shortcode) {
		this.shortcode = shortcode;
	}

	/**
	 * @return the province
	 */
	public Integer getProvince() {
		return province;
	}

	/**
	 * @param province
	 *            the province to set
	 */
	public void setProvince(Integer province) {
		this.province = province;
	}
}
