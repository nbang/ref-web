package com.xaphe.xleap.dto;

import java.io.Serializable;

public class LocalizeDistrictsDto implements Serializable {
	private static final long serialVersionUID = -2692147934199708244L;
	private int id;
	private String district;

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the district
	 */
	public String getDistrict() {
		return district;
	}

	/**
	 * @param district
	 *            the district to set
	 */
	public void setDistrict(String district) {
		this.district = district;
	}
}
