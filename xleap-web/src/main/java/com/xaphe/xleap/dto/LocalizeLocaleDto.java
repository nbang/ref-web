package com.xaphe.xleap.dto;

import java.io.Serializable;

public class LocalizeLocaleDto implements Serializable {
	private static final long serialVersionUID = 4555609061890560508L;
	private int id;
	private String code;
	private String local_name;
	private String en_name;

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code
	 *            the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the local_name
	 */
	public String getLocal_name() {
		return local_name;
	}

	/**
	 * @param local_name
	 *            the local_name to set
	 */
	public void setLocal_name(String local_name) {
		this.local_name = local_name;
	}

	/**
	 * @return the en_name
	 */
	public String getEn_name() {
		return en_name;
	}

	/**
	 * @param en_name
	 *            the en_name to set
	 */
	public void setEn_name(String en_name) {
		this.en_name = en_name;
	}
}
