package com.xaphe.xleap.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class LocalizeFullItemDto implements Serializable {

	private static final long serialVersionUID = -7384968215482526693L;

	private int id;
	private String lang;
	private String title;
	private String addr_num;
	private String street;
	private String ward;
	private String district;
	private String city;
	private String con1val;
	private String con2val;
	private String con3val;
	private String con4val;
	private String url;
	private String url_desc;
	private String cat_title;
	private String cat_icon;
	private String subcats;
	private Integer weight;
	private BigDecimal cost;
	private String shrt_desc;
	private String description;
	private String itm_img;
	private boolean visible;

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the lang
	 */
	public String getLang() {
		return lang;
	}

	/**
	 * @param lang
	 *            the lang to set
	 */
	public void setLang(String lang) {
		this.lang = lang;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the addr_num
	 */
	public String getAddr_num() {
		return addr_num;
	}

	/**
	 * @param addr_num
	 *            the addr_num to set
	 */
	public void setAddr_num(String addr_num) {
		this.addr_num = addr_num;
	}

	/**
	 * @return the street
	 */
	public String getStreet() {
		return street;
	}

	/**
	 * @param street
	 *            the street to set
	 */
	public void setStreet(String street) {
		this.street = street;
	}

	/**
	 * @return the ward
	 */
	public String getWard() {
		return ward;
	}

	/**
	 * @param ward
	 *            the ward to set
	 */
	public void setWard(String ward) {
		this.ward = ward;
	}

	/**
	 * @return the district
	 */
	public String getDistrict() {
		return district;
	}

	/**
	 * @param district
	 *            the district to set
	 */
	public void setDistrict(String district) {
		this.district = district;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city
	 *            the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the con1val
	 */
	public String getCon1val() {
		return con1val;
	}

	/**
	 * @param con1val the con1val to set
	 */
	public void setCon1val(String con1val) {
		this.con1val = con1val;
	}

	/**
	 * @return the con2val
	 */
	public String getCon2val() {
		return con2val;
	}

	/**
	 * @param con2val the con2val to set
	 */
	public void setCon2val(String con2val) {
		this.con2val = con2val;
	}

	/**
	 * @return the con3val
	 */
	public String getCon3val() {
		return con3val;
	}

	/**
	 * @param con3val the con3val to set
	 */
	public void setCon3val(String con3val) {
		this.con3val = con3val;
	}

	/**
	 * @return the con4val
	 */
	public String getCon4val() {
		return con4val;
	}

	/**
	 * @param con4val the con4val to set
	 */
	public void setCon4val(String con4val) {
		this.con4val = con4val;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url
	 *            the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return the url_desc
	 */
	public String getUrl_desc() {
		return url_desc;
	}

	/**
	 * @param url_desc
	 *            the url_desc to set
	 */
	public void setUrl_desc(String url_desc) {
		this.url_desc = url_desc;
	}

	/**
	 * @return the cat_title
	 */
	public String getCat_title() {
		return cat_title;
	}

	/**
	 * @param cat_title
	 *            the cat_title to set
	 */
	public void setCat_title(String cat_title) {
		this.cat_title = cat_title;
	}

	/**
	 * @return the catTitle
	 */
	public String getCatTitle() {
		return cat_title;
	}

	/**
	 * @param catTitle
	 *            the catTitle to set
	 */
	public void setCatTitle(String cat_title) {
		this.cat_title = cat_title;
	}

	/**
	 * @return the cat_icon
	 */
	public String getCat_icon() {
		return cat_icon;
	}

	/**
	 * @param cat_icon
	 *            the cat_icon to set
	 */
	public void setCat_icon(String cat_icon) {
		this.cat_icon = cat_icon;
	}

	/**
	 * @return the catIcon
	 */
	public String getCatIcon() {
		return cat_icon;
	}

	/**
	 * @param catIcon
	 *            the catIcon to set
	 */
	public void setCatIcon(String cat_icon) {
		this.cat_icon = cat_icon;
	}

	/**
	 * @return the subcats
	 */
	public String getSubcats() {
		return subcats;
	}

	/**
	 * @param subcats
	 *            the subcats to set
	 */
	public void setSubcats(String subcats) {
		this.subcats = subcats;
	}

	/**
	 * @return the weight
	 */
	public Integer getWeight() {
		return weight;
	}

	/**
	 * @param weight
	 *            the weight to set
	 */
	public void setWeight(Integer weight) {
		this.weight = weight;
	}

	/**
	 * @return the cost
	 */
	public BigDecimal getCost() {
		return cost;
	}

	/**
	 * @param cost
	 *            the cost to set
	 */
	public void setCost(BigDecimal cost) {
		this.cost = cost;
	}

	/**
	 * @return the shrt_desc
	 */
	public String getShrt_desc() {
		return shrt_desc;
	}

	/**
	 * @param shrt_desc
	 *            the shrt_desc to set
	 */
	public void setShrt_desc(String shrt_desc) {
		this.shrt_desc = shrt_desc;
	}

	/**
	 * @return the shrtDesc
	 */
	public String getShrtDesc() {
		return shrt_desc;
	}

	/**
	 * @param shrtDesc
	 *            the shrtDesc to set
	 */
	public void setShrtDesc(String shrt_desc) {
		this.shrt_desc = shrt_desc;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the itm_img
	 */
	public String getItm_img() {
		return itm_img;
	}

	/**
	 * @param itm_img
	 *            the itm_img to set
	 */
	public void setItm_img(String itm_img) {
		this.itm_img = itm_img;
	}

	/**
	 * @return the itmImg
	 */
	public String getItmImg() {
		return itm_img;
	}

	/**
	 * @param itmImg
	 *            the itmImg to set
	 */
	public void setItmImg(String itm_img) {
		this.itm_img = itm_img;
	}

	/**
	 * @return the visible
	 */
	public boolean isVisible() {
		return visible;
	}

	/**
	 * @param visible
	 *            the visible to set
	 */
	public void setVisible(boolean visible) {
		this.visible = visible;
	}
}
