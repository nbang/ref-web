package com.xaphe.xleap.dto;

import java.io.Serializable;

public class LocalizeCategoriesDto implements Serializable {
	private static final long serialVersionUID = 3604595546132738466L;
	private int id;
	private String title;
	private String catdesc;
	private String filepath;

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the catdesc
	 */
	public String getCatdesc() {
		return catdesc;
	}

	/**
	 * @param catdesc
	 *            the catdesc to set
	 */
	public void setCatdesc(String catdesc) {
		this.catdesc = catdesc;
	}

	/**
	 * @return the filepath
	 */
	public String getFilepath() {
		return filepath;
	}

	/**
	 * @param filepath
	 *            the filepath to set
	 */
	public void setFilepath(String filepath) {
		this.filepath = filepath;
	}
}
