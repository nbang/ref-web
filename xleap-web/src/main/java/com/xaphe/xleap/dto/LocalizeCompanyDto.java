package com.xaphe.xleap.dto;

import java.io.Serializable;

import org.postgis.Geometry;

public class LocalizeCompanyDto implements Serializable {
	private static final long serialVersionUID = 2761801286044668729L;

	private int id;
	private String lang;
	private String title;
	private String titleLocal;
	private String titleEng;
	private String addrnum;
	private String street;
	private String ward;
	private String district;
	private String city;
	private String cityshrtcode;
	private String province;
	private Geometry geomarker;
	private Integer con1typ;
	private String con1val;
	private Integer con2typ;
	private String con2val;
	private Integer con3typ;
	private String con3val;
	private Integer con4typ;
	private String con4val;
	private String url;
	private String urldesc;

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the lang
	 */
	public String getLang() {
		return lang;
	}

	/**
	 * @param lang
	 *            the lang to set
	 */
	public void setLang(String lang) {
		this.lang = lang;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the titleLocal
	 */
	public String getTitleLocal() {
		return titleLocal;
	}

	/**
	 * @param titleLocal
	 *            the titleLocal to set
	 */
	public void setTitleLocal(String titleLocal) {
		this.titleLocal = titleLocal;
	}

	/**
	 * @return the titleEng
	 */
	public String getTitleEng() {
		return titleEng;
	}

	/**
	 * @param titleEng
	 *            the titleEng to set
	 */
	public void setTitleEng(String titleEng) {
		this.titleEng = titleEng;
	}

	/**
	 * @return the addrnum
	 */
	public String getAddrnum() {
		return addrnum;
	}

	/**
	 * @param addrnum
	 *            the addrnum to set
	 */
	public void setAddrnum(String addrnum) {
		this.addrnum = addrnum;
	}

	/**
	 * @return the street
	 */
	public String getStreet() {
		return street;
	}

	/**
	 * @param street
	 *            the street to set
	 */
	public void setStreet(String street) {
		this.street = street;
	}

	/**
	 * @return the ward
	 */
	public String getWard() {
		return ward;
	}

	/**
	 * @param ward
	 *            the ward to set
	 */
	public void setWard(String ward) {
		this.ward = ward;
	}

	/**
	 * @return the district
	 */
	public String getDistrict() {
		return district;
	}

	/**
	 * @param district
	 *            the district to set
	 */
	public void setDistrict(String district) {
		this.district = district;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city
	 *            the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the cityshrtcode
	 */
	public String getCityshrtcode() {
		return cityshrtcode;
	}

	/**
	 * @param cityshrtcode
	 *            the cityshrtcode to set
	 */
	public void setCityshrtcode(String cityshrtcode) {
		this.cityshrtcode = cityshrtcode;
	}

	/**
	 * @return the province
	 */
	public String getProvince() {
		return province;
	}

	/**
	 * @param province
	 *            the province to set
	 */
	public void setProvince(String province) {
		this.province = province;
	}

	/**
	 * @return the geomarker
	 */
	public Geometry getGeomarker() {
		return geomarker;
	}

	/**
	 * @param geomarker
	 *            the geomarker to set
	 */
	public void setGeomarker(Geometry geomarker) {
		this.geomarker = geomarker;
	}

	/**
	 * @return the con1typ
	 */
	public Integer getCon1typ() {
		return con1typ;
	}

	/**
	 * @param con1typ
	 *            the con1typ to set
	 */
	public void setCon1typ(Integer con1typ) {
		this.con1typ = con1typ;
	}

	/**
	 * @return the con1val
	 */
	public String getCon1val() {
		return con1val;
	}

	/**
	 * @param con1val
	 *            the con1val to set
	 */
	public void setCon1val(String con1val) {
		this.con1val = con1val;
	}

	/**
	 * @return the con2typ
	 */
	public Integer getCon2typ() {
		return con2typ;
	}

	/**
	 * @param con2typ
	 *            the con2typ to set
	 */
	public void setCon2typ(Integer con2typ) {
		this.con2typ = con2typ;
	}

	/**
	 * @return the con2val
	 */
	public String getCon2val() {
		return con2val;
	}

	/**
	 * @param con2val
	 *            the con2val to set
	 */
	public void setCon2val(String con2val) {
		this.con2val = con2val;
	}

	/**
	 * @return the con3typ
	 */
	public Integer getCon3typ() {
		return con3typ;
	}

	/**
	 * @param con3typ
	 *            the con3typ to set
	 */
	public void setCon3typ(Integer con3typ) {
		this.con3typ = con3typ;
	}

	/**
	 * @return the con3val
	 */
	public String getCon3val() {
		return con3val;
	}

	/**
	 * @param con3val
	 *            the con3val to set
	 */
	public void setCon3val(String con3val) {
		this.con3val = con3val;
	}

	/**
	 * @return the con4typ
	 */
	public Integer getCon4typ() {
		return con4typ;
	}

	/**
	 * @param con4typ
	 *            the con4typ to set
	 */
	public void setCon4typ(Integer con4typ) {
		this.con4typ = con4typ;
	}

	/**
	 * @return the con4val
	 */
	public String getCon4val() {
		return con4val;
	}

	/**
	 * @param con4val
	 *            the con4val to set
	 */
	public void setCon4val(String con4val) {
		this.con4val = con4val;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url
	 *            the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return the urldesc
	 */
	public String getUrldesc() {
		return urldesc;
	}

	/**
	 * @param urldesc
	 *            the urldesc to set
	 */
	public void setUrldesc(String urldesc) {
		this.urldesc = urldesc;
	}

}
