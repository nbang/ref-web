package com.xaphe.xleap.dto;

import java.io.Serializable;

public class PageLocDto implements Serializable {
	private static final long serialVersionUID = 768004818377920374L;
	private int id;
	private String pageloc;

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the pageloc
	 */
	public String getPageloc() {
		return pageloc;
	}

	/**
	 * @param pageloc
	 *            the pageloc to set
	 */
	public void setPageloc(String pageloc) {
		this.pageloc = pageloc;
	}
}
