package com.xaphe.xleap.dto;

import java.io.Serializable;

public class LocalizeSubCategoriesDto implements Serializable {
	private static final long serialVersionUID = 2736515338489910358L;
	private int id;
	private String lang;
	private String parentcat;
	private String caticon;
	private String subname;
	private String definition;

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the lang
	 */
	public String getLang() {
		return lang;
	}

	/**
	 * @param lang
	 *            the lang to set
	 */
	public void setLang(String lang) {
		this.lang = lang;
	}

	/**
	 * @return the parentcat
	 */
	public String getParentcat() {
		return parentcat;
	}

	/**
	 * @param parentcat
	 *            the parentcat to set
	 */
	public void setParentcat(String parentcat) {
		this.parentcat = parentcat;
	}

	/**
	 * @return the caticon
	 */
	public String getCaticon() {
		return caticon;
	}

	/**
	 * @param caticon
	 *            the caticon to set
	 */
	public void setCaticon(String caticon) {
		this.caticon = caticon;
	}

	/**
	 * @return the subname
	 */
	public String getSubname() {
		return subname;
	}

	/**
	 * @param subname
	 *            the subname to set
	 */
	public void setSubname(String subname) {
		this.subname = subname;
	}

	/**
	 * @return the definition
	 */
	public String getDefinition() {
		return definition;
	}

	/**
	 * @param definition
	 *            the definition to set
	 */
	public void setDefinition(String definition) {
		this.definition = definition;
	}
}
