package com.xaphe.xleap.dto;

import java.io.Serializable;

public class LocalizeStreetsDto implements Serializable {
	private static final long serialVersionUID = 7766292316893425604L;
	private int id;
	private String street;

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the street
	 */
	public String getStreet() {
		return street;
	}

	/**
	 * @param street
	 *            the street to set
	 */
	public void setStreet(String street) {
		this.street = street;
	}
}
