package com.xaphe.xleap.dto;

import java.io.Serializable;

public class LocalizeBannerDto implements Serializable {
	private static final long serialVersionUID = 8329976308047702093L;
	private int id;
	private String name;
	private String pageloc;
	private String clientname;
	private String cattitle;
	private String image;
	private String url;
	private int width;
	private int height;
	private boolean visible;
	private int weight;
	private String startdate;
	private String enddate;

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the pageloc
	 */
	public String getPageloc() {
		return pageloc;
	}

	/**
	 * @param pageloc
	 *            the pageloc to set
	 */
	public void setPageloc(String pageloc) {
		this.pageloc = pageloc;
	}

	/**
	 * @return the clientname
	 */
	public String getClientname() {
		return clientname;
	}

	/**
	 * @param clientname
	 *            the clientname to set
	 */
	public void setClientname(String clientname) {
		this.clientname = clientname;
	}

	/**
	 * @return the cattitle
	 */
	public String getCattitle() {
		return cattitle;
	}

	/**
	 * @param cattitle
	 *            the cattitle to set
	 */
	public void setCattitle(String cattitle) {
		this.cattitle = cattitle;
	}

	/**
	 * @return the image
	 */
	public String getImage() {
		return image;
	}

	/**
	 * @param image
	 *            the image to set
	 */
	public void setImage(String image) {
		this.image = image;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url
	 *            the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return the width
	 */
	public int getWidth() {
		return width;
	}

	/**
	 * @param width
	 *            the width to set
	 */
	public void setWidth(int width) {
		this.width = width;
	}

	/**
	 * @return the height
	 */
	public int getHeight() {
		return height;
	}

	/**
	 * @param height
	 *            the height to set
	 */
	public void setHeight(int height) {
		this.height = height;
	}

	/**
	 * @return the visible
	 */
	public boolean isVisible() {
		return visible;
	}

	/**
	 * @param visible
	 *            the visible to set
	 */
	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	/**
	 * @return the weight
	 */
	public int getWeight() {
		return weight;
	}

	/**
	 * @param weight
	 *            the weight to set
	 */
	public void setWeight(int weight) {
		this.weight = weight;
	}

	/**
	 * @return the startdate
	 */
	public String getStartdate() {
		return startdate;
	}

	/**
	 * @param startdate
	 *            the startdate to set
	 */
	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}

	/**
	 * @return the enddate
	 */
	public String getEnddate() {
		return enddate;
	}

	/**
	 * @param enddate
	 *            the enddate to set
	 */
	public void setEnddate(String enddate) {
		this.enddate = enddate;
	}
}
