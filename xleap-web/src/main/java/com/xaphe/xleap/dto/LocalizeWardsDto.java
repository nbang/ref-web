package com.xaphe.xleap.dto;

import java.io.Serializable;

public class LocalizeWardsDto implements Serializable {
	private static final long serialVersionUID = 8234122957178915710L;
	private int id;
	private String ward;

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the ward
	 */
	public String getWard() {
		return ward;
	}

	/**
	 * @param ward
	 *            the ward to set
	 */
	public void setWard(String ward) {
		this.ward = ward;
	}
}
