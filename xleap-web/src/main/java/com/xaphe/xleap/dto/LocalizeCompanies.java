package com.xaphe.xleap.dto;

import com.xaphe.xleap.entity.Companies;

public class LocalizeCompanies extends Companies {
	private static final long serialVersionUID = -7362569114348566641L;
	private String localizeTitle;
	private String localizeAddress;

	public LocalizeCompanies(int id, String localizeTitle) {
		setId(id);
		this.localizeTitle = localizeTitle;
	}

	public LocalizeCompanies(Companies company) {
		super(company.getId(), company.getStreetbind(), company.getContactsByDefMail(), company
				.getContactsByDefContact(), company.getContactsByDefContact(), company.getUrls(), company
				.getContactsByDefContact(), company.getTitleId(), company.getAddrNum(), company.getGeomarker(), company
				.getItemses());
	}

	/**
	 * @return the localizeTitle
	 */
	public String getLocalizeTitle() {
		return localizeTitle;
	}

	/**
	 * @param localizeTitle
	 *            the localizeTitle to set
	 */
	public void setLocalizeTitle(String localizeTitle) {
		this.localizeTitle = localizeTitle;
	}

	public String getLocalizeAddress() {
		this.localizeAddress = this.getAddrNum() + " " + this.getStreetbind().getStreets().getStreet() + " Street " + this.getStreetbind().getWards().getWard() + " Ward "
				+ this.getStreetbind().getDistricts().getDistrict() + " District " + this.getStreetbind().getCities().getName() + " City";
		return localizeAddress;
	}

	public void setLocalizeAddress(String localizeAddress) {
		this.localizeAddress = localizeAddress;
	}

}
