package vn.xaphe.xleap;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;

import com.xaphe.xleap.dao.localize.LocalizeLocaleDao;
import com.xaphe.xleap.dto.LocalizeLocaleDto;
import com.xaphe.xleap.model.ItemModelView;
import com.xaphe.xleap.service.ItemAdminService;

@ContextConfiguration(locations = { "classpath:**/application-context.xml" })
public class ItemAdminTest {

	private static ApplicationContext springContext;

	@BeforeClass
	public static void oneTimeSetUp() {
		// one-time initialization code
		System.out.println("@BeforeClass - oneTimeSetUp");
		springContext = new ClassPathXmlApplicationContext("/application-context.xml");
	}

	@AfterClass
	public static void oneTimeTearDown() {
		// one-time cleanup code
		System.out.println("@AfterClass - oneTimeTearDown");
	}

	@Before
	public void setUp() {
		System.out.println("@Before - setUp");
	}

	@After
	public void tearDown() {
		System.out.println("@After - tearDown");
	}
	
	@Test
	public void test1() {
		LocalizeLocaleDao localizeLocaleDao = springContext.getBean(LocalizeLocaleDao.class);
		List<LocalizeLocaleDto> locales = localizeLocaleDao.getAllLocale();
		for (LocalizeLocaleDto localizeLocaleDto : locales) {
			System.out.println(localizeLocaleDto.getLocal_name());
		}
	}

	@Test
	public void testInsertItem() throws Exception {
		ItemAdminService itemAdminService = springContext.getBean(ItemAdminService.class);
		ItemModelView itemModelView = new ItemModelView();
		itemModelView.setCompany1("Saigon Fun Club, Youth Cultural Center of Ho Chi Minh City");
		itemModelView.setCategory("Activities");
		List<String> subcat = new ArrayList<String>();
		subcat.add("Archery");
		subcat.add("Aerobics");
		itemModelView.setSubCategory1(subcat);
		itemModelView.setCost("1");
		itemModelView.setWeight("203");
		itemModelView.setLocale1("en_US");
		itemModelView.setShortDesc1("Travel in zoo");
		itemModelView.setLongDesc1("Travel in zoo");
		itemModelView.setLocale2("vi_VN");
		itemModelView.setShortDesc2("Thảo cầm viên Sài Gòn");
		itemModelView.setLongDesc2("Sở thú : gồm các loài thú quý hiếm");
		itemModelView.setImage("thaocamviensaigon.png");
		//itemAdminService.createItem("en_US", itemModelView );
	}

	@Test
	public void testUpdateItem() throws Exception {
		ItemAdminService itemAdminService = springContext.getBean(ItemAdminService.class);
		ItemModelView itemModelView = new ItemModelView();
		itemModelView.setItemId(819);
		itemModelView.setCompany1("Saigon Fun Club, Youth Cultural Center of Ho Chi Minh City");
		itemModelView.setCategory("Activities");
		List<String> subcat = new ArrayList<String>();
		subcat.add("Archery");
		subcat.add("Aerobics");
		itemModelView.setSubCategory1(subcat);
		itemModelView.setCost("1");
		itemModelView.setWeight("203");
		itemModelView.setLocale1("en_US");
		itemModelView.setShortDesc1("Travel in zoo");
		itemModelView.setLongDesc1("Travel in zoo");
		itemModelView.setLocale2("en_US");
		itemModelView.setShortDesc2("Thảo cầm viên Sài Gòn");
		itemModelView.setLongDesc2("Sở thú : gồm các loài thú quý hiếm");
		itemModelView.setImage("thaocamviensaigon.png");
		itemAdminService.updateItem("en_US", itemModelView );
	}
}
